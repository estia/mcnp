#!/bin/sh
PLOT_NAME=`git describe --tags --long`

# if the first argument is an runtpe file, use different plot command
if [[ $1 == runs/* ]]; then
  /mnt/nfs/home/glavic_a/shielding/mcnp6/MCNP_SM/bin/mcnp6 p  runtpe=$1 \
        outp=tmp/plota.out comout=tmp/plota.com \
        wwout=tmp/plota.wwg wwone=tmp/plota.wwo \
        wwinp=weight_windows/current.wwg \
        plotm=results/e2-estia_$PLOT_NAME ${@:2}
else
  /mnt/nfs/home/glavic_a/shielding/mcnp6/MCNP_SM/bin/mcnp6 ip  inp=e2-estia.i \
        outp=tmp/plota.out comout=tmp/plota.com \
        wwout=tmp/plota.wwg wwone=tmp/plota.wwo \
        mctal=tmp/plota.mct mdata=tmp/plota.mdt \
        wwinp=weight_windows/current.wwg \
        plotm=results/e2-estia_$PLOT_NAME $@
fi

if [ -f results/e2-estia_$PLOT_NAME.ps ]; then
  gs -q -dSAFER -dNOPAUSE -dBATCH -sOutputFile=results/e2-estia_$PLOT_NAME.pdf \
     -sDEVICE=pdfwrite -c .setpdfwrite -c "<</Orientation 3>> setpagedevice" \
     -f  results/e2-estia_$PLOT_NAME.ps 
  rm results/e2-estia_$PLOT_NAME.ps
fi
