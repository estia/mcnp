from dataclasses import dataclass
from typing import List, Union, Optional
import periodictable

@dataclass
class Composition:
    element: int
    isotope: int
    abundance: float
    library: Optional[str] = None

    def __repr__(self):
        ele = periodictable.elements[self.element]
        return f'{self.isotope}{ele} ({self.abundance})'

@dataclass
class Material:
    ID: int
    elements: List[Composition]

def get_elements(istring):
    ilines = istring.splitlines()
    alines = []
    for li in ilines:
        alines.append(li.split('$', 1)[0])
    tstring = ' '.join(alines)
    items = tstring.split()
    elements = []
    for i in range(len(items)//2):
        eil = items[i*2]
        if '.' in eil:
            ei, l = eil.split('.', 1)
        else:
            ei = eil
            l = None
        ei = int(ei)
        a = float(items[i*2+1])
        comp = Composition(ei//1000, ei%1000, a, l)
        elements.append(comp)
    return elements


txt=open('geometry/materials.i', 'r').read()
lns=[]

while txt.find('\n')>0:
    nn = txt.find('\n')
    while len(txt)>(nn+1) and txt[nn+1] in [' ', '\t'] and txt[nn+1:].find('\n')>0:
        nn = nn+1+txt[nn+1:].find('\n')
    lns.append(txt[:nn+1])
    txt = txt[nn+1:]

flns = [li for li in lns if not li.startswith('c')]
mat={}
for fi in flns:
    if fi[0]=='m' and fi[1].isdigit():
        mat[int(fi.split()[0][1:])] = fi.split(None, 1)[1]

