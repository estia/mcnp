Input File:
c RunCmd:-r -defaultConfig PortsOnly d 
c ---------- VERSION NUMBER ------------------
c  ===Git:  ====== 
c  ========= 1 ========== 
c ----------------------------------------------
c --------------- VARIABLE CARDS ---------------
c ----------------------------------------------
c ABHighBayHeight 330
c ABHighBayLength 450
c ABHighBayRoofMat Concrete
c ABHighBayThick 200
c ABHighBayWallMat Concrete
c ABunkerFloorDepth 120
c ABunkerFloorThick 100
c ABunkerLeftAngle 0
c ABunkerLeftPhase -65
c ABunkerNLayers 1
c ABunkerNSectors 10
c ABunkerNSegment 5
c ABunkerNSide 5
c ABunkerNSideThick 5
c ABunkerNSideVert 5
c ABunkerNVert 1
c ABunkerRightAngle 0
c ABunkerRightPhase -12
c ABunkerRoofActive 0
c ABunkerRoofHeight 150
c ABunkerRoofMat Aluminium
c ABunkerRoofMat0 Void
c ABunkerRoofMat1 Poly
c ABunkerRoofMat2 ChipIRSteel
c ABunkerRoofMat3 Poly
c ABunkerRoofMat4 ChipIRSteel
c ABunkerRoofMat5 Poly
c ABunkerRoofMat6 ChipIRSteel
c ABunkerRoofMat7 Poly
c ABunkerRoofNBasicVert 8
c ABunkerRoofThick 175
c ABunkerRoofVert1 20
c ABunkerRoofVert2 10
c ABunkerRoofVert3 5
c ABunkerRoofVert4 20
c ABunkerRoofVert5 45
c ABunkerRoofVert6 40
c ABunkerRoofVert7 25
c ABunkerSideThick 80
c ABunkerVoidMat Void
c ABunkerWallActive 0
c ABunkerWallHeight 170
c ABunkerWallLen1 45
c ABunkerWallLen10 15
c ABunkerWallLen11 31
c ABunkerWallLen12 15.5
c ABunkerWallLen13 25.5
c ABunkerWallLen14 15.5
c ABunkerWallLen2 15
c ABunkerWallLen3 15
c ABunkerWallLen4 30
c ABunkerWallLen5 15
c ABunkerWallLen6 20
c ABunkerWallLen7 15
c ABunkerWallLen8 20
c ABunkerWallLen9 30
c ABunkerWallMat Steel71
c ABunkerWallMat0 Poly
c ABunkerWallMat1 ChipIRSteel
c ABunkerWallMat10 Poly
c ABunkerWallMat11 ChipIRSteel
c ABunkerWallMat12 Poly
c ABunkerWallMat13 ChipIRSteel
c ABunkerWallMat14 Poly
c ABunkerWallMat2 Poly
c ABunkerWallMat3 ChipIRSteel
c ABunkerWallMat4 Poly
c ABunkerWallMat5 ChipIRSteel
c ABunkerWallMat6 Poly
c ABunkerWallMat7 ChipIRSteel
c ABunkerWallMat8 Poly
c ABunkerWallMat9 ChipIRSteel
c ABunkerWallNBasic 15
c ABunkerWallRadius 2450
c ABunkerWallThick 350
c BBunkerFloorDepth 120
c BBunkerFloorThick 100
c BBunkerLeftAngle 0
c BBunkerLeftPhase -12
c BBunkerNLayers 1
c BBunkerNSectors 9
c BBunkerNSide 5
c BBunkerNSideThick 5
c BBunkerNSideVert 5
c BBunkerNVert 1
c BBunkerRightAngle 0
c BBunkerRightPhase 65
c BBunkerRoofHeight 150
c BBunkerRoofMat Aluminium
c BBunkerRoofMat0 Void
c BBunkerRoofMat1 Poly
c BBunkerRoofMat2 ChipIRSteel
c BBunkerRoofMat3 Poly
c BBunkerRoofMat4 ChipIRSteel
c BBunkerRoofMat5 Poly
c BBunkerRoofMat6 ChipIRSteel
c BBunkerRoofMat7 Poly
c BBunkerRoofNBasicVert 8
c BBunkerRoofThick 175
c BBunkerRoofVert1 20
c BBunkerRoofVert2 10
c BBunkerRoofVert3 5
c BBunkerRoofVert4 20
c BBunkerRoofVert5 45
c BBunkerRoofVert6 40
c BBunkerRoofVert7 25
c BBunkerSideThick 80
c BBunkerVoidMat Void
c BBunkerWallActive 0
c BBunkerWallHeight 170
c BBunkerWallLen1 45
c BBunkerWallLen10 15
c BBunkerWallLen11 31
c BBunkerWallLen12 15.5
c BBunkerWallLen13 25.5
c BBunkerWallLen14 15.5
c BBunkerWallLen2 15
c BBunkerWallLen3 15
c BBunkerWallLen4 30
c BBunkerWallLen5 15
c BBunkerWallLen6 20
c BBunkerWallLen7 15
c BBunkerWallLen8 20
c BBunkerWallLen9 30
c BBunkerWallMat Steel71
c BBunkerWallMat0 Poly
c BBunkerWallMat1 ChipIRSteel
c BBunkerWallMat10 Poly
c BBunkerWallMat11 ChipIRSteel
c BBunkerWallMat12 Poly
c BBunkerWallMat13 ChipIRSteel
c BBunkerWallMat14 Poly
c BBunkerWallMat2 Poly
c BBunkerWallMat3 ChipIRSteel
c BBunkerWallMat4 Poly
c BBunkerWallMat5 ChipIRSteel
c BBunkerWallMat6 Poly
c BBunkerWallMat7 ChipIRSteel
c BBunkerWallMat8 Poly
c BBunkerWallMat9 ChipIRSteel
c BBunkerWallNBasic 15
c BBunkerWallRadius 1150
c BBunkerWallThick 350
c BeRefHeight 74.2
c BeRefLowRefMat Be5H2O
c BeRefLowWallMat Stainless304
c BeRefRadius 34.3
c BeRefTargSepMat Void
c BeRefTopRefMat Be5H2O
c BeRefTopWallMat Stainless304
c BeRefWallThick 3
c BeRefWallThickLow 0
c BeRefXStep 0
c BeRefXYAngle 0
c BeRefYStep 0
c BeRefZAngle 0
c BeRefZStep 0
c BilbaoWheelAspectRatio 0.00138
c BilbaoWheelCaseRadius 129.15
c BilbaoWheelCaseThick 1
c BilbaoWheelCaseThickIn 3
c BilbaoWheelCatcherHeight 10
c BilbaoWheelCatcherMiddleHeight 10
c BilbaoWheelCatcherMiddleRadius 32
c BilbaoWheelCatcherNotchDepth 5
c BilbaoWheelCatcherNotchRadius 22
c BilbaoWheelCatcherRadius 42
c BilbaoWheelCatcherRingDepth 24
c BilbaoWheelCatcherRingRadius 34
c BilbaoWheelCatcherRingThick 2
c BilbaoWheelCatcherTopSteelThick 2
c BilbaoWheelCirclePipesBigRad 30
c BilbaoWheelCirclePipesRad 1.5
c BilbaoWheelCirclePipesWallThick 0.2
c BilbaoWheelCoolantRadiusIn 64.07
c BilbaoWheelCoolantRadiusOut 128.95
c BilbaoWheelCoolantThick 0.5
c BilbaoWheelHeMat Helium
c BilbaoWheelInnerHoleHeight 4.5
c BilbaoWheelInnerHoleSize 0.25
c BilbaoWheelInnerHoleXYangle -1
c BilbaoWheelInnerMat SS316L785
c BilbaoWheelInnerRadius 45
c BilbaoWheelMatTYPE1 1
c BilbaoWheelMatTYPE2 1
c BilbaoWheelMatTYPE3 3
c BilbaoWheelNLayers 3
c BilbaoWheelNSectors 36
c BilbaoWheelNShaftLayers 6
c BilbaoWheelRadius1 48
c BilbaoWheelRadius2 85
c BilbaoWheelRadius3 125
c BilbaoWheelSS316LVoidMat M2644
c BilbaoWheelShaft2StepConnectionDist 5
c BilbaoWheelShaft2StepConnectionHeight 4
c BilbaoWheelShaft2StepConnectionRadius 27.5
c BilbaoWheelShaft2StepHeight 15
c BilbaoWheelShaftBaseDepth 35
c BilbaoWheelShaftHeight 435
c BilbaoWheelShaftHoleHeight 4.5
c BilbaoWheelShaftHoleSize 0.25
c BilbaoWheelShaftHoleXYangle -1
c BilbaoWheelShaftMat1 SS316L
c BilbaoWheelShaftMat2 SS316L
c BilbaoWheelShaftMat3 SS316L
c BilbaoWheelShaftMat4 SS316L
c BilbaoWheelShaftMat5 SS316L
c BilbaoWheelShaftMat6 Void
c BilbaoWheelShaftRadius1 5
c BilbaoWheelShaftRadius2 13.5
c BilbaoWheelShaftRadius3 14
c BilbaoWheelShaftRadius4 20
c BilbaoWheelShaftRadius5 23
c BilbaoWheelShaftRadius6 25
c BilbaoWheelSteelMat SS316L785
c BilbaoWheelSteelTungstenInnerThick 0.5
c BilbaoWheelSteelTungstenThick 0.2
c BilbaoWheelTargetHeight 8
c BilbaoWheelTargetInnerHeight 6.6
c BilbaoWheelTargetInnerHeightRadius 74.5
c BilbaoWheelTemp 600
c BilbaoWheelVoidRadius 131.15
c BilbaoWheelVoidThick 2
c BilbaoWheelVoidTungstenThick 0.1
c BilbaoWheelWMat Tungsten151
c BilbaoWheelXStep 0
c BilbaoWheelYStep 112.2
c BilbaoWheelZStep 0
c BulkDepth1 41.3
c BulkDepth2 75
c BulkDepth3 200
c BulkHeight1 41.3
c BulkHeight2 75
c BulkHeight3 200
c BulkMat1 Void
c BulkMat2 Iron10H2O
c BulkMat3 Iron10H2O
c BulkNLayer 3
c BulkRadius1 37.5
c BulkRadius2 65
c BulkRadius3 200
c BulkXStep 0
c BulkXYAngle 0
c BulkYStep 0
c BulkZAngle 0
c BulkZStep 0
c CBunkerFloorDepth 120
c CBunkerFloorThick 100
c CBunkerLeftAngle 0
c CBunkerLeftPhase -65
c CBunkerNLayers 1
c CBunkerNSectors 10
c CBunkerNSegment 5
c CBunkerNSide 5
c CBunkerNSideThick 5
c CBunkerNSideVert 5
c CBunkerNVert 1
c CBunkerRightAngle 0
c CBunkerRightPhase -12
c CBunkerRoofActive 0
c CBunkerRoofHeight 150
c CBunkerRoofMat Aluminium
c CBunkerRoofMat0 Void
c CBunkerRoofMat1 Poly
c CBunkerRoofMat2 ChipIRSteel
c CBunkerRoofMat3 Poly
c CBunkerRoofMat4 ChipIRSteel
c CBunkerRoofMat5 Poly
c CBunkerRoofMat6 ChipIRSteel
c CBunkerRoofMat7 Poly
c CBunkerRoofNBasicVert 8
c CBunkerRoofThick 175
c CBunkerRoofVert1 20
c CBunkerRoofVert2 10
c CBunkerRoofVert3 5
c CBunkerRoofVert4 20
c CBunkerRoofVert5 45
c CBunkerRoofVert6 40
c CBunkerRoofVert7 25
c CBunkerSideThick 80
c CBunkerVoidMat Void
c CBunkerWallActive 0
c CBunkerWallHeight 170
c CBunkerWallLen1 45
c CBunkerWallLen10 15
c CBunkerWallLen11 31
c CBunkerWallLen12 15.5
c CBunkerWallLen13 25.5
c CBunkerWallLen14 15.5
c CBunkerWallLen2 15
c CBunkerWallLen3 15
c CBunkerWallLen4 30
c CBunkerWallLen5 15
c CBunkerWallLen6 20
c CBunkerWallLen7 15
c CBunkerWallLen8 20
c CBunkerWallLen9 30
c CBunkerWallMat Steel71
c CBunkerWallMat0 Poly
c CBunkerWallMat1 ChipIRSteel
c CBunkerWallMat10 Poly
c CBunkerWallMat11 ChipIRSteel
c CBunkerWallMat12 Poly
c CBunkerWallMat13 ChipIRSteel
c CBunkerWallMat14 Poly
c CBunkerWallMat2 Poly
c CBunkerWallMat3 ChipIRSteel
c CBunkerWallMat4 Poly
c CBunkerWallMat5 ChipIRSteel
c CBunkerWallMat6 Poly
c CBunkerWallMat7 ChipIRSteel
c CBunkerWallMat8 Poly
c CBunkerWallMat9 ChipIRSteel
c CBunkerWallNBasic 15
c CBunkerWallRadius 2450
c CBunkerWallThick 350
c CDHighBayHeight 330
c CDHighBayLength 450
c CDHighBayRoofMat Concrete
c CDHighBayThick 200
c CDHighBayWallMat Concrete
c CurtainBaseGap 6
c CurtainBaseLen1 70
c CurtainBaseLen2 6
c CurtainBaseMat0 Stainless304
c CurtainBaseMat1 Void
c CurtainBaseMat2 Concrete
c CurtainDepth 186
c CurtainHeight 450
c CurtainInnerStep 30
c CurtainLeftPhase -65
c CurtainNBaseLayers 3
c CurtainNMidLayers 1
c CurtainNTopLayers 1
c CurtainOuterGap 5
c CurtainRightPhase 65
c CurtainTopRaise 60
c CurtainWallMat Concrete
c CurtainWallThick 30
c DBunkerFloorDepth 120
c DBunkerFloorThick 100
c DBunkerLeftAngle 0
c DBunkerLeftPhase -12
c DBunkerNLayers 1
c DBunkerNSectors 9
c DBunkerNSide 5
c DBunkerNSideThick 5
c DBunkerNSideVert 5
c DBunkerNVert 1
c DBunkerRightAngle 0
c DBunkerRightPhase 65
c DBunkerRoofHeight 150
c DBunkerRoofMat Aluminium
c DBunkerRoofMat0 Void
c DBunkerRoofMat1 Poly
c DBunkerRoofMat2 ChipIRSteel
c DBunkerRoofMat3 Poly
c DBunkerRoofMat4 ChipIRSteel
c DBunkerRoofMat5 Poly
c DBunkerRoofMat6 ChipIRSteel
c DBunkerRoofMat7 Poly
c DBunkerRoofNBasicVert 8
c DBunkerRoofThick 175
c DBunkerRoofVert1 20
c DBunkerRoofVert2 10
c DBunkerRoofVert3 5
c DBunkerRoofVert4 20
c DBunkerRoofVert5 45
c DBunkerRoofVert6 40
c DBunkerRoofVert7 25
c DBunkerSideThick 80
c DBunkerVoidMat Void
c DBunkerWallActive 0
c DBunkerWallHeight 170
c DBunkerWallLen1 45
c DBunkerWallLen10 15
c DBunkerWallLen11 31
c DBunkerWallLen12 15.5
c DBunkerWallLen13 25.5
c DBunkerWallLen14 15.5
c DBunkerWallLen2 15
c DBunkerWallLen3 15
c DBunkerWallLen4 30
c DBunkerWallLen5 15
c DBunkerWallLen6 20
c DBunkerWallLen7 15
c DBunkerWallLen8 20
c DBunkerWallLen9 30
c DBunkerWallMat Steel71
c DBunkerWallMat0 Poly
c DBunkerWallMat1 ChipIRSteel
c DBunkerWallMat10 Poly
c DBunkerWallMat11 ChipIRSteel
c DBunkerWallMat12 Poly
c DBunkerWallMat13 ChipIRSteel
c DBunkerWallMat14 Poly
c DBunkerWallMat2 Poly
c DBunkerWallMat3 ChipIRSteel
c DBunkerWallMat4 Poly
c DBunkerWallMat5 ChipIRSteel
c DBunkerWallMat6 Poly
c DBunkerWallMat7 ChipIRSteel
c DBunkerWallMat8 Poly
c DBunkerWallMat9 ChipIRSteel
c DBunkerWallNBasic 15
c DBunkerWallRadius 1150
c DBunkerWallThick 350
c EngineeringActive 0
c G1BLineLow10XYAngle 6.7
c G1BLineLow11XYAngle 0
c G1BLineLow12XYAngle -6
c G1BLineLow13XYAngle -12
c G1BLineLow14XYAngle -18
c G1BLineLow15XYAngle -24
c G1BLineLow16XYAngle -30
c G1BLineLow17XYAngle -36
c G1BLineLow18XYAngle -42
c G1BLineLow19XYAngle -48
c G1BLineLow1XYAngle 60
c G1BLineLow20XYAngle -54
c G1BLineLow21XYAngle -60
c G1BLineLow2XYAngle 54.7
c G1BLineLow3XYAngle 48
c G1BLineLow4XYAngle 42.7
c G1BLineLow5XYAngle 36
c G1BLineLow6XYAngle 30.7
c G1BLineLow7XYAngle 24
c G1BLineLow8XYAngle 18.7
c G1BLineLow9XYAngle 12
c G1BLineLowActive 0
c G1BLineLowBaseGap 0.1
c G1BLineLowBeamHeight 6
c G1BLineLowBeamWidth 6
c G1BLineLowBeamXStep 0
c G1BLineLowBeamXYAngle 0
c G1BLineLowBeamZAngle 0
c G1BLineLowBeamZStep 0
c G1BLineLowDepth1 10
c G1BLineLowDepth2 22
c G1BLineLowFilled 0
c G1BLineLowHeight1 12
c G1BLineLowHeight2 22
c G1BLineLowLength1 170
c G1BLineLowMat Stainless304
c G1BLineLowNSegment 2
c G1BLineLowSideGap 0.6
c G1BLineLowTopGap 0.8
c G1BLineLowWidth1 20
c G1BLineLowWidth2 28
c G1BLineTop10Active 1
c G1BLineTop10XYAngle 6.7
c G1BLineTop11Active 1
c G1BLineTop11XYAngle 0
c G1BLineTop12Active 1
c G1BLineTop12XYAngle -6
c G1BLineTop13Active 1
c G1BLineTop13XYAngle -12
c G1BLineTop14Active 1
c G1BLineTop14XYAngle -18
c G1BLineTop15Active 1
c G1BLineTop15XYAngle -24
c G1BLineTop16Active 1
c G1BLineTop16XYAngle -30
c G1BLineTop17Active 1
c G1BLineTop17XYAngle -36
c G1BLineTop18Active 1
c G1BLineTop18XYAngle -42
c G1BLineTop19Active 1
c G1BLineTop19XYAngle -48
c G1BLineTop1Active 1
c G1BLineTop1XYAngle 60
c G1BLineTop20Active 1
c G1BLineTop20XYAngle -54
c G1BLineTop21Active 1
c G1BLineTop21XYAngle -60
c G1BLineTop2Active 1
c G1BLineTop2XYAngle 54.7
c G1BLineTop3Active 1
c G1BLineTop3XYAngle 48
c G1BLineTop4Active 1
c G1BLineTop4XYAngle 42.7
c G1BLineTop5Active 1
c G1BLineTop5XYAngle 36
c G1BLineTop6Active 1
c G1BLineTop6XYAngle 30.7
c G1BLineTop7Active 1
c G1BLineTop7XYAngle 24
c G1BLineTop8Active 1
c G1BLineTop8XYAngle 18.7
c G1BLineTop9Active 1
c G1BLineTop9XYAngle 12
c G1BLineTopBaseGap 0.1
c G1BLineTopBeamHeight 6
c G1BLineTopBeamWidth 6
c G1BLineTopBeamXStep 0
c G1BLineTopBeamXYAngle 0
c G1BLineTopBeamZAngle 0
c G1BLineTopBeamZStep 0
c G1BLineTopDepth1 10
c G1BLineTopDepth2 22
c G1BLineTopFilled 0
c G1BLineTopHeight1 12
c G1BLineTopHeight2 22
c G1BLineTopLength1 170
c G1BLineTopMat Stainless304
c G1BLineTopNSegment 2
c G1BLineTopSideGap 0.6
c G1BLineTopTopGap 0.8
c G1BLineTopWidth1 20
c G1BLineTopWidth2 28
c G2BLineLow10XYAngle 6.7
c G2BLineLow11XYAngle 0
c G2BLineLow12XYAngle -6
c G2BLineLow13XYAngle -12
c G2BLineLow14XYAngle -18
c G2BLineLow15XYAngle -24
c G2BLineLow16XYAngle -30
c G2BLineLow17XYAngle -36
c G2BLineLow18XYAngle -42
c G2BLineLow19XYAngle -48
c G2BLineLow1XYAngle 60
c G2BLineLow20XYAngle -54
c G2BLineLow21XYAngle -60
c G2BLineLow2XYAngle 54.7
c G2BLineLow3XYAngle 48
c G2BLineLow4XYAngle 42.7
c G2BLineLow5XYAngle 36
c G2BLineLow6XYAngle 30.7
c G2BLineLow7XYAngle 24
c G2BLineLow8XYAngle 18.7
c G2BLineLow9XYAngle 12
c G2BLineLowActive 0
c G2BLineLowBaseGap 0.1
c G2BLineLowBeamHeight 6
c G2BLineLowBeamWidth 6
c G2BLineLowBeamXStep 0
c G2BLineLowBeamXYAngle 0
c G2BLineLowBeamZAngle 0
c G2BLineLowBeamZStep 0
c G2BLineLowDepth1 10
c G2BLineLowDepth2 22
c G2BLineLowFilled 0
c G2BLineLowHeight1 12
c G2BLineLowHeight2 22
c G2BLineLowLength1 170
c G2BLineLowMat Stainless304
c G2BLineLowNSegment 2
c G2BLineLowSideGap 0.6
c G2BLineLowTopGap 0.8
c G2BLineLowWidth1 20
c G2BLineLowWidth2 28
c G2BLineTop10XYAngle 6.7
c G2BLineTop11XYAngle 0
c G2BLineTop12XYAngle -6
c G2BLineTop13XYAngle -12
c G2BLineTop14XYAngle -18
c G2BLineTop15XYAngle -24
c G2BLineTop16XYAngle -30
c G2BLineTop17XYAngle -36
c G2BLineTop18XYAngle -42
c G2BLineTop19XYAngle -48
c G2BLineTop1XYAngle 60
c G2BLineTop20XYAngle -54
c G2BLineTop21XYAngle -60
c G2BLineTop2XYAngle 54.7
c G2BLineTop3XYAngle 48
c G2BLineTop4XYAngle 42.7
c G2BLineTop5XYAngle 36
c G2BLineTop6XYAngle 30.7
c G2BLineTop7XYAngle 24
c G2BLineTop8XYAngle 18.7
c G2BLineTop9XYAngle 12
c G2BLineTopActive 0
c G2BLineTopBaseGap 0.1
c G2BLineTopBeamHeight 6
c G2BLineTopBeamWidth 6
c G2BLineTopBeamXStep 0
c G2BLineTopBeamXYAngle 0
c G2BLineTopBeamZAngle 0
c G2BLineTopBeamZStep 0
c G2BLineTopDepth1 10
c G2BLineTopDepth2 22
c G2BLineTopFilled 0
c G2BLineTopHeight1 12
c G2BLineTopHeight2 22
c G2BLineTopLength1 170
c G2BLineTopMat Stainless304
c G2BLineTopNSegment 2
c G2BLineTopSideGap 0.6
c G2BLineTopTopGap 0.8
c G2BLineTopWidth1 20
c G2BLineTopWidth2 28
c GuideBay1NItems 21
c GuideBay1XYAngle 270
c GuideBay2NItems 21
c GuideBay2XYAngle 90
c GuideBayDepth 50
c GuideBayHeight 50
c GuideBayInnerDepth 40
c GuideBayInnerHeight 40
c GuideBayMat CastIron
c GuideBayMidRadius 170
c GuideBayViewAngle 128
c GuideBayXStep 0
c GuideBayYStep 0
c GuideBayZAngle 0
c GuideBayZStep 0
c LowAFlightAngleXY1 60
c LowAFlightAngleXY2 60
c LowAFlightAngleZBase 0.9
c LowAFlightAngleZTop 0.9
c LowAFlightHeight 6.1
c LowAFlightLinerMat1 Aluminium
c LowAFlightLinerThick1 0.4
c LowAFlightNLiner 1
c LowAFlightWidth 10.7
c LowAFlightXStep 0
c LowAFlightXYAngle 0
c LowAFlightZAngle 0
c LowAFlightZStep 0
c LowBFlightAngleXY1 60
c LowBFlightAngleXY2 60
c LowBFlightAngleZBase 0.9
c LowBFlightAngleZTop 0.9
c LowBFlightHeight 6.1
c LowBFlightLinerMat1 Aluminium
c LowBFlightLinerThick1 0.4
c LowBFlightNLiner 1
c LowBFlightTapSurf cone
c LowBFlightWidth 10.7
c LowBFlightXStep 0
c LowBFlightXYAngle 180
c LowBFlightZAngle 0
c LowBFlightZStep 0
c LowBeRefWaterDiscDepth0 0.3
c LowBeRefWaterDiscHeight0 0.3
c LowBeRefWaterDiscHeight1 0.4
c LowCapModMat0x0 Void
c LowCapModMat1x0 Aluminium
c LowCapModMat1x1 Void
c LowCapModMat1x2 Stainless304
c LowCapModMat2x0 H2O
c LowCapModMat2x1 Aluminium
c LowCapModMat2x2 Void
c LowCapModMat2x3 Stainless304
c LowCapModMat3x0 Aluminium
c LowCapModMat3x1 Void
c LowCapModMat3x2 Stainless304
c LowCapModNLayers 4
c LowCapModRadius1x0 32.3
c LowCapModRadius1x1 32.6
c LowCapModRadius2x0 32
c LowCapModRadius2x1 32.3
c LowCapModRadius2x2 32.6
c LowCapModRadius3x0 32.3
c LowCapModRadius3x1 32.6
c LowCapModThick0 0.2
c LowCapModThick1 0.3
c LowCapModThick2 0.7
c LowCapModThick3 0.3
c LowFlyLeftLobeCorner1 0 0.45 0
c LowFlyLeftLobeCorner2 -14.4 -13.2 0
c LowFlyLeftLobeCorner3 14.4 -13.2 0
c LowFlyLeftLobeDepth1 0.3
c LowFlyLeftLobeDepth2 0.5
c LowFlyLeftLobeDepth3 0
c LowFlyLeftLobeHeight1 0.3
c LowFlyLeftLobeHeight2 0.5
c LowFlyLeftLobeHeight3 0
c LowFlyLeftLobeMat1 Aluminium20K
c LowFlyLeftLobeMat2 Void
c LowFlyLeftLobeMat3 Aluminium
c LowFlyLeftLobeModMat ParaOrtho%99.5
c LowFlyLeftLobeModTemp 20
c LowFlyLeftLobeNLayers 4
c LowFlyLeftLobeRadius1 5
c LowFlyLeftLobeRadius2 2.506
c LowFlyLeftLobeRadius3 2.506
c LowFlyLeftLobeTemp1 20
c LowFlyLeftLobeThick1 0.3
c LowFlyLeftLobeThick2 0.5
c LowFlyLeftLobeThick3 0.3
c LowFlyLeftLobeXStep 1
c LowFlyLeftLobeYStep 0
c LowFlyLeftWaterCutAngle 30
c LowFlyLeftWaterCutWidth 10.56
c LowFlyLeftWaterModMat H2O
c LowFlyLeftWaterModTemp 300
c LowFlyLeftWaterWallMat Aluminium
c LowFlyLeftWaterWallThick 0.347
c LowFlyLeftWaterWidth 15.76
c LowFlyMidWaterCutLayer 3
c LowFlyMidWaterLength 10.98
c LowFlyMidWaterMidAngle 90
c LowFlyMidWaterMidYStep 4.635
c LowFlyMidWaterModMat H2O
c LowFlyMidWaterModTemp 300
c LowFlyMidWaterWallMat Aluminium
c LowFlyMidWaterWallThick 0.2
c LowFlyRightLobeCorner1 0 0.45 0
c LowFlyRightLobeCorner2 -14.4 -13.2 0
c LowFlyRightLobeCorner3 14.4 -13.2 0
c LowFlyRightLobeDepth1 0.3
c LowFlyRightLobeDepth2 0.5
c LowFlyRightLobeDepth3 0
c LowFlyRightLobeHeight1 0.3
c LowFlyRightLobeHeight2 0.5
c LowFlyRightLobeHeight3 0
c LowFlyRightLobeMat1 Aluminium
c LowFlyRightLobeMat2 Void
c LowFlyRightLobeMat3 Aluminium
c LowFlyRightLobeModMat ParaOrtho%99.5
c LowFlyRightLobeModTemp 20
c LowFlyRightLobeNLayers 4
c LowFlyRightLobeRadius1 5
c LowFlyRightLobeRadius2 2.506
c LowFlyRightLobeRadius3 2.506
c LowFlyRightLobeTemp1 20
c LowFlyRightLobeThick1 0.3
c LowFlyRightLobeThick2 0.5
c LowFlyRightLobeThick3 0.3
c LowFlyRightLobeXStep -1
c LowFlyRightLobeYStep 0
c LowFlyRightWaterCutAngle 30
c LowFlyRightWaterCutWidth 10.56
c LowFlyRightWaterModMat H2O
c LowFlyRightWaterModTemp 300
c LowFlyRightWaterWallMat Aluminium
c LowFlyRightWaterWallThick 0.347
c LowFlyRightWaterWidth 15.76
c LowFlyTotalHeight 7.6
c LowFlyXStep 0
c LowFlyXYAngle 90
c LowFlyYStep 0
c LowFlyZAngle 180
c LowFlyZStep 0
c LowFocusDistance 8.9
c LowFocusWidth 5.4
c LowFocusXYAngle 90
c LowFocusZStep -15.2
c LowPreModMat0x0 Aluminium
c LowPreModMat0x1 Void
c LowPreModMat0x2 Stainless304
c LowPreModMat1x0 H2O
c LowPreModMat1x1 Aluminium
c LowPreModMat1x2 Void
c LowPreModMat1x3 Stainless304
c LowPreModNLayers 2
c LowPreModRadius0x0 30.3
c LowPreModRadius0x1 30.6
c LowPreModRadius1x0 30
c LowPreModRadius1x1 30.3
c LowPreModRadius1x2 30.6
c LowPreModThick0 0.3
c LowPreModThick1 2.85
c ProtonTubeInnerMat1 helium
c ProtonTubeInnerMat2 helium
c ProtonTubeInnerMat3 helium
c ProtonTubeInnerMat4 Void
c ProtonTubeLength1 148.25
c ProtonTubeLength2 200
c ProtonTubeLength3 127.5
c ProtonTubeLength4 152.5
c ProtonTubeNSection 4
c ProtonTubeRadius1 11.5
c ProtonTubeRadius2 15
c ProtonTubeRadius3 15
c ProtonTubeRadius4 15
c ProtonTubeWallMat1 CastIron
c ProtonTubeWallMat2 CastIron
c ProtonTubeWallMat3 CastIron
c ProtonTubeWallMat4 CastIron
c ProtonTubeWallThick1 0
c ProtonTubeWallThick2 1
c ProtonTubeWallThick3 1
c ProtonTubeWallThick4 1
c ProtonTubeZcut1 3.7
c ProtonTubeZcut2 0
c ProtonTubeZcut3 0
c ProtonTubeZcut4 0
c ShutterBayCurtainMat0 Stainless304
c ShutterBayCurtainMat1 Void
c ShutterBayCurtainMat2 Concrete
c ShutterBayCurtainThick0 70
c ShutterBayCurtainThick1 6
c ShutterBayCutSkin 6
c ShutterBayDepth 400
c ShutterBayHeight 400
c ShutterBayMat CastIron
c ShutterBayNCurtain 3
c ShutterBayRadius 550
c ShutterBaySkin 6
c ShutterBaySkinMat Void
c ShutterBayTopCut 186
c ShutterBayTopRadius 500
c ShutterBayTopSkin 6
c TopAFlightAngleXY1 60
c TopAFlightAngleXY2 60
c TopAFlightAngleZBase 1.33
c TopAFlightAngleZTop 1.1
c TopAFlightHeight 2.9
c TopAFlightLinerMat1 Aluminium
c TopAFlightLinerThick1 0.3
c TopAFlightNLiner 1
c TopAFlightNWedges 0
c TopAFlightTapSurf cone
c TopAFlightWidth 10.7
c TopAFlightXStep 0
c TopAFlightXYAngle 180
c TopAFlightZAngle 0
c TopAFlightZStep 0
c TopBFlightAngleXY1 60
c TopBFlightAngleXY2 60
c TopBFlightAngleZBase 1.33
c TopBFlightAngleZTop 1.1
c TopBFlightHeight 2.9
c TopBFlightLinerMat1 Aluminium
c TopBFlightLinerThick1 0.3
c TopBFlightNLiner 1
c TopBFlightNWedges 0
c TopBFlightWidth 10.7
c TopBFlightXStep 0
c TopBFlightXYAngle 0
c TopBFlightZAngle 0
c TopBFlightZStep 0
c TopBeRefWaterDiscDepth0 0.3
c TopBeRefWaterDiscHeight0 0.3
c TopBeRefWaterDiscHeight1 0.4
c TopCapModMat0x0 Void
c TopCapModMat1x0 Aluminium
c TopCapModMat1x1 Void
c TopCapModMat1x2 Stainless304
c TopCapModMat2x0 H2O
c TopCapModMat2x1 Aluminium
c TopCapModMat2x2 Void
c TopCapModMat2x3 Stainless304
c TopCapModMat3x0 Aluminium
c TopCapModMat3x1 Void
c TopCapModMat3x2 Stainless304
c TopCapModNLayers 4
c TopCapModRadius1x0 32.3
c TopCapModRadius1x1 32.6
c TopCapModRadius2x0 32
c TopCapModRadius2x1 32.3
c TopCapModRadius2x2 32.6
c TopCapModRadius3x0 32.3
c TopCapModRadius3x1 32.6
c TopCapModThick0 0.2
c TopCapModThick1 0.3
c TopCapModThick2 0.7
c TopCapModThick3 0.3
c TopFlyLeftLobeCorner1 0 0.45 0
c TopFlyLeftLobeCorner2 -14.4 -13.2 0
c TopFlyLeftLobeCorner3 14.4 -13.2 0
c TopFlyLeftLobeDepth1 0.3
c TopFlyLeftLobeDepth2 0.5
c TopFlyLeftLobeDepth3 0.3
c TopFlyLeftLobeHeight1 0.3
c TopFlyLeftLobeHeight2 0.5
c TopFlyLeftLobeHeight3 0.6
c TopFlyLeftLobeMat1 Aluminium
c TopFlyLeftLobeMat2 Void
c TopFlyLeftLobeMat3 Aluminium
c TopFlyLeftLobeModMat ParaOrtho%99.5
c TopFlyLeftLobeModTemp 20
c TopFlyLeftLobeNLayers 4
c TopFlyLeftLobeRadius1 5
c TopFlyLeftLobeRadius2 2.506
c TopFlyLeftLobeRadius3 2.506
c TopFlyLeftLobeTemp1 20
c TopFlyLeftLobeThick1 0.3
c TopFlyLeftLobeThick2 0.5
c TopFlyLeftLobeThick3 0.3
c TopFlyLeftLobeXStep 1
c TopFlyLeftLobeYStep 0
c TopFlyLeftWaterCutAngle 30
c TopFlyLeftWaterCutWidth 10.562
c TopFlyLeftWaterModMat H2O
c TopFlyLeftWaterModTemp 300
c TopFlyLeftWaterWallMat Aluminium
c TopFlyLeftWaterWallThick 0.347
c TopFlyLeftWaterWidth 15.76
c TopFlyMidWaterBaseThick 0.2
c TopFlyMidWaterCornerRadius 2
c TopFlyMidWaterCutLayer 3
c TopFlyMidWaterLength 10.98
c TopFlyMidWaterMidAngle 90
c TopFlyMidWaterMidYStep 4.635
c TopFlyMidWaterModMat H2O
c TopFlyMidWaterModTemp 300
c TopFlyMidWaterTopThick 0.2
c TopFlyMidWaterWallMat Aluminium
c TopFlyMidWaterWallThick 0.2
c TopFlyRightLobeCorner1 0 0.45 0
c TopFlyRightLobeCorner2 -14.4 -13.2 0
c TopFlyRightLobeCorner3 14.4 -13.2 0
c TopFlyRightLobeDepth1 0.3
c TopFlyRightLobeDepth2 0.5
c TopFlyRightLobeDepth3 0.3
c TopFlyRightLobeHeight1 0.3
c TopFlyRightLobeHeight2 0.5
c TopFlyRightLobeHeight3 0.3
c TopFlyRightLobeMat1 Aluminium20K
c TopFlyRightLobeMat2 Void
c TopFlyRightLobeMat3 Aluminium
c TopFlyRightLobeModMat ParaOrtho%99.5
c TopFlyRightLobeModTemp 20
c TopFlyRightLobeNLayers 4
c TopFlyRightLobeRadius1 5
c TopFlyRightLobeRadius2 2.506
c TopFlyRightLobeRadius3 2.506
c TopFlyRightLobeTemp1 20
c TopFlyRightLobeThick1 0.3
c TopFlyRightLobeThick2 0.5
c TopFlyRightLobeThick3 0.3
c TopFlyRightLobeXStep -1
c TopFlyRightLobeYStep 0
c TopFlyRightWaterCutAngle 30
c TopFlyRightWaterCutWidth 10.56
c TopFlyRightWaterModMat H2O
c TopFlyRightWaterModTemp 300
c TopFlyRightWaterWallMat Aluminium
c TopFlyRightWaterWallThick 0.347
c TopFlyRightWaterWidth 15.76
c TopFlyTotalHeight 5.5
c TopFlyXStep 0
c TopFlyXYAngle 90
c TopFlyYStep 0
c TopFlyZAngle 0
c TopFlyZStep 0
c TopFocusDistance 8.9
c TopFocusWidth 5.4
c TopFocusXYAngle 90
c TopFocusZStep 13.7
c TopLeftPreWingInnerDepth 2
c TopLeftPreWingInnerHeight 2
c TopLeftPreWingInnerMat1 Aluminium
c TopLeftPreWingInnerMat2 Void
c TopLeftPreWingInnerMat3 Stainless304
c TopLeftPreWingInnerRadius 10
c TopLeftPreWingInnerYCut 8
c TopLeftPreWingLayerRadius1 30
c TopLeftPreWingLayerRadius2 30.3
c TopLeftPreWingLayerRadius3 30.6
c TopLeftPreWingMat H2O
c TopLeftPreWingNLayers 4
c TopLeftPreWingOuterDepth 2.5
c TopLeftPreWingOuterHeight 2.5
c TopLeftPreWingOuterRadius 38
c TopLeftPreWingSurfMat1 Aluminium
c TopLeftPreWingSurfMat2 Void
c TopLeftPreWingSurfMat3 Stainless304
c TopLeftPreWingWallMat Aluminium
c TopLeftPreWingWallThick 0.3
c TopPreModMat0x0 Aluminium
c TopPreModMat0x1 Void
c TopPreModMat0x2 Stainless304
c TopPreModMat1x0 H2O
c TopPreModMat1x1 Aluminium
c TopPreModMat1x2 Void
c TopPreModMat1x3 Stainless304
c TopPreModNLayers 2
c TopPreModRadius0x0 30.3
c TopPreModRadius0x1 30.6
c TopPreModRadius1x0 30
c TopPreModRadius1x1 30.3
c TopPreModRadius1x2 30.6
c TopPreModThick0 0.3
c TopPreModThick1 2.85
c TopRightPreWingInnerDepth 2
c TopRightPreWingInnerHeight 2
c TopRightPreWingInnerMat1 Aluminium
c TopRightPreWingInnerMat2 Void
c TopRightPreWingInnerMat3 Stainless304
c TopRightPreWingInnerRadius 10
c TopRightPreWingInnerYCut 8
c TopRightPreWingLayerRadius1 30
c TopRightPreWingLayerRadius2 30.3
c TopRightPreWingLayerRadius3 30.6
c TopRightPreWingMat H2O
c TopRightPreWingNLayers 4
c TopRightPreWingOuterDepth 2.5
c TopRightPreWingOuterHeight 2.5
c TopRightPreWingOuterRadius 38
c TopRightPreWingSurfMat1 Aluminium
c TopRightPreWingSurfMat2 Void
c TopRightPreWingSurfMat3 Stainless304
c TopRightPreWingWallMat Aluminium
c TopRightPreWingWallThick 0.3
c TopRightPreWingXYAngle 180
c sdefEnergy 2000
c -------------------------------------------------------
c --------------- CELL CARDS --------------------------
c -------------------------------------------------------
1 0   1
2 0   ( -127 : -83 : -99 : 3 : 140 : 156 : 157 ) ( -150 : -91 : -151 :
        149 : 112 : 83 ) ( -91 : -84 : 82 : 112 : 153 ) ( -125 : -127 :
        -98 : -83 : 3 : 99 : 128 : 100 ) ( -90 : -96 : -98 : -3 : -83 :
        99 : 97 : 100 ) ( -171 : 172 : 183 ) -1 ( -205 : 198 : 197 ) (
        -192 : ( ( 185 : 191 ) ( 193 : 194 ) ( 204 : 201 ) ) ) ( -79 :
        83 : 82 ) ( -111 : -92 : -98 : -3 : -83 : 99 : 115 : 116 ) (
        -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) ( -150 : -91 :
        -82 : 151 : 112 : 153 ) ( -96 : -3 : -83 : -99 : 115 : 156 :
        154 )
3 118 0.122325  24 4 -6
4 0  183 -158 -15 171 -172
5 118 0.122325  49 -5 -6
6 3 0.0878729  ( 6 : -4 ) 24 7 -15
7 3 0.0878729  ( 6 : 5 ) 49 -8 -15
8 9 2.45e-05  -9 -2 15 -172 171 -10
9 9 2.45e-05  -11 9 -12
10 54 0.0833854  -11 9 -13 12
11 9 2.45e-05  -14 11 -12
12 54 0.0833854  -14 11 -13 12
13 0  -2 -83 14 -12
14 54 0.0833854  -2 -83 14 -13 12
15 5 0.0582256  -17 -16 -171
16 0  -18 17 -16 -171
17 3 0.0878729  -15 18 -16 -171
18 11 0.100283  -20 -19 16
19 5 0.0582256  -17 20 -19 16
20 0  -18 17 -19 16
21 3 0.0878729  -15 18 -19 16
22 0  -15 -21 341
23 5 0.0582256  -45 -22 21
24 0  -46 45 -22 21
25 3 0.0878729  -15 46 -22 21
26 11 0.100283  -48 -23 22
27 5 0.0582256  -45 48 -23 22
28 0  -46 45 -23 22
29 3 0.0878729  -15 46 -23 22
30 5 0.0582256  -45 -24 23
31 0  -46 45 -24 23
32 3 0.0878729  -15 46 -24 23
33 0  -78 3 15 26 25 50 -51
34 5 0.0582256  ( -50 : -25 : -26 : 51 ) -78 3 15 30 29 -28 27
35 0  -78 -3 15 ( 32 : -38 ) ( 31 : 37 ) 62 -63
36 5 0.0582256  ( -62 : 63 : ( -37 -31 ) : ( -32 38 ) ) -78 -3 15 ( 36
        : -38 ) ( 35 : 37 ) -34 33
37 5 0.0582256  -17 -41 172
38 0  -18 17 -41 172
39 3 0.0878729  -15 18 -41 172
40 11 0.100283  -20 -42 41
41 5 0.0582256  -17 20 -42 41
42 0  -18 17 -42 41
43 3 0.0878729  -15 18 -42 41
44 0  -15 -43 450
45 5 0.0582256  -45 -44 43
46 0  -46 45 -44 43
47 3 0.0878729  -15 46 -44 43
48 11 0.100283  -48 -47 44
49 5 0.0582256  -45 48 -47 44
50 0  -46 45 -47 44
51 3 0.0878729  -15 46 -47 44
52 5 0.0582256  -45 -49 47
53 0  -46 45 -49 47
54 3 0.0878729  -15 46 -49 47
55 0  -78 3 15 ( 53 : -59 ) ( 52 : 58 ) -51 50
56 5 0.0582256  ( -50 : 51 : ( -58 -52 ) : ( -53 59 ) ) -78 3 15 ( 57 :
        -59 ) ( 56 : 58 ) -55 54
57 0  -78 -3 15 65 64 -63 62
58 5 0.0582256  ( -62 : -64 : -65 : 63 ) -78 -3 15 69 68 -67 66
59 0  ( -9 : 11 : 13 ) ( -171 : -15 : 2 : 9 : 172 : 10 ) ( -11 : 14 :
        13 ) ( -14 : 2 : 13 : 83 ) ( -54 : 55 : ( -56 -60 ) : ( 61 -57
        ) ) ( -27 : -29 : -30 : 28 ) ( -171 : 172 : 183 ) 70 -71 -72 (
        -7 : 8 : 15 ) ( -205 : 198 : 197 ) ( -192 : ( ( 185 : 191 ) (
        193 : 194 ) ( 204 : 201 ) ) ) ( -33 : 34 : ( -39 -35 ) : ( -36
        40 ) ) ( -66 : -68 : -69 : 67 )
60 121 0.0862739  ( -9 : 11 : 13 ) ( -171 : -15 : 2 : 9 : 172 : 10 ) (
        -11 : 14 : 13 ) ( -14 : 2 : 13 : 83 ) ( -54 : 55 : ( -56 -60 )
        : ( 61 -57 ) ) ( -27 : -29 : -30 : 28 ) ( -171 : 172 : 183 ) 73
        -74 -75 ( -70 : 71 : 72 ) ( -205 : 198 : 197 ) ( -192 : ( ( 185
        : 191 ) ( 193 : 194 ) ( 204 : 201 ) ) ) ( -33 : 34 : ( -39 -35
        ) : ( -36 40 ) ) ( -66 : -68 : -69 : 67 )
61 121 0.0862739  ( -9 : 11 : 13 ) ( -171 : -15 : 2 : 9 : 172 : 10 ) (
        -11 : 14 : 13 ) ( -14 : 2 : 13 : 83 ) ( -54 : 55 : ( -56 -60 )
        : ( 61 -57 ) ) ( -27 : -29 : -30 : 28 ) ( -171 : 172 : 183 ) 76
        -77 -78 ( -73 : 74 : 75 ) ( -205 : 198 : 197 ) ( -192 : ( ( 185
        : 191 ) ( 193 : 194 ) ( 204 : 201 ) ) ) ( -33 : 34 : ( -39 -35
        ) : ( -36 40 ) ) ( -66 : -68 : -69 : 67 )
62 54 0.0833854  ( -9 : 11 : 13 ) ( -171 : -15 : 2 : 9 : 172 : 10 ) (
        -11 : 14 : 13 ) ( -14 : 2 : 13 : 83 ) ( -78 : -488 : -489 : 490
        : 487 : 493 : 3 ) ( -3 : -78 : -487 : -489 : 490 : 488 : 493 )
        ( -171 : 172 : 183 ) 79 -84 -81 ( -76 : 78 : 77 ) ( -205 : 198
        : 197 ) ( -192 : ( ( 185 : 191 ) ( 193 : 194 ) ( 204 : 201 ) )
        ) ( -3 : -493 : -487 : -491 : 492 : 488 : 81 ) ( -493 : -488 :
        -491 : 492 : 487 : 81 : 3 )
63 54 0.0833854  ( -192 : ( ( 204 : 201 ) ( 193 : 194 ) ( 185 : 191 ) )
        ) ( -205 : 198 : 197 ) -80 -85 84 ( -171 : 172 : 183 )
64 0  ( -9 : 11 : 13 ) ( -171 : -15 : 2 : 9 : 172 : 10 ) ( -11 : 14 :
        13 ) ( -14 : 2 : 13 : 83 ) ( -171 : 172 : 183 ) 79 -84 81 -83 (
        -205 : 198 : 197 ) ( -192 : ( ( 185 : 191 ) ( 193 : 194 ) ( 204
        : 201 ) ) )
65 0  ( -192 : ( ( 204 : 201 ) ( 193 : 194 ) ( 185 : 191 ) ) ) ( -205 :
        198 : 197 ) -83 85 -87 84 ( -171 : 172 : 183 )
66 0  ( -192 : ( ( 204 : 201 ) ( 193 : 194 ) ( 185 : 191 ) ) ) ( -205 :
        198 : 197 ) -86 85 -80 87 ( -171 : 172 : 183 )
67 0  ( -192 : ( ( 204 : 201 ) ( 193 : 194 ) ( 185 : 191 ) ) ) ( -205 :
        198 : 197 ) -86 -82 80 ( -171 : 172 : 183 )
68 3 0.0878729  ( -192 : ( ( 204 : 201 ) ( 193 : 194 ) ( 185 : 191 ) )
        ) ( -205 : 198 : 197 ) -88 87 -83 86 ( -171 : 172 : 183 )
69 0  ( -192 : ( ( 204 : 201 ) ( 193 : 194 ) ( 185 : 191 ) ) ) ( -205 :
        198 : 197 ) -89 88 -83 86 ( -171 : 172 : 183 )
70 49 0.074981  ( -192 : ( ( 204 : 201 ) ( 193 : 194 ) ( 185 : 191 ) )
        ) ( -205 : 198 : 197 ) -82 89 -83 86 ( -171 : 172 : 183 )
71 0  83 3 -94 93 -92 91 -95 90
72 71 0.0843223  83 3 -101 93 96 -91 -95 90
73 71 0.0843223  ( -111 : -92 : -98 : -3 : -83 : 99 : 115 : 116 ) 90
        -95 92 -97 93 -101 3 83
74 71 0.0843223  ( -111 : -92 : -98 : -3 : -83 : 99 : 115 : 116 ) 90
        -100 96 -97 -93 98 3 83
75 0  83 3 -94 93 -112 92 -113 111
76 71 0.0843223  83 3 -101 93 -115 112 -113 111
77 71 0.0843223  83 3 98 -93 -115 92 -116 111
78 0  83 -3 -94 93 -126 112 -95 125
79 71 0.0843223  83 -3 -101 93 127 -112 -95 125
80 71 0.0843223  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 125
        -95 126 -128 93 -101 -3 83
81 71 0.0843223  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 125
        -100 127 -128 -93 98 -3 83
82 0  83 -3 -94 93 -91 126 -113 138
83 71 0.0843223  83 -3 -101 93 -140 91 -113 138
84 71 0.0843223  83 -3 98 -93 -140 126 -116 138
85 49 0.074981  -149 151 -112 91 -83 150
86 49 0.074981  82 -151 -112 91 -152 150
87 0  82 -151 -112 91 -153 152
88 0  83 3 84 -87 -112 91 -152
89 0  83 3 -82 84 -112 91 -153 152
90 3 0.0878729  -152 91 -112 87 3 -88 83
91 0  -152 91 -112 3 88 -89 83
92 49 0.074981  -152 91 -112 -82 3 89 83
93 0  ( 153 : 151 ) 99 83 3 -112 91 -155 -154
94 49 0.074981  83 3 -112 91 -156 155 -154
95 49 0.074981  83 3 99 -91 96 -156 -154
96 49 0.074981  83 3 99 112 -115 -156 -154
97 0  99 83 -3 -91 112 -155 -157
98 49 0.074981  83 -3 -91 112 -156 155 -157
99 49 0.074981  83 -3 99 -112 127 -156 -157
100 49 0.074981  83 -3 99 91 -140 -156 -157
101 0  177 -166 -164 161 -306 305
102 3 0.0878729  177 -166 -164 161 -307 306  tmp=5.17040532e-08
103 0  177 -166 -164 161 -308 307
104 3 0.0878729  177 -166 -164 161 -309 308  tmp=5.17040532e-08
105 0  177 -166 -164 161 -310 309
106 3 0.0878729  177 -166 -164 161 -311 310  tmp=5.17040532e-08
107 0  177 -166 -164 161 -312 311
108 3 0.0878729  177 -166 -164 161 -313 312  tmp=5.17040532e-08
109 0  177 -166 -164 161 -314 313
110 3 0.0878729  177 -166 -164 161 -315 314  tmp=5.17040532e-08
111 0  177 -166 -164 161 -316 315
112 3 0.0878729  177 -166 -164 161 -317 316  tmp=5.17040532e-08
113 0  177 -166 -164 161 -318 317
114 3 0.0878729  177 -166 -164 161 -319 318  tmp=5.17040532e-08
115 0  177 -166 -164 161 -320 319
116 3 0.0878729  177 -166 -164 161 -321 320  tmp=5.17040532e-08
117 0  177 -166 -164 161 -322 321
118 3 0.0878729  177 -166 -164 161 -323 322  tmp=5.17040532e-08
119 0  177 -166 -164 161 -324 323
120 3 0.0878729  177 -166 -164 161 -325 324  tmp=5.17040532e-08
121 0  177 -166 -164 161 -326 325
122 3 0.0878729  177 -166 -164 161 -327 326  tmp=5.17040532e-08
123 0  177 -166 -164 161 -328 327
124 3 0.0878729  177 -166 -164 161 -329 328  tmp=5.17040532e-08
125 0  177 -166 -164 161 -330 329
126 3 0.0878729  177 -166 -164 161 -331 330  tmp=5.17040532e-08
127 0  177 -166 -164 161 -332 331
128 3 0.0878729  177 -166 -164 161 -333 332  tmp=5.17040532e-08
129 0  177 -166 -164 161 -334 333
130 3 0.0878729  177 -166 -164 161 -335 334  tmp=5.17040532e-08
131 0  177 -166 -164 161 -336 335
132 3 0.0878729  177 -166 -164 161 -337 336  tmp=5.17040532e-08
133 0  177 -166 -164 161 -338 337
134 3 0.0878729  177 -166 -164 161 -339 338  tmp=5.17040532e-08
135 0  177 -166 -164 161 -340 339
136 3 0.0878729  177 -166 -164 161 305 340  tmp=5.17040532e-08
137 0  177 -166 -164 161 306 -305
138 3 0.0878729  177 -166 -164 161 307 -306  tmp=5.17040532e-08
139 0  177 -166 -164 161 308 -307
140 3 0.0878729  177 -166 -164 161 309 -308  tmp=5.17040532e-08
141 0  177 -166 -164 161 310 -309
142 3 0.0878729  177 -166 -164 161 311 -310  tmp=5.17040532e-08
143 0  177 -166 -164 161 312 -311
144 3 0.0878729  177 -166 -164 161 313 -312  tmp=5.17040532e-08
145 0  177 -166 -164 161 314 -313
146 3 0.0878729  177 -166 -164 161 315 -314  tmp=5.17040532e-08
147 0  177 -166 -164 161 316 -315
148 3 0.0878729  177 -166 -164 161 317 -316  tmp=5.17040532e-08
149 0  177 -166 -164 161 318 -317
150 3 0.0878729  177 -166 -164 161 319 -318  tmp=5.17040532e-08
151 0  177 -166 -164 161 320 -319
152 3 0.0878729  177 -166 -164 161 321 -320  tmp=5.17040532e-08
153 0  177 -166 -164 161 322 -321
154 3 0.0878729  177 -166 -164 161 323 -322  tmp=5.17040532e-08
155 0  177 -166 -164 161 324 -323
156 3 0.0878729  177 -166 -164 161 325 -324  tmp=5.17040532e-08
157 0  177 -166 -164 161 326 -325
158 3 0.0878729  177 -166 -164 161 327 -326  tmp=5.17040532e-08
159 0  177 -166 -164 161 328 -327
160 3 0.0878729  177 -166 -164 161 329 -328  tmp=5.17040532e-08
161 0  177 -166 -164 161 330 -329
162 3 0.0878729  177 -166 -164 161 331 -330  tmp=5.17040532e-08
163 0  177 -166 -164 161 332 -331
164 3 0.0878729  177 -166 -164 161 333 -332  tmp=5.17040532e-08
165 0  177 -166 -164 161 334 -333
166 3 0.0878729  177 -166 -164 161 335 -334  tmp=5.17040532e-08
167 0  177 -166 -164 161 336 -335
168 3 0.0878729  177 -166 -164 161 337 -336  tmp=5.17040532e-08
169 0  177 -166 -164 161 338 -337
170 3 0.0878729  177 -166 -164 161 339 -338  tmp=5.17040532e-08
171 0  177 -166 -164 161 340 -339
172 3 0.0878729  177 -166 -164 161 -305 -340  tmp=5.17040532e-08
173 0  165 -176 -164 161 -306 305
174 3 0.0878729  165 -176 -164 161 -307 306  tmp=5.17040532e-08
175 0  165 -176 -164 161 -308 307
176 3 0.0878729  165 -176 -164 161 -309 308  tmp=5.17040532e-08
177 0  165 -176 -164 161 -310 309
178 3 0.0878729  165 -176 -164 161 -311 310  tmp=5.17040532e-08
179 0  165 -176 -164 161 -312 311
180 3 0.0878729  165 -176 -164 161 -313 312  tmp=5.17040532e-08
181 0  165 -176 -164 161 -314 313
182 3 0.0878729  165 -176 -164 161 -315 314  tmp=5.17040532e-08
183 0  165 -176 -164 161 -316 315
184 3 0.0878729  165 -176 -164 161 -317 316  tmp=5.17040532e-08
185 0  165 -176 -164 161 -318 317
186 3 0.0878729  165 -176 -164 161 -319 318  tmp=5.17040532e-08
187 0  165 -176 -164 161 -320 319
188 3 0.0878729  165 -176 -164 161 -321 320  tmp=5.17040532e-08
189 0  165 -176 -164 161 -322 321
190 3 0.0878729  165 -176 -164 161 -323 322  tmp=5.17040532e-08
191 0  165 -176 -164 161 -324 323
192 3 0.0878729  165 -176 -164 161 -325 324  tmp=5.17040532e-08
193 0  165 -176 -164 161 -326 325
194 3 0.0878729  165 -176 -164 161 -327 326  tmp=5.17040532e-08
195 0  165 -176 -164 161 -328 327
196 3 0.0878729  165 -176 -164 161 -329 328  tmp=5.17040532e-08
197 0  165 -176 -164 161 -330 329
198 3 0.0878729  165 -176 -164 161 -331 330  tmp=5.17040532e-08
199 0  165 -176 -164 161 -332 331
200 3 0.0878729  165 -176 -164 161 -333 332  tmp=5.17040532e-08
201 0  165 -176 -164 161 -334 333
202 3 0.0878729  165 -176 -164 161 -335 334  tmp=5.17040532e-08
203 0  165 -176 -164 161 -336 335
204 3 0.0878729  165 -176 -164 161 -337 336  tmp=5.17040532e-08
205 0  165 -176 -164 161 -338 337
206 3 0.0878729  165 -176 -164 161 -339 338  tmp=5.17040532e-08
207 0  165 -176 -164 161 -340 339
208 3 0.0878729  165 -176 -164 161 305 340  tmp=5.17040532e-08
209 0  165 -176 -164 161 306 -305
210 3 0.0878729  165 -176 -164 161 307 -306  tmp=5.17040532e-08
211 0  165 -176 -164 161 308 -307
212 3 0.0878729  165 -176 -164 161 309 -308  tmp=5.17040532e-08
213 0  165 -176 -164 161 310 -309
214 3 0.0878729  165 -176 -164 161 311 -310  tmp=5.17040532e-08
215 0  165 -176 -164 161 312 -311
216 3 0.0878729  165 -176 -164 161 313 -312  tmp=5.17040532e-08
217 0  165 -176 -164 161 314 -313
218 3 0.0878729  165 -176 -164 161 315 -314  tmp=5.17040532e-08
219 0  165 -176 -164 161 316 -315
220 3 0.0878729  165 -176 -164 161 317 -316  tmp=5.17040532e-08
221 0  165 -176 -164 161 318 -317
222 3 0.0878729  165 -176 -164 161 319 -318  tmp=5.17040532e-08
223 0  165 -176 -164 161 320 -319
224 3 0.0878729  165 -176 -164 161 321 -320  tmp=5.17040532e-08
225 0  165 -176 -164 161 322 -321
226 3 0.0878729  165 -176 -164 161 323 -322  tmp=5.17040532e-08
227 0  165 -176 -164 161 324 -323
228 3 0.0878729  165 -176 -164 161 325 -324  tmp=5.17040532e-08
229 0  165 -176 -164 161 326 -325
230 3 0.0878729  165 -176 -164 161 327 -326  tmp=5.17040532e-08
231 0  165 -176 -164 161 328 -327
232 3 0.0878729  165 -176 -164 161 329 -328  tmp=5.17040532e-08
233 0  165 -176 -164 161 330 -329
234 3 0.0878729  165 -176 -164 161 331 -330  tmp=5.17040532e-08
235 0  165 -176 -164 161 332 -331
236 3 0.0878729  165 -176 -164 161 333 -332  tmp=5.17040532e-08
237 0  165 -176 -164 161 334 -333
238 3 0.0878729  165 -176 -164 161 335 -334  tmp=5.17040532e-08
239 0  165 -176 -164 161 336 -335
240 3 0.0878729  165 -176 -164 161 337 -336  tmp=5.17040532e-08
241 0  165 -176 -164 161 338 -337
242 3 0.0878729  165 -176 -164 161 339 -338  tmp=5.17040532e-08
243 0  165 -176 -164 161 340 -339
244 3 0.0878729  165 -176 -164 161 -305 -340  tmp=5.17040532e-08
245 3 0.0878729  168 -165 -164 161  tmp=5.17040532e-08
246 3 0.0878729  -164 161 176 -303  tmp=5.17040532e-08
247 3 0.0878729  176 -164 161 -177 304  tmp=5.17040532e-08
248 0  -164 161 -304 303 -306 305
249 3 0.0878729  -164 161 -304 303 -307 306  tmp=5.17040532e-08
250 0  -164 161 -304 303 -308 307
251 3 0.0878729  -164 161 -304 303 -309 308  tmp=5.17040532e-08
252 0  -164 161 -304 303 -310 309
253 3 0.0878729  -164 161 -304 303 -311 310  tmp=5.17040532e-08
254 0  -164 161 -304 303 -312 311
255 3 0.0878729  -164 161 -304 303 -313 312  tmp=5.17040532e-08
256 0  -164 161 -304 303 -314 313
257 3 0.0878729  -164 161 -304 303 -315 314  tmp=5.17040532e-08
258 0  -164 161 -304 303 -316 315
259 3 0.0878729  -164 161 -304 303 -317 316  tmp=5.17040532e-08
260 0  -164 161 -304 303 -318 317
261 3 0.0878729  -164 161 -304 303 -319 318  tmp=5.17040532e-08
262 0  -164 161 -304 303 -320 319
263 3 0.0878729  -164 161 -304 303 -321 320  tmp=5.17040532e-08
264 0  -164 161 -304 303 -322 321
265 3 0.0878729  -164 161 -304 303 -323 322  tmp=5.17040532e-08
266 0  -164 161 -304 303 -324 323
267 3 0.0878729  -164 161 -304 303 -325 324  tmp=5.17040532e-08
268 0  -164 161 -304 303 -326 325
269 3 0.0878729  -164 161 -304 303 -327 326  tmp=5.17040532e-08
270 0  -164 161 -304 303 -328 327
271 3 0.0878729  -164 161 -304 303 -329 328  tmp=5.17040532e-08
272 0  -164 161 -304 303 -330 329
273 3 0.0878729  -164 161 -304 303 -331 330  tmp=5.17040532e-08
274 0  -164 161 -304 303 -332 331
275 3 0.0878729  -164 161 -304 303 -333 332  tmp=5.17040532e-08
276 0  -164 161 -304 303 -334 333
277 3 0.0878729  -164 161 -304 303 -335 334  tmp=5.17040532e-08
278 0  -164 161 -304 303 -336 335
279 3 0.0878729  -164 161 -304 303 -337 336  tmp=5.17040532e-08
280 0  -164 161 -304 303 -338 337
281 3 0.0878729  -164 161 -304 303 -339 338  tmp=5.17040532e-08
282 0  -164 161 -304 303 -340 339
283 3 0.0878729  -164 161 -304 303 305 340  tmp=5.17040532e-08
284 0  -164 161 -304 303 306 -305
285 3 0.0878729  -164 161 -304 303 307 -306  tmp=5.17040532e-08
286 0  -164 161 -304 303 308 -307
287 3 0.0878729  -164 161 -304 303 309 -308  tmp=5.17040532e-08
288 0  -164 161 -304 303 310 -309
289 3 0.0878729  -164 161 -304 303 311 -310  tmp=5.17040532e-08
290 0  -164 161 -304 303 312 -311
291 3 0.0878729  -164 161 -304 303 313 -312  tmp=5.17040532e-08
292 0  -164 161 -304 303 314 -313
293 3 0.0878729  -164 161 -304 303 315 -314  tmp=5.17040532e-08
294 0  -164 161 -304 303 316 -315
295 3 0.0878729  -164 161 -304 303 317 -316  tmp=5.17040532e-08
296 0  -164 161 -304 303 318 -317
297 3 0.0878729  -164 161 -304 303 319 -318  tmp=5.17040532e-08
298 0  -164 161 -304 303 320 -319
299 3 0.0878729  -164 161 -304 303 321 -320  tmp=5.17040532e-08
300 0  -164 161 -304 303 322 -321
301 3 0.0878729  -164 161 -304 303 323 -322  tmp=5.17040532e-08
302 0  -164 161 -304 303 324 -323
303 3 0.0878729  -164 161 -304 303 325 -324  tmp=5.17040532e-08
304 0  -164 161 -304 303 326 -325
305 3 0.0878729  -164 161 -304 303 327 -326  tmp=5.17040532e-08
306 0  -164 161 -304 303 328 -327
307 3 0.0878729  -164 161 -304 303 329 -328  tmp=5.17040532e-08
308 0  -164 161 -304 303 330 -329
309 3 0.0878729  -164 161 -304 303 331 -330  tmp=5.17040532e-08
310 0  -164 161 -304 303 332 -331
311 3 0.0878729  -164 161 -304 303 333 -332  tmp=5.17040532e-08
312 0  -164 161 -304 303 334 -333
313 3 0.0878729  -164 161 -304 303 335 -334  tmp=5.17040532e-08
314 0  -164 161 -304 303 336 -335
315 3 0.0878729  -164 161 -304 303 337 -336  tmp=5.17040532e-08
316 0  -164 161 -304 303 338 -337
317 3 0.0878729  -164 161 -304 303 339 -338  tmp=5.17040532e-08
318 0  -164 161 -304 303 340 -339
319 3 0.0878729  -164 161 -304 303 -305 -340  tmp=5.17040532e-08
320 3 0.0878729  -160 159 -167 178  tmp=5.17040532e-08
321 3 0.0878729  -174 173 175 -178  tmp=5.17040532e-08
322 3 0.0878729  -166 174 175 -178  tmp=5.17040532e-08
323 3 0.0878729  165 -173 175 -178  tmp=5.17040532e-08
324 3 0.0878729  168 -165 175 -178  tmp=5.17040532e-08
325 3 0.0878729  -169 166 175 -178  tmp=5.17040532e-08
326 3 0.0878729  -174 173 -175 164  tmp=5.17040532e-08
327 3 0.0878729  -177 174 -175 164  tmp=5.17040532e-08
328 3 0.0878729  176 -173 -175 164  tmp=5.17040532e-08
329 3 0.0878729  -169 177 -175 164  tmp=5.17040532e-08
330 3 0.0878729  168 -176 -175 164  tmp=5.17040532e-08
331 120 0.0494621  -160 159 -170 167  tmp=5.17040532e-08
332 0  -169 168 -181 170 -287 3  tmp=5.17040532e-08
333 0  -169 168 -181 170 -288 287  tmp=5.17040532e-08
334 0  -169 168 -181 170 -289 288  tmp=5.17040532e-08
335 0  -169 168 -181 170 -290 289  tmp=5.17040532e-08
336 0  -169 168 -181 170 -291 290  tmp=5.17040532e-08
337 0  -169 168 -181 170 -292 291  tmp=5.17040532e-08
338 0  -169 168 -181 170 -293 292  tmp=5.17040532e-08
339 0  -169 168 -181 170 -294 293  tmp=5.17040532e-08
340 0  -169 168 -181 170 -158 294  tmp=5.17040532e-08
341 0  -169 168 -181 170 -295 158  tmp=5.17040532e-08
342 0  -169 168 -181 170 -296 295  tmp=5.17040532e-08
343 0  -169 168 -181 170 -297 296  tmp=5.17040532e-08
344 0  -169 168 -181 170 -298 297  tmp=5.17040532e-08
345 0  -169 168 -181 170 -299 298  tmp=5.17040532e-08
346 0  -169 168 -181 170 -300 299  tmp=5.17040532e-08
347 0  -169 168 -181 170 -301 300  tmp=5.17040532e-08
348 0  -169 168 -181 170 -302 301  tmp=5.17040532e-08
349 0  -169 168 -181 170 3 302  tmp=5.17040532e-08
350 0  -169 168 -181 170 287 -3  tmp=5.17040532e-08
351 0  -169 168 -181 170 288 -287  tmp=5.17040532e-08
352 0  -169 168 -181 170 289 -288  tmp=5.17040532e-08
353 0  -169 168 -181 170 290 -289  tmp=5.17040532e-08
354 0  -169 168 -181 170 291 -290  tmp=5.17040532e-08
355 0  -169 168 -181 170 292 -291  tmp=5.17040532e-08
356 0  -169 168 -181 170 293 -292  tmp=5.17040532e-08
357 0  -169 168 -181 170 294 -293  tmp=5.17040532e-08
358 0  -169 168 -181 170 158 -294  tmp=5.17040532e-08
359 0  -169 168 -181 170 295 -158  tmp=5.17040532e-08
360 0  -169 168 -181 170 296 -295  tmp=5.17040532e-08
361 0  -169 168 -181 170 297 -296  tmp=5.17040532e-08
362 0  -169 168 -181 170 298 -297  tmp=5.17040532e-08
363 0  -169 168 -181 170 299 -298  tmp=5.17040532e-08
364 0  -169 168 -181 170 300 -299  tmp=5.17040532e-08
365 0  -169 168 -181 170 301 -300  tmp=5.17040532e-08
366 0  -169 168 -181 170 302 -301  tmp=5.17040532e-08
367 0  -169 168 -181 170 -3 -302  tmp=5.17040532e-08
368 0  178 -163 160 -170
369 0  178 -159 162 -170
370 3 0.0878729  178 -166 163 -170  tmp=5.17040532e-08
371 3 0.0878729  178 -162 165 -170  tmp=5.17040532e-08
372 0  178 -169 166 -170  tmp=5.17040532e-08
373 0  178 -165 168 -170  tmp=5.17040532e-08
374 3 0.0878729  -169 168 181 -182 -287 3  tmp=5.17040532e-08
375 3 0.0878729  -169 168 181 -182 -288 287  tmp=5.17040532e-08
376 3 0.0878729  -169 168 181 -182 -289 288  tmp=5.17040532e-08
377 3 0.0878729  -169 168 181 -182 -290 289  tmp=5.17040532e-08
378 3 0.0878729  -169 168 181 -182 -291 290  tmp=5.17040532e-08
379 3 0.0878729  -169 168 181 -182 -292 291  tmp=5.17040532e-08
380 3 0.0878729  -169 168 181 -182 -293 292  tmp=5.17040532e-08
381 3 0.0878729  -169 168 181 -182 -294 293  tmp=5.17040532e-08
382 3 0.0878729  -169 168 181 -182 -158 294  tmp=5.17040532e-08
383 3 0.0878729  -169 168 181 -182 -295 158  tmp=5.17040532e-08
384 3 0.0878729  -169 168 181 -182 -296 295  tmp=5.17040532e-08
385 3 0.0878729  -169 168 181 -182 -297 296  tmp=5.17040532e-08
386 3 0.0878729  -169 168 181 -182 -298 297  tmp=5.17040532e-08
387 3 0.0878729  -169 168 181 -182 -299 298  tmp=5.17040532e-08
388 3 0.0878729  -169 168 181 -182 -300 299  tmp=5.17040532e-08
389 3 0.0878729  -169 168 181 -182 -301 300  tmp=5.17040532e-08
390 3 0.0878729  -169 168 181 -182 -302 301  tmp=5.17040532e-08
391 3 0.0878729  -169 168 181 -182 3 302  tmp=5.17040532e-08
392 3 0.0878729  -169 168 181 -182 287 -3  tmp=5.17040532e-08
393 3 0.0878729  -169 168 181 -182 288 -287  tmp=5.17040532e-08
394 3 0.0878729  -169 168 181 -182 289 -288  tmp=5.17040532e-08
395 3 0.0878729  -169 168 181 -182 290 -289  tmp=5.17040532e-08
396 3 0.0878729  -169 168 181 -182 291 -290  tmp=5.17040532e-08
397 3 0.0878729  -169 168 181 -182 292 -291  tmp=5.17040532e-08
398 3 0.0878729  -169 168 181 -182 293 -292  tmp=5.17040532e-08
399 3 0.0878729  -169 168 181 -182 294 -293  tmp=5.17040532e-08
400 3 0.0878729  -169 168 181 -182 158 -294  tmp=5.17040532e-08
401 3 0.0878729  -169 168 181 -182 295 -158  tmp=5.17040532e-08
402 3 0.0878729  -169 168 181 -182 296 -295  tmp=5.17040532e-08
403 3 0.0878729  -169 168 181 -182 297 -296  tmp=5.17040532e-08
404 3 0.0878729  -169 168 181 -182 298 -297  tmp=5.17040532e-08
405 3 0.0878729  -169 168 181 -182 299 -298  tmp=5.17040532e-08
406 3 0.0878729  -169 168 181 -182 300 -299  tmp=5.17040532e-08
407 3 0.0878729  -169 168 181 -182 301 -300  tmp=5.17040532e-08
408 3 0.0878729  -169 168 181 -182 302 -301  tmp=5.17040532e-08
409 3 0.0878729  -169 168 181 -182 -3 -302  tmp=5.17040532e-08
410 3 0.0878729  169 -180 184 -182  tmp=5.17040532e-08
411 3 0.0878729  -168 179 184 -182  tmp=5.17040532e-08
412 0  ( -179 : 180 : 182 ) -183 -172 171 194 -287 3
        tmp=5.17040532e-08
413 0  ( -179 : 180 : 182 ) -183 -172 171 194 -288 287
        tmp=5.17040532e-08
414 0  ( -179 : 180 : 182 ) -183 -172 171 194 -289 288
        tmp=5.17040532e-08
415 0  ( -179 : 180 : 182 ) -183 -172 171 194 -290 289
        tmp=5.17040532e-08
416 0  ( -179 : 180 : 182 ) -183 -172 171 194 -291 290
        tmp=5.17040532e-08
417 0  ( -179 : 180 : 182 ) -183 -172 171 194 -292 291
        tmp=5.17040532e-08
418 0  ( -179 : 180 : 182 ) -183 -172 171 194 -293 292
        tmp=5.17040532e-08
419 0  ( -179 : 180 : 182 ) -183 -172 171 194 -294 293
        tmp=5.17040532e-08
420 0  ( -179 : 180 : 182 ) -183 -172 171 194 -158 294
        tmp=5.17040532e-08
421 0  ( -179 : 180 : 182 ) -183 -172 171 194 -295 158
        tmp=5.17040532e-08
422 0  ( -179 : 180 : 182 ) -183 -172 171 194 -296 295
        tmp=5.17040532e-08
423 0  ( -179 : 180 : 182 ) -183 -172 171 194 -297 296
        tmp=5.17040532e-08
424 0  ( -179 : 180 : 182 ) -183 -172 171 194 -298 297
        tmp=5.17040532e-08
425 0  ( -179 : 180 : 182 ) -183 -172 171 194 -299 298
        tmp=5.17040532e-08
426 0  ( -179 : 180 : 182 ) -183 -172 171 194 -300 299
        tmp=5.17040532e-08
427 0  ( -179 : 180 : 182 ) -183 -172 171 194 -301 300
        tmp=5.17040532e-08
428 0  ( -179 : 180 : 182 ) -183 -172 171 194 -302 301
        tmp=5.17040532e-08
429 0  ( -179 : 180 : 182 ) -183 -172 171 194 3 302  tmp=5.17040532e-08
430 0  ( -179 : 180 : 182 ) -183 -172 171 194 287 -3
        tmp=5.17040532e-08
431 0  ( -179 : 180 : 182 ) -183 -172 171 194 288 -287
        tmp=5.17040532e-08
432 0  ( -179 : 180 : 182 ) -183 -172 171 194 289 -288
        tmp=5.17040532e-08
433 0  ( -179 : 180 : 182 ) -183 -172 171 194 290 -289
        tmp=5.17040532e-08
434 0  ( -179 : 180 : 182 ) -183 -172 171 194 291 -290
        tmp=5.17040532e-08
435 0  ( -179 : 180 : 182 ) -183 -172 171 194 292 -291
        tmp=5.17040532e-08
436 0  ( -179 : 180 : 182 ) -183 -172 171 194 293 -292
        tmp=5.17040532e-08
437 0  ( -179 : 180 : 182 ) -183 -172 171 194 294 -293
        tmp=5.17040532e-08
438 0  ( -179 : 180 : 182 ) -183 -172 171 194 158 -294
        tmp=5.17040532e-08
439 0  ( -179 : 180 : 182 ) -183 -172 171 194 295 -158
        tmp=5.17040532e-08
440 0  ( -179 : 180 : 182 ) -183 -172 171 194 296 -295
        tmp=5.17040532e-08
441 0  ( -179 : 180 : 182 ) -183 -172 171 194 297 -296
        tmp=5.17040532e-08
442 0  ( -179 : 180 : 182 ) -183 -172 171 194 298 -297
        tmp=5.17040532e-08
443 0  ( -179 : 180 : 182 ) -183 -172 171 194 299 -298
        tmp=5.17040532e-08
444 0  ( -179 : 180 : 182 ) -183 -172 171 194 300 -299
        tmp=5.17040532e-08
445 0  ( -179 : 180 : 182 ) -183 -172 171 194 301 -300
        tmp=5.17040532e-08
446 0  ( -179 : 180 : 182 ) -183 -172 171 194 302 -301
        tmp=5.17040532e-08
447 0  ( -179 : 180 : 182 ) -183 -172 171 194 -3 -302
        tmp=5.17040532e-08
448 3 0.0878729  -160 159 -187  tmp=5.17040532e-08
449 3 0.0878729  -174 173 -215  tmp=5.17040532e-08
450 3 0.0878729  -174 173 -216 215  tmp=5.17040532e-08
451 0  -161 188 -174 173 -287 218 3 216  tmp=5.17040532e-08
452 3 0.0878729  -174 173 -217  tmp=5.17040532e-08
453 3 0.0878729  -174 173 -218 217  tmp=5.17040532e-08
454 0  -161 188 -174 173 -288 220 287 218  tmp=5.17040532e-08
455 3 0.0878729  -174 173 -219  tmp=5.17040532e-08
456 3 0.0878729  -174 173 -220 219  tmp=5.17040532e-08
457 0  -161 188 -174 173 -289 222 288 220  tmp=5.17040532e-08
458 3 0.0878729  -174 173 -221  tmp=5.17040532e-08
459 3 0.0878729  -174 173 -222 221  tmp=5.17040532e-08
460 0  -161 188 -174 173 -290 224 289 222  tmp=5.17040532e-08
461 3 0.0878729  -174 173 -223  tmp=5.17040532e-08
462 3 0.0878729  -174 173 -224 223  tmp=5.17040532e-08
463 0  -161 188 -174 173 -291 226 290 224  tmp=5.17040532e-08
464 3 0.0878729  -174 173 -225  tmp=5.17040532e-08
465 3 0.0878729  -174 173 -226 225  tmp=5.17040532e-08
466 0  -161 188 -174 173 -292 228 291 226  tmp=5.17040532e-08
467 3 0.0878729  -174 173 -227  tmp=5.17040532e-08
468 3 0.0878729  -174 173 -228 227  tmp=5.17040532e-08
469 0  -161 188 -174 173 -293 230 292 228  tmp=5.17040532e-08
470 3 0.0878729  -174 173 -229  tmp=5.17040532e-08
471 3 0.0878729  -174 173 -230 229  tmp=5.17040532e-08
472 0  -161 188 -174 173 -294 232 293 230  tmp=5.17040532e-08
473 3 0.0878729  -174 173 -231  tmp=5.17040532e-08
474 3 0.0878729  -174 173 -232 231  tmp=5.17040532e-08
475 0  -161 188 -174 173 -158 234 294 232  tmp=5.17040532e-08
476 3 0.0878729  -174 173 -233  tmp=5.17040532e-08
477 3 0.0878729  -174 173 -234 233  tmp=5.17040532e-08
478 0  -161 188 -174 173 -295 236 158 234  tmp=5.17040532e-08
479 3 0.0878729  -174 173 -235  tmp=5.17040532e-08
480 3 0.0878729  -174 173 -236 235  tmp=5.17040532e-08
481 0  -161 188 -174 173 -296 238 295 236  tmp=5.17040532e-08
482 3 0.0878729  -174 173 -237  tmp=5.17040532e-08
483 3 0.0878729  -174 173 -238 237  tmp=5.17040532e-08
484 0  -161 188 -174 173 -297 240 296 238  tmp=5.17040532e-08
485 3 0.0878729  -174 173 -239  tmp=5.17040532e-08
486 3 0.0878729  -174 173 -240 239  tmp=5.17040532e-08
487 0  -161 188 -174 173 -298 242 297 240  tmp=5.17040532e-08
488 3 0.0878729  -174 173 -241  tmp=5.17040532e-08
489 3 0.0878729  -174 173 -242 241  tmp=5.17040532e-08
490 0  -161 188 -174 173 -299 244 298 242  tmp=5.17040532e-08
491 3 0.0878729  -174 173 -243  tmp=5.17040532e-08
492 3 0.0878729  -174 173 -244 243  tmp=5.17040532e-08
493 0  -161 188 -174 173 -300 246 299 244  tmp=5.17040532e-08
494 3 0.0878729  -174 173 -245  tmp=5.17040532e-08
495 3 0.0878729  -174 173 -246 245  tmp=5.17040532e-08
496 0  -161 188 -174 173 -301 248 300 246  tmp=5.17040532e-08
497 3 0.0878729  -174 173 -247  tmp=5.17040532e-08
498 3 0.0878729  -174 173 -248 247  tmp=5.17040532e-08
499 0  -161 188 -174 173 -302 250 301 248  tmp=5.17040532e-08
500 3 0.0878729  -174 173 -249  tmp=5.17040532e-08
501 3 0.0878729  -174 173 -250 249  tmp=5.17040532e-08
502 0  -161 188 -174 173 3 252 302 250  tmp=5.17040532e-08
503 3 0.0878729  -174 173 -251  tmp=5.17040532e-08
504 3 0.0878729  -174 173 -252 251  tmp=5.17040532e-08
505 0  -161 188 -174 173 287 254 -3 252  tmp=5.17040532e-08
506 3 0.0878729  -174 173 -253  tmp=5.17040532e-08
507 3 0.0878729  -174 173 -254 253  tmp=5.17040532e-08
508 0  -161 188 -174 173 288 256 -287 254  tmp=5.17040532e-08
509 3 0.0878729  -174 173 -255  tmp=5.17040532e-08
510 3 0.0878729  -174 173 -256 255  tmp=5.17040532e-08
511 0  -161 188 -174 173 289 258 -288 256  tmp=5.17040532e-08
512 3 0.0878729  -174 173 -257  tmp=5.17040532e-08
513 3 0.0878729  -174 173 -258 257  tmp=5.17040532e-08
514 0  -161 188 -174 173 290 260 -289 258  tmp=5.17040532e-08
515 3 0.0878729  -174 173 -259  tmp=5.17040532e-08
516 3 0.0878729  -174 173 -260 259  tmp=5.17040532e-08
517 0  -161 188 -174 173 291 262 -290 260  tmp=5.17040532e-08
518 3 0.0878729  -174 173 -261  tmp=5.17040532e-08
519 3 0.0878729  -174 173 -262 261  tmp=5.17040532e-08
520 0  -161 188 -174 173 292 264 -291 262  tmp=5.17040532e-08
521 3 0.0878729  -174 173 -263  tmp=5.17040532e-08
522 3 0.0878729  -174 173 -264 263  tmp=5.17040532e-08
523 0  -161 188 -174 173 293 266 -292 264  tmp=5.17040532e-08
524 3 0.0878729  -174 173 -265  tmp=5.17040532e-08
525 3 0.0878729  -174 173 -266 265  tmp=5.17040532e-08
526 0  -161 188 -174 173 294 268 -293 266  tmp=5.17040532e-08
527 3 0.0878729  -174 173 -267  tmp=5.17040532e-08
528 3 0.0878729  -174 173 -268 267  tmp=5.17040532e-08
529 0  -161 188 -174 173 158 270 -294 268  tmp=5.17040532e-08
530 3 0.0878729  -174 173 -269  tmp=5.17040532e-08
531 3 0.0878729  -174 173 -270 269  tmp=5.17040532e-08
532 0  -161 188 -174 173 295 272 -158 270  tmp=5.17040532e-08
533 3 0.0878729  -174 173 -271  tmp=5.17040532e-08
534 3 0.0878729  -174 173 -272 271  tmp=5.17040532e-08
535 0  -161 188 -174 173 296 274 -295 272  tmp=5.17040532e-08
536 3 0.0878729  -174 173 -273  tmp=5.17040532e-08
537 3 0.0878729  -174 173 -274 273  tmp=5.17040532e-08
538 0  -161 188 -174 173 297 276 -296 274  tmp=5.17040532e-08
539 3 0.0878729  -174 173 -275  tmp=5.17040532e-08
540 3 0.0878729  -174 173 -276 275  tmp=5.17040532e-08
541 0  -161 188 -174 173 298 278 -297 276  tmp=5.17040532e-08
542 3 0.0878729  -174 173 -277  tmp=5.17040532e-08
543 3 0.0878729  -174 173 -278 277  tmp=5.17040532e-08
544 0  -161 188 -174 173 299 280 -298 278  tmp=5.17040532e-08
545 3 0.0878729  -174 173 -279  tmp=5.17040532e-08
546 3 0.0878729  -174 173 -280 279  tmp=5.17040532e-08
547 0  -161 188 -174 173 300 282 -299 280  tmp=5.17040532e-08
548 3 0.0878729  -174 173 -281  tmp=5.17040532e-08
549 3 0.0878729  -174 173 -282 281  tmp=5.17040532e-08
550 0  -161 188 -174 173 301 284 -300 282  tmp=5.17040532e-08
551 3 0.0878729  -174 173 -283  tmp=5.17040532e-08
552 3 0.0878729  -174 173 -284 283  tmp=5.17040532e-08
553 0  -161 188 -174 173 302 286 -301 284  tmp=5.17040532e-08
554 3 0.0878729  -174 173 -285  tmp=5.17040532e-08
555 3 0.0878729  -174 173 -286 285  tmp=5.17040532e-08
556 0  -161 188 -174 173 -3 216 -302 286  tmp=5.17040532e-08
557 3 0.0878729  -177 174 -215  tmp=5.17040532e-08
558 3 0.0878729  -177 174 -216 215  tmp=5.17040532e-08
559 3 0.0878729  188 -161 -177 174 -287 218 3 216  tmp=5.17040532e-08
560 3 0.0878729  -177 174 -217  tmp=5.17040532e-08
561 3 0.0878729  -177 174 -218 217  tmp=5.17040532e-08
562 3 0.0878729  188 -161 -177 174 -288 220 287 218  tmp=5.17040532e-08
563 3 0.0878729  -177 174 -219  tmp=5.17040532e-08
564 3 0.0878729  -177 174 -220 219  tmp=5.17040532e-08
565 3 0.0878729  188 -161 -177 174 -289 222 288 220  tmp=5.17040532e-08
566 3 0.0878729  -177 174 -221  tmp=5.17040532e-08
567 3 0.0878729  -177 174 -222 221  tmp=5.17040532e-08
568 3 0.0878729  188 -161 -177 174 -290 224 289 222  tmp=5.17040532e-08
569 3 0.0878729  -177 174 -223  tmp=5.17040532e-08
570 3 0.0878729  -177 174 -224 223  tmp=5.17040532e-08
571 3 0.0878729  188 -161 -177 174 -291 226 290 224  tmp=5.17040532e-08
572 3 0.0878729  -177 174 -225  tmp=5.17040532e-08
573 3 0.0878729  -177 174 -226 225  tmp=5.17040532e-08
574 3 0.0878729  188 -161 -177 174 -292 228 291 226  tmp=5.17040532e-08
575 3 0.0878729  -177 174 -227  tmp=5.17040532e-08
576 3 0.0878729  -177 174 -228 227  tmp=5.17040532e-08
577 3 0.0878729  188 -161 -177 174 -293 230 292 228  tmp=5.17040532e-08
578 3 0.0878729  -177 174 -229  tmp=5.17040532e-08
579 3 0.0878729  -177 174 -230 229  tmp=5.17040532e-08
580 3 0.0878729  188 -161 -177 174 -294 232 293 230  tmp=5.17040532e-08
581 3 0.0878729  -177 174 -231  tmp=5.17040532e-08
582 3 0.0878729  -177 174 -232 231  tmp=5.17040532e-08
583 3 0.0878729  188 -161 -177 174 -158 234 294 232  tmp=5.17040532e-08
584 3 0.0878729  -177 174 -233  tmp=5.17040532e-08
585 3 0.0878729  -177 174 -234 233  tmp=5.17040532e-08
586 3 0.0878729  188 -161 -177 174 -295 236 158 234  tmp=5.17040532e-08
587 3 0.0878729  -177 174 -235  tmp=5.17040532e-08
588 3 0.0878729  -177 174 -236 235  tmp=5.17040532e-08
589 3 0.0878729  188 -161 -177 174 -296 238 295 236  tmp=5.17040532e-08
590 3 0.0878729  -177 174 -237  tmp=5.17040532e-08
591 3 0.0878729  -177 174 -238 237  tmp=5.17040532e-08
592 3 0.0878729  188 -161 -177 174 -297 240 296 238  tmp=5.17040532e-08
593 3 0.0878729  -177 174 -239  tmp=5.17040532e-08
594 3 0.0878729  -177 174 -240 239  tmp=5.17040532e-08
595 3 0.0878729  188 -161 -177 174 -298 242 297 240  tmp=5.17040532e-08
596 3 0.0878729  -177 174 -241  tmp=5.17040532e-08
597 3 0.0878729  -177 174 -242 241  tmp=5.17040532e-08
598 3 0.0878729  188 -161 -177 174 -299 244 298 242  tmp=5.17040532e-08
599 3 0.0878729  -177 174 -243  tmp=5.17040532e-08
600 3 0.0878729  -177 174 -244 243  tmp=5.17040532e-08
601 3 0.0878729  188 -161 -177 174 -300 246 299 244  tmp=5.17040532e-08
602 3 0.0878729  -177 174 -245  tmp=5.17040532e-08
603 3 0.0878729  -177 174 -246 245  tmp=5.17040532e-08
604 3 0.0878729  188 -161 -177 174 -301 248 300 246  tmp=5.17040532e-08
605 3 0.0878729  -177 174 -247  tmp=5.17040532e-08
606 3 0.0878729  -177 174 -248 247  tmp=5.17040532e-08
607 3 0.0878729  188 -161 -177 174 -302 250 301 248  tmp=5.17040532e-08
608 3 0.0878729  -177 174 -249  tmp=5.17040532e-08
609 3 0.0878729  -177 174 -250 249  tmp=5.17040532e-08
610 3 0.0878729  188 -161 -177 174 3 252 302 250  tmp=5.17040532e-08
611 3 0.0878729  -177 174 -251  tmp=5.17040532e-08
612 3 0.0878729  -177 174 -252 251  tmp=5.17040532e-08
613 3 0.0878729  188 -161 -177 174 287 254 -3 252  tmp=5.17040532e-08
614 3 0.0878729  -177 174 -253  tmp=5.17040532e-08
615 3 0.0878729  -177 174 -254 253  tmp=5.17040532e-08
616 3 0.0878729  188 -161 -177 174 288 256 -287 254  tmp=5.17040532e-08
617 3 0.0878729  -177 174 -255  tmp=5.17040532e-08
618 3 0.0878729  -177 174 -256 255  tmp=5.17040532e-08
619 3 0.0878729  188 -161 -177 174 289 258 -288 256  tmp=5.17040532e-08
620 3 0.0878729  -177 174 -257  tmp=5.17040532e-08
621 3 0.0878729  -177 174 -258 257  tmp=5.17040532e-08
622 3 0.0878729  188 -161 -177 174 290 260 -289 258  tmp=5.17040532e-08
623 3 0.0878729  -177 174 -259  tmp=5.17040532e-08
624 3 0.0878729  -177 174 -260 259  tmp=5.17040532e-08
625 3 0.0878729  188 -161 -177 174 291 262 -290 260  tmp=5.17040532e-08
626 3 0.0878729  -177 174 -261  tmp=5.17040532e-08
627 3 0.0878729  -177 174 -262 261  tmp=5.17040532e-08
628 3 0.0878729  188 -161 -177 174 292 264 -291 262  tmp=5.17040532e-08
629 3 0.0878729  -177 174 -263  tmp=5.17040532e-08
630 3 0.0878729  -177 174 -264 263  tmp=5.17040532e-08
631 3 0.0878729  188 -161 -177 174 293 266 -292 264  tmp=5.17040532e-08
632 3 0.0878729  -177 174 -265  tmp=5.17040532e-08
633 3 0.0878729  -177 174 -266 265  tmp=5.17040532e-08
634 3 0.0878729  188 -161 -177 174 294 268 -293 266  tmp=5.17040532e-08
635 3 0.0878729  -177 174 -267  tmp=5.17040532e-08
636 3 0.0878729  -177 174 -268 267  tmp=5.17040532e-08
637 3 0.0878729  188 -161 -177 174 158 270 -294 268  tmp=5.17040532e-08
638 3 0.0878729  -177 174 -269  tmp=5.17040532e-08
639 3 0.0878729  -177 174 -270 269  tmp=5.17040532e-08
640 3 0.0878729  188 -161 -177 174 295 272 -158 270  tmp=5.17040532e-08
641 3 0.0878729  -177 174 -271  tmp=5.17040532e-08
642 3 0.0878729  -177 174 -272 271  tmp=5.17040532e-08
643 3 0.0878729  188 -161 -177 174 296 274 -295 272  tmp=5.17040532e-08
644 3 0.0878729  -177 174 -273  tmp=5.17040532e-08
645 3 0.0878729  -177 174 -274 273  tmp=5.17040532e-08
646 3 0.0878729  188 -161 -177 174 297 276 -296 274  tmp=5.17040532e-08
647 3 0.0878729  -177 174 -275  tmp=5.17040532e-08
648 3 0.0878729  -177 174 -276 275  tmp=5.17040532e-08
649 3 0.0878729  188 -161 -177 174 298 278 -297 276  tmp=5.17040532e-08
650 3 0.0878729  -177 174 -277  tmp=5.17040532e-08
651 3 0.0878729  -177 174 -278 277  tmp=5.17040532e-08
652 3 0.0878729  188 -161 -177 174 299 280 -298 278  tmp=5.17040532e-08
653 3 0.0878729  -177 174 -279  tmp=5.17040532e-08
654 3 0.0878729  -177 174 -280 279  tmp=5.17040532e-08
655 3 0.0878729  188 -161 -177 174 300 282 -299 280  tmp=5.17040532e-08
656 3 0.0878729  -177 174 -281  tmp=5.17040532e-08
657 3 0.0878729  -177 174 -282 281  tmp=5.17040532e-08
658 3 0.0878729  188 -161 -177 174 301 284 -300 282  tmp=5.17040532e-08
659 3 0.0878729  -177 174 -283  tmp=5.17040532e-08
660 3 0.0878729  -177 174 -284 283  tmp=5.17040532e-08
661 3 0.0878729  188 -161 -177 174 302 286 -301 284  tmp=5.17040532e-08
662 3 0.0878729  -177 174 -285  tmp=5.17040532e-08
663 3 0.0878729  -177 174 -286 285  tmp=5.17040532e-08
664 3 0.0878729  188 -161 -177 174 -3 216 -302 286  tmp=5.17040532e-08
665 3 0.0878729  176 -173 -215  tmp=5.17040532e-08
666 3 0.0878729  176 -173 -216 215  tmp=5.17040532e-08
667 3 0.0878729  188 -161 176 -173 -287 218 3 216  tmp=5.17040532e-08
668 3 0.0878729  176 -173 -217  tmp=5.17040532e-08
669 3 0.0878729  176 -173 -218 217  tmp=5.17040532e-08
670 3 0.0878729  188 -161 176 -173 -288 220 287 218  tmp=5.17040532e-08
671 3 0.0878729  176 -173 -219  tmp=5.17040532e-08
672 3 0.0878729  176 -173 -220 219  tmp=5.17040532e-08
673 3 0.0878729  188 -161 176 -173 -289 222 288 220  tmp=5.17040532e-08
674 3 0.0878729  176 -173 -221  tmp=5.17040532e-08
675 3 0.0878729  176 -173 -222 221  tmp=5.17040532e-08
676 3 0.0878729  188 -161 176 -173 -290 224 289 222  tmp=5.17040532e-08
677 3 0.0878729  176 -173 -223  tmp=5.17040532e-08
678 3 0.0878729  176 -173 -224 223  tmp=5.17040532e-08
679 3 0.0878729  188 -161 176 -173 -291 226 290 224  tmp=5.17040532e-08
680 3 0.0878729  176 -173 -225  tmp=5.17040532e-08
681 3 0.0878729  176 -173 -226 225  tmp=5.17040532e-08
682 3 0.0878729  188 -161 176 -173 -292 228 291 226  tmp=5.17040532e-08
683 3 0.0878729  176 -173 -227  tmp=5.17040532e-08
684 3 0.0878729  176 -173 -228 227  tmp=5.17040532e-08
685 3 0.0878729  188 -161 176 -173 -293 230 292 228  tmp=5.17040532e-08
686 3 0.0878729  176 -173 -229  tmp=5.17040532e-08
687 3 0.0878729  176 -173 -230 229  tmp=5.17040532e-08
688 3 0.0878729  188 -161 176 -173 -294 232 293 230  tmp=5.17040532e-08
689 3 0.0878729  176 -173 -231  tmp=5.17040532e-08
690 3 0.0878729  176 -173 -232 231  tmp=5.17040532e-08
691 3 0.0878729  188 -161 176 -173 -158 234 294 232  tmp=5.17040532e-08
692 3 0.0878729  176 -173 -233  tmp=5.17040532e-08
693 3 0.0878729  176 -173 -234 233  tmp=5.17040532e-08
694 3 0.0878729  188 -161 176 -173 -295 236 158 234  tmp=5.17040532e-08
695 3 0.0878729  176 -173 -235  tmp=5.17040532e-08
696 3 0.0878729  176 -173 -236 235  tmp=5.17040532e-08
697 3 0.0878729  188 -161 176 -173 -296 238 295 236  tmp=5.17040532e-08
698 3 0.0878729  176 -173 -237  tmp=5.17040532e-08
699 3 0.0878729  176 -173 -238 237  tmp=5.17040532e-08
700 3 0.0878729  188 -161 176 -173 -297 240 296 238  tmp=5.17040532e-08
701 3 0.0878729  176 -173 -239  tmp=5.17040532e-08
702 3 0.0878729  176 -173 -240 239  tmp=5.17040532e-08
703 3 0.0878729  188 -161 176 -173 -298 242 297 240  tmp=5.17040532e-08
704 3 0.0878729  176 -173 -241  tmp=5.17040532e-08
705 3 0.0878729  176 -173 -242 241  tmp=5.17040532e-08
706 3 0.0878729  188 -161 176 -173 -299 244 298 242  tmp=5.17040532e-08
707 3 0.0878729  176 -173 -243  tmp=5.17040532e-08
708 3 0.0878729  176 -173 -244 243  tmp=5.17040532e-08
709 3 0.0878729  188 -161 176 -173 -300 246 299 244  tmp=5.17040532e-08
710 3 0.0878729  176 -173 -245  tmp=5.17040532e-08
711 3 0.0878729  176 -173 -246 245  tmp=5.17040532e-08
712 3 0.0878729  188 -161 176 -173 -301 248 300 246  tmp=5.17040532e-08
713 3 0.0878729  176 -173 -247  tmp=5.17040532e-08
714 3 0.0878729  176 -173 -248 247  tmp=5.17040532e-08
715 3 0.0878729  188 -161 176 -173 -302 250 301 248  tmp=5.17040532e-08
716 3 0.0878729  176 -173 -249  tmp=5.17040532e-08
717 3 0.0878729  176 -173 -250 249  tmp=5.17040532e-08
718 3 0.0878729  188 -161 176 -173 3 252 302 250  tmp=5.17040532e-08
719 3 0.0878729  176 -173 -251  tmp=5.17040532e-08
720 3 0.0878729  176 -173 -252 251  tmp=5.17040532e-08
721 3 0.0878729  188 -161 176 -173 287 254 -3 252  tmp=5.17040532e-08
722 3 0.0878729  176 -173 -253  tmp=5.17040532e-08
723 3 0.0878729  176 -173 -254 253  tmp=5.17040532e-08
724 3 0.0878729  188 -161 176 -173 288 256 -287 254  tmp=5.17040532e-08
725 3 0.0878729  176 -173 -255  tmp=5.17040532e-08
726 3 0.0878729  176 -173 -256 255  tmp=5.17040532e-08
727 3 0.0878729  188 -161 176 -173 289 258 -288 256  tmp=5.17040532e-08
728 3 0.0878729  176 -173 -257  tmp=5.17040532e-08
729 3 0.0878729  176 -173 -258 257  tmp=5.17040532e-08
730 3 0.0878729  188 -161 176 -173 290 260 -289 258  tmp=5.17040532e-08
731 3 0.0878729  176 -173 -259  tmp=5.17040532e-08
732 3 0.0878729  176 -173 -260 259  tmp=5.17040532e-08
733 3 0.0878729  188 -161 176 -173 291 262 -290 260  tmp=5.17040532e-08
734 3 0.0878729  176 -173 -261  tmp=5.17040532e-08
735 3 0.0878729  176 -173 -262 261  tmp=5.17040532e-08
736 3 0.0878729  188 -161 176 -173 292 264 -291 262  tmp=5.17040532e-08
737 3 0.0878729  176 -173 -263  tmp=5.17040532e-08
738 3 0.0878729  176 -173 -264 263  tmp=5.17040532e-08
739 3 0.0878729  188 -161 176 -173 293 266 -292 264  tmp=5.17040532e-08
740 3 0.0878729  176 -173 -265  tmp=5.17040532e-08
741 3 0.0878729  176 -173 -266 265  tmp=5.17040532e-08
742 3 0.0878729  188 -161 176 -173 294 268 -293 266  tmp=5.17040532e-08
743 3 0.0878729  176 -173 -267  tmp=5.17040532e-08
744 3 0.0878729  176 -173 -268 267  tmp=5.17040532e-08
745 3 0.0878729  188 -161 176 -173 158 270 -294 268  tmp=5.17040532e-08
746 3 0.0878729  176 -173 -269  tmp=5.17040532e-08
747 3 0.0878729  176 -173 -270 269  tmp=5.17040532e-08
748 3 0.0878729  188 -161 176 -173 295 272 -158 270  tmp=5.17040532e-08
749 3 0.0878729  176 -173 -271  tmp=5.17040532e-08
750 3 0.0878729  176 -173 -272 271  tmp=5.17040532e-08
751 3 0.0878729  188 -161 176 -173 296 274 -295 272  tmp=5.17040532e-08
752 3 0.0878729  176 -173 -273  tmp=5.17040532e-08
753 3 0.0878729  176 -173 -274 273  tmp=5.17040532e-08
754 3 0.0878729  188 -161 176 -173 297 276 -296 274  tmp=5.17040532e-08
755 3 0.0878729  176 -173 -275  tmp=5.17040532e-08
756 3 0.0878729  176 -173 -276 275  tmp=5.17040532e-08
757 3 0.0878729  188 -161 176 -173 298 278 -297 276  tmp=5.17040532e-08
758 3 0.0878729  176 -173 -277  tmp=5.17040532e-08
759 3 0.0878729  176 -173 -278 277  tmp=5.17040532e-08
760 3 0.0878729  188 -161 176 -173 299 280 -298 278  tmp=5.17040532e-08
761 3 0.0878729  176 -173 -279  tmp=5.17040532e-08
762 3 0.0878729  176 -173 -280 279  tmp=5.17040532e-08
763 3 0.0878729  188 -161 176 -173 300 282 -299 280  tmp=5.17040532e-08
764 3 0.0878729  176 -173 -281  tmp=5.17040532e-08
765 3 0.0878729  176 -173 -282 281  tmp=5.17040532e-08
766 3 0.0878729  188 -161 176 -173 301 284 -300 282  tmp=5.17040532e-08
767 3 0.0878729  176 -173 -283  tmp=5.17040532e-08
768 3 0.0878729  176 -173 -284 283  tmp=5.17040532e-08
769 3 0.0878729  188 -161 176 -173 302 286 -301 284  tmp=5.17040532e-08
770 3 0.0878729  176 -173 -285  tmp=5.17040532e-08
771 3 0.0878729  176 -173 -286 285  tmp=5.17040532e-08
772 3 0.0878729  188 -161 176 -173 -3 216 -302 286  tmp=5.17040532e-08
773 0  199 -176 168 -161
774 3 0.0878729  188 -199 -176 168  tmp=5.17040532e-08
775 3 0.0878729  -188 -159 168  tmp=5.17040532e-08
776 3 0.0878729  164 169 -172 -184  tmp=5.17040532e-08
777 3 0.0878729  -168 171 -184  tmp=5.17040532e-08
778 0  197 172 -193 -184
779 0  180 -193 184 -194
780 0  -171 192 164 -184
781 3 0.0878729  -171 195 -164  tmp=5.17040532e-08
782 0  -192 195 -197 164
783 0  -179 192 184 -194
784 3 0.0878729  191 -196 166 -164 161  tmp=5.17040532e-08
785 3 0.0878729  190 -196 200 -161  tmp=5.17040532e-08
786 0  188 -200 177 -161
787 0  -187 -200 160
788 0  201 -198 196 -164
789 0  -198 172 164 -197
790 3 0.0878729  -203 202 190 -199
791 0  -202 196 190 -201
792 0  -204 202 -201 199
793 0  -204 203 190 -199
794 0  -195 205 206 -197
795 0  -195 207 214 -206
796 0  -195 207 208 -212
797 0  -195 209 -208
798 3 0.0878729  -207 205 -206  tmp=5.17040532e-08
799 3 0.0878729  -211 207 -208  tmp=5.17040532e-08
800 3 0.0878729  -209 211 210 -208  tmp=5.17040532e-08
801 0  -209 211 -210
802 3 0.0878729  -195 213 -214 212  tmp=5.17040532e-08
803 0  207 -213 -214 212
804 3 0.0878729  -185 200 -186  tmp=5.17040532e-08
805 3 0.0878729  -185 200 186 -187  tmp=5.17040532e-08
806 3 0.0878729  187 -188 159 -303  tmp=5.17040532e-08
807 3 0.0878729  159 187 -188 -185 304  tmp=5.17040532e-08
808 0  187 -188 -304 303 -306 305
809 3 0.0878729  187 -188 -304 303 -307 306  tmp=5.17040532e-08
810 0  187 -188 -304 303 -308 307
811 3 0.0878729  187 -188 -304 303 -309 308  tmp=5.17040532e-08
812 0  187 -188 -304 303 -310 309
813 3 0.0878729  187 -188 -304 303 -311 310  tmp=5.17040532e-08
814 0  187 -188 -304 303 -312 311
815 3 0.0878729  187 -188 -304 303 -313 312  tmp=5.17040532e-08
816 0  187 -188 -304 303 -314 313
817 3 0.0878729  187 -188 -304 303 -315 314  tmp=5.17040532e-08
818 0  187 -188 -304 303 -316 315
819 3 0.0878729  187 -188 -304 303 -317 316  tmp=5.17040532e-08
820 0  187 -188 -304 303 -318 317
821 3 0.0878729  187 -188 -304 303 -319 318  tmp=5.17040532e-08
822 0  187 -188 -304 303 -320 319
823 3 0.0878729  187 -188 -304 303 -321 320  tmp=5.17040532e-08
824 0  187 -188 -304 303 -322 321
825 3 0.0878729  187 -188 -304 303 -323 322  tmp=5.17040532e-08
826 0  187 -188 -304 303 -324 323
827 3 0.0878729  187 -188 -304 303 -325 324  tmp=5.17040532e-08
828 0  187 -188 -304 303 -326 325
829 3 0.0878729  187 -188 -304 303 -327 326  tmp=5.17040532e-08
830 0  187 -188 -304 303 -328 327
831 3 0.0878729  187 -188 -304 303 -329 328  tmp=5.17040532e-08
832 0  187 -188 -304 303 -330 329
833 3 0.0878729  187 -188 -304 303 -331 330  tmp=5.17040532e-08
834 0  187 -188 -304 303 -332 331
835 3 0.0878729  187 -188 -304 303 -333 332  tmp=5.17040532e-08
836 0  187 -188 -304 303 -334 333
837 3 0.0878729  187 -188 -304 303 -335 334  tmp=5.17040532e-08
838 0  187 -188 -304 303 -336 335
839 3 0.0878729  187 -188 -304 303 -337 336  tmp=5.17040532e-08
840 0  187 -188 -304 303 -338 337
841 3 0.0878729  187 -188 -304 303 -339 338  tmp=5.17040532e-08
842 0  187 -188 -304 303 -340 339
843 3 0.0878729  187 -188 -304 303 305 340  tmp=5.17040532e-08
844 0  187 -188 -304 303 306 -305
845 3 0.0878729  187 -188 -304 303 307 -306  tmp=5.17040532e-08
846 0  187 -188 -304 303 308 -307
847 3 0.0878729  187 -188 -304 303 309 -308  tmp=5.17040532e-08
848 0  187 -188 -304 303 310 -309
849 3 0.0878729  187 -188 -304 303 311 -310  tmp=5.17040532e-08
850 0  187 -188 -304 303 312 -311
851 3 0.0878729  187 -188 -304 303 313 -312  tmp=5.17040532e-08
852 0  187 -188 -304 303 314 -313
853 3 0.0878729  187 -188 -304 303 315 -314  tmp=5.17040532e-08
854 0  187 -188 -304 303 316 -315
855 3 0.0878729  187 -188 -304 303 317 -316  tmp=5.17040532e-08
856 0  187 -188 -304 303 318 -317
857 3 0.0878729  187 -188 -304 303 319 -318  tmp=5.17040532e-08
858 0  187 -188 -304 303 320 -319
859 3 0.0878729  187 -188 -304 303 321 -320  tmp=5.17040532e-08
860 0  187 -188 -304 303 322 -321
861 3 0.0878729  187 -188 -304 303 323 -322  tmp=5.17040532e-08
862 0  187 -188 -304 303 324 -323
863 3 0.0878729  187 -188 -304 303 325 -324  tmp=5.17040532e-08
864 0  187 -188 -304 303 326 -325
865 3 0.0878729  187 -188 -304 303 327 -326  tmp=5.17040532e-08
866 0  187 -188 -304 303 328 -327
867 3 0.0878729  187 -188 -304 303 329 -328  tmp=5.17040532e-08
868 0  187 -188 -304 303 330 -329
869 3 0.0878729  187 -188 -304 303 331 -330  tmp=5.17040532e-08
870 0  187 -188 -304 303 332 -331
871 3 0.0878729  187 -188 -304 303 333 -332  tmp=5.17040532e-08
872 0  187 -188 -304 303 334 -333
873 3 0.0878729  187 -188 -304 303 335 -334  tmp=5.17040532e-08
874 0  187 -188 -304 303 336 -335
875 3 0.0878729  187 -188 -304 303 337 -336  tmp=5.17040532e-08
876 0  187 -188 -304 303 338 -337
877 3 0.0878729  187 -188 -304 303 339 -338  tmp=5.17040532e-08
878 0  187 -188 -304 303 340 -339
879 3 0.0878729  187 -188 -304 303 -305 -340  tmp=5.17040532e-08
880 3 0.0878729  -185 200 188 -189  tmp=5.17040532e-08
881 3 0.0878729  -185 200 189 -190  tmp=5.17040532e-08
882 0  -185 204 190 -191  tmp=5.17040532e-08
883 0  ( 3 : 436 : 437 : ( 434 -435 ) ) ( -19 : 341 : 412 : 411 : 413 :
        ( 416 -419 ) : ( 415 -418 ) : ( 414 -417 ) ) ( -19 : 373 : 374
        : 375 : 341 : ( 378 -381 ) : ( 377 -380 ) : ( 376 -379 ) ) (
        -412 : -448 : -440 : 441 : 449 ) ( -3 : 432 : 433 : ( 430 -431
        ) ) ( -444 : -440 : -374 : 441 : 445 ) -341 19 -15
884 143 0.041975  383 -382 ( 350 : -347 ) -346 345 -344 -342
        tmp=1.72346844e-09
885 143 0.041975  -3 382 ( 351 : -348 ) -346 345 -343 -342
        tmp=1.72346844e-09
886 143 0.041975  -383 3 ( 352 : -349 ) -346 345 -344 -343
        tmp=1.72346844e-09
887 5 0.0582256  383 -382 ( -345 : 346 : 344 : 342 : ( -350 347 ) ) (
        361 : -358 ) -357 356 -355 -353  tmp=1.72346844e-09
888 5 0.0582256  -3 382 ( -345 : 346 : 343 : 342 : ( -351 348 ) ) ( 362
        : -359 ) -357 356 -354 -353  tmp=1.72346844e-09
889 5 0.0582256  -383 3 ( -345 : 346 : 344 : 343 : ( -352 349 ) ) ( 363
        : -360 ) -357 356 -355 -354  tmp=1.72346844e-09
890 0  383 -382 ( -356 : 357 : 355 : 353 : ( -361 358 ) ) ( 370 : -367
        ) -341 19 -366 -364
891 0  -3 382 ( -356 : 357 : 354 : 353 : ( -362 359 ) ) ( 371 : -368 )
        -341 19 -365 -364
892 0  -383 3 ( -356 : 357 : 355 : 354 : ( -363 360 ) ) ( 372 : -369 )
        -341 19 -366 -365
893 5 0.0582256  383 -382 ( -19 : 341 : 366 : 364 : ( -370 367 ) ) (
        379 : -376 ) -341 19 -375 -373
894 5 0.0582256  -3 382 ( -19 : 341 : 365 : 364 : ( -371 368 ) ) ( 380
        : -377 ) -341 19 -374 -373
895 5 0.0582256  -383 3 ( -19 : 341 : 366 : 365 : ( -372 369 ) ) ( 381
        : -378 ) -341 19 -375 -374
896 143 0.041975  421 -420 ( 390 : -387 ) -346 345 -386 -384
        tmp=1.72346844e-09
897 143 0.041975  3 420 ( 391 : -388 ) -346 345 -385 -384
        tmp=1.72346844e-09
898 143 0.041975  -421 -3 ( 392 : -389 ) -346 345 -386 -385
        tmp=1.72346844e-09
899 5 0.0582256  421 -420 ( -345 : 346 : 386 : 384 : ( -390 387 ) ) (
        399 : -396 ) -357 356 -395 -393  tmp=1.72346844e-09
900 5 0.0582256  3 420 ( -345 : 346 : 385 : 384 : ( -391 388 ) ) ( 400
        : -397 ) -357 356 -394 -393  tmp=1.72346844e-09
901 5 0.0582256  -421 -3 ( -345 : 346 : 386 : 385 : ( -392 389 ) ) (
        401 : -398 ) -357 356 -395 -394  tmp=1.72346844e-09
902 0  421 -420 ( -356 : 357 : 395 : 393 : ( -399 396 ) ) ( 408 : -405
        ) -341 19 -404 -402
903 0  3 420 ( -356 : 357 : 394 : 393 : ( -400 397 ) ) ( 409 : -406 )
        -341 19 -403 -402
904 0  -421 -3 ( -356 : 357 : 395 : 394 : ( -401 398 ) ) ( 410 : -407 )
        -341 19 -404 -403
905 5 0.0582256  421 -420 ( -19 : 341 : 404 : 402 : ( -408 405 ) ) (
        417 : -414 ) -341 19 -413 -411
906 5 0.0582256  3 420 ( -19 : 341 : 403 : 402 : ( -409 406 ) ) ( 418 :
        -415 ) -341 19 -412 -411
907 5 0.0582256  -421 -3 ( -19 : 341 : 404 : 403 : ( -410 407 ) ) ( 419
        : -416 ) -341 19 -413 -412
908 11 0.100283  -341 19 ( 375 : 373 : ( -379 376 ) ) -424 423 3 2
909 11 0.100283  -341 19 ( 413 : 411 : ( -417 414 ) ) -425 -422 3 -2
910 11 0.100283  -341 19 ( 375 : 373 : ( -379 376 ) ) -428 -426 -3 2
911 11 0.100283  -341 19 ( 413 : 411 : ( -417 414 ) ) -429 427 -3 -2
912 5 0.0582256  -341 19 ( 375 : 373 : ( -379 376 ) ) ( -423 : 424 )
        -432 431 3 2
913 5 0.0582256  -341 19 ( 413 : 411 : ( -417 414 ) ) ( 422 : 425 )
        -433 -430 3 -2
914 5 0.0582256  -341 19 ( 375 : 373 : ( -379 376 ) ) ( 426 : 428 )
        -436 -434 -3 2
915 5 0.0582256  -341 19 ( 413 : 411 : ( -417 414 ) ) ( -427 : 429 )
        -437 435 -3 -2
916 11 0.100283  374 -341 19 -15 -443 442 -439 438
917 5 0.0582256  -341 19 -15 442 -438 440
918 5 0.0582256  -341 19 -15 -443 -441 439
919 5 0.0582256  374 -341 19 -15 444 -442 440
920 5 0.0582256  374 -341 19 -15 -445 443 -441
921 11 0.100283  412 -341 19 -15 -447 446 438 -439
922 5 0.0582256  -341 19 -15 446 439 -441
923 5 0.0582256  -341 19 -15 -447 440 -438
924 5 0.0582256  412 -341 19 -15 448 -446 -441
925 5 0.0582256  412 -341 19 -15 -449 447 440
926 0  ( 3 : 15 : ( ( 200 : 481 ) ( -480 : 482 ) ) ) ( 3 : 436 : 437 :
        ( ( 434 : ( 465 -466 ) ) ( -435 : ( 467 -466 ) ) ) ) ( -42 :
        450 : 412 : 413 : 411 : ( 415 -418 ) : ( 416 -419 ) : ( 414
        -417 ) ) ( -42 : 375 : 374 : 373 : 450 : ( 377 -380 ) : ( 378
        -381 ) : ( 376 -379 ) ) ( -412 : -448 : -440 : 441 : 449 ) ( -3
        : 432 : 433 : ( ( 430 : ( 468 -469 ) ) ( -431 : ( 470 -469 ) )
        ) ) ( -473 : -440 : -374 : 441 : 474 ) -450 42 -15 ( -3 : 15 :
        ( ( -480 : 482 ) ( 200 : 481 ) ) )
927 143 0.041975  -382 383 ( 350 : -347 ) -452 451 -342 -344
        tmp=1.72346844e-09
928 143 0.041975  3 -383 ( 352 : -349 ) -452 451 -343 -344
        tmp=1.72346844e-09
929 143 0.041975  382 -3 ( 351 : -348 ) -452 451 -342 -343
        tmp=1.72346844e-09
930 5 0.0582256  -382 383 ( -451 : 452 : 342 : 344 : ( -350 347 ) ) (
        361 : -358 ) -454 453 -353 -355  tmp=1.72346844e-09
931 5 0.0582256  3 -383 ( -451 : 452 : 343 : 344 : ( -352 349 ) ) ( 363
        : -360 ) -454 453 -354 -355  tmp=1.72346844e-09
932 5 0.0582256  382 -3 ( -451 : 452 : 342 : 343 : ( -351 348 ) ) ( 362
        : -359 ) -454 453 -353 -354  tmp=1.72346844e-09
933 0  -382 383 ( -453 : 454 : 353 : 355 : ( -361 358 ) ) ( 370 : -367
        ) -456 455 -364 -366
934 0  3 -383 ( -453 : 454 : 354 : 355 : ( -363 360 ) ) ( 372 : -369 )
        -456 455 -365 -366
935 0  382 -3 ( -453 : 454 : 353 : 354 : ( -362 359 ) ) ( 371 : -368 )
        -456 455 -364 -365
936 5 0.0582256  -382 383 ( -455 : 456 : 364 : 366 : ( -370 367 ) ) (
        379 : -376 ) -450 42 -373 -375
937 5 0.0582256  3 -383 ( -455 : 456 : 365 : 366 : ( -372 369 ) ) ( 381
        : -378 ) -450 42 -374 -375
938 5 0.0582256  382 -3 ( -455 : 456 : 364 : 365 : ( -371 368 ) ) ( 380
        : -377 ) -450 42 -373 -374
939 143 0.041975  -420 421 ( 390 : -387 ) -454 451 -384 -386
        tmp=1.72346844e-09
940 143 0.041975  -3 -421 ( 392 : -389 ) -454 451 -385 -386
        tmp=1.72346844e-09
941 143 0.041975  420 3 ( 391 : -388 ) -454 451 -384 -385
        tmp=1.72346844e-09
942 5 0.0582256  -420 421 ( -451 : 454 : 384 : 386 : ( -390 387 ) ) (
        399 : -396 ) -457 453 -393 -395  tmp=1.72346844e-09
943 5 0.0582256  -3 -421 ( -451 : 454 : 385 : 386 : ( -392 389 ) ) (
        401 : -398 ) -457 453 -394 -395  tmp=1.72346844e-09
944 5 0.0582256  420 3 ( -451 : 454 : 384 : 385 : ( -391 388 ) ) ( 400
        : -397 ) -457 453 -393 -394  tmp=1.72346844e-09
945 0  -420 421 ( -453 : 457 : 393 : 395 : ( -399 396 ) ) ( 408 : -405
        ) -458 455 -402 -404
946 0  -3 -421 ( -453 : 457 : 394 : 395 : ( -401 398 ) ) ( 410 : -407 )
        -458 455 -403 -404
947 0  420 3 ( -453 : 457 : 393 : 394 : ( -400 397 ) ) ( 409 : -406 )
        -458 455 -402 -403
948 5 0.0582256  -420 421 ( -455 : 458 : 402 : 404 : ( -408 405 ) ) (
        417 : -414 ) -450 42 -411 -413
949 5 0.0582256  -3 -421 ( -455 : 458 : 403 : 404 : ( -410 407 ) ) (
        419 : -416 ) -450 42 -412 -413
950 5 0.0582256  420 3 ( -455 : 458 : 402 : 403 : ( -409 406 ) ) ( 418
        : -415 ) -450 42 -411 -412
951 11 0.100283  42 ( 373 : 375 : ( -379 376 ) ) ( -459 : 460 ) -458
        -428 -426 -3 2
952 11 0.100283  42 ( 411 : 413 : ( -417 414 ) ) ( -461 : 460 ) -458
        -429 427 -3 -2
953 5 0.0582256  -450 ( 373 : 375 : ( -379 376 ) ) ( -459 : 460 ) 458
        -428 -426 -3 2
954 5 0.0582256  -450 ( 411 : 413 : ( -417 414 ) ) ( -461 : 460 ) 458
        -429 427 -3 -2
955 11 0.100283  42 ( 373 : 375 : ( -379 376 ) ) ( -464 : 463 ) -458
        -424 423 3 2
956 11 0.100283  42 ( 411 : 413 : ( -417 414 ) ) ( -462 : 463 ) -458
        -425 -422 3 -2
957 5 0.0582256  -450 ( 373 : 375 : ( -379 376 ) ) ( -464 : 463 ) 458
        -424 423 3 2
958 5 0.0582256  -450 ( 411 : 413 : ( -417 414 ) ) ( -462 : 463 ) 458
        -425 -422 3 -2
959 5 0.0582256  -450 42 ( 373 : 375 : ( -379 376 ) ) ( 426 : 428 : (
        -460 459 ) ) ( -465 : 466 ) -436 -434 -3 2
960 5 0.0582256  -450 42 ( 411 : 413 : ( -417 414 ) ) ( -427 : 429 : (
        -460 461 ) ) ( -467 : 466 ) -437 435 -3 -2
961 5 0.0582256  -450 42 ( 373 : 375 : ( -379 376 ) ) ( -423 : 424 : (
        -463 464 ) ) ( -470 : 469 ) -432 431 3 2
962 5 0.0582256  -450 42 ( 411 : 413 : ( -417 414 ) ) ( 422 : 425 : (
        -463 462 ) ) ( -468 : 469 ) -433 -430 3 -2
963 11 0.100283  374 -450 42 -15 -472 471 438 -439
964 5 0.0582256  -450 42 -15 471 439 -441
965 5 0.0582256  -450 42 -15 -472 440 -438
966 5 0.0582256  374 -450 42 -15 473 -471 -441
967 5 0.0582256  374 -450 42 -15 -474 472 440
968 11 0.100283  412 -450 42 -15 446 -447 -439 438
969 5 0.0582256  -450 42 -15 -447 -438 440
970 5 0.0582256  -450 42 -15 446 -441 439
971 5 0.0582256  412 -450 42 -15 -449 447 440
972 5 0.0582256  412 -450 42 -15 448 -446 -441
973 11 0.100283  42 ( 374 : 375 : ( -381 378 ) ) ( 411 : 412 : ( -418
        415 ) ) 3 ( 433 : 432 : ( ( 430 : ( 468 -469 ) ) ( -431 : ( 470
        -469 ) ) ) ) ( -440 : 474 ) ( -440 : 449 ) -477 -475
974 5 0.0582256  ( 374 : 375 : ( -381 378 ) ) ( 411 : 412 : ( -418 415
        ) ) 3 ( 433 : 432 : ( ( 430 : ( 468 -469 ) ) ( -431 : ( 470
        -469 ) ) ) ) ( -440 : 474 ) ( -440 : 449 ) -477 475 -200
975 11 0.100283  -20 477 ( 374 : 375 : ( -381 378 ) ) ( 411 : 412 : (
        -418 415 ) ) 3 ( 433 : 432 : ( ( 430 : ( 468 -469 ) ) ( -431 :
        ( 470 -469 ) ) ) ) ( -440 : 474 ) ( -440 : 449 ) ( -440 : 474 )
        ( -440 : 449 ) 42 -483 -478
976 5 0.0582256  42 -20 477 ( 374 : 375 : ( -381 378 ) ) ( 411 : 412 :
        ( -418 415 ) ) 3 ( 433 : 432 : ( ( 430 : ( 468 -469 ) ) ( -431
        : ( 470 -469 ) ) ) ) ( -440 : 474 ) ( -440 : 449 ) ( -440 : 474
        ) ( -440 : 449 ) ( 478 : 483 ) -485 -481
977 11 0.100283  -20 477 ( 374 : 375 : ( -381 378 ) ) ( 411 : 412 : (
        -418 415 ) ) 3 ( 433 : 432 : ( ( 430 : ( 468 -469 ) ) ( -431 :
        ( 470 -469 ) ) ) ) ( -440 : 474 ) ( -440 : 449 ) -450 -20 477 (
        374 : 375 : ( -381 378 ) ) ( 411 : 412 : ( -418 415 ) ) 3 ( 433
        : 432 : ( ( 430 : ( 468 -469 ) ) ( -431 : ( 470 -469 ) ) ) ) (
        -440 : 474 ) ( -440 : 449 ) ( -440 : 474 ) ( -440 : 449 ) 484
        -479
978 5 0.0582256  -20 477 ( 374 : 375 : ( -381 378 ) ) ( 411 : 412 : (
        -418 415 ) ) 3 ( 433 : 432 : ( ( 430 : ( 468 -469 ) ) ( -431 :
        ( 470 -469 ) ) ) ) ( -440 : 474 ) ( -440 : 449 ) -450 ( -440 :
        474 ) ( -440 : 449 ) ( 479 : -484 ) 486 -482
979 5 0.0582256  -17 20 3 ( -440 : 474 ) ( -440 : 449 ) 42 -483 -478
980 5 0.0582256  42 -17 20 3 ( -440 : 474 ) ( -440 : 449 ) ( 478 : 483
        ) -485 -481
981 5 0.0582256  -17 20 3 -450 -17 20 3 ( -440 : 474 ) ( -440 : 449 )
        484 -479
982 5 0.0582256  -17 20 3 -450 ( -440 : 474 ) ( -440 : 449 ) ( 479 :
        -484 ) 486 -482
983 0  -18 17 3 ( -440 : 474 ) ( -440 : 449 ) 42 -483 -478
984 0  42 -18 17 3 ( -440 : 474 ) ( -440 : 449 ) ( 478 : 483 ) -485
        -481
985 0  -18 17 3 -450 -18 17 3 ( -440 : 474 ) ( -440 : 449 ) 484 -479
986 0  -18 17 3 -450 ( -440 : 474 ) ( -440 : 449 ) ( 479 : -484 ) 486
        -482
987 3 0.0878729  -15 18 3 ( -440 : 474 ) ( -440 : 449 ) 42 -483 -478
988 3 0.0878729  42 -15 18 3 ( -440 : 474 ) ( -440 : 449 ) ( 478 : 483
        ) -485 -481
989 3 0.0878729  -15 18 3 -450 -15 18 3 ( -440 : 474 ) ( -440 : 449 )
        484 -479
990 3 0.0878729  -15 18 3 -450 ( -440 : 474 ) ( -440 : 449 ) ( 479 :
        -484 ) 486 -482
991 11 0.100283  -450 ( 374 : 375 : ( -381 378 ) ) ( 411 : 412 : ( -418
        415 ) ) 3 ( 433 : 432 : ( ( 430 : ( 468 -469 ) ) ( -431 : ( 470
        -469 ) ) ) ) ( -440 : 474 ) ( -440 : 449 ) -477 476
992 5 0.0582256  ( 374 : 375 : ( -381 378 ) ) ( 411 : 412 : ( -418 415
        ) ) 3 ( 433 : 432 : ( ( 430 : ( 468 -469 ) ) ( -431 : ( 470
        -469 ) ) ) ) ( -440 : 474 ) ( -440 : 449 ) -477 -476 480
993 11 0.100283  42 ( 373 : 374 : ( -380 377 ) ) ( 412 : 413 : ( -419
        416 ) ) -3 ( 437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435 : (
        467 -466 ) ) ) ) ( -473 : 441 ) ( -448 : 441 ) -477 -475
994 5 0.0582256  ( 373 : 374 : ( -380 377 ) ) ( 412 : 413 : ( -419 416
        ) ) -3 ( 437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435 : ( 467
        -466 ) ) ) ) ( -473 : 441 ) ( -448 : 441 ) -477 475 -200
995 11 0.100283  -20 477 ( 373 : 374 : ( -380 377 ) ) ( 412 : 413 : (
        -419 416 ) ) -3 ( 437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435 :
        ( 467 -466 ) ) ) ) ( -473 : 441 ) ( -448 : 441 ) ( -473 : 441 )
        ( -448 : 441 ) 42 -483 -478
996 5 0.0582256  42 -20 477 ( 373 : 374 : ( -380 377 ) ) ( 412 : 413 :
        ( -419 416 ) ) -3 ( 437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435
        : ( 467 -466 ) ) ) ) ( -473 : 441 ) ( -448 : 441 ) ( -473 : 441
        ) ( -448 : 441 ) ( 478 : 483 ) -485 -481
997 11 0.100283  -20 477 ( 373 : 374 : ( -380 377 ) ) ( 412 : 413 : (
        -419 416 ) ) -3 ( 437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435 :
        ( 467 -466 ) ) ) ) ( -473 : 441 ) ( -448 : 441 ) -450 -20 477 (
        373 : 374 : ( -380 377 ) ) ( 412 : 413 : ( -419 416 ) ) -3 (
        437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435 : ( 467 -466 ) ) )
        ) ( -473 : 441 ) ( -448 : 441 ) ( -473 : 441 ) ( -448 : 441 )
        484 -479
998 5 0.0582256  -20 477 ( 373 : 374 : ( -380 377 ) ) ( 412 : 413 : (
        -419 416 ) ) -3 ( 437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435 :
        ( 467 -466 ) ) ) ) ( -473 : 441 ) ( -448 : 441 ) -450 ( -473 :
        441 ) ( -448 : 441 ) ( 479 : -484 ) 486 -482
999 5 0.0582256  -17 20 -3 ( -473 : 441 ) ( -448 : 441 ) 42 -483 -478
1000 5 0.0582256  42 -17 20 -3 ( -473 : 441 ) ( -448 : 441 ) ( 478 :
        483 ) -485 -481
1001 5 0.0582256  -17 20 -3 -450 -17 20 -3 ( -473 : 441 ) ( -448 : 441
        ) 484 -479
1002 5 0.0582256  -17 20 -3 -450 ( -473 : 441 ) ( -448 : 441 ) ( 479 :
        -484 ) 486 -482
1003 0  -18 17 -3 ( -473 : 441 ) ( -448 : 441 ) 42 -483 -478
1004 0  42 -18 17 -3 ( -473 : 441 ) ( -448 : 441 ) ( 478 : 483 ) -485
        -481
1005 0  -18 17 -3 -450 -18 17 -3 ( -473 : 441 ) ( -448 : 441 ) 484 -479
1006 0  -18 17 -3 -450 ( -473 : 441 ) ( -448 : 441 ) ( 479 : -484 ) 486
        -482
1007 3 0.0878729  -15 18 -3 ( -473 : 441 ) ( -448 : 441 ) 42 -483 -478
1008 3 0.0878729  42 -15 18 -3 ( -473 : 441 ) ( -448 : 441 ) ( 478 :
        483 ) -485 -481
1009 3 0.0878729  -15 18 -3 -450 -15 18 -3 ( -473 : 441 ) ( -448 : 441
        ) 484 -479
1010 3 0.0878729  -15 18 -3 -450 ( -473 : 441 ) ( -448 : 441 ) ( 479 :
        -484 ) 486 -482
1011 11 0.100283  -450 ( 373 : 374 : ( -380 377 ) ) ( 412 : 413 : (
        -419 416 ) ) -3 ( 437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435 :
        ( 467 -466 ) ) ) ) ( -473 : 441 ) ( -448 : 441 ) -477 476
1012 5 0.0582256  ( 373 : 374 : ( -380 377 ) ) ( 412 : 413 : ( -419 416
        ) ) -3 ( 437 : 436 : ( ( 434 : ( 465 -466 ) ) ( -435 : ( 467
        -466 ) ) ) ) ( -473 : 441 ) ( -448 : 441 ) -477 -476 480
1013 54 0.0833854  ( -3 : -78 : -703 : -496 : 497 : 493 : 704 : ( 496
        -694 -497 ) ) ( -3 : -78 : -683 : -496 : 497 : 493 : 684 : (
        496 -674 -497 ) ) ( -3 : -78 : -663 : -496 : 497 : 493 : 664 :
        ( 496 -654 -497 ) ) ( -3 : -78 : -643 : -496 : 497 : 493 : 644
        : ( 496 -634 -497 ) ) ( -3 : -78 : -623 : -496 : 497 : 493 :
        624 : ( 496 -614 -497 ) ) ( -3 : -78 : -603 : -496 : 497 : 493
        : 604 : ( 496 -594 -497 ) ) ( -3 : -78 : -583 : -496 : 497 :
        493 : 584 : ( 496 -574 -497 ) ) ( -3 : -78 : -563 : -496 : 497
        : 493 : 564 : ( 496 -554 -497 ) ) ( -3 : -78 : -543 : -496 :
        497 : 493 : 544 : ( 496 -534 -497 ) ) ( -3 : -78 : -523 : -496
        : 497 : 493 : 524 : ( 496 -514 -497 ) ) ( -3 : -78 : -494 :
        -496 : 493 : 497 : 495 ) ( -171 : 172 : 183 ) 3 78 -493 487
        -488 489 -490 ( -171 : 172 : 183 ) ( -3 : -78 : -513 : -496 :
        497 : 493 : 514 : ( -497 -495 496 ) ) ( -3 : -78 : -533 : -496
        : 497 : 493 : 534 : ( -497 -524 496 ) ) ( -3 : -78 : -553 :
        -496 : 497 : 493 : 554 : ( -497 -544 496 ) ) ( -3 : -78 : -573
        : -496 : 497 : 493 : 574 : ( -497 -564 496 ) ) ( -3 : -78 :
        -593 : -496 : 497 : 493 : 594 : ( -497 -584 496 ) ) ( -3 : -78
        : -613 : -496 : 497 : 493 : 614 : ( -497 -604 496 ) ) ( -3 :
        -78 : -633 : -496 : 497 : 493 : 634 : ( -497 -624 496 ) ) ( -3
        : -78 : -653 : -496 : 497 : 493 : 654 : ( -497 -644 496 ) ) (
        -3 : -78 : -673 : -496 : 497 : 493 : 674 : ( -497 -664 496 ) )
        ( -3 : -78 : -693 : -496 : 497 : 493 : 694 : ( -497 -684 496 )
        )
1014 54 0.0833854  ( -3 : -493 : -707 : -503 : 81 : 504 : 708 ) ( -3 :
        -493 : -687 : -503 : 81 : 504 : 688 ) ( -3 : -493 : -667 : -503
        : 81 : 504 : 668 ) ( -3 : -493 : -647 : -503 : 81 : 504 : 648 )
        ( -3 : -493 : -627 : -503 : 81 : 504 : 628 ) ( -3 : -493 : -607
        : -503 : 81 : 504 : 608 ) ( -3 : -493 : -587 : -503 : 81 : 504
        : 588 ) ( -3 : -493 : -567 : -503 : 81 : 504 : 568 ) ( -3 :
        -493 : -547 : -503 : 81 : 504 : 548 ) ( -3 : -493 : -527 : -503
        : 81 : 504 : 528 ) ( -3 : -493 : -501 : -503 : 81 : 504 : 502 )
        ( -171 : 172 : 183 ) 3 493 -81 487 -488 491 -492 ( -171 : 172 :
        183 ) ( -3 : -493 : -517 : -503 : 81 : 504 : 518 ) ( -3 : -493
        : -537 : -503 : 81 : 504 : 538 ) ( -3 : -493 : -557 : -503 : 81
        : 504 : 558 ) ( -3 : -493 : -577 : -503 : 81 : 504 : 578 ) ( -3
        : -493 : -597 : -503 : 81 : 504 : 598 ) ( -3 : -493 : -617 :
        -503 : 81 : 504 : 618 ) ( -3 : -493 : -637 : -503 : 81 : 504 :
        638 ) ( -3 : -493 : -657 : -503 : 81 : 504 : 658 ) ( -3 : -493
        : -677 : -503 : 81 : 504 : 678 ) ( -3 : -493 : -697 : -503 : 81
        : 504 : 698 )
1015 54 0.0833854  ( -171 : 172 : 183 ) -490 489 488 -487 -493 78 -3 (
        -171 : 172 : 183 )
1016 54 0.0833854  ( -171 : 172 : 183 ) -492 491 488 -487 -81 493 -3 (
        -171 : 172 : 183 )
1017 0  ( -171 : 172 : 183 ) 3 78 494 -495 496 -497 -493 ( -498 : 499 :
        -177 : 500 )
1018 3 0.0878729  ( -171 : 172 : 183 ) 3 78 498 -499 177 -500 -493 (
        -509 : 510 : -511 : 512 )
1019 0  ( -171 : 172 : 183 ) 3 493 501 -502 503 -504 -81 ( -505 : 506 :
        -507 : 508 )
1020 3 0.0878729  ( -171 : 172 : 183 ) 3 493 505 -506 507 -508 -81 (
        -509 : 510 : -511 : 512 )
1021 0  -512 511 -510 509 -81 78 3
1022 0  ( -171 : 172 : 183 ) 3 78 513 -514 496 -497 -493 ( 495 : -496 :
        497 ) ( -515 : 516 : -177 : 500 )
1023 3 0.0878729  ( -171 : 172 : 183 ) 3 78 515 -516 177 -500 -493 (
        495 : -496 : 497 ) ( -521 : 522 : -511 : 512 )
1024 0  ( -171 : 172 : 183 ) 3 493 517 -518 503 -504 -81 ( -519 : 520 :
        -507 : 508 )
1025 3 0.0878729  ( -171 : 172 : 183 ) 3 493 519 -520 507 -508 -81 (
        -521 : 522 : -511 : 512 )
1026 0  -512 511 -522 521 -81 78 3
1027 0  ( -171 : 172 : 183 ) 3 78 523 -524 496 -497 -493 ( 514 : -496 :
        497 ) ( -525 : 526 : -177 : 500 )
1028 3 0.0878729  ( -171 : 172 : 183 ) 3 78 525 -526 177 -500 -493 (
        514 : -496 : 497 ) ( -531 : 532 : -511 : 512 )
1029 0  ( -171 : 172 : 183 ) 3 493 527 -528 503 -504 -81 ( -529 : 530 :
        -507 : 508 )
1030 3 0.0878729  ( -171 : 172 : 183 ) 3 493 529 -530 507 -508 -81 (
        -531 : 532 : -511 : 512 )
1031 0  -512 511 -532 531 -81 78 3
1032 0  ( -171 : 172 : 183 ) 3 78 533 -534 496 -497 -493 ( 524 : -496 :
        497 ) ( -535 : 536 : -177 : 500 )
1033 3 0.0878729  ( -171 : 172 : 183 ) 3 78 535 -536 177 -500 -493 (
        524 : -496 : 497 ) ( -541 : 542 : -511 : 512 )
1034 0  ( -171 : 172 : 183 ) 3 493 537 -538 503 -504 -81 ( -539 : 540 :
        -507 : 508 )
1035 3 0.0878729  ( -171 : 172 : 183 ) 3 493 539 -540 507 -508 -81 (
        -541 : 542 : -511 : 512 )
1036 0  -512 511 -542 541 -81 78 3
1037 0  ( -171 : 172 : 183 ) 3 78 543 -544 496 -497 -493 ( 534 : -496 :
        497 ) ( -545 : 546 : -177 : 500 )
1038 3 0.0878729  ( -171 : 172 : 183 ) 3 78 545 -546 177 -500 -493 (
        534 : -496 : 497 ) ( -551 : 552 : -511 : 512 )
1039 0  ( -171 : 172 : 183 ) 3 493 547 -548 503 -504 -81 ( -549 : 550 :
        -507 : 508 )
1040 3 0.0878729  ( -171 : 172 : 183 ) 3 493 549 -550 507 -508 -81 (
        -551 : 552 : -511 : 512 )
1041 0  -512 511 -552 551 -81 78 3
1042 0  ( -171 : 172 : 183 ) 3 78 553 -554 496 -497 -493 ( 544 : -496 :
        497 ) ( -555 : 556 : -177 : 500 )
1043 3 0.0878729  ( -171 : 172 : 183 ) 3 78 555 -556 177 -500 -493 (
        544 : -496 : 497 ) ( -561 : 562 : -511 : 512 )
1044 0  ( -171 : 172 : 183 ) 3 493 557 -558 503 -504 -81 ( -559 : 560 :
        -507 : 508 )
1045 3 0.0878729  ( -171 : 172 : 183 ) 3 493 559 -560 507 -508 -81 (
        -561 : 562 : -511 : 512 )
1046 0  -512 511 -562 561 -81 78 3
1047 0  ( -171 : 172 : 183 ) 3 78 563 -564 496 -497 -493 ( 554 : -496 :
        497 ) ( -565 : 566 : -177 : 500 )
1048 3 0.0878729  ( -171 : 172 : 183 ) 3 78 565 -566 177 -500 -493 (
        554 : -496 : 497 ) ( -571 : 572 : -511 : 512 )
1049 0  ( -171 : 172 : 183 ) 3 493 567 -568 503 -504 -81 ( -569 : 570 :
        -507 : 508 )
1050 3 0.0878729  ( -171 : 172 : 183 ) 3 493 569 -570 507 -508 -81 (
        -571 : 572 : -511 : 512 )
1051 0  -512 511 -572 571 -81 78 3
1052 0  ( -171 : 172 : 183 ) 3 78 573 -574 496 -497 -493 ( 564 : -496 :
        497 ) ( -575 : 576 : -177 : 500 )
1053 3 0.0878729  ( -171 : 172 : 183 ) 3 78 575 -576 177 -500 -493 (
        564 : -496 : 497 ) ( -581 : 582 : -511 : 512 )
1054 0  ( -171 : 172 : 183 ) 3 493 577 -578 503 -504 -81 ( -579 : 580 :
        -507 : 508 )
1055 3 0.0878729  ( -171 : 172 : 183 ) 3 493 579 -580 507 -508 -81 (
        -581 : 582 : -511 : 512 )
1056 0  -512 511 -582 581 -81 78 3
1057 0  ( -171 : 172 : 183 ) 3 78 583 -584 496 -497 -493 ( 574 : -496 :
        497 ) ( -585 : 586 : -177 : 500 )
1058 3 0.0878729  ( -171 : 172 : 183 ) 3 78 585 -586 177 -500 -493 (
        574 : -496 : 497 ) ( -591 : 592 : -511 : 512 )
1059 0  ( -171 : 172 : 183 ) 3 493 587 -588 503 -504 -81 ( -589 : 590 :
        -507 : 508 )
1060 3 0.0878729  ( -171 : 172 : 183 ) 3 493 589 -590 507 -508 -81 (
        -591 : 592 : -511 : 512 )
1061 0  -512 511 -592 591 -81 78 3
1062 0  ( -171 : 172 : 183 ) 3 78 593 -594 496 -497 -493 ( 584 : -496 :
        497 ) ( -595 : 596 : -177 : 500 )
1063 3 0.0878729  ( -171 : 172 : 183 ) 3 78 595 -596 177 -500 -493 (
        584 : -496 : 497 ) ( -601 : 602 : -511 : 512 )
1064 0  ( -171 : 172 : 183 ) 3 493 597 -598 503 -504 -81 ( -599 : 600 :
        -507 : 508 )
1065 3 0.0878729  ( -171 : 172 : 183 ) 3 493 599 -600 507 -508 -81 (
        -601 : 602 : -511 : 512 )
1066 0  -512 511 -602 601 -81 78 3
1067 0  ( -171 : 172 : 183 ) 3 78 603 -604 496 -497 -493 ( 594 : -496 :
        497 ) ( -605 : 606 : -177 : 500 )
1068 3 0.0878729  ( -171 : 172 : 183 ) 3 78 605 -606 177 -500 -493 (
        594 : -496 : 497 ) ( -611 : 612 : -511 : 512 )
1069 0  ( -171 : 172 : 183 ) 3 493 607 -608 503 -504 -81 ( -609 : 610 :
        -507 : 508 )
1070 3 0.0878729  ( -171 : 172 : 183 ) 3 493 609 -610 507 -508 -81 (
        -611 : 612 : -511 : 512 )
1071 0  -512 511 -612 611 -81 78 3
1072 0  ( -171 : 172 : 183 ) 3 78 613 -614 496 -497 -493 ( 604 : -496 :
        497 ) ( -615 : 616 : -177 : 500 )
1073 3 0.0878729  ( -171 : 172 : 183 ) 3 78 615 -616 177 -500 -493 (
        604 : -496 : 497 ) ( -621 : 622 : -511 : 512 )
1074 0  ( -171 : 172 : 183 ) 3 493 617 -618 503 -504 -81 ( -619 : 620 :
        -507 : 508 )
1075 3 0.0878729  ( -171 : 172 : 183 ) 3 493 619 -620 507 -508 -81 (
        -621 : 622 : -511 : 512 )
1076 0  -512 511 -622 621 -81 78 3
1077 0  ( -171 : 172 : 183 ) 3 78 623 -624 496 -497 -493 ( 614 : -496 :
        497 ) ( -625 : 626 : -177 : 500 )
1078 3 0.0878729  ( -171 : 172 : 183 ) 3 78 625 -626 177 -500 -493 (
        614 : -496 : 497 ) ( -631 : 632 : -511 : 512 )
1079 0  ( -171 : 172 : 183 ) 3 493 627 -628 503 -504 -81 ( -629 : 630 :
        -507 : 508 )
1080 3 0.0878729  ( -171 : 172 : 183 ) 3 493 629 -630 507 -508 -81 (
        -631 : 632 : -511 : 512 )
1081 0  -512 511 -632 631 -81 78 3
1082 0  ( -171 : 172 : 183 ) 3 78 633 -634 496 -497 -493 ( 624 : -496 :
        497 ) ( -635 : 636 : -177 : 500 )
1083 3 0.0878729  ( -171 : 172 : 183 ) 3 78 635 -636 177 -500 -493 (
        624 : -496 : 497 ) ( -641 : 642 : -511 : 512 )
1084 0  ( -171 : 172 : 183 ) 3 493 637 -638 503 -504 -81 ( -639 : 640 :
        -507 : 508 )
1085 3 0.0878729  ( -171 : 172 : 183 ) 3 493 639 -640 507 -508 -81 (
        -641 : 642 : -511 : 512 )
1086 0  -512 511 -642 641 -81 78 3
1087 0  ( -171 : 172 : 183 ) 3 78 643 -644 496 -497 -493 ( 634 : -496 :
        497 ) ( -645 : 646 : -177 : 500 )
1088 3 0.0878729  ( -171 : 172 : 183 ) 3 78 645 -646 177 -500 -493 (
        634 : -496 : 497 ) ( -651 : 652 : -511 : 512 )
1089 0  ( -171 : 172 : 183 ) 3 493 647 -648 503 -504 -81 ( -649 : 650 :
        -507 : 508 )
1090 3 0.0878729  ( -171 : 172 : 183 ) 3 493 649 -650 507 -508 -81 (
        -651 : 652 : -511 : 512 )
1091 0  -512 511 -652 651 -81 78 3
1092 0  ( -171 : 172 : 183 ) 3 78 653 -654 496 -497 -493 ( 644 : -496 :
        497 ) ( -655 : 656 : -177 : 500 )
1093 3 0.0878729  ( -171 : 172 : 183 ) 3 78 655 -656 177 -500 -493 (
        644 : -496 : 497 ) ( -661 : 662 : -511 : 512 )
1094 0  ( -171 : 172 : 183 ) 3 493 657 -658 503 -504 -81 ( -659 : 660 :
        -507 : 508 )
1095 3 0.0878729  ( -171 : 172 : 183 ) 3 493 659 -660 507 -508 -81 (
        -661 : 662 : -511 : 512 )
1096 0  -512 511 -662 661 -81 78 3
1097 0  ( -171 : 172 : 183 ) 3 78 663 -664 496 -497 -493 ( 654 : -496 :
        497 ) ( -665 : 666 : -177 : 500 )
1098 3 0.0878729  ( -171 : 172 : 183 ) 3 78 665 -666 177 -500 -493 (
        654 : -496 : 497 ) ( -671 : 672 : -511 : 512 )
1099 0  ( -171 : 172 : 183 ) 3 493 667 -668 503 -504 -81 ( -669 : 670 :
        -507 : 508 )
1100 3 0.0878729  ( -171 : 172 : 183 ) 3 493 669 -670 507 -508 -81 (
        -671 : 672 : -511 : 512 )
1101 0  -512 511 -672 671 -81 78 3
1102 0  ( -171 : 172 : 183 ) 3 78 673 -674 496 -497 -493 ( 664 : -496 :
        497 ) ( -675 : 676 : -177 : 500 )
1103 3 0.0878729  ( -171 : 172 : 183 ) 3 78 675 -676 177 -500 -493 (
        664 : -496 : 497 ) ( -681 : 682 : -511 : 512 )
1104 0  ( -171 : 172 : 183 ) 3 493 677 -678 503 -504 -81 ( -679 : 680 :
        -507 : 508 )
1105 3 0.0878729  ( -171 : 172 : 183 ) 3 493 679 -680 507 -508 -81 (
        -681 : 682 : -511 : 512 )
1106 0  -512 511 -682 681 -81 78 3
1107 0  ( -171 : 172 : 183 ) 3 78 683 -684 496 -497 -493 ( 674 : -496 :
        497 ) ( -685 : 686 : -177 : 500 )
1108 3 0.0878729  ( -171 : 172 : 183 ) 3 78 685 -686 177 -500 -493 (
        674 : -496 : 497 ) ( -691 : 692 : -511 : 512 )
1109 0  ( -171 : 172 : 183 ) 3 493 687 -688 503 -504 -81 ( -689 : 690 :
        -507 : 508 )
1110 3 0.0878729  ( -171 : 172 : 183 ) 3 493 689 -690 507 -508 -81 (
        -691 : 692 : -511 : 512 )
1111 0  -512 511 -692 691 -81 78 3
1112 0  ( -171 : 172 : 183 ) 3 78 693 -694 496 -497 -493 ( 684 : -496 :
        497 ) ( -695 : 696 : -177 : 500 )
1113 3 0.0878729  ( -171 : 172 : 183 ) 3 78 695 -696 177 -500 -493 (
        684 : -496 : 497 ) ( -701 : 702 : -511 : 512 )
1114 0  ( -171 : 172 : 183 ) 3 493 697 -698 503 -504 -81 ( -699 : 700 :
        -507 : 508 )
1115 3 0.0878729  ( -171 : 172 : 183 ) 3 493 699 -700 507 -508 -81 (
        -701 : 702 : -511 : 512 )
1116 0  -512 511 -702 701 -81 78 3
1117 0  ( -171 : 172 : 183 ) 3 78 703 -704 496 -497 -493 ( 694 : -496 :
        497 ) ( -705 : 706 : -177 : 500 )
1118 3 0.0878729  ( -171 : 172 : 183 ) 3 78 705 -706 177 -500 -493 (
        694 : -496 : 497 ) ( -711 : 712 : -511 : 512 )
1119 0  ( -171 : 172 : 183 ) 3 493 707 -708 503 -504 -81 ( -709 : 710 :
        -507 : 508 )
1120 3 0.0878729  ( -171 : 172 : 183 ) 3 493 709 -710 507 -508 -81 (
        -711 : 712 : -511 : 512 )
1121 0  -512 511 -712 711 -81 78 3
1122 48 0.117208  90 -713 95 -102 96 -101 93
1123 74 0.0847227  90 -714 713 -102 96 -101 93
1124 48 0.117208  90 -715 714 -102 96 -101 93
1125 74 0.0847227  90 -716 715 -102 96 -101 93
1126 48 0.117208  90 -717 716 -102 96 -101 93
1127 74 0.0847227  90 -718 717 -102 96 -101 93
1128 48 0.117208  90 -719 718 -102 96 -101 93
1129 74 0.0847227  90 -720 719 -102 96 -101 93
1130 48 0.117208  90 -721 720 -102 96 -101 93
1131 74 0.0847227  90 -722 721 -102 96 -101 93
1132 48 0.117208  90 -723 722 -102 96 -101 93
1133 74 0.0847227  90 -724 723 -102 96 -101 93
1134 48 0.117208  90 -725 724 -102 96 -101 93
1135 74 0.0847227  90 -726 725 -102 96 -101 93
1136 48 0.117208  90 -100 726 -102 96 -101 93
1137 48 0.117208  90 -713 95 -103 102 -101 93
1138 74 0.0847227  90 -714 713 -103 102 -101 93
1139 48 0.117208  90 -715 714 -103 102 -101 93
1140 74 0.0847227  90 -716 715 -103 102 -101 93
1141 48 0.117208  90 -717 716 -103 102 -101 93
1142 74 0.0847227  90 -718 717 -103 102 -101 93
1143 48 0.117208  90 -719 718 -103 102 -101 93
1144 74 0.0847227  90 -720 719 -103 102 -101 93
1145 48 0.117208  90 -721 720 -103 102 -101 93
1146 74 0.0847227  90 -722 721 -103 102 -101 93
1147 48 0.117208  90 -723 722 -103 102 -101 93
1148 74 0.0847227  90 -724 723 -103 102 -101 93
1149 48 0.117208  90 -725 724 -103 102 -101 93
1150 74 0.0847227  90 -726 725 -103 102 -101 93
1151 48 0.117208  90 -100 726 -103 102 -101 93
1152 48 0.117208  90 -713 95 -104 103 -101 93
1153 74 0.0847227  90 -714 713 -104 103 -101 93
1154 48 0.117208  90 -715 714 -104 103 -101 93
1155 74 0.0847227  90 -716 715 -104 103 -101 93
1156 48 0.117208  90 -717 716 -104 103 -101 93
1157 74 0.0847227  90 -718 717 -104 103 -101 93
1158 48 0.117208  90 -719 718 -104 103 -101 93
1159 74 0.0847227  90 -720 719 -104 103 -101 93
1160 48 0.117208  90 -721 720 -104 103 -101 93
1161 74 0.0847227  90 -722 721 -104 103 -101 93
1162 48 0.117208  90 -723 722 -104 103 -101 93
1163 74 0.0847227  90 -724 723 -104 103 -101 93
1164 48 0.117208  90 -725 724 -104 103 -101 93
1165 74 0.0847227  90 -726 725 -104 103 -101 93
1166 48 0.117208  90 -100 726 -104 103 -101 93
1167 48 0.117208  90 -713 95 -105 104 -101 93
1168 74 0.0847227  90 -714 713 -105 104 -101 93
1169 48 0.117208  90 -715 714 -105 104 -101 93
1170 74 0.0847227  90 -716 715 -105 104 -101 93
1171 48 0.117208  90 -717 716 -105 104 -101 93
1172 74 0.0847227  90 -718 717 -105 104 -101 93
1173 48 0.117208  90 -719 718 -105 104 -101 93
1174 74 0.0847227  90 -720 719 -105 104 -101 93
1175 48 0.117208  90 -721 720 -105 104 -101 93
1176 74 0.0847227  90 -722 721 -105 104 -101 93
1177 48 0.117208  90 -723 722 -105 104 -101 93
1178 74 0.0847227  90 -724 723 -105 104 -101 93
1179 48 0.117208  90 -725 724 -105 104 -101 93
1180 74 0.0847227  90 -726 725 -105 104 -101 93
1181 48 0.117208  90 -100 726 -105 104 -101 93
1182 48 0.117208  90 -713 95 -106 105 -101 93
1183 74 0.0847227  90 -714 713 -106 105 -101 93
1184 48 0.117208  90 -715 714 -106 105 -101 93
1185 74 0.0847227  90 -716 715 -106 105 -101 93
1186 48 0.117208  90 -717 716 -106 105 -101 93
1187 74 0.0847227  90 -718 717 -106 105 -101 93
1188 48 0.117208  90 -719 718 -106 105 -101 93
1189 74 0.0847227  90 -720 719 -106 105 -101 93
1190 48 0.117208  90 -721 720 -106 105 -101 93
1191 74 0.0847227  90 -722 721 -106 105 -101 93
1192 48 0.117208  90 -723 722 -106 105 -101 93
1193 74 0.0847227  90 -724 723 -106 105 -101 93
1194 48 0.117208  90 -725 724 -106 105 -101 93
1195 74 0.0847227  90 -726 725 -106 105 -101 93
1196 48 0.117208  90 -100 726 -106 105 -101 93
1197 48 0.117208  90 -713 95 -107 106 -101 93
1198 74 0.0847227  90 -714 713 -107 106 -101 93
1199 48 0.117208  90 -715 714 -107 106 -101 93
1200 74 0.0847227  90 -716 715 -107 106 -101 93
1201 48 0.117208  90 -717 716 -107 106 -101 93
1202 74 0.0847227  90 -718 717 -107 106 -101 93
1203 48 0.117208  90 -719 718 -107 106 -101 93
1204 74 0.0847227  90 -720 719 -107 106 -101 93
1205 48 0.117208  90 -721 720 -107 106 -101 93
1206 74 0.0847227  90 -722 721 -107 106 -101 93
1207 48 0.117208  90 -723 722 -107 106 -101 93
1208 74 0.0847227  90 -724 723 -107 106 -101 93
1209 48 0.117208  90 -725 724 -107 106 -101 93
1210 74 0.0847227  90 -726 725 -107 106 -101 93
1211 48 0.117208  90 -100 726 -107 106 -101 93
1212 48 0.117208  90 -713 95 -108 107 -101 93
1213 74 0.0847227  90 -714 713 -108 107 -101 93
1214 48 0.117208  90 -715 714 -108 107 -101 93
1215 74 0.0847227  90 -716 715 -108 107 -101 93
1216 48 0.117208  90 -717 716 -108 107 -101 93
1217 74 0.0847227  90 -718 717 -108 107 -101 93
1218 48 0.117208  90 -719 718 -108 107 -101 93
1219 74 0.0847227  90 -720 719 -108 107 -101 93
1220 48 0.117208  90 -721 720 -108 107 -101 93
1221 74 0.0847227  90 -722 721 -108 107 -101 93
1222 48 0.117208  90 -723 722 -108 107 -101 93
1223 74 0.0847227  90 -724 723 -108 107 -101 93
1224 48 0.117208  90 -725 724 -108 107 -101 93
1225 74 0.0847227  90 -726 725 -108 107 -101 93
1226 48 0.117208  90 -100 726 -108 107 -101 93
1227 48 0.117208  90 -713 95 -109 108 -101 93
1228 74 0.0847227  90 -714 713 -109 108 -101 93
1229 48 0.117208  90 -715 714 -109 108 -101 93
1230 74 0.0847227  90 -716 715 -109 108 -101 93
1231 48 0.117208  90 -717 716 -109 108 -101 93
1232 74 0.0847227  90 -718 717 -109 108 -101 93
1233 48 0.117208  90 -719 718 -109 108 -101 93
1234 74 0.0847227  90 -720 719 -109 108 -101 93
1235 48 0.117208  90 -721 720 -109 108 -101 93
1236 74 0.0847227  90 -722 721 -109 108 -101 93
1237 48 0.117208  90 -723 722 -109 108 -101 93
1238 74 0.0847227  90 -724 723 -109 108 -101 93
1239 48 0.117208  90 -725 724 -109 108 -101 93
1240 74 0.0847227  90 -726 725 -109 108 -101 93
1241 48 0.117208  90 -100 726 -109 108 -101 93
1242 48 0.117208  90 -713 95 -110 109 -101 93
1243 74 0.0847227  90 -714 713 -110 109 -101 93
1244 48 0.117208  90 -715 714 -110 109 -101 93
1245 74 0.0847227  90 -716 715 -110 109 -101 93
1246 48 0.117208  90 -717 716 -110 109 -101 93
1247 74 0.0847227  90 -718 717 -110 109 -101 93
1248 48 0.117208  90 -719 718 -110 109 -101 93
1249 74 0.0847227  90 -720 719 -110 109 -101 93
1250 48 0.117208  90 -721 720 -110 109 -101 93
1251 74 0.0847227  90 -722 721 -110 109 -101 93
1252 48 0.117208  90 -723 722 -110 109 -101 93
1253 74 0.0847227  90 -724 723 -110 109 -101 93
1254 48 0.117208  90 -725 724 -110 109 -101 93
1255 74 0.0847227  90 -726 725 -110 109 -101 93
1256 48 0.117208  90 -100 726 -110 109 -101 93
1257 48 0.117208  90 -713 95 -97 110 -101 93
1258 74 0.0847227  90 -714 713 -97 110 -101 93
1259 48 0.117208  90 -715 714 -97 110 -101 93
1260 74 0.0847227  90 -716 715 -97 110 -101 93
1261 48 0.117208  90 -717 716 -97 110 -101 93
1262 74 0.0847227  90 -718 717 -97 110 -101 93
1263 48 0.117208  90 -719 718 -97 110 -101 93
1264 74 0.0847227  90 -720 719 -97 110 -101 93
1265 48 0.117208  90 -721 720 -97 110 -101 93
1266 74 0.0847227  90 -722 721 -97 110 -101 93
1267 48 0.117208  90 -723 722 -97 110 -101 93
1268 74 0.0847227  90 -724 723 -97 110 -101 93
1269 48 0.117208  90 -725 724 -97 110 -101 93
1270 74 0.0847227  90 -726 725 -97 110 -101 93
1271 48 0.117208  90 -100 726 -97 110 -101 93
1272 0  ( -84 : -91 : 112 : 153 ) 94 -101 96 -102 83 -100 3 ( 101 : 91
        ) ( 101 : -95 ) 90
1273 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 96 -102 83 -100 3
        ( 101 : 91 ) ( 101 : -95 ) 90
1274 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 96 -102 83 -100 3
        ( 101 : 91 ) ( 101 : -95 ) 90
1275 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 96 -102 83 -100 3
        ( 101 : 91 ) ( 101 : -95 ) 90
1276 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 96 -102 83 -100 3
        ( 101 : 91 ) ( 101 : -95 ) 90
1277 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 96 -102 83 -100 3 (
        101 : 91 ) ( 101 : -95 ) 90
1278 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 96 -102 83 -100 3
        ( 101 : 91 ) ( 101 : -95 ) 90
1279 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 96 -102 83 -100 3 (
        101 : 91 ) ( 101 : -95 ) 90
1280 0  ( -84 : -91 : 112 : 153 ) 94 -101 102 -103 83 -100 3 ( 101 :
        -95 ) 90
1281 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 102 -103 83 -100 3
        ( 101 : -95 ) 90
1282 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 102 -103 83 -100
        3 ( 101 : -95 ) 90
1283 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 102 -103 83 -100 3
        ( 101 : -95 ) 90
1284 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 102 -103 83 -100
        3 ( 101 : -95 ) 90
1285 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 102 -103 83 -100 3
        ( 101 : -95 ) 90
1286 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 102 -103 83 -100 3
        ( 101 : -95 ) 90
1287 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 102 -103 83 -100 3
        ( 101 : -95 ) 90
1288 0  ( -84 : -91 : 112 : 153 ) 94 -101 103 -104 83 -100 3 ( 101 :
        -95 ) 90
1289 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 103 -104 83 -100 3
        ( 101 : -95 ) 90
1290 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 103 -104 83 -100
        3 ( 101 : -95 ) 90
1291 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 103 -104 83 -100 3
        ( 101 : -95 ) 90
1292 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 103 -104 83 -100
        3 ( 101 : -95 ) 90
1293 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 103 -104 83 -100 3
        ( 101 : -95 ) 90
1294 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 103 -104 83 -100 3
        ( 101 : -95 ) 90
1295 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 103 -104 83 -100 3
        ( 101 : -95 ) 90
1296 0  ( -84 : -91 : 112 : 153 ) 94 -101 104 -105 83 -100 3 ( 101 :
        -95 ) 90
1297 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 104 -105 83 -100 3
        ( 101 : -95 ) 90
1298 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 104 -105 83 -100
        3 ( 101 : -95 ) 90
1299 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 104 -105 83 -100 3
        ( 101 : -95 ) 90
1300 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 104 -105 83 -100
        3 ( 101 : -95 ) 90
1301 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 104 -105 83 -100 3
        ( 101 : -95 ) 90
1302 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 104 -105 83 -100 3
        ( 101 : -95 ) 90
1303 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 104 -105 83 -100 3
        ( 101 : -95 ) 90
1304 0  ( -84 : -91 : 112 : 153 ) 94 -101 105 -106 83 -100 3 ( 101 :
        -95 ) 90
1305 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 105 -106 83 -100 3
        ( 101 : -95 ) 90
1306 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 105 -106 83 -100
        3 ( 101 : -95 ) 90
1307 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 105 -106 83 -100 3
        ( 101 : -95 ) 90
1308 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 105 -106 83 -100
        3 ( 101 : -95 ) 90
1309 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 105 -106 83 -100 3
        ( 101 : -95 ) 90
1310 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 105 -106 83 -100 3
        ( 101 : -95 ) 90
1311 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 105 -106 83 -100 3
        ( 101 : -95 ) 90
1312 0  ( -84 : -91 : 112 : 153 ) 94 -101 106 -107 83 -100 3 ( 101 :
        -95 ) 90
1313 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 106 -107 83 -100 3
        ( 101 : -95 ) 90
1314 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 106 -107 83 -100
        3 ( 101 : -95 ) 90
1315 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 106 -107 83 -100 3
        ( 101 : -95 ) 90
1316 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 106 -107 83 -100
        3 ( 101 : -95 ) 90
1317 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 106 -107 83 -100 3
        ( 101 : -95 ) 90
1318 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 106 -107 83 -100 3
        ( 101 : -95 ) 90
1319 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 106 -107 83 -100 3
        ( 101 : -95 ) 90
1320 0  ( -84 : -91 : 112 : 153 ) 94 -101 107 -108 83 -100 3 ( 101 :
        -95 ) 90
1321 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 107 -108 83 -100 3
        ( 101 : -95 ) 90
1322 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 107 -108 83 -100
        3 ( 101 : -95 ) 90
1323 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 107 -108 83 -100 3
        ( 101 : -95 ) 90
1324 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 107 -108 83 -100
        3 ( 101 : -95 ) 90
1325 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 107 -108 83 -100 3
        ( 101 : -95 ) 90
1326 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 107 -108 83 -100 3
        ( 101 : -95 ) 90
1327 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 107 -108 83 -100 3
        ( 101 : -95 ) 90
1328 0  ( -84 : -91 : 112 : 153 ) 94 -101 108 -109 83 -100 3 ( 101 :
        -95 ) 90
1329 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 108 -109 83 -100 3
        ( 101 : -95 ) 90
1330 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 108 -109 83 -100
        3 ( 101 : -95 ) 90
1331 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 108 -109 83 -100 3
        ( 101 : -95 ) 90
1332 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 108 -109 83 -100
        3 ( 101 : -95 ) 90
1333 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 108 -109 83 -100 3
        ( 101 : -95 ) 90
1334 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 108 -109 83 -100 3
        ( 101 : -95 ) 90
1335 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 108 -109 83 -100 3
        ( 101 : -95 ) 90
1336 0  ( -84 : -91 : 112 : 153 ) 94 -101 109 -110 83 -100 3 ( 101 :
        -95 ) 90
1337 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 109 -110 83 -100 3
        ( 101 : -95 ) 90
1338 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 109 -110 83 -100
        3 ( 101 : -95 ) 90
1339 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 109 -110 83 -100 3
        ( 101 : -95 ) 90
1340 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 109 -110 83 -100
        3 ( 101 : -95 ) 90
1341 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 109 -110 83 -100 3
        ( 101 : -95 ) 90
1342 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 109 -110 83 -100 3
        ( 101 : -95 ) 90
1343 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 109 -110 83 -100 3
        ( 101 : -95 ) 90
1344 0  ( -84 : -91 : 112 : 153 ) 90 ( 101 : -95 ) ( 101 : -92 ) 3 -100
        83 -97 110 -101 94 ( -111 : -92 : -98 : -3 : -83 : 99 : 115 :
        116 )
1345 48 0.117208  ( -84 : -91 : 112 : 153 ) 90 ( 101 : -95 ) ( 101 :
        -92 ) 3 -100 83 -97 110 -727 101 ( -111 : -92 : -98 : -3 : -83
        : 99 : 115 : 116 )
1346 74 0.0847227  ( -84 : -91 : 112 : 153 ) 90 ( 101 : -95 ) ( 101 :
        -92 ) 3 -100 83 -97 110 -728 727 ( -111 : -92 : -98 : -3 : -83
        : 99 : 115 : 116 )
1347 48 0.117208  ( -84 : -91 : 112 : 153 ) 90 ( 101 : -95 ) ( 101 :
        -92 ) 3 -100 83 -97 110 -729 728 ( -111 : -92 : -98 : -3 : -83
        : 99 : 115 : 116 )
1348 74 0.0847227  ( -84 : -91 : 112 : 153 ) 90 ( 101 : -95 ) ( 101 :
        -92 ) 3 -100 83 -97 110 -730 729 ( -111 : -92 : -98 : -3 : -83
        : 99 : 115 : 116 )
1349 48 0.117208  ( -84 : -91 : 112 : 153 ) 90 ( 101 : -95 ) ( 101 :
        -92 ) 3 -100 83 -97 110 -88 730 ( -111 : -92 : -98 : -3 : -83 :
        99 : 115 : 116 )
1350 74 0.0847227  ( -84 : -91 : 112 : 153 ) 90 ( 101 : -95 ) ( 101 :
        -92 ) 3 -100 83 -97 110 -731 88 ( -111 : -92 : -98 : -3 : -83 :
        99 : 115 : 116 )
1351 48 0.117208  ( -84 : -91 : 112 : 153 ) 90 ( 101 : -95 ) ( 101 :
        -92 ) 3 -100 83 -97 110 -99 731 ( -111 : -92 : -98 : -3 : -83 :
        99 : 115 : 116 )
1352 48 0.117208  111 -732 113 -117 92 -101 93
1353 74 0.0847227  111 -733 732 -117 92 -101 93
1354 48 0.117208  111 -734 733 -117 92 -101 93
1355 74 0.0847227  111 -735 734 -117 92 -101 93
1356 48 0.117208  111 -736 735 -117 92 -101 93
1357 74 0.0847227  111 -737 736 -117 92 -101 93
1358 48 0.117208  111 -738 737 -117 92 -101 93
1359 74 0.0847227  111 -739 738 -117 92 -101 93
1360 48 0.117208  111 -740 739 -117 92 -101 93
1361 74 0.0847227  111 -741 740 -117 92 -101 93
1362 48 0.117208  111 -742 741 -117 92 -101 93
1363 74 0.0847227  111 -743 742 -117 92 -101 93
1364 48 0.117208  111 -744 743 -117 92 -101 93
1365 74 0.0847227  111 -745 744 -117 92 -101 93
1366 48 0.117208  111 -116 745 -117 92 -101 93
1367 48 0.117208  111 -732 113 -118 117 -101 93
1368 74 0.0847227  111 -733 732 -118 117 -101 93
1369 48 0.117208  111 -734 733 -118 117 -101 93
1370 74 0.0847227  111 -735 734 -118 117 -101 93
1371 48 0.117208  111 -736 735 -118 117 -101 93
1372 74 0.0847227  111 -737 736 -118 117 -101 93
1373 48 0.117208  111 -738 737 -118 117 -101 93
1374 74 0.0847227  111 -739 738 -118 117 -101 93
1375 48 0.117208  111 -740 739 -118 117 -101 93
1376 74 0.0847227  111 -741 740 -118 117 -101 93
1377 48 0.117208  111 -742 741 -118 117 -101 93
1378 74 0.0847227  111 -743 742 -118 117 -101 93
1379 48 0.117208  111 -744 743 -118 117 -101 93
1380 74 0.0847227  111 -745 744 -118 117 -101 93
1381 48 0.117208  111 -116 745 -118 117 -101 93
1382 48 0.117208  111 -732 113 -119 118 -101 93
1383 74 0.0847227  111 -733 732 -119 118 -101 93
1384 48 0.117208  111 -734 733 -119 118 -101 93
1385 74 0.0847227  111 -735 734 -119 118 -101 93
1386 48 0.117208  111 -736 735 -119 118 -101 93
1387 74 0.0847227  111 -737 736 -119 118 -101 93
1388 48 0.117208  111 -738 737 -119 118 -101 93
1389 74 0.0847227  111 -739 738 -119 118 -101 93
1390 48 0.117208  111 -740 739 -119 118 -101 93
1391 74 0.0847227  111 -741 740 -119 118 -101 93
1392 48 0.117208  111 -742 741 -119 118 -101 93
1393 74 0.0847227  111 -743 742 -119 118 -101 93
1394 48 0.117208  111 -744 743 -119 118 -101 93
1395 74 0.0847227  111 -745 744 -119 118 -101 93
1396 48 0.117208  111 -116 745 -119 118 -101 93
1397 48 0.117208  111 -732 113 -120 119 -101 93
1398 74 0.0847227  111 -733 732 -120 119 -101 93
1399 48 0.117208  111 -734 733 -120 119 -101 93
1400 74 0.0847227  111 -735 734 -120 119 -101 93
1401 48 0.117208  111 -736 735 -120 119 -101 93
1402 74 0.0847227  111 -737 736 -120 119 -101 93
1403 48 0.117208  111 -738 737 -120 119 -101 93
1404 74 0.0847227  111 -739 738 -120 119 -101 93
1405 48 0.117208  111 -740 739 -120 119 -101 93
1406 74 0.0847227  111 -741 740 -120 119 -101 93
1407 48 0.117208  111 -742 741 -120 119 -101 93
1408 74 0.0847227  111 -743 742 -120 119 -101 93
1409 48 0.117208  111 -744 743 -120 119 -101 93
1410 74 0.0847227  111 -745 744 -120 119 -101 93
1411 48 0.117208  111 -116 745 -120 119 -101 93
1412 48 0.117208  111 -732 113 -121 120 -101 93
1413 74 0.0847227  111 -733 732 -121 120 -101 93
1414 48 0.117208  111 -734 733 -121 120 -101 93
1415 74 0.0847227  111 -735 734 -121 120 -101 93
1416 48 0.117208  111 -736 735 -121 120 -101 93
1417 74 0.0847227  111 -737 736 -121 120 -101 93
1418 48 0.117208  111 -738 737 -121 120 -101 93
1419 74 0.0847227  111 -739 738 -121 120 -101 93
1420 48 0.117208  111 -740 739 -121 120 -101 93
1421 74 0.0847227  111 -741 740 -121 120 -101 93
1422 48 0.117208  111 -742 741 -121 120 -101 93
1423 74 0.0847227  111 -743 742 -121 120 -101 93
1424 48 0.117208  111 -744 743 -121 120 -101 93
1425 74 0.0847227  111 -745 744 -121 120 -101 93
1426 48 0.117208  111 -116 745 -121 120 -101 93
1427 48 0.117208  111 -732 113 -122 121 -101 93
1428 74 0.0847227  111 -733 732 -122 121 -101 93
1429 48 0.117208  111 -734 733 -122 121 -101 93
1430 74 0.0847227  111 -735 734 -122 121 -101 93
1431 48 0.117208  111 -736 735 -122 121 -101 93
1432 74 0.0847227  111 -737 736 -122 121 -101 93
1433 48 0.117208  111 -738 737 -122 121 -101 93
1434 74 0.0847227  111 -739 738 -122 121 -101 93
1435 48 0.117208  111 -740 739 -122 121 -101 93
1436 74 0.0847227  111 -741 740 -122 121 -101 93
1437 48 0.117208  111 -742 741 -122 121 -101 93
1438 74 0.0847227  111 -743 742 -122 121 -101 93
1439 48 0.117208  111 -744 743 -122 121 -101 93
1440 74 0.0847227  111 -745 744 -122 121 -101 93
1441 48 0.117208  111 -116 745 -122 121 -101 93
1442 48 0.117208  111 -732 113 -123 122 -101 93
1443 74 0.0847227  111 -733 732 -123 122 -101 93
1444 48 0.117208  111 -734 733 -123 122 -101 93
1445 74 0.0847227  111 -735 734 -123 122 -101 93
1446 48 0.117208  111 -736 735 -123 122 -101 93
1447 74 0.0847227  111 -737 736 -123 122 -101 93
1448 48 0.117208  111 -738 737 -123 122 -101 93
1449 74 0.0847227  111 -739 738 -123 122 -101 93
1450 48 0.117208  111 -740 739 -123 122 -101 93
1451 74 0.0847227  111 -741 740 -123 122 -101 93
1452 48 0.117208  111 -742 741 -123 122 -101 93
1453 74 0.0847227  111 -743 742 -123 122 -101 93
1454 48 0.117208  111 -744 743 -123 122 -101 93
1455 74 0.0847227  111 -745 744 -123 122 -101 93
1456 48 0.117208  111 -116 745 -123 122 -101 93
1457 48 0.117208  111 -732 113 -124 123 -101 93
1458 74 0.0847227  111 -733 732 -124 123 -101 93
1459 48 0.117208  111 -734 733 -124 123 -101 93
1460 74 0.0847227  111 -735 734 -124 123 -101 93
1461 48 0.117208  111 -736 735 -124 123 -101 93
1462 74 0.0847227  111 -737 736 -124 123 -101 93
1463 48 0.117208  111 -738 737 -124 123 -101 93
1464 74 0.0847227  111 -739 738 -124 123 -101 93
1465 48 0.117208  111 -740 739 -124 123 -101 93
1466 74 0.0847227  111 -741 740 -124 123 -101 93
1467 48 0.117208  111 -742 741 -124 123 -101 93
1468 74 0.0847227  111 -743 742 -124 123 -101 93
1469 48 0.117208  111 -744 743 -124 123 -101 93
1470 74 0.0847227  111 -745 744 -124 123 -101 93
1471 48 0.117208  111 -116 745 -124 123 -101 93
1472 48 0.117208  111 -732 113 -115 124 -101 93
1473 74 0.0847227  111 -733 732 -115 124 -101 93
1474 48 0.117208  111 -734 733 -115 124 -101 93
1475 74 0.0847227  111 -735 734 -115 124 -101 93
1476 48 0.117208  111 -736 735 -115 124 -101 93
1477 74 0.0847227  111 -737 736 -115 124 -101 93
1478 48 0.117208  111 -738 737 -115 124 -101 93
1479 74 0.0847227  111 -739 738 -115 124 -101 93
1480 48 0.117208  111 -740 739 -115 124 -101 93
1481 74 0.0847227  111 -741 740 -115 124 -101 93
1482 48 0.117208  111 -742 741 -115 124 -101 93
1483 74 0.0847227  111 -743 742 -115 124 -101 93
1484 48 0.117208  111 -744 743 -115 124 -101 93
1485 74 0.0847227  111 -745 744 -115 124 -101 93
1486 48 0.117208  111 -116 745 -115 124 -101 93
1487 0  ( -84 : -91 : 112 : 153 ) 94 -101 92 -117 83 -116 3 114 ( 101 :
        -113 ) 111
1488 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 92 -117 83 -116 3
        114 ( 101 : -113 ) 111
1489 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 92 -117 83 -116 3
        114 ( 101 : -113 ) 111
1490 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 92 -117 83 -116 3
        114 ( 101 : -113 ) 111
1491 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 92 -117 83 -116 3
        114 ( 101 : -113 ) 111
1492 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 92 -117 83 -116 3
        114 ( 101 : -113 ) 111
1493 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 92 -117 83 -116 3
        114 ( 101 : -113 ) 111
1494 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 92 -117 83 -116 3
        114 ( 101 : -113 ) 111
1495 0  ( -84 : -91 : 112 : 153 ) 94 -101 117 -118 83 -116 3 ( 101 :
        -113 ) 111
1496 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 117 -118 83 -116 3
        ( 101 : -113 ) 111
1497 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 117 -118 83 -116
        3 ( 101 : -113 ) 111
1498 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 117 -118 83 -116 3
        ( 101 : -113 ) 111
1499 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 117 -118 83 -116
        3 ( 101 : -113 ) 111
1500 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 117 -118 83 -116 3
        ( 101 : -113 ) 111
1501 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 117 -118 83 -116 3
        ( 101 : -113 ) 111
1502 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 117 -118 83 -116 3
        ( 101 : -113 ) 111
1503 0  ( -84 : -91 : 112 : 153 ) 94 -101 118 -119 83 -116 3 ( 101 :
        -113 ) 111
1504 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 118 -119 83 -116 3
        ( 101 : -113 ) 111
1505 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 118 -119 83 -116
        3 ( 101 : -113 ) 111
1506 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 118 -119 83 -116 3
        ( 101 : -113 ) 111
1507 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 118 -119 83 -116
        3 ( 101 : -113 ) 111
1508 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 118 -119 83 -116 3
        ( 101 : -113 ) 111
1509 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 118 -119 83 -116 3
        ( 101 : -113 ) 111
1510 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 118 -119 83 -116 3
        ( 101 : -113 ) 111
1511 0  ( -84 : -91 : 112 : 153 ) 94 -101 119 -120 83 -116 3 ( 101 :
        -113 ) 111
1512 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 119 -120 83 -116 3
        ( 101 : -113 ) 111
1513 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 119 -120 83 -116
        3 ( 101 : -113 ) 111
1514 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 119 -120 83 -116 3
        ( 101 : -113 ) 111
1515 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 119 -120 83 -116
        3 ( 101 : -113 ) 111
1516 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 119 -120 83 -116 3
        ( 101 : -113 ) 111
1517 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 119 -120 83 -116 3
        ( 101 : -113 ) 111
1518 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 119 -120 83 -116 3
        ( 101 : -113 ) 111
1519 0  ( -84 : -91 : 112 : 153 ) 94 -101 120 -121 83 -116 3 ( 101 :
        -113 ) 111
1520 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 120 -121 83 -116 3
        ( 101 : -113 ) 111
1521 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 120 -121 83 -116
        3 ( 101 : -113 ) 111
1522 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 120 -121 83 -116 3
        ( 101 : -113 ) 111
1523 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 120 -121 83 -116
        3 ( 101 : -113 ) 111
1524 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 120 -121 83 -116 3
        ( 101 : -113 ) 111
1525 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 120 -121 83 -116 3
        ( 101 : -113 ) 111
1526 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 120 -121 83 -116 3
        ( 101 : -113 ) 111
1527 0  ( -84 : -91 : 112 : 153 ) 94 -101 121 -122 83 -116 3 ( 101 :
        -113 ) 111
1528 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 121 -122 83 -116 3
        ( 101 : -113 ) 111
1529 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 121 -122 83 -116
        3 ( 101 : -113 ) 111
1530 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 121 -122 83 -116 3
        ( 101 : -113 ) 111
1531 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 121 -122 83 -116
        3 ( 101 : -113 ) 111
1532 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 121 -122 83 -116 3
        ( 101 : -113 ) 111
1533 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 121 -122 83 -116 3
        ( 101 : -113 ) 111
1534 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 121 -122 83 -116 3
        ( 101 : -113 ) 111
1535 0  ( -84 : -91 : 112 : 153 ) 94 -101 122 -123 83 -116 3 ( 101 :
        -113 ) 111
1536 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 122 -123 83 -116 3
        ( 101 : -113 ) 111
1537 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 122 -123 83 -116
        3 ( 101 : -113 ) 111
1538 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 122 -123 83 -116 3
        ( 101 : -113 ) 111
1539 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 122 -123 83 -116
        3 ( 101 : -113 ) 111
1540 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 122 -123 83 -116 3
        ( 101 : -113 ) 111
1541 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 122 -123 83 -116 3
        ( 101 : -113 ) 111
1542 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 122 -123 83 -116 3
        ( 101 : -113 ) 111
1543 0  ( -84 : -91 : 112 : 153 ) 94 -101 123 -124 83 -116 3 ( 101 :
        -113 ) 111
1544 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 123 -124 83 -116 3
        ( 101 : -113 ) 111
1545 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 123 -124 83 -116
        3 ( 101 : -113 ) 111
1546 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 123 -124 83 -116 3
        ( 101 : -113 ) 111
1547 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 123 -124 83 -116
        3 ( 101 : -113 ) 111
1548 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 123 -124 83 -116 3
        ( 101 : -113 ) 111
1549 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 123 -124 83 -116 3
        ( 101 : -113 ) 111
1550 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 123 -124 83 -116 3
        ( 101 : -113 ) 111
1551 0  ( -84 : -91 : 112 : 153 ) 94 -101 124 -115 83 -116 3 ( 101 :
        -112 ) ( 101 : -113 ) 111
1552 48 0.117208  ( -84 : -91 : 112 : 153 ) 101 -727 124 -115 83 -116 3
        ( 101 : -112 ) ( 101 : -113 ) 111
1553 74 0.0847227  ( -84 : -91 : 112 : 153 ) 727 -728 124 -115 83 -116
        3 ( 101 : -112 ) ( 101 : -113 ) 111
1554 48 0.117208  ( -84 : -91 : 112 : 153 ) 728 -729 124 -115 83 -116 3
        ( 101 : -112 ) ( 101 : -113 ) 111
1555 74 0.0847227  ( -84 : -91 : 112 : 153 ) 729 -730 124 -115 83 -116
        3 ( 101 : -112 ) ( 101 : -113 ) 111
1556 48 0.117208  ( -84 : -91 : 112 : 153 ) 730 -88 124 -115 83 -116 3
        ( 101 : -112 ) ( 101 : -113 ) 111
1557 74 0.0847227  ( -84 : -91 : 112 : 153 ) 88 -731 124 -115 83 -116 3
        ( 101 : -112 ) ( 101 : -113 ) 111
1558 48 0.117208  ( -84 : -91 : 112 : 153 ) 731 -99 124 -115 83 -116 3
        ( 101 : -112 ) ( 101 : -113 ) 111
1559 48 0.117208  125 -713 95 -129 127 -101 93
1560 74 0.0847227  125 -714 713 -129 127 -101 93
1561 48 0.117208  125 -715 714 -129 127 -101 93
1562 74 0.0847227  125 -716 715 -129 127 -101 93
1563 48 0.117208  125 -717 716 -129 127 -101 93
1564 74 0.0847227  125 -718 717 -129 127 -101 93
1565 48 0.117208  125 -719 718 -129 127 -101 93
1566 74 0.0847227  125 -720 719 -129 127 -101 93
1567 48 0.117208  125 -721 720 -129 127 -101 93
1568 74 0.0847227  125 -722 721 -129 127 -101 93
1569 48 0.117208  125 -723 722 -129 127 -101 93
1570 74 0.0847227  125 -724 723 -129 127 -101 93
1571 48 0.117208  125 -725 724 -129 127 -101 93
1572 74 0.0847227  125 -726 725 -129 127 -101 93
1573 48 0.117208  125 -100 726 -129 127 -101 93
1574 48 0.117208  125 -713 95 -130 129 -101 93
1575 74 0.0847227  125 -714 713 -130 129 -101 93
1576 48 0.117208  125 -715 714 -130 129 -101 93
1577 74 0.0847227  125 -716 715 -130 129 -101 93
1578 48 0.117208  125 -717 716 -130 129 -101 93
1579 74 0.0847227  125 -718 717 -130 129 -101 93
1580 48 0.117208  125 -719 718 -130 129 -101 93
1581 74 0.0847227  125 -720 719 -130 129 -101 93
1582 48 0.117208  125 -721 720 -130 129 -101 93
1583 74 0.0847227  125 -722 721 -130 129 -101 93
1584 48 0.117208  125 -723 722 -130 129 -101 93
1585 74 0.0847227  125 -724 723 -130 129 -101 93
1586 48 0.117208  125 -725 724 -130 129 -101 93
1587 74 0.0847227  125 -726 725 -130 129 -101 93
1588 48 0.117208  125 -100 726 -130 129 -101 93
1589 48 0.117208  125 -713 95 -131 130 -101 93
1590 74 0.0847227  125 -714 713 -131 130 -101 93
1591 48 0.117208  125 -715 714 -131 130 -101 93
1592 74 0.0847227  125 -716 715 -131 130 -101 93
1593 48 0.117208  125 -717 716 -131 130 -101 93
1594 74 0.0847227  125 -718 717 -131 130 -101 93
1595 48 0.117208  125 -719 718 -131 130 -101 93
1596 74 0.0847227  125 -720 719 -131 130 -101 93
1597 48 0.117208  125 -721 720 -131 130 -101 93
1598 74 0.0847227  125 -722 721 -131 130 -101 93
1599 48 0.117208  125 -723 722 -131 130 -101 93
1600 74 0.0847227  125 -724 723 -131 130 -101 93
1601 48 0.117208  125 -725 724 -131 130 -101 93
1602 74 0.0847227  125 -726 725 -131 130 -101 93
1603 48 0.117208  125 -100 726 -131 130 -101 93
1604 48 0.117208  125 -713 95 -132 131 -101 93
1605 74 0.0847227  125 -714 713 -132 131 -101 93
1606 48 0.117208  125 -715 714 -132 131 -101 93
1607 74 0.0847227  125 -716 715 -132 131 -101 93
1608 48 0.117208  125 -717 716 -132 131 -101 93
1609 74 0.0847227  125 -718 717 -132 131 -101 93
1610 48 0.117208  125 -719 718 -132 131 -101 93
1611 74 0.0847227  125 -720 719 -132 131 -101 93
1612 48 0.117208  125 -721 720 -132 131 -101 93
1613 74 0.0847227  125 -722 721 -132 131 -101 93
1614 48 0.117208  125 -723 722 -132 131 -101 93
1615 74 0.0847227  125 -724 723 -132 131 -101 93
1616 48 0.117208  125 -725 724 -132 131 -101 93
1617 74 0.0847227  125 -726 725 -132 131 -101 93
1618 48 0.117208  125 -100 726 -132 131 -101 93
1619 48 0.117208  125 -713 95 -133 132 -101 93
1620 74 0.0847227  125 -714 713 -133 132 -101 93
1621 48 0.117208  125 -715 714 -133 132 -101 93
1622 74 0.0847227  125 -716 715 -133 132 -101 93
1623 48 0.117208  125 -717 716 -133 132 -101 93
1624 74 0.0847227  125 -718 717 -133 132 -101 93
1625 48 0.117208  125 -719 718 -133 132 -101 93
1626 74 0.0847227  125 -720 719 -133 132 -101 93
1627 48 0.117208  125 -721 720 -133 132 -101 93
1628 74 0.0847227  125 -722 721 -133 132 -101 93
1629 48 0.117208  125 -723 722 -133 132 -101 93
1630 74 0.0847227  125 -724 723 -133 132 -101 93
1631 48 0.117208  125 -725 724 -133 132 -101 93
1632 74 0.0847227  125 -726 725 -133 132 -101 93
1633 48 0.117208  125 -100 726 -133 132 -101 93
1634 48 0.117208  125 -713 95 -134 133 -101 93
1635 74 0.0847227  125 -714 713 -134 133 -101 93
1636 48 0.117208  125 -715 714 -134 133 -101 93
1637 74 0.0847227  125 -716 715 -134 133 -101 93
1638 48 0.117208  125 -717 716 -134 133 -101 93
1639 74 0.0847227  125 -718 717 -134 133 -101 93
1640 48 0.117208  125 -719 718 -134 133 -101 93
1641 74 0.0847227  125 -720 719 -134 133 -101 93
1642 48 0.117208  125 -721 720 -134 133 -101 93
1643 74 0.0847227  125 -722 721 -134 133 -101 93
1644 48 0.117208  125 -723 722 -134 133 -101 93
1645 74 0.0847227  125 -724 723 -134 133 -101 93
1646 48 0.117208  125 -725 724 -134 133 -101 93
1647 74 0.0847227  125 -726 725 -134 133 -101 93
1648 48 0.117208  125 -100 726 -134 133 -101 93
1649 48 0.117208  125 -713 95 -135 134 -101 93
1650 74 0.0847227  125 -714 713 -135 134 -101 93
1651 48 0.117208  125 -715 714 -135 134 -101 93
1652 74 0.0847227  125 -716 715 -135 134 -101 93
1653 48 0.117208  125 -717 716 -135 134 -101 93
1654 74 0.0847227  125 -718 717 -135 134 -101 93
1655 48 0.117208  125 -719 718 -135 134 -101 93
1656 74 0.0847227  125 -720 719 -135 134 -101 93
1657 48 0.117208  125 -721 720 -135 134 -101 93
1658 74 0.0847227  125 -722 721 -135 134 -101 93
1659 48 0.117208  125 -723 722 -135 134 -101 93
1660 74 0.0847227  125 -724 723 -135 134 -101 93
1661 48 0.117208  125 -725 724 -135 134 -101 93
1662 74 0.0847227  125 -726 725 -135 134 -101 93
1663 48 0.117208  125 -100 726 -135 134 -101 93
1664 48 0.117208  125 -713 95 -136 135 -101 93
1665 74 0.0847227  125 -714 713 -136 135 -101 93
1666 48 0.117208  125 -715 714 -136 135 -101 93
1667 74 0.0847227  125 -716 715 -136 135 -101 93
1668 48 0.117208  125 -717 716 -136 135 -101 93
1669 74 0.0847227  125 -718 717 -136 135 -101 93
1670 48 0.117208  125 -719 718 -136 135 -101 93
1671 74 0.0847227  125 -720 719 -136 135 -101 93
1672 48 0.117208  125 -721 720 -136 135 -101 93
1673 74 0.0847227  125 -722 721 -136 135 -101 93
1674 48 0.117208  125 -723 722 -136 135 -101 93
1675 74 0.0847227  125 -724 723 -136 135 -101 93
1676 48 0.117208  125 -725 724 -136 135 -101 93
1677 74 0.0847227  125 -726 725 -136 135 -101 93
1678 48 0.117208  125 -100 726 -136 135 -101 93
1679 48 0.117208  125 -713 95 -137 136 -101 93
1680 74 0.0847227  125 -714 713 -137 136 -101 93
1681 48 0.117208  125 -715 714 -137 136 -101 93
1682 74 0.0847227  125 -716 715 -137 136 -101 93
1683 48 0.117208  125 -717 716 -137 136 -101 93
1684 74 0.0847227  125 -718 717 -137 136 -101 93
1685 48 0.117208  125 -719 718 -137 136 -101 93
1686 74 0.0847227  125 -720 719 -137 136 -101 93
1687 48 0.117208  125 -721 720 -137 136 -101 93
1688 74 0.0847227  125 -722 721 -137 136 -101 93
1689 48 0.117208  125 -723 722 -137 136 -101 93
1690 74 0.0847227  125 -724 723 -137 136 -101 93
1691 48 0.117208  125 -725 724 -137 136 -101 93
1692 74 0.0847227  125 -726 725 -137 136 -101 93
1693 48 0.117208  125 -100 726 -137 136 -101 93
1694 48 0.117208  125 -713 95 -128 137 -101 93
1695 74 0.0847227  125 -714 713 -128 137 -101 93
1696 48 0.117208  125 -715 714 -128 137 -101 93
1697 74 0.0847227  125 -716 715 -128 137 -101 93
1698 48 0.117208  125 -717 716 -128 137 -101 93
1699 74 0.0847227  125 -718 717 -128 137 -101 93
1700 48 0.117208  125 -719 718 -128 137 -101 93
1701 74 0.0847227  125 -720 719 -128 137 -101 93
1702 48 0.117208  125 -721 720 -128 137 -101 93
1703 74 0.0847227  125 -722 721 -128 137 -101 93
1704 48 0.117208  125 -723 722 -128 137 -101 93
1705 74 0.0847227  125 -724 723 -128 137 -101 93
1706 48 0.117208  125 -725 724 -128 137 -101 93
1707 74 0.0847227  125 -726 725 -128 137 -101 93
1708 48 0.117208  125 -100 726 -128 137 -101 93
1709 0  125 ( 101 : -95 ) ( 101 : 112 ) -3 -100 83 -129 127 -101 94
1710 48 0.117208  125 ( 101 : -95 ) ( 101 : 112 ) -3 -100 83 -129 127
        -727 101
1711 74 0.0847227  125 ( 101 : -95 ) ( 101 : 112 ) -3 -100 83 -129 127
        -728 727
1712 48 0.117208  125 ( 101 : -95 ) ( 101 : 112 ) -3 -100 83 -129 127
        -729 728
1713 74 0.0847227  125 ( 101 : -95 ) ( 101 : 112 ) -3 -100 83 -129 127
        -730 729
1714 48 0.117208  125 ( 101 : -95 ) ( 101 : 112 ) -3 -100 83 -129 127
        -88 730
1715 74 0.0847227  125 ( 101 : -95 ) ( 101 : 112 ) -3 -100 83 -129 127
        -731 88
1716 48 0.117208  125 ( 101 : -95 ) ( 101 : 112 ) -3 -100 83 -129 127
        -99 731
1717 0  125 ( 101 : -95 ) -3 -100 83 -130 129 -101 94
1718 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -130 129 -727 101
1719 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -130 129 -728 727
1720 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -130 129 -729 728
1721 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -130 129 -730 729
1722 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -130 129 -88 730
1723 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -130 129 -731 88
1724 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -130 129 -99 731
1725 0  125 ( 101 : -95 ) -3 -100 83 -131 130 -101 94
1726 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -131 130 -727 101
1727 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -131 130 -728 727
1728 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -131 130 -729 728
1729 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -131 130 -730 729
1730 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -131 130 -88 730
1731 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -131 130 -731 88
1732 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -131 130 -99 731
1733 0  125 ( 101 : -95 ) -3 -100 83 -132 131 -101 94
1734 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -132 131 -727 101
1735 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -132 131 -728 727
1736 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -132 131 -729 728
1737 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -132 131 -730 729
1738 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -132 131 -88 730
1739 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -132 131 -731 88
1740 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -132 131 -99 731
1741 0  125 ( 101 : -95 ) -3 -100 83 -133 132 -101 94
1742 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -133 132 -727 101
1743 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -133 132 -728 727
1744 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -133 132 -729 728
1745 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -133 132 -730 729
1746 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -133 132 -88 730
1747 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -133 132 -731 88
1748 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -133 132 -99 731
1749 0  125 ( 101 : -95 ) -3 -100 83 -134 133 -101 94
1750 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -134 133 -727 101
1751 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -134 133 -728 727
1752 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -134 133 -729 728
1753 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -134 133 -730 729
1754 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -134 133 -88 730
1755 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -134 133 -731 88
1756 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -134 133 -99 731
1757 0  125 ( 101 : -95 ) -3 -100 83 -135 134 -101 94
1758 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -135 134 -727 101
1759 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -135 134 -728 727
1760 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -135 134 -729 728
1761 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -135 134 -730 729
1762 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -135 134 -88 730
1763 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -135 134 -731 88
1764 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -135 134 -99 731
1765 0  125 ( 101 : -95 ) -3 -100 83 -136 135 -101 94
1766 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -136 135 -727 101
1767 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -136 135 -728 727
1768 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -136 135 -729 728
1769 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -136 135 -730 729
1770 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -136 135 -88 730
1771 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -136 135 -731 88
1772 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -136 135 -99 731
1773 0  125 ( 101 : -95 ) -3 -100 83 -137 136 -101 94
1774 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -137 136 -727 101
1775 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -137 136 -728 727
1776 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -137 136 -729 728
1777 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -137 136 -730 729
1778 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -137 136 -88 730
1779 74 0.0847227  125 ( 101 : -95 ) -3 -100 83 -137 136 -731 88
1780 48 0.117208  125 ( 101 : -95 ) -3 -100 83 -137 136 -99 731
1781 0  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 94 -101 137
        -128 83 -100 -3 ( 101 : -126 ) ( 101 : -95 ) 125
1782 48 0.117208  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 101
        -727 137 -128 83 -100 -3 ( 101 : -126 ) ( 101 : -95 ) 125
1783 74 0.0847227  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 727
        -728 137 -128 83 -100 -3 ( 101 : -126 ) ( 101 : -95 ) 125
1784 48 0.117208  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 728
        -729 137 -128 83 -100 -3 ( 101 : -126 ) ( 101 : -95 ) 125
1785 74 0.0847227  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 729
        -730 137 -128 83 -100 -3 ( 101 : -126 ) ( 101 : -95 ) 125
1786 48 0.117208  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 730
        -88 137 -128 83 -100 -3 ( 101 : -126 ) ( 101 : -95 ) 125
1787 74 0.0847227  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 88
        -731 137 -128 83 -100 -3 ( 101 : -126 ) ( 101 : -95 ) 125
1788 48 0.117208  ( -138 : -126 : -98 : -83 : 3 : 99 : 140 : 116 ) 731
        -99 137 -128 83 -100 -3 ( 101 : -126 ) ( 101 : -95 ) 125
1789 48 0.117208  138 -732 113 -141 126 -101 93
1790 74 0.0847227  138 -733 732 -141 126 -101 93
1791 48 0.117208  138 -734 733 -141 126 -101 93
1792 74 0.0847227  138 -735 734 -141 126 -101 93
1793 48 0.117208  138 -736 735 -141 126 -101 93
1794 74 0.0847227  138 -737 736 -141 126 -101 93
1795 48 0.117208  138 -738 737 -141 126 -101 93
1796 74 0.0847227  138 -739 738 -141 126 -101 93
1797 48 0.117208  138 -740 739 -141 126 -101 93
1798 74 0.0847227  138 -741 740 -141 126 -101 93
1799 48 0.117208  138 -742 741 -141 126 -101 93
1800 74 0.0847227  138 -743 742 -141 126 -101 93
1801 48 0.117208  138 -744 743 -141 126 -101 93
1802 74 0.0847227  138 -745 744 -141 126 -101 93
1803 48 0.117208  138 -116 745 -141 126 -101 93
1804 48 0.117208  138 -732 113 -142 141 -101 93
1805 74 0.0847227  138 -733 732 -142 141 -101 93
1806 48 0.117208  138 -734 733 -142 141 -101 93
1807 74 0.0847227  138 -735 734 -142 141 -101 93
1808 48 0.117208  138 -736 735 -142 141 -101 93
1809 74 0.0847227  138 -737 736 -142 141 -101 93
1810 48 0.117208  138 -738 737 -142 141 -101 93
1811 74 0.0847227  138 -739 738 -142 141 -101 93
1812 48 0.117208  138 -740 739 -142 141 -101 93
1813 74 0.0847227  138 -741 740 -142 141 -101 93
1814 48 0.117208  138 -742 741 -142 141 -101 93
1815 74 0.0847227  138 -743 742 -142 141 -101 93
1816 48 0.117208  138 -744 743 -142 141 -101 93
1817 74 0.0847227  138 -745 744 -142 141 -101 93
1818 48 0.117208  138 -116 745 -142 141 -101 93
1819 48 0.117208  138 -732 113 -143 142 -101 93
1820 74 0.0847227  138 -733 732 -143 142 -101 93
1821 48 0.117208  138 -734 733 -143 142 -101 93
1822 74 0.0847227  138 -735 734 -143 142 -101 93
1823 48 0.117208  138 -736 735 -143 142 -101 93
1824 74 0.0847227  138 -737 736 -143 142 -101 93
1825 48 0.117208  138 -738 737 -143 142 -101 93
1826 74 0.0847227  138 -739 738 -143 142 -101 93
1827 48 0.117208  138 -740 739 -143 142 -101 93
1828 74 0.0847227  138 -741 740 -143 142 -101 93
1829 48 0.117208  138 -742 741 -143 142 -101 93
1830 74 0.0847227  138 -743 742 -143 142 -101 93
1831 48 0.117208  138 -744 743 -143 142 -101 93
1832 74 0.0847227  138 -745 744 -143 142 -101 93
1833 48 0.117208  138 -116 745 -143 142 -101 93
1834 48 0.117208  138 -732 113 -144 143 -101 93
1835 74 0.0847227  138 -733 732 -144 143 -101 93
1836 48 0.117208  138 -734 733 -144 143 -101 93
1837 74 0.0847227  138 -735 734 -144 143 -101 93
1838 48 0.117208  138 -736 735 -144 143 -101 93
1839 74 0.0847227  138 -737 736 -144 143 -101 93
1840 48 0.117208  138 -738 737 -144 143 -101 93
1841 74 0.0847227  138 -739 738 -144 143 -101 93
1842 48 0.117208  138 -740 739 -144 143 -101 93
1843 74 0.0847227  138 -741 740 -144 143 -101 93
1844 48 0.117208  138 -742 741 -144 143 -101 93
1845 74 0.0847227  138 -743 742 -144 143 -101 93
1846 48 0.117208  138 -744 743 -144 143 -101 93
1847 74 0.0847227  138 -745 744 -144 143 -101 93
1848 48 0.117208  138 -116 745 -144 143 -101 93
1849 48 0.117208  138 -732 113 -145 144 -101 93
1850 74 0.0847227  138 -733 732 -145 144 -101 93
1851 48 0.117208  138 -734 733 -145 144 -101 93
1852 74 0.0847227  138 -735 734 -145 144 -101 93
1853 48 0.117208  138 -736 735 -145 144 -101 93
1854 74 0.0847227  138 -737 736 -145 144 -101 93
1855 48 0.117208  138 -738 737 -145 144 -101 93
1856 74 0.0847227  138 -739 738 -145 144 -101 93
1857 48 0.117208  138 -740 739 -145 144 -101 93
1858 74 0.0847227  138 -741 740 -145 144 -101 93
1859 48 0.117208  138 -742 741 -145 144 -101 93
1860 74 0.0847227  138 -743 742 -145 144 -101 93
1861 48 0.117208  138 -744 743 -145 144 -101 93
1862 74 0.0847227  138 -745 744 -145 144 -101 93
1863 48 0.117208  138 -116 745 -145 144 -101 93
1864 48 0.117208  138 -732 113 -146 145 -101 93
1865 74 0.0847227  138 -733 732 -146 145 -101 93
1866 48 0.117208  138 -734 733 -146 145 -101 93
1867 74 0.0847227  138 -735 734 -146 145 -101 93
1868 48 0.117208  138 -736 735 -146 145 -101 93
1869 74 0.0847227  138 -737 736 -146 145 -101 93
1870 48 0.117208  138 -738 737 -146 145 -101 93
1871 74 0.0847227  138 -739 738 -146 145 -101 93
1872 48 0.117208  138 -740 739 -146 145 -101 93
1873 74 0.0847227  138 -741 740 -146 145 -101 93
1874 48 0.117208  138 -742 741 -146 145 -101 93
1875 74 0.0847227  138 -743 742 -146 145 -101 93
1876 48 0.117208  138 -744 743 -146 145 -101 93
1877 74 0.0847227  138 -745 744 -146 145 -101 93
1878 48 0.117208  138 -116 745 -146 145 -101 93
1879 48 0.117208  138 -732 113 -147 146 -101 93
1880 74 0.0847227  138 -733 732 -147 146 -101 93
1881 48 0.117208  138 -734 733 -147 146 -101 93
1882 74 0.0847227  138 -735 734 -147 146 -101 93
1883 48 0.117208  138 -736 735 -147 146 -101 93
1884 74 0.0847227  138 -737 736 -147 146 -101 93
1885 48 0.117208  138 -738 737 -147 146 -101 93
1886 74 0.0847227  138 -739 738 -147 146 -101 93
1887 48 0.117208  138 -740 739 -147 146 -101 93
1888 74 0.0847227  138 -741 740 -147 146 -101 93
1889 48 0.117208  138 -742 741 -147 146 -101 93
1890 74 0.0847227  138 -743 742 -147 146 -101 93
1891 48 0.117208  138 -744 743 -147 146 -101 93
1892 74 0.0847227  138 -745 744 -147 146 -101 93
1893 48 0.117208  138 -116 745 -147 146 -101 93
1894 48 0.117208  138 -732 113 -148 147 -101 93
1895 74 0.0847227  138 -733 732 -148 147 -101 93
1896 48 0.117208  138 -734 733 -148 147 -101 93
1897 74 0.0847227  138 -735 734 -148 147 -101 93
1898 48 0.117208  138 -736 735 -148 147 -101 93
1899 74 0.0847227  138 -737 736 -148 147 -101 93
1900 48 0.117208  138 -738 737 -148 147 -101 93
1901 74 0.0847227  138 -739 738 -148 147 -101 93
1902 48 0.117208  138 -740 739 -148 147 -101 93
1903 74 0.0847227  138 -741 740 -148 147 -101 93
1904 48 0.117208  138 -742 741 -148 147 -101 93
1905 74 0.0847227  138 -743 742 -148 147 -101 93
1906 48 0.117208  138 -744 743 -148 147 -101 93
1907 74 0.0847227  138 -745 744 -148 147 -101 93
1908 48 0.117208  138 -116 745 -148 147 -101 93
1909 48 0.117208  138 -732 113 -140 148 -101 93
1910 74 0.0847227  138 -733 732 -140 148 -101 93
1911 48 0.117208  138 -734 733 -140 148 -101 93
1912 74 0.0847227  138 -735 734 -140 148 -101 93
1913 48 0.117208  138 -736 735 -140 148 -101 93
1914 74 0.0847227  138 -737 736 -140 148 -101 93
1915 48 0.117208  138 -738 737 -140 148 -101 93
1916 74 0.0847227  138 -739 738 -140 148 -101 93
1917 48 0.117208  138 -740 739 -140 148 -101 93
1918 74 0.0847227  138 -741 740 -140 148 -101 93
1919 48 0.117208  138 -742 741 -140 148 -101 93
1920 74 0.0847227  138 -743 742 -140 148 -101 93
1921 48 0.117208  138 -744 743 -140 148 -101 93
1922 74 0.0847227  138 -745 744 -140 148 -101 93
1923 48 0.117208  138 -116 745 -140 148 -101 93
1924 0  138 ( 101 : -113 ) 139 -3 -116 83 -141 126 -101 94
1925 48 0.117208  138 ( 101 : -113 ) 139 -3 -116 83 -141 126 -727 101
1926 74 0.0847227  138 ( 101 : -113 ) 139 -3 -116 83 -141 126 -728 727
1927 48 0.117208  138 ( 101 : -113 ) 139 -3 -116 83 -141 126 -729 728
1928 74 0.0847227  138 ( 101 : -113 ) 139 -3 -116 83 -141 126 -730 729
1929 48 0.117208  138 ( 101 : -113 ) 139 -3 -116 83 -141 126 -88 730
1930 74 0.0847227  138 ( 101 : -113 ) 139 -3 -116 83 -141 126 -731 88
1931 48 0.117208  138 ( 101 : -113 ) 139 -3 -116 83 -141 126 -99 731
1932 0  138 ( 101 : -113 ) -3 -116 83 -142 141 -101 94
1933 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -142 141 -727 101
1934 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -142 141 -728 727
1935 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -142 141 -729 728
1936 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -142 141 -730 729
1937 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -142 141 -88 730
1938 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -142 141 -731 88
1939 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -142 141 -99 731
1940 0  138 ( 101 : -113 ) -3 -116 83 -143 142 -101 94
1941 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -143 142 -727 101
1942 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -143 142 -728 727
1943 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -143 142 -729 728
1944 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -143 142 -730 729
1945 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -143 142 -88 730
1946 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -143 142 -731 88
1947 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -143 142 -99 731
1948 0  138 ( 101 : -113 ) -3 -116 83 -144 143 -101 94
1949 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -144 143 -727 101
1950 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -144 143 -728 727
1951 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -144 143 -729 728
1952 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -144 143 -730 729
1953 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -144 143 -88 730
1954 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -144 143 -731 88
1955 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -144 143 -99 731
1956 0  138 ( 101 : -113 ) -3 -116 83 -145 144 -101 94
1957 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -145 144 -727 101
1958 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -145 144 -728 727
1959 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -145 144 -729 728
1960 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -145 144 -730 729
1961 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -145 144 -88 730
1962 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -145 144 -731 88
1963 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -145 144 -99 731
1964 0  138 ( 101 : -113 ) -3 -116 83 -146 145 -101 94
1965 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -146 145 -727 101
1966 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -146 145 -728 727
1967 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -146 145 -729 728
1968 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -146 145 -730 729
1969 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -146 145 -88 730
1970 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -146 145 -731 88
1971 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -146 145 -99 731
1972 0  138 ( 101 : -113 ) -3 -116 83 -147 146 -101 94
1973 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -147 146 -727 101
1974 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -147 146 -728 727
1975 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -147 146 -729 728
1976 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -147 146 -730 729
1977 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -147 146 -88 730
1978 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -147 146 -731 88
1979 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -147 146 -99 731
1980 0  138 ( 101 : -113 ) -3 -116 83 -148 147 -101 94
1981 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -148 147 -727 101
1982 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -148 147 -728 727
1983 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -148 147 -729 728
1984 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -148 147 -730 729
1985 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -148 147 -88 730
1986 74 0.0847227  138 ( 101 : -113 ) -3 -116 83 -148 147 -731 88
1987 48 0.117208  138 ( 101 : -113 ) -3 -116 83 -148 147 -99 731
1988 0  138 ( 101 : -113 ) ( 101 : -91 ) -3 -116 83 -140 148 -101 94
1989 48 0.117208  138 ( 101 : -113 ) ( 101 : -91 ) -3 -116 83 -140 148
        -727 101
1990 74 0.0847227  138 ( 101 : -113 ) ( 101 : -91 ) -3 -116 83 -140 148
        -728 727
1991 48 0.117208  138 ( 101 : -113 ) ( 101 : -91 ) -3 -116 83 -140 148
        -729 728
1992 74 0.0847227  138 ( 101 : -113 ) ( 101 : -91 ) -3 -116 83 -140 148
        -730 729
1993 48 0.117208  138 ( 101 : -113 ) ( 101 : -91 ) -3 -116 83 -140 148
        -88 730
1994 74 0.0847227  138 ( 101 : -113 ) ( 101 : -91 ) -3 -116 83 -140 148
        -731 88
1995 48 0.117208  138 ( 101 : -113 ) ( 101 : -91 ) -3 -116 83 -140 148
        -99 731
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++

c -------------------------------------------------------
c --------------- SURFACE CARDS -------------------------
c -------------------------------------------------------
1 so 20000
2 py 0.0
3 px 0.0
4 pz -37.1
5 pz 37.1
6 cz 34.3
7 pz -40.1
8 pz 40.1
9 p 0.0 -1 0.0 113.95
10 cy 11.5
11 p 0.0 -1 0.0 313.95
12 cy 15
13 cy 16
14 p 0.0 -1 0.0 441.45
15 cz 37.3
16 p 0.0 0.0 -1 8.1
17 cz 30.3
18 cz 30.6
19 p 0.0 0.0 -1 10.95
20 cz 30
21 p 0.0 0.0 -1 18.75
22 p 0.0 0.0 -1 19.05
23 p 0.0 0.0 -1 19.75
24 p 0.0 0.0 -1 20.05
25 p 0.0157073173 0.0 -0.999876632 11.6985566
26 p 0.0157073173 0.0 0.999876632 -17.7978041
27 p 0.866025404 0.5 0.0 -3.075
28 p -0.866025404 0.5 0.0 3.075
29 p 0.0157073173 0.0 -0.999876632 11.2985566
30 p 0.0157073173 0.0 0.999876632 -18.1978041
31 kz -11.7 4052.180695
32 kz -17.8 4052.180695
33 p -0.866025404 -0.5 0.0 -3.075
34 p 0.866025404 -0.5 0.0 3.075
35 kz -11.3 4052.180695
36 kz -18.2 4052.180695
37 p 0.0 0.0 -1 11.7
38 p 0.0 0.0 -1 17.8
39 p 0.0 0.0 -1 11.3
40 p 0.0 0.0 -1 18.2
41 pz 8.1
42 pz 10.95
43 pz 16.65
44 pz 16.95
45 cz 32.3
46 cz 32.6
47 pz 17.65
48 cz 32
49 pz 17.95
50 p 0.866025404 -0.5 0.0 -2.675
51 p -0.866025404 -0.5 0.0 2.675
52 kz 12.25 2712.396457
53 kz 15.15 1855.179573
54 p 0.866025404 -0.5 0.0 -2.975
55 p -0.866025404 -0.5 0.0 2.975
56 kz 11.95 2712.396457
57 kz 15.45 1855.179573
58 pz 12.25
59 pz 15.15
60 pz 11.95
61 pz 15.45
62 p -0.866025404 0.5 0.0 -2.675
63 p 0.866025404 0.5 0.0 2.675
64 p -0.0191974424 0.0 0.999815712 12.2477425
65 p -0.0232107944 0.0 -0.999730593 -15.1459185
66 p -0.866025404 0.5 0.0 -2.975
67 p 0.866025404 0.5 0.0 2.975
68 p -0.0191974424 0.0 0.999815712 11.9477425
69 p -0.0232107944 0.0 -0.999730593 -15.4459185
70 pz -41.3
71 pz 41.3
72 cz 37.5
73 pz -75
74 pz 75
75 cz 65
76 pz -200
77 pz 200
78 cz 200
79 pz -400
80 pz 400
81 cz 550
82 pz 406
83 cz 556
84 pz 214
85 cz 500
86 cz 506
87 pz 220
88 pz 290
89 pz 296
90 p 0.782608157 0.622514637 0.0 0.0
91 p 0.906307787 -0.422618262 0.0 0.0
92 p 0.207911691 -0.978147601 0.0 1.42108547e-14
93 pz -120
94 pz 150
95 cz 2450
96 p 0.906307787 -0.422618262 0.0 -80
97 p 0.207911691 -0.978147601 0.0 80
98 pz -220
99 pz 325
100 cz 2800
101 pz 170
102 p 0.863395551 -0.504527624 0.0 -1.13686838e-13
103 p 0.813100761 -0.58212297 0.0 0.0
104 p 0.755853469 -0.654740814 0.0 0.0
105 p 0.692143174 -0.721760228 0.0 5.68434189e-14
106 p 0.622514637 -0.782608157 0.0 0.0
107 p 0.547563223 -0.836764313 0.0 0.0
108 p 0.467929814 -0.88376563 0.0 2.84217094e-14
109 p 0.384295323 -0.923210217 0.0 0.0
110 p 0.297374874 -0.9547608 0.0 0.0
111 p 0.894934362 -0.446197813 0.0 0.0
112 p -0.906307787 -0.422618262 0.0 0.0
113 cz 1150
114 p 0.207911691 -0.978147601 0.0 -80
115 p -0.906307787 -0.422618262 0.0 80
116 cz 1500
117 p 0.0600806922 -0.998193524 0.0 0.0
118 p -0.0890874529 -0.996023808 0.0 0.0
119 p -0.236272882 -0.971686742 0.0 -1.42108547e-14
120 p -0.378199858 -0.925723969 0.0 -2.84217094e-14
121 p -0.511709678 -0.859158429 0.0 2.84217094e-14
122 p -0.633830967 -0.773471593 0.0 5.68434189e-14
123 p -0.741845814 -0.670570494 0.0 0.0
124 p -0.833350257 -0.552745284 0.0 0.0
125 p -0.782608157 0.622514637 0.0 0.0
126 p -0.207911691 -0.978147601 0.0 1.42108547e-14
127 p -0.906307787 -0.422618262 0.0 -80
128 p -0.207911691 -0.978147601 0.0 80
129 p -0.863395551 -0.504527624 0.0 -1.13686838e-13
130 p -0.813100761 -0.58212297 0.0 0.0
131 p -0.755853469 -0.654740814 0.0 0.0
132 p -0.692143174 -0.721760228 0.0 5.68434189e-14
133 p -0.622514637 -0.782608157 0.0 0.0
134 p -0.547563223 -0.836764313 0.0 0.0
135 p -0.467929814 -0.88376563 0.0 2.84217094e-14
136 p -0.384295323 -0.923210217 0.0 0.0
137 p -0.297374874 -0.9547608 0.0 0.0
138 p -0.894934362 -0.446197813 0.0 0.0
139 p -0.207911691 -0.978147601 0.0 -80
140 p 0.906307787 -0.422618262 0.0 80
141 p -0.0600806922 -0.998193524 0.0 0.0
142 p 0.0890874529 -0.996023808 0.0 0.0
143 p 0.236272882 -0.971686742 0.0 -1.42108547e-14
144 p 0.378199858 -0.925723969 0.0 -2.84217094e-14
145 p 0.511709678 -0.859158429 0.0 2.84217094e-14
146 p 0.633830967 -0.773471593 0.0 5.68434189e-14
147 p 0.741845814 -0.670570494 0.0 0.0
148 p 0.833350257 -0.552745284 0.0 0.0
149 pz 856
150 cz 526
151 pz 466
152 cz 586
153 cz 591
154 px 1006
155 pz 655
156 pz 855
157 p -1 0.0 0.0 1006
158 py 112.2
159 pz -4
160 pz 4
161 c/z 0 112.2 45
162 pz -4.1
163 pz 4.1
164 c/z 0 112.2 48
165 pz -4.3
166 pz 4.3
167 c/z 0 112.2 85
168 pz -4.8
169 pz 4.8
170 c/z 0 112.2 125
171 pz -7.8
172 pz 7.8
173 pz -3.3
174 pz 3.3
175 c/z 0 112.2 74.5
176 pz -3.8
177 pz 3.8
178 c/z 0 112.2 75
179 pz -5.8
180 pz 5.8
181 gq  6.01392e-05  6.01392e-05  0.00138  0  0  0  0  -0.01349523648
        0  -0.2429172335
182 gq  1  1  23.01803079  0  0  0  0  -224.4  0  -4090.892457
183 c/z 0 112.2 131.15
184 c/z 0 112.2 64.07
185 pz 435
186 c/z 0 112.2 5
187 c/z 0 112.2 13.5
188 c/z 0 112.2 14
189 c/z 0 112.2 20
190 c/z 0 112.2 23
191 c/z 0 112.2 25
192 pz -8.8
193 pz 8.8
194 c/z 0 112.2 66.07
195 pz -10.8
196 pz 15
197 c/z 0 112.2 50
198 pz 17
199 c/z 0 112.2 27.5
200 pz 12
201 c/z 0 112.2 29.5
202 pz 20
203 pz 24
204 pz 26
205 pz -35
206 c/z 0 112.2 42
207 pz -25
208 c/z 0 112.2 32
209 pz -15
210 c/z 0 112.2 22
211 pz -20
212 c/z 0 112.2 34
213 pz -24
214 c/z 0 112.2 36
215 c/z 0 82.2 1.5
216 c/z 0 82.2 1.7
217 c/z 5.20944533 82.65576741 1.5
218 c/z 5.20944533 82.65576741 1.7
219 c/z 10.2606043 84.00922138 1.5
220 c/z 10.2606043 84.00922138 1.7
221 c/z 15 86.21923789 1.5
222 c/z 15 86.21923789 1.7
223 c/z 19.28362829 89.21866671 1.5
224 c/z 19.28362829 89.21866671 1.7
225 c/z 22.98133329 92.91637171 1.5
226 c/z 22.98133329 92.91637171 1.7
227 c/z 25.98076211 97.2 1.5
228 c/z 25.98076211 97.2 1.7
229 c/z 28.19077862 101.9393957 1.5
230 c/z 28.19077862 101.9393957 1.7
231 c/z 29.54423259 106.9905547 1.5
232 c/z 29.54423259 106.9905547 1.7
233 c/z 30 112.2 1.5
234 c/z 30 112.2 1.7
235 c/z 29.54423259 117.4094453 1.5
236 c/z 29.54423259 117.4094453 1.7
237 c/z 28.19077862 122.4606043 1.5
238 c/z 28.19077862 122.4606043 1.7
239 c/z 25.98076211 127.2 1.5
240 c/z 25.98076211 127.2 1.7
241 c/z 22.98133329 131.4836283 1.5
242 c/z 22.98133329 131.4836283 1.7
243 c/z 19.28362829 135.1813333 1.5
244 c/z 19.28362829 135.1813333 1.7
245 c/z 15 138.1807621 1.5
246 c/z 15 138.1807621 1.7
247 c/z 10.2606043 140.3907786 1.5
248 c/z 10.2606043 140.3907786 1.7
249 c/z 5.20944533 141.7442326 1.5
250 c/z 5.20944533 141.7442326 1.7
251 c/z 3.673940397e-15 142.2 1.5
252 c/z 3.673940397e-15 142.2 1.7
253 c/z -5.20944533 141.7442326 1.5
254 c/z -5.20944533 141.7442326 1.7
255 c/z -10.2606043 140.3907786 1.5
256 c/z -10.2606043 140.3907786 1.7
257 c/z -15 138.1807621 1.5
258 c/z -15 138.1807621 1.7
259 c/z -19.28362829 135.1813333 1.5
260 c/z -19.28362829 135.1813333 1.7
261 c/z -22.98133329 131.4836283 1.5
262 c/z -22.98133329 131.4836283 1.7
263 c/z -25.98076211 127.2 1.5
264 c/z -25.98076211 127.2 1.7
265 c/z -28.19077862 122.4606043 1.5
266 c/z -28.19077862 122.4606043 1.7
267 c/z -29.54423259 117.4094453 1.5
268 c/z -29.54423259 117.4094453 1.7
269 c/z -30 112.2 1.5
270 c/z -30 112.2 1.7
271 c/z -29.54423259 106.9905547 1.5
272 c/z -29.54423259 106.9905547 1.7
273 c/z -28.19077862 101.9393957 1.5
274 c/z -28.19077862 101.9393957 1.7
275 c/z -25.98076211 97.2 1.5
276 c/z -25.98076211 97.2 1.7
277 c/z -22.98133329 92.91637171 1.5
278 c/z -22.98133329 92.91637171 1.7
279 c/z -19.28362829 89.21866671 1.5
280 c/z -19.28362829 89.21866671 1.7
281 c/z -15 86.21923789 1.5
282 c/z -15 86.21923789 1.7
283 c/z -10.2606043 84.00922138 1.5
284 c/z -10.2606043 84.00922138 1.7
285 c/z -5.20944533 82.65576741 1.5
286 c/z -5.20944533 82.65576741 1.7
287 p 0.984807753 0.173648178 0.0 19.4833255
288 p 0.939692621 0.342020143 0.0 38.3746601
289 p 0.866025404 0.5 0.0 56.1
290 p 0.766044443 0.64278761 0.0 72.1207698
291 p 0.64278761 0.766044443 0.0 85.9501865
292 p 0.5 0.866025404 0.0 97.1680503
293 p 0.342020143 0.939692621 0.0 105.433512
294 p 0.173648178 0.984807753 0.0 110.49543
295 p -0.173648178 0.984807753 0.0 110.49543
296 p -0.342020143 0.939692621 0.0 105.433512
297 p -0.5 0.866025404 0.0 97.1680503
298 p -0.64278761 0.766044443 0.0 85.9501865
299 p -0.766044443 0.64278761 0.0 72.1207698
300 p -0.866025404 0.5 0.0 56.1
301 p -0.939692621 0.342020143 0.0 38.3746601
302 p -0.984807753 0.173648178 0.0 19.4833255
303 pz -2.25
304 pz 2.25
305 p 0.987688341 0.156434465 0.0 17.551947
306 p 0.979924705 0.199367934 0.0 22.3690822
307 p 0.945518576 0.325568154 0.0 36.5287469
308 p 0.930417568 0.366501227 0.0 41.1214376
309 p 0.874619707 0.48480962 0.0 54.3956394
310 p 0.852640164 0.522498565 0.0 58.624339
311 p 0.777145961 0.629320391 0.0 70.6097479
312 p 0.748955721 0.662620048 0.0 74.3459694
313 p 0.656059029 0.75470958 0.0 84.6784149
314 p 0.622514637 0.782608157 0.0 87.8086352
315 p 0.515038075 0.857167301 0.0 96.1741711
316 p 0.47715876 0.878817113 0.0 98.60328
317 p 0.35836795 0.933580426 0.0 104.747724
318 p 0.317304656 0.948323655 0.0 106.401914
319 p 0.190808995 0.981627183 0.0 110.13857
320 p 0.147809411 0.989015863 0.0 110.96758
321 p 0.0174524064 0.999847695 0.0 112.182911
322 p -0.0261769483 0.999657325 0.0 112.161552
323 p -0.156434465 0.987688341 0.0 110.818632
324 p -0.199367934 0.979924705 0.0 109.947552
325 p -0.325568154 0.945518576 0.0 106.087184
326 p -0.366501227 0.930417568 0.0 104.392851
327 p -0.48480962 0.874619707 0.0 98.1323311
328 p -0.522498565 0.852640164 0.0 95.6662264
329 p -0.629320391 0.777145961 0.0 87.1957769
330 p -0.662620048 0.748955721 0.0 84.0328319
331 p -0.75470958 0.656059029 0.0 73.6098231
332 p -0.782608157 0.622514637 0.0 69.8461422
333 p -0.857167301 0.515038075 0.0 57.787272
334 p -0.878817113 0.47715876 0.0 53.5372129
335 p -0.933580426 0.35836795 0.0 40.2088839
336 p -0.948323655 0.317304656 0.0 35.6015824
337 p -0.981627183 0.190808995 0.0 21.4087693
338 p -0.989015863 0.147809411 0.0 16.5842159
339 p -0.999847695 0.0174524064 0.0 1.95816
340 p -0.999657325 -0.0261769483 0.0 -2.9370536
341 p 0.0 0.0 -1 18.55
342 p -0.687954311 -0.725753998 0.0 -0.399164699
343 py 14.2
344 p 0.687954311 -0.725753998 0.0 -0.399164699
345 p 0.0 0.0 -1 11.75
346 p 0.0 0.0 -1 17.75
347 c/z 6.874103886e-16 7.439386778 5
348 c/z -8.113623926 11.694 2.506
349 c/z 8.113623926 11.694 2.506
350 py 3.81061679
351 p 0.928911728 -0.37030123 0.0 -12.7951179
352 p -0.928911728 -0.37030123 0.0 -12.7951179
353 p -0.687954311 -0.725753998 0.0 -0.0991646991
354 py 14.5
355 p 0.687954311 -0.725753998 0.0 -0.0991646991
356 p 0.0 0.0 -1 11.45
357 p 0.0 0.0 -1 18.05
358 c/z 6.874103886e-16 7.439386778 5.3
359 c/z -8.113623926 11.694 2.806
360 c/z 8.113623926 11.694 2.806
361 py 3.59289059
362 p 0.928911728 -0.37030123 0.0 -12.9062083
363 p -0.928911728 -0.37030123 0.0 -12.9062083
364 p -0.687954311 -0.725753998 0.0 0.400835301
365 py 15
366 p 0.687954311 -0.725753998 0.0 0.400835301
367 c/z 6.874103886e-16 7.439386778 5.8
368 c/z -8.113623926 11.694 3.306
369 c/z 8.113623926 11.694 3.306
370 py 3.23001359
371 p 0.928911728 -0.37030123 0.0 -13.0913589
372 p -0.928911728 -0.37030123 0.0 -13.0913589
373 p -0.687954311 -0.725753998 0.0 0.700835301
374 py 15.3
375 p 0.687954311 -0.725753998 0.0 0.700835301
376 c/z 6.874103886e-16 7.439386778 6.1
377 c/z -8.113623926 11.694 3.606
378 c/z 8.113623926 11.694 3.606
379 py 3.01228739
380 p 0.928911728 -0.37030123 0.0 -13.2024492
381 p -0.928911728 -0.37030123 0.0 -13.2024492
382 p -0.301289819 0.953532614 0.0 9.20158972
383 p -0.301289819 -0.953532614 0.0 -9.20158972
384 p 0.687954311 0.725753998 0.0 -0.399164699
385 p 0.0 -1 0.0 14.2
386 p -0.687954311 0.725753998 0.0 -0.399164699
387 c/z 2.433211788e-16 -7.439386778 5
388 c/z 8.113623926 -11.694 2.506
389 c/z -8.113623926 -11.694 2.506
390 p 0.0 -1 0.0 3.81061679
391 p -0.928911728 0.37030123 0.0 -12.7951179
392 p 0.928911728 0.37030123 0.0 -12.7951179
393 p 0.687954311 0.725753998 0.0 -0.0991646991
394 p 0.0 -1 0.0 14.5
395 p -0.687954311 0.725753998 0.0 -0.0991646991
396 c/z 2.433211788e-16 -7.439386778 5.3
397 c/z 8.113623926 -11.694 2.806
398 c/z -8.113623926 -11.694 2.806
399 p 0.0 -1 0.0 3.59289059
400 p -0.928911728 0.37030123 0.0 -12.9062083
401 p 0.928911728 0.37030123 0.0 -12.9062083
402 p 0.687954311 0.725753998 0.0 0.400835301
403 p 0.0 -1 0.0 15
404 p -0.687954311 0.725753998 0.0 0.400835301
405 c/z 2.433211788e-16 -7.439386778 5.8
406 c/z 8.113623926 -11.694 3.306
407 c/z -8.113623926 -11.694 3.306
408 p 0.0 -1 0.0 3.23001359
409 p -0.928911728 0.37030123 0.0 -13.0913589
410 p 0.928911728 0.37030123 0.0 -13.0913589
411 p 0.687954311 0.725753998 0.0 0.700835301
412 p 0.0 -1 0.0 15.3
413 p -0.687954311 0.725753998 0.0 0.700835301
414 c/z 2.433211788e-16 -7.439386778 6.1
415 c/z 8.113623926 -11.694 3.606
416 c/z -8.113623926 -11.694 3.606
417 p 0.0 -1 0.0 3.01228739
418 p -0.928911728 0.37030123 0.0 -13.2024492
419 p 0.928911728 0.37030123 0.0 -13.2024492
420 p 0.301289819 -0.953532614 0.0 9.20158972
421 p 0.301289819 0.953532614 0.0 -9.20158972
422 p 0.707106781 0.707106781 0.0 3.27743993
423 p -0.707106781 0.707106781 0.0 -3.27743993
424 p 0.707106781 0.707106781 0.0 10.98
425 p 0.707106781 -0.707106781 0.0 10.98
426 p -0.707106781 -0.707106781 0.0 3.27743993
427 p 0.707106781 -0.707106781 0.0 -3.27743993
428 p -0.707106781 0.707106781 0.0 10.98
429 p -0.707106781 -0.707106781 0.0 10.98
430 p 0.707106781 0.707106781 0.0 3.47743993
431 p -0.707106781 0.707106781 0.0 -3.47743993
432 p 0.707106781 0.707106781 0.0 11.18
433 p 0.707106781 -0.707106781 0.0 11.18
434 p -0.707106781 -0.707106781 0.0 3.47743993
435 p 0.707106781 -0.707106781 0.0 -3.47743993
436 p -0.707106781 0.707106781 0.0 11.18
437 p -0.707106781 -0.707106781 0.0 11.18
438 p -1 0.0 0.0 -7.88
439 p -1 0.0 0.0 7.88
440 p -1 0.0 0.0 -8.227
441 p -1 0.0 0.0 8.227
442 p -0.866025404 0.5 0.0 3.07738587
443 p -0.866025404 -0.5 0.0 -3.07738587
444 p -0.866025404 0.5 0.0 2.77687505
445 p -0.866025404 -0.5 0.0 -2.77687505
446 p 0.866025404 -0.5 0.0 3.07738587
447 p 0.866025404 0.5 0.0 -3.07738587
448 p 0.866025404 -0.5 0.0 2.77687505
449 p 0.866025404 0.5 0.0 -2.77687505
450 pz 16.45
451 pz 12.05
452 pz 15.05
453 pz 11.75
454 pz 15.35
455 pz 11.25
456 pz 15.85
457 pz 15.65
458 pz 16.15
459 c/z -7.253105333 5.446532457 2
460 px -8.6673189
461 c/z -7.253105333 -5.446532457 2
462 c/z 7.253105333 -5.446532457 2
463 p -1 0.0 0.0 -8.6673189
464 c/z 7.253105333 5.446532457 2
465 c/z -7.253105333 5.446532457 2.2
466 px -8.8673189
467 c/z -7.253105333 -5.446532457 2.2
468 c/z 7.253105333 -5.446532457 2.2
469 p -1 0.0 0.0 -8.8673189
470 c/z 7.253105333 5.446532457 2.2
471 p 0.866025404 0.5 0.0 3.07651984
472 p 0.866025404 -0.5 0.0 -3.07651984
473 p 0.866025404 0.5 0.0 2.77600903
474 p 0.866025404 -0.5 0.0 -2.77600903
475 pz 11.7
476 pz 15.7
477 cz 10
478 kz 11.87857143 3136
479 kz 15.52142857 3136
480 pz 15.4
481 kz 12.17857143 3136
482 kz 15.22142857 3136
483 pz 11.8785714
484 pz 15.5214286
485 pz 12.1785714
486 pz 15.2214286
487 p 0.898794046 -0.438371147 0.0 -1.42108547e-14
488 p -0.898794046 -0.438371147 0.0 1.42108547e-14
489 pz -40
490 pz 40
491 pz -50
492 pz 50
493 cz 370
494 p 0.866025404 -0.5 0.0 -4.99237391
495 p 0.866025404 -0.5 0.0 15.0076261
496 pz 3.7
497 pz 25.7
498 p 0.866025404 -0.5 0.0 -4.39237391
499 p 0.866025404 -0.5 0.0 14.4076261
500 pz 24.9
501 p 0.866025404 -0.5 0.0 -8.99237391
502 p 0.866025404 -0.5 0.0 19.0076261
503 pz -8.3
504 pz 35.7
505 p 0.866025404 -0.5 0.0 -8.39237391
506 p 0.866025404 -0.5 0.0 18.4076261
507 pz -8.2
508 pz 34.9
509 p 0.866025404 -0.5 0.0 2.00762609
510 p 0.866025404 -0.5 0.0 8.00762609
511 pz 10.7
512 pz 16.7
513 p 0.81613759 -0.577857624 0.0 -5.85680662
514 p 0.81613759 -0.577857624 0.0 14.1431934
515 p 0.81613759 -0.577857624 0.0 -5.25680662
516 p 0.81613759 -0.577857624 0.0 13.5431934
517 p 0.81613759 -0.577857624 0.0 -9.85680662
518 p 0.81613759 -0.577857624 0.0 18.1431934
519 p 0.81613759 -0.577857624 0.0 -9.25680662
520 p 0.81613759 -0.577857624 0.0 17.5431934
521 p 0.81613759 -0.577857624 0.0 1.14319338
522 p 0.81613759 -0.577857624 0.0 7.14319338
523 p 0.743144825 -0.669130606 0.0 -6.99931633
524 p 0.743144825 -0.669130606 0.0 13.0006837
525 p 0.743144825 -0.669130606 0.0 -6.39931633
526 p 0.743144825 -0.669130606 0.0 12.4006837
527 p 0.743144825 -0.669130606 0.0 -10.9993163
528 p 0.743144825 -0.669130606 0.0 17.0006837
529 p 0.743144825 -0.669130606 0.0 -10.3993163
530 p 0.743144825 -0.669130606 0.0 16.4006837
531 p 0.743144825 -0.669130606 0.0 0.000683672411
532 p 0.743144825 -0.669130606 0.0 6.00068367
533 p 0.67815967 -0.734914595 0.0 -7.93291775
534 p 0.67815967 -0.734914595 0.0 12.0670822
535 p 0.67815967 -0.734914595 0.0 -7.33291775
536 p 0.67815967 -0.734914595 0.0 11.4670822
537 p 0.67815967 -0.734914595 0.0 -11.9329178
538 p 0.67815967 -0.734914595 0.0 16.0670822
539 p 0.67815967 -0.734914595 0.0 -11.3329178
540 p 0.67815967 -0.734914595 0.0 15.4670822
541 p 0.67815967 -0.734914595 0.0 -0.932917752
542 p 0.67815967 -0.734914595 0.0 5.06708225
543 p 0.587785252 -0.809016994 0.0 -9.13740302
544 p 0.587785252 -0.809016994 0.0 10.862597
545 p 0.587785252 -0.809016994 0.0 -8.53740302
546 p 0.587785252 -0.809016994 0.0 10.262597
547 p 0.587785252 -0.809016994 0.0 -13.137403
548 p 0.587785252 -0.809016994 0.0 14.862597
549 p 0.587785252 -0.809016994 0.0 -12.537403
550 p 0.587785252 -0.809016994 0.0 14.262597
551 p 0.587785252 -0.809016994 0.0 -2.13740302
552 p 0.587785252 -0.809016994 0.0 3.86259698
553 p 0.510542918 -0.859852272 0.0 -10.0993703
554 p 0.510542918 -0.859852272 0.0 9.9006297
555 p 0.510542918 -0.859852272 0.0 -9.4993703
556 p 0.510542918 -0.859852272 0.0 9.3006297
557 p 0.510542918 -0.859852272 0.0 -14.0993703
558 p 0.510542918 -0.859852272 0.0 13.9006297
559 p 0.510542918 -0.859852272 0.0 -13.4993703
560 p 0.510542918 -0.859852272 0.0 13.3006297
561 p 0.510542918 -0.859852272 0.0 -3.0993703
562 p 0.510542918 -0.859852272 0.0 2.9006297
563 p 0.406736643 -0.913545458 0.0 -11.3131893
564 p 0.406736643 -0.913545458 0.0 8.68681065
565 p 0.406736643 -0.913545458 0.0 -10.7131893
566 p 0.406736643 -0.913545458 0.0 8.08681065
567 p 0.406736643 -0.913545458 0.0 -15.3131893
568 p 0.406736643 -0.913545458 0.0 12.6868107
569 p 0.406736643 -0.913545458 0.0 -14.7131893
570 p 0.406736643 -0.913545458 0.0 12.0868107
571 p 0.406736643 -0.913545458 0.0 -4.31318935
572 p 0.406736643 -0.913545458 0.0 1.68681065
573 p 0.320612991 -0.947210278 0.0 -12.2614799
574 p 0.320612991 -0.947210278 0.0 7.73852012
575 p 0.320612991 -0.947210278 0.0 -11.6614799
576 p 0.320612991 -0.947210278 0.0 7.13852012
577 p 0.320612991 -0.947210278 0.0 -16.2614799
578 p 0.320612991 -0.947210278 0.0 11.7385201
579 p 0.320612991 -0.947210278 0.0 -15.6614799
580 p 0.320612991 -0.947210278 0.0 11.1385201
581 p 0.320612991 -0.947210278 0.0 -5.26147988
582 p 0.320612991 -0.947210278 0.0 0.738520116
583 p 0.207911691 -0.978147601 0.0 -13.431583
584 p 0.207911691 -0.978147601 0.0 6.568417
585 p 0.207911691 -0.978147601 0.0 -12.831583
586 p 0.207911691 -0.978147601 0.0 5.968417
587 p 0.207911691 -0.978147601 0.0 -17.431583
588 p 0.207911691 -0.978147601 0.0 10.568417
589 p 0.207911691 -0.978147601 0.0 -16.831583
590 p 0.207911691 -0.978147601 0.0 9.968417
591 p 0.207911691 -0.978147601 0.0 -6.431583
592 p 0.207911691 -0.978147601 0.0 -0.431582996
593 p 0.116670737 -0.99317065 0.0 -14.3247519
594 p 0.116670737 -0.99317065 0.0 5.67524805
595 p 0.116670737 -0.99317065 0.0 -13.7247519
596 p 0.116670737 -0.99317065 0.0 5.07524805
597 p 0.116670737 -0.99317065 0.0 -18.3247519
598 p 0.116670737 -0.99317065 0.0 9.67524805
599 p 0.116670737 -0.99317065 0.0 -17.7247519
600 p 0.116670737 -0.99317065 0.0 9.07524805
601 p 0.116670737 -0.99317065 0.0 -7.32475195
602 p 0.116670737 -0.99317065 0.0 -1.32475195
603 p 0.0 -1 0.0 -4.6
604 p 0.0 -1 0.0 15.4
605 p 0.0 -1 0.0 -4
606 p 0.0 -1 0.0 14.8
607 p 0.0 -1 0.0 -8.6
608 p 0.0 -1 0.0 19.4
609 p 0.0 -1 0.0 -8
610 p 0.0 -1 0.0 18.8
611 p 0.0 -1 0.0 2.4
612 p 0.0 -1 0.0 8.4
613 p -0.104528463 -0.994521895 0.0 -5.55988509
614 p -0.104528463 -0.994521895 0.0 14.4401149
615 p -0.104528463 -0.994521895 0.0 -4.95988509
616 p -0.104528463 -0.994521895 0.0 13.8401149
617 p -0.104528463 -0.994521895 0.0 -9.55988509
618 p -0.104528463 -0.994521895 0.0 18.4401149
619 p -0.104528463 -0.994521895 0.0 -8.95988509
620 p -0.104528463 -0.994521895 0.0 17.8401149
621 p -0.104528463 -0.994521895 0.0 1.44011491
622 p -0.104528463 -0.994521895 0.0 7.44011491
623 p -0.207911691 -0.978147601 0.0 -6.568417
624 p -0.207911691 -0.978147601 0.0 13.431583
625 p -0.207911691 -0.978147601 0.0 -5.968417
626 p -0.207911691 -0.978147601 0.0 12.831583
627 p -0.207911691 -0.978147601 0.0 -10.568417
628 p -0.207911691 -0.978147601 0.0 17.431583
629 p -0.207911691 -0.978147601 0.0 -9.968417
630 p -0.207911691 -0.978147601 0.0 16.831583
631 p -0.207911691 -0.978147601 0.0 0.431582996
632 p -0.207911691 -0.978147601 0.0 6.431583
633 p -0.309016994 -0.951056516 0.0 -7.61454606
634 p -0.309016994 -0.951056516 0.0 12.3854539
635 p -0.309016994 -0.951056516 0.0 -7.01454606
636 p -0.309016994 -0.951056516 0.0 11.7854539
637 p -0.309016994 -0.951056516 0.0 -11.6145461
638 p -0.309016994 -0.951056516 0.0 16.3854539
639 p -0.309016994 -0.951056516 0.0 -11.0145461
640 p -0.309016994 -0.951056516 0.0 15.7854539
641 p -0.309016994 -0.951056516 0.0 -0.614546062
642 p -0.309016994 -0.951056516 0.0 5.38545394
643 p -0.406736643 -0.913545458 0.0 -8.68681065
644 p -0.406736643 -0.913545458 0.0 11.3131893
645 p -0.406736643 -0.913545458 0.0 -8.08681065
646 p -0.406736643 -0.913545458 0.0 10.7131893
647 p -0.406736643 -0.913545458 0.0 -12.6868107
648 p -0.406736643 -0.913545458 0.0 15.3131893
649 p -0.406736643 -0.913545458 0.0 -12.0868107
650 p -0.406736643 -0.913545458 0.0 14.7131893
651 p -0.406736643 -0.913545458 0.0 -1.68681065
652 p -0.406736643 -0.913545458 0.0 4.31318935
653 p -0.5 -0.866025404 0.0 -9.77346282
654 p -0.5 -0.866025404 0.0 10.2265372
655 p -0.5 -0.866025404 0.0 -9.17346282
656 p -0.5 -0.866025404 0.0 9.62653718
657 p -0.5 -0.866025404 0.0 -13.7734628
658 p -0.5 -0.866025404 0.0 14.2265372
659 p -0.5 -0.866025404 0.0 -13.1734628
660 p -0.5 -0.866025404 0.0 13.6265372
661 p -0.5 -0.866025404 0.0 -2.77346282
662 p -0.5 -0.866025404 0.0 3.22653718
663 p -0.587785252 -0.809016994 0.0 -10.862597
664 p -0.587785252 -0.809016994 0.0 9.13740302
665 p -0.587785252 -0.809016994 0.0 -10.262597
666 p -0.587785252 -0.809016994 0.0 8.53740302
667 p -0.587785252 -0.809016994 0.0 -14.862597
668 p -0.587785252 -0.809016994 0.0 13.137403
669 p -0.587785252 -0.809016994 0.0 -14.262597
670 p -0.587785252 -0.809016994 0.0 12.537403
671 p -0.587785252 -0.809016994 0.0 -3.86259698
672 p -0.587785252 -0.809016994 0.0 2.13740302
673 p -0.669130606 -0.743144825 0.0 -11.9422803
674 p -0.669130606 -0.743144825 0.0 8.05771966
675 p -0.669130606 -0.743144825 0.0 -11.3422803
676 p -0.669130606 -0.743144825 0.0 7.45771966
677 p -0.669130606 -0.743144825 0.0 -15.9422803
678 p -0.669130606 -0.743144825 0.0 12.0577197
679 p -0.669130606 -0.743144825 0.0 -15.3422803
680 p -0.669130606 -0.743144825 0.0 11.4577197
681 p -0.669130606 -0.743144825 0.0 -4.94228034
682 p -0.669130606 -0.743144825 0.0 1.05771966
683 p -0.743144825 -0.669130606 0.0 -13.0006837
684 p -0.743144825 -0.669130606 0.0 6.99931633
685 p -0.743144825 -0.669130606 0.0 -12.4006837
686 p -0.743144825 -0.669130606 0.0 6.39931633
687 p -0.743144825 -0.669130606 0.0 -17.0006837
688 p -0.743144825 -0.669130606 0.0 10.9993163
689 p -0.743144825 -0.669130606 0.0 -16.4006837
690 p -0.743144825 -0.669130606 0.0 10.3993163
691 p -0.743144825 -0.669130606 0.0 -6.00068367
692 p -0.743144825 -0.669130606 0.0 -0.000683672411
693 p -0.809016994 -0.587785252 0.0 -14.0262109
694 p -0.809016994 -0.587785252 0.0 5.97378911
695 p -0.809016994 -0.587785252 0.0 -13.4262109
696 p -0.809016994 -0.587785252 0.0 5.37378911
697 p -0.809016994 -0.587785252 0.0 -18.0262109
698 p -0.809016994 -0.587785252 0.0 9.97378911
699 p -0.809016994 -0.587785252 0.0 -17.4262109
700 p -0.809016994 -0.587785252 0.0 9.37378911
701 p -0.809016994 -0.587785252 0.0 -7.02621089
702 p -0.809016994 -0.587785252 0.0 -1.02621089
703 p -0.866025404 -0.5 0.0 -15.0076261
704 p -0.866025404 -0.5 0.0 4.99237391
705 p -0.866025404 -0.5 0.0 -14.4076261
706 p -0.866025404 -0.5 0.0 4.39237391
707 p -0.866025404 -0.5 0.0 -19.0076261
708 p -0.866025404 -0.5 0.0 8.99237391
709 p -0.866025404 -0.5 0.0 -18.4076261
710 p -0.866025404 -0.5 0.0 8.39237391
711 p -0.866025404 -0.5 0.0 -8.00762609
712 p -0.866025404 -0.5 0.0 -2.00762609
713 cz 2495
714 cz 2510
715 cz 2525
716 cz 2555
717 cz 2570
718 cz 2590
719 cz 2605
720 cz 2625
721 cz 2655
722 cz 2670
723 cz 2701
724 cz 2716.5
725 cz 2742
726 cz 2757.5
727 pz 180
728 pz 185
729 pz 205
730 pz 250
731 pz 315
732 cz 1195
733 cz 1210
734 cz 1225
735 cz 1255
736 cz 1270
737 cz 1290
738 cz 1305
739 cz 1325
740 cz 1355
741 cz 1370
742 cz 1401
743 cz 1416.5
744 cz 1442
745 cz 1457.5
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++

c -------------------------------------------------------
c --------------- MATERIAL CARDS ------------------------
c -------------------------------------------------------
c c
c Material : Stainless304 rho=0.0878729
c           (rho=7.96703
c         g/cc)
m3      6000.70c 0.00031864 14028.70c 0.00170336 15031.70c 6.95038e-05
        16032.70c 4.4752e-05 24000.50c 0.0174813 25055.70c 0.00174159
        26054.70c 0.00338 26056.70c 0.053455 26057.70c 0.00128218
        27059.70c 0.0002453 28000.50c 0.00815128 hlib=.70h  pnlib=70u
mx3:h 6012.70h j j j j j j j j j j
c c
c Material : Aluminium rho=0.0582256
c           (rho=2.64651 g/cc)
m5      13027.70c 0.054381 12024.70c 0.00207 12025.70c 0.0002621
        12026.70c 0.0002885 14028.70c 0.000217 22048.70c 8.7e-05
        24052.70c 7.8e-05 24053.70c 1e-05 25055.70c 0.000408 26056.70c
        0.000214 26057.70c 5e-06 29063.70c 4e-05 29065.70c 2e-05
        30000.70c 0.000145 hlib=.70h  pnlib=70u
mt5     al27.10t
c c
c Material : helium rho=2.45e-05
c           (rho=0.000162839 g/cc)
m9      2004.70c 2.45e-05 hlib=.70h  pnlib=70u
c c
c Material : H2O rho=0.100283
c           (rho=0.99975 g/cc)
m11     1001.70c 0.0668456 1002.70c 1.003e-05 8016.70c 0.0334278
        hlib=.70h  pnlib=70u
mt11    lwtr.01t
c c
c Material : Poly rho=0.117208
c           (rho=0.91 g/cc)
m48     6000.70c 0.0390694 1001.70c 0.078139 hlib=.70h  pnlib=70u
mx48:h 6012.70h j
mt48    poly.01t
c c
c Material : Concrete rho=0.074981
c           (rho=2.33578 g/cc)
m49     1001.70c 0.00776555 1002.70c 1.16501e-06 8016.70c 0.0438499
        11023.70c 0.00104778 12000.60c 0.000148662 13027.70c 0.00238815
        14028.70c 0.0158026 16032.70c 5.63433e-05 19000.60c 0.000693104
        20000.60c 0.00291501 26054.70c 1.84504e-05 26056.70c
        0.000286826 26057.70c 6.5671e-06 26058.70c 8.75613e-07
        hlib=.70h  pnlib=70u
mt49    lwtr.01t
c c
c Material : CastIron rho=0.0833854
c           (rho=7.03049 g/cc)
m54     6000.70c 0.00636 14028.70c 0.0051404 25055.70c 0.000386
        26054.70c 0.00419 26056.70c 0.065789 26057.70c 0.00152
        hlib=.70h  pnlib=70u
mx54:h 6012.70h j j j j j
c c
c Material : Steel71 rho=0.0843223
c           (rho=7.79995 g/cc)
m71     6000.70c 0.000101187 14028.70c 0.000295128 24000.50c
        0.000168645 25055.70c 0.000505934 26054.70c 0.0048414 26056.70c
        0.0759996 26057.70c 0.00175516 26058.70c 0.00023358 28058.70c
        0.000172218 28060.70c 6.6335e-05 28061.70c 2.88126e-06
        28062.70c 9.19272e-06 28064.70c 2.33992e-06 29063.70c
        0.000116651 29065.70c 5.19931e-05 hlib=.70h  pnlib=70u
mx71:h 6012.70h j j j j j j j j j j j j j j
c c
c Material : ChipIRSteel rho=0.0847227
c           (rho=7.79997 g/cc)
m74     5010.70c 3.45922e-06 5011.70c 1.39238e-05 6000.70c 0.000469403
        7014.70c 3.01962e-05 13027.70c 0.000217659 14028.70c
        0.000167282 25055.70c 0.000342073 15031.70c 7.58418e-05
        16032.70c 5.56352e-05 16033.70c 4.4541e-07 16034.70c
        2.51422e-06 16036.70c 1.17213e-08 26054.70c 0.00484959
        26056.70c 0.0761283 26057.70c 0.00175813 26058.70c 0.000233975
        27059.70c 1.59442e-05 29063.70c 7.67107e-05 29065.70c
        3.4191e-05 28058.70c 5.45039e-05 28060.70c 2.09938e-05
        28061.70c 9.11867e-07 28062.70c 2.90933e-06 28064.70c
        7.40542e-07 24050.70c 3.92601e-06 24052.70c 7.57092e-05
        24053.70c 8.58481e-06 24054.70c 2.13694e-06 42092.70c
        7.26785e-06 42094.70c 4.53016e-06 42095.70c 7.79678e-06
        42096.70c 8.16899e-06 42097.70c 4.67709e-06 42098.70c
        1.18176e-05 42100.70c 4.71627e-06 41093.70c 5.05693e-06
        23051.42c 9.22231e-06 50112.70c 4.05881e-08 50114.70c
        2.76167e-08 50115.70c 1.42268e-08 50116.70c 6.08404e-07
        50117.70c 3.21358e-07 50118.70c 1.01345e-06 50119.70c
        3.59435e-07 50120.70c 1.36326e-06 50122.70c 1.93735e-07
        22046.70c 8.09753e-07 22047.70c 7.3025e-07 22048.70c
        7.23575e-06 22049.70c 5.31002e-07 22050.70c 5.08427e-07
        hlib=.70h  pnlib=70u
mx74:h j j 6012.70h j j j j j j j j j j j j j j j j j j j j j j j j j j
        j j j j j j j j j j j j j j j j j j j j j j
c c
c Material : Be5H2O rho=0.122325
c           (rho=1.80555 g/cc)
m118     1001.70c 0.00334228 4009.70c 0.117311 8016.70c 0.00167114
        hlib=.70h  pnlib=70u
mx118:h j model j
mt118    be.60t lwtr.01t
c c
c Material : Tungsten_15.1g rho=0.0494621
c           (rho=15.1 g/cc)
m120     74182.70c 0.0131232 74183.70c 0.00708653 74184.70c 0.0151734
        74186.70c 0.014079 hlib=.70h  pnlib=70u
c c
c Material : Iron_10H2O rho=0.0862739
c           (rho=7.17 g/cc)
m121     1001.70c 0.00668924 8016.70c 0.00334462 26054.70c 0.00445623
        26056.70c 0.0699533 26057.70c 0.00161553 26058.70c 0.000214997
        hlib=.70h  pnlib=70u
mt121    lwtr.01t
c c
c Material : ParaOrtho%99.5 rho=0.041975
c           (rho=0.071299
c         g/cc)
m143     1001.70c 0.0417651 1004.70c 0.000209875 hlib=.70h  pnlib=70u
mt143    hpara.10t hortho.10t
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -------------------------------------------------------
c --------------- TRANSFORM CARDS -----------------------
c -------------------------------------------------------
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -------------------------------------------------------
c --------------- WEIGHT CARDS --------------------------
c -------------------------------------------------------
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -----------------------------------------------------------
c ------------------- TALLY CARDS ---------------------------
c -----------------------------------------------------------
c -------------------------------------------------------
c --------------- SOURCE CARDS --------------------------
c -------------------------------------------------------
sdef ara=44.8 axs=0.0 1 0.0 dir=1 erg=2000 par=9 vec=0.0 1 0.0 x=d1
        y=-30 z=d2
si1 H -7 7
sp1 D 0 1
si2 H -1.6 1.6
sp2 D 0 1
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -------------------------------------------------------
c --------------- PHYSICS CARDS --------------------------
c -------------------------------------------------------
nps 10000
RAND  SEED=375642321
mode n p h / d t s a z * k ?
vol 1 1994r
imp:n 0 0 1 1992r
imp:p 0 0 1 1992r
imp:h 0 0 1 1992r
imp:/ 0 0 1 1992r
imp:d 0 0 1 1992r
imp:t 0 0 1 1992r
imp:s 0 0 1 1992r
imp:a 0 0 1 1992r
imp:z 0 0 1 1992r
imp:* 0 0 1 1992r
imp:k 0 0 1 1992r
imp:? 0 0 1 1992r
cut:n 1e+08 0 0.4 -0.1
cut:h,/,d,t,s,a,z,/,*,k,? 1e+08 0 0.5 0.25
cut:p 1e+08 0.001 0.5 0.25
phys:n 2000 0
phys:p 100
phys:h 2000
phys:/,d,t,s,a,z,/,*,k,? 2000
lca 2 1 1 0023 1 1 0 1 1 0
lea 1 4 1 0 1 0 0 1
prdmp 1e7 1e7 0 2 1e7
print 10 20 40 50 110 120
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++

