Input File:
c RunCmd:-r -defaultConfig Single SKADI -angle objAxis skadiAxis 0 -v
c         skadiStopPoint 0 -lowMod None -lowPipe None skadi 
c ---------- VERSION NUMBER ------------------
c  ===Git:  ====== 
c  ========= 1 ========== 
c ----------------------------------------------
c --------------- VARIABLE CARDS ---------------
c ----------------------------------------------
c ABHighBayHeight 330
c ABHighBayLength 450
c ABHighBayRoofMat Concrete
c ABHighBayThick 200
c ABHighBayWallMat Concrete
c ABunkerFloorDepth 120
c ABunkerFloorThick 100
c ABunkerLeftAngle 0
c ABunkerLeftPhase -65
c ABunkerNLayers 1
c ABunkerNSectors 10
c ABunkerNSegment 5
c ABunkerNSide 5
c ABunkerNSideThick 5
c ABunkerNSideVert 5
c ABunkerNVert 1
c ABunkerRightAngle 0
c ABunkerRightPhase -12
c ABunkerRoofActive 0
c ABunkerRoofHeight 150
c ABunkerRoofMat Aluminium
c ABunkerRoofMat0 Void
c ABunkerRoofMat1 Poly
c ABunkerRoofMat2 ChipIRSteel
c ABunkerRoofMat3 Poly
c ABunkerRoofMat4 ChipIRSteel
c ABunkerRoofMat5 Poly
c ABunkerRoofMat6 ChipIRSteel
c ABunkerRoofMat7 Poly
c ABunkerRoofNBasicVert 8
c ABunkerRoofThick 175
c ABunkerRoofVert1 20
c ABunkerRoofVert2 10
c ABunkerRoofVert3 5
c ABunkerRoofVert4 20
c ABunkerRoofVert5 45
c ABunkerRoofVert6 40
c ABunkerRoofVert7 25
c ABunkerSideThick 80
c ABunkerVoidMat Void
c ABunkerWallActive 0
c ABunkerWallHeight 170
c ABunkerWallLen1 45
c ABunkerWallLen10 15
c ABunkerWallLen11 31
c ABunkerWallLen12 15.5
c ABunkerWallLen13 25.5
c ABunkerWallLen14 15.5
c ABunkerWallLen2 15
c ABunkerWallLen3 15
c ABunkerWallLen4 30
c ABunkerWallLen5 15
c ABunkerWallLen6 20
c ABunkerWallLen7 15
c ABunkerWallLen8 20
c ABunkerWallLen9 30
c ABunkerWallMat Steel71
c ABunkerWallMat0 Poly
c ABunkerWallMat1 ChipIRSteel
c ABunkerWallMat10 Poly
c ABunkerWallMat11 ChipIRSteel
c ABunkerWallMat12 Poly
c ABunkerWallMat13 ChipIRSteel
c ABunkerWallMat14 Poly
c ABunkerWallMat2 Poly
c ABunkerWallMat3 ChipIRSteel
c ABunkerWallMat4 Poly
c ABunkerWallMat5 ChipIRSteel
c ABunkerWallMat6 Poly
c ABunkerWallMat7 ChipIRSteel
c ABunkerWallMat8 Poly
c ABunkerWallMat9 ChipIRSteel
c ABunkerWallNBasic 15
c ABunkerWallRadius 2450
c ABunkerWallThick 350
c BBunkerFloorDepth 120
c BBunkerFloorThick 100
c BBunkerLeftAngle 0
c BBunkerLeftPhase -12
c BBunkerNLayers 1
c BBunkerNSectors 9
c BBunkerNSide 5
c BBunkerNSideThick 5
c BBunkerNSideVert 5
c BBunkerNVert 1
c BBunkerRightAngle 0
c BBunkerRightPhase 65
c BBunkerRoofHeight 150
c BBunkerRoofMat Aluminium
c BBunkerRoofMat0 Void
c BBunkerRoofMat1 Poly
c BBunkerRoofMat2 ChipIRSteel
c BBunkerRoofMat3 Poly
c BBunkerRoofMat4 ChipIRSteel
c BBunkerRoofMat5 Poly
c BBunkerRoofMat6 ChipIRSteel
c BBunkerRoofMat7 Poly
c BBunkerRoofNBasicVert 8
c BBunkerRoofThick 175
c BBunkerRoofVert1 20
c BBunkerRoofVert2 10
c BBunkerRoofVert3 5
c BBunkerRoofVert4 20
c BBunkerRoofVert5 45
c BBunkerRoofVert6 40
c BBunkerRoofVert7 25
c BBunkerSideThick 80
c BBunkerVoidMat Void
c BBunkerWallActive 0
c BBunkerWallHeight 170
c BBunkerWallLen1 45
c BBunkerWallLen10 15
c BBunkerWallLen11 31
c BBunkerWallLen12 15.5
c BBunkerWallLen13 25.5
c BBunkerWallLen14 15.5
c BBunkerWallLen2 15
c BBunkerWallLen3 15
c BBunkerWallLen4 30
c BBunkerWallLen5 15
c BBunkerWallLen6 20
c BBunkerWallLen7 15
c BBunkerWallLen8 20
c BBunkerWallLen9 30
c BBunkerWallMat Steel71
c BBunkerWallMat0 Poly
c BBunkerWallMat1 ChipIRSteel
c BBunkerWallMat10 Poly
c BBunkerWallMat11 ChipIRSteel
c BBunkerWallMat12 Poly
c BBunkerWallMat13 ChipIRSteel
c BBunkerWallMat14 Poly
c BBunkerWallMat2 Poly
c BBunkerWallMat3 ChipIRSteel
c BBunkerWallMat4 Poly
c BBunkerWallMat5 ChipIRSteel
c BBunkerWallMat6 Poly
c BBunkerWallMat7 ChipIRSteel
c BBunkerWallMat8 Poly
c BBunkerWallMat9 ChipIRSteel
c BBunkerWallNBasic 15
c BBunkerWallRadius 1150
c BBunkerWallThick 350
c BeRefHeight 74.2
c BeRefLowRefMat Be5H2O
c BeRefLowVoidThick 2.3
c BeRefLowWallMat Stainless304
c BeRefRadius 34.3
c BeRefTargSepMat Void
c BeRefTopRefMat Be5H2O
c BeRefTopWallMat Stainless304
c BeRefWallThick 3
c BeRefWallThickLow 0
c BeRefXStep 0
c BeRefXYAngle 0
c BeRefYStep 0
c BeRefZAngle 0
c BeRefZStep 0
c BilbaoWheelAspectRatio 0.00138
c BilbaoWheelCaseRadius 129.15
c BilbaoWheelCaseThick 1
c BilbaoWheelCaseThickIn 3
c BilbaoWheelCatcherHeight 10
c BilbaoWheelCatcherMiddleHeight 10
c BilbaoWheelCatcherMiddleRadius 32
c BilbaoWheelCatcherNotchDepth 5
c BilbaoWheelCatcherNotchRadius 22
c BilbaoWheelCatcherRadius 42
c BilbaoWheelCatcherRingDepth 24
c BilbaoWheelCatcherRingRadius 34
c BilbaoWheelCatcherRingThick 2
c BilbaoWheelCatcherTopSteelThick 2
c BilbaoWheelCirclePipesBigRad 30
c BilbaoWheelCirclePipesRad 1.5
c BilbaoWheelCirclePipesWallThick 0.2
c BilbaoWheelCoolantRadiusIn 64.07
c BilbaoWheelCoolantRadiusOut 128.95
c BilbaoWheelCoolantThick 0.5
c BilbaoWheelHeMat Helium
c BilbaoWheelInnerHoleHeight 4.5
c BilbaoWheelInnerHoleSize 0.25
c BilbaoWheelInnerHoleXYangle -1
c BilbaoWheelInnerMat SS316L785
c BilbaoWheelInnerRadius 45
c BilbaoWheelMatTYPE1 1
c BilbaoWheelMatTYPE2 1
c BilbaoWheelMatTYPE3 3
c BilbaoWheelNLayers 3
c BilbaoWheelNSectors 36
c BilbaoWheelNShaftLayers 6
c BilbaoWheelRadius1 48
c BilbaoWheelRadius2 85
c BilbaoWheelRadius3 125
c BilbaoWheelSS316LVoidMat M2644
c BilbaoWheelShaft2StepConnectionDist 5
c BilbaoWheelShaft2StepConnectionHeight 4
c BilbaoWheelShaft2StepConnectionRadius 27.5
c BilbaoWheelShaft2StepHeight 15
c BilbaoWheelShaftBaseDepth 35
c BilbaoWheelShaftHeight 435
c BilbaoWheelShaftHoleHeight 4.5
c BilbaoWheelShaftHoleSize 0.25
c BilbaoWheelShaftHoleXYangle -1
c BilbaoWheelShaftMat1 SS316L
c BilbaoWheelShaftMat2 SS316L
c BilbaoWheelShaftMat3 SS316L
c BilbaoWheelShaftMat4 SS316L
c BilbaoWheelShaftMat5 SS316L
c BilbaoWheelShaftMat6 Void
c BilbaoWheelShaftRadius1 5
c BilbaoWheelShaftRadius2 13.5
c BilbaoWheelShaftRadius3 14
c BilbaoWheelShaftRadius4 20
c BilbaoWheelShaftRadius5 23
c BilbaoWheelShaftRadius6 25
c BilbaoWheelSteelMat SS316L785
c BilbaoWheelSteelTungstenInnerThick 0.5
c BilbaoWheelSteelTungstenThick 0.2
c BilbaoWheelTargetHeight 8
c BilbaoWheelTargetInnerHeight 6.6
c BilbaoWheelTargetInnerHeightRadius 74.5
c BilbaoWheelTemp 600
c BilbaoWheelVoidRadius 131.15
c BilbaoWheelVoidThick 2
c BilbaoWheelVoidTungstenThick 0.1
c BilbaoWheelWMat Tungsten151
c BilbaoWheelXStep 0
c BilbaoWheelYStep 112.2
c BilbaoWheelZStep 0
c BulkDepth1 41.3
c BulkDepth2 75
c BulkDepth3 200
c BulkHeight1 41.3
c BulkHeight2 75
c BulkHeight3 200
c BulkMat1 Void
c BulkMat2 Iron10H2O
c BulkMat3 Iron10H2O
c BulkNLayer 3
c BulkRadius1 37.5
c BulkRadius2 65
c BulkRadius3 200
c BulkXStep 0
c BulkXYAngle 0
c BulkYStep 0
c BulkZAngle 0
c BulkZStep 0
c CBunkerFloorDepth 120
c CBunkerFloorThick 100
c CBunkerLeftAngle 0
c CBunkerLeftPhase -65
c CBunkerNLayers 1
c CBunkerNSectors 10
c CBunkerNSegment 5
c CBunkerNSide 5
c CBunkerNSideThick 5
c CBunkerNSideVert 5
c CBunkerNVert 1
c CBunkerRightAngle 0
c CBunkerRightPhase -12
c CBunkerRoofActive 0
c CBunkerRoofHeight 150
c CBunkerRoofMat Aluminium
c CBunkerRoofMat0 Void
c CBunkerRoofMat1 Poly
c CBunkerRoofMat2 ChipIRSteel
c CBunkerRoofMat3 Poly
c CBunkerRoofMat4 ChipIRSteel
c CBunkerRoofMat5 Poly
c CBunkerRoofMat6 ChipIRSteel
c CBunkerRoofMat7 Poly
c CBunkerRoofNBasicVert 8
c CBunkerRoofThick 175
c CBunkerRoofVert1 20
c CBunkerRoofVert2 10
c CBunkerRoofVert3 5
c CBunkerRoofVert4 20
c CBunkerRoofVert5 45
c CBunkerRoofVert6 40
c CBunkerRoofVert7 25
c CBunkerSideThick 80
c CBunkerVoidMat Void
c CBunkerWallActive 0
c CBunkerWallHeight 170
c CBunkerWallLen1 45
c CBunkerWallLen10 15
c CBunkerWallLen11 31
c CBunkerWallLen12 15.5
c CBunkerWallLen13 25.5
c CBunkerWallLen14 15.5
c CBunkerWallLen2 15
c CBunkerWallLen3 15
c CBunkerWallLen4 30
c CBunkerWallLen5 15
c CBunkerWallLen6 20
c CBunkerWallLen7 15
c CBunkerWallLen8 20
c CBunkerWallLen9 30
c CBunkerWallMat Steel71
c CBunkerWallMat0 Poly
c CBunkerWallMat1 ChipIRSteel
c CBunkerWallMat10 Poly
c CBunkerWallMat11 ChipIRSteel
c CBunkerWallMat12 Poly
c CBunkerWallMat13 ChipIRSteel
c CBunkerWallMat14 Poly
c CBunkerWallMat2 Poly
c CBunkerWallMat3 ChipIRSteel
c CBunkerWallMat4 Poly
c CBunkerWallMat5 ChipIRSteel
c CBunkerWallMat6 Poly
c CBunkerWallMat7 ChipIRSteel
c CBunkerWallMat8 Poly
c CBunkerWallMat9 ChipIRSteel
c CBunkerWallNBasic 15
c CBunkerWallRadius 2450
c CBunkerWallThick 350
c CDHighBayHeight 330
c CDHighBayLength 450
c CDHighBayRoofMat Concrete
c CDHighBayThick 200
c CDHighBayWallMat Concrete
c CurtainBaseGap 6
c CurtainBaseLen1 70
c CurtainBaseLen2 6
c CurtainBaseMat0 Stainless304
c CurtainBaseMat1 Void
c CurtainBaseMat2 Concrete
c CurtainDepth 186
c CurtainHeight 450
c CurtainInnerStep 30
c CurtainLeftPhase -65
c CurtainNBaseLayers 3
c CurtainNMidLayers 1
c CurtainNTopLayers 1
c CurtainOuterGap 5
c CurtainRightPhase 65
c CurtainTopRaise 60
c CurtainWallMat Concrete
c CurtainWallThick 30
c DBunkerFloorDepth 120
c DBunkerFloorThick 100
c DBunkerLeftAngle 0
c DBunkerLeftPhase -12
c DBunkerNLayers 1
c DBunkerNSectors 9
c DBunkerNSide 5
c DBunkerNSideThick 5
c DBunkerNSideVert 5
c DBunkerNVert 1
c DBunkerRightAngle 0
c DBunkerRightPhase 65
c DBunkerRoofHeight 150
c DBunkerRoofMat Aluminium
c DBunkerRoofMat0 Void
c DBunkerRoofMat1 Poly
c DBunkerRoofMat2 ChipIRSteel
c DBunkerRoofMat3 Poly
c DBunkerRoofMat4 ChipIRSteel
c DBunkerRoofMat5 Poly
c DBunkerRoofMat6 ChipIRSteel
c DBunkerRoofMat7 Poly
c DBunkerRoofNBasicVert 8
c DBunkerRoofThick 175
c DBunkerRoofVert1 20
c DBunkerRoofVert2 10
c DBunkerRoofVert3 5
c DBunkerRoofVert4 20
c DBunkerRoofVert5 45
c DBunkerRoofVert6 40
c DBunkerRoofVert7 25
c DBunkerSideThick 80
c DBunkerVoidMat Void
c DBunkerWallActive 0
c DBunkerWallHeight 170
c DBunkerWallLen1 45
c DBunkerWallLen10 15
c DBunkerWallLen11 31
c DBunkerWallLen12 15.5
c DBunkerWallLen13 25.5
c DBunkerWallLen14 15.5
c DBunkerWallLen2 15
c DBunkerWallLen3 15
c DBunkerWallLen4 30
c DBunkerWallLen5 15
c DBunkerWallLen6 20
c DBunkerWallLen7 15
c DBunkerWallLen8 20
c DBunkerWallLen9 30
c DBunkerWallMat Steel71
c DBunkerWallMat0 Poly
c DBunkerWallMat1 ChipIRSteel
c DBunkerWallMat10 Poly
c DBunkerWallMat11 ChipIRSteel
c DBunkerWallMat12 Poly
c DBunkerWallMat13 ChipIRSteel
c DBunkerWallMat14 Poly
c DBunkerWallMat2 Poly
c DBunkerWallMat3 ChipIRSteel
c DBunkerWallMat4 Poly
c DBunkerWallMat5 ChipIRSteel
c DBunkerWallMat6 Poly
c DBunkerWallMat7 ChipIRSteel
c DBunkerWallMat8 Poly
c DBunkerWallMat9 ChipIRSteel
c DBunkerWallNBasic 15
c DBunkerWallRadius 1150
c DBunkerWallThick 350
c EngineeringActive 0
c G1BLineLow10XYAngle 6.7
c G1BLineLow11XYAngle 0
c G1BLineLow12XYAngle -6
c G1BLineLow13XYAngle -12
c G1BLineLow14XYAngle -18
c G1BLineLow15XYAngle -24
c G1BLineLow16XYAngle -30
c G1BLineLow17XYAngle -36
c G1BLineLow18XYAngle -42
c G1BLineLow19XYAngle -48.12
c G1BLineLow1XYAngle 60
c G1BLineLow20XYAngle -54
c G1BLineLow21XYAngle -60
c G1BLineLow2XYAngle 54.7
c G1BLineLow3XYAngle 48
c G1BLineLow4XYAngle 42.7
c G1BLineLow5XYAngle 36
c G1BLineLow6XYAngle 30.7
c G1BLineLow7XYAngle 24
c G1BLineLow8XYAngle 18.7
c G1BLineLow9XYAngle 12
c G1BLineLowActive 0
c G1BLineLowBaseGap 0.1
c G1BLineLowBeamHeight 6
c G1BLineLowBeamWidth 6
c G1BLineLowBeamXStep 0
c G1BLineLowBeamXYAngle 0
c G1BLineLowBeamZAngle 0
c G1BLineLowBeamZStep 0
c G1BLineLowDepth1 10
c G1BLineLowDepth2 22
c G1BLineLowFilled 0
c G1BLineLowHeight1 12
c G1BLineLowHeight2 22
c G1BLineLowLength1 170
c G1BLineLowMat Stainless304
c G1BLineLowNSegment 2
c G1BLineLowSideGap 0.6
c G1BLineLowTopGap 0.8
c G1BLineLowWidth1 20
c G1BLineLowWidth2 28
c G1BLineTop10XYAngle 6.7
c G1BLineTop11XYAngle 0
c G1BLineTop12XYAngle -6
c G1BLineTop13XYAngle -12
c G1BLineTop14XYAngle -18
c G1BLineTop15XYAngle -24
c G1BLineTop16XYAngle -30
c G1BLineTop17XYAngle -36
c G1BLineTop18XYAngle -42
c G1BLineTop19XYAngle -48.12
c G1BLineTop1XYAngle 60
c G1BLineTop20XYAngle -54
c G1BLineTop21XYAngle -60
c G1BLineTop2XYAngle 54.7
c G1BLineTop3XYAngle 48
c G1BLineTop4XYAngle 42.7
c G1BLineTop5XYAngle 36
c G1BLineTop6XYAngle 30.7
c G1BLineTop7XYAngle 24
c G1BLineTop8XYAngle 18.7
c G1BLineTop9XYAngle 12
c G1BLineTopActive 0
c G1BLineTopBaseGap 0.1
c G1BLineTopBeamHeight 6
c G1BLineTopBeamWidth 6
c G1BLineTopBeamXStep 0
c G1BLineTopBeamXYAngle 0
c G1BLineTopBeamZAngle 0
c G1BLineTopBeamZStep 0
c G1BLineTopDepth1 10
c G1BLineTopDepth2 22
c G1BLineTopFilled 0
c G1BLineTopHeight1 12
c G1BLineTopHeight2 22
c G1BLineTopLength1 170
c G1BLineTopMat Stainless304
c G1BLineTopNSegment 2
c G1BLineTopSideGap 0.6
c G1BLineTopTopGap 0.8
c G1BLineTopWidth1 20
c G1BLineTopWidth2 28
c G2BLineLow10XYAngle 6.7
c G2BLineLow11XYAngle 0
c G2BLineLow12XYAngle -6
c G2BLineLow13XYAngle -12
c G2BLineLow14XYAngle -18
c G2BLineLow15XYAngle -24
c G2BLineLow16XYAngle -30
c G2BLineLow17XYAngle -36
c G2BLineLow18XYAngle -42
c G2BLineLow19XYAngle -48.12
c G2BLineLow1XYAngle 60
c G2BLineLow20XYAngle -54
c G2BLineLow21XYAngle -60
c G2BLineLow2XYAngle 54.7
c G2BLineLow3XYAngle 48
c G2BLineLow4XYAngle 42.7
c G2BLineLow5XYAngle 36
c G2BLineLow6XYAngle 30.7
c G2BLineLow7XYAngle 24
c G2BLineLow8XYAngle 18.7
c G2BLineLow9XYAngle 12
c G2BLineLowActive 0
c G2BLineLowBaseGap 0.1
c G2BLineLowBeamHeight 6
c G2BLineLowBeamWidth 6
c G2BLineLowBeamXStep 0
c G2BLineLowBeamXYAngle 0
c G2BLineLowBeamZAngle 0
c G2BLineLowBeamZStep 0
c G2BLineLowDepth1 10
c G2BLineLowDepth2 22
c G2BLineLowFilled 0
c G2BLineLowHeight1 12
c G2BLineLowHeight2 22
c G2BLineLowLength1 170
c G2BLineLowMat Stainless304
c G2BLineLowNSegment 2
c G2BLineLowSideGap 0.6
c G2BLineLowTopGap 0.8
c G2BLineLowWidth1 20
c G2BLineLowWidth2 28
c G2BLineTop10XYAngle 6.7
c G2BLineTop11XYAngle 0
c G2BLineTop12XYAngle -6
c G2BLineTop13XYAngle -12
c G2BLineTop14XYAngle -18
c G2BLineTop15XYAngle -24
c G2BLineTop16XYAngle -30
c G2BLineTop17XYAngle -36
c G2BLineTop18XYAngle -42
c G2BLineTop19XYAngle -48.12
c G2BLineTop1XYAngle 60
c G2BLineTop20XYAngle -54
c G2BLineTop21XYAngle -60
c G2BLineTop2XYAngle 54.7
c G2BLineTop3Filled 1
c G2BLineTop3XYAngle 48
c G2BLineTop4XYAngle 42.7
c G2BLineTop5XYAngle 36
c G2BLineTop6XYAngle 30.7
c G2BLineTop7XYAngle 24
c G2BLineTop8XYAngle 18.7
c G2BLineTop9XYAngle 12
c G2BLineTopActive 0
c G2BLineTopBaseGap 0.1
c G2BLineTopBeamHeight 6
c G2BLineTopBeamWidth 6
c G2BLineTopBeamXStep 0
c G2BLineTopBeamXYAngle 0
c G2BLineTopBeamZAngle 0
c G2BLineTopBeamZStep 0
c G2BLineTopDepth1 10
c G2BLineTopDepth2 22
c G2BLineTopFilled 0
c G2BLineTopHeight1 12
c G2BLineTopHeight2 22
c G2BLineTopLength1 170
c G2BLineTopMat Stainless304
c G2BLineTopNSegment 2
c G2BLineTopSideGap 0.6
c G2BLineTopTopGap 0.8
c G2BLineTopWidth1 20
c G2BLineTopWidth2 28
c GuideBay1NItems 21
c GuideBay1XYAngle 270
c GuideBay2NItems 21
c GuideBay2XYAngle 90
c GuideBayDepth 50
c GuideBayHeight 50
c GuideBayInnerDepth 40
c GuideBayInnerHeight 40
c GuideBayMat CastIron
c GuideBayMidRadius 170
c GuideBayViewAngle 128
c GuideBayXStep 0
c GuideBayYStep 0
c GuideBayZAngle 0
c GuideBayZStep 0
c LowBeRefWaterDiscDepth0 0.3
c LowBeRefWaterDiscHeight0 0.3
c LowBeRefWaterDiscHeight1 0.4
c LowFocusDistance 8.9
c LowFocusWidth 5.4
c LowFocusXYAngle 90
c LowFocusZStep -15.2
c ProtonTubeInnerMat1 helium
c ProtonTubeInnerMat2 helium
c ProtonTubeInnerMat3 helium
c ProtonTubeInnerMat4 Void
c ProtonTubeLength1 148.25
c ProtonTubeLength2 200
c ProtonTubeLength3 127.5
c ProtonTubeLength4 152.5
c ProtonTubeNSection 4
c ProtonTubeRadius1 11.5
c ProtonTubeRadius2 15
c ProtonTubeRadius3 15
c ProtonTubeRadius4 15
c ProtonTubeWallMat1 CastIron
c ProtonTubeWallMat2 CastIron
c ProtonTubeWallMat3 CastIron
c ProtonTubeWallMat4 CastIron
c ProtonTubeWallThick1 0
c ProtonTubeWallThick2 1
c ProtonTubeWallThick3 1
c ProtonTubeWallThick4 1
c ProtonTubeZcut1 3.7
c ProtonTubeZcut2 0
c ProtonTubeZcut3 0
c ProtonTubeZcut4 0
c ShutterBayCurtainMat0 Stainless304
c ShutterBayCurtainMat1 Void
c ShutterBayCurtainMat2 Concrete
c ShutterBayCurtainThick0 70
c ShutterBayCurtainThick1 6
c ShutterBayCutSkin 6
c ShutterBayDepth 400
c ShutterBayHeight 400
c ShutterBayMat CastIron
c ShutterBayNCurtain 3
c ShutterBayRadius 550
c ShutterBaySkin 6
c ShutterBaySkinMat Void
c ShutterBayTopCut 186
c ShutterBayTopRadius 500
c ShutterBayTopSkin 6
c TReturnLeftAlActive0 3
c TReturnLeftAlActive1 15
c TReturnLeftAlMat0 HPARA
c TReturnLeftAlMat1 Aluminium20K
c TReturnLeftAlMat2 Void
c TReturnLeftAlMat3 Aluminium
c TReturnLeftAlNRadii 4
c TReturnLeftAlNSegIn 3
c TReturnLeftAlPPt0 -1.9 0 0
c TReturnLeftAlPPt1 -1.9 2 0
c TReturnLeftAlPPt2 -3.597 3 0
c TReturnLeftAlPPt3 -3.597 11 0
c TReturnLeftAlRadius0 1.1
c TReturnLeftAlRadius1 1.3
c TReturnLeftAlRadius2 1.5
c TReturnLeftAlRadius3 1.8
c TReturnLeftAlTemp0 20
c TReturnLeftAlTemp1 20
c TReturnLeftAlTemp2 300
c TReturnLeftAlTemp3 300
c TReturnLeftConnectActive0 15
c TReturnLeftConnectMat0 HPARA
c TReturnLeftConnectMat1 SS316L
c TReturnLeftConnectMat2 Void
c TReturnLeftConnectMat3 SS316L
c TReturnLeftConnectNRadii 4
c TReturnLeftConnectNSegIn 1
c TReturnLeftConnectPPt0 0 0 0
c TReturnLeftConnectPPt1 0 2 0
c TReturnLeftConnectRadius0 1.1
c TReturnLeftConnectRadius1 1.3
c TReturnLeftConnectRadius2 1.5
c TReturnLeftConnectRadius3 1.8
c TReturnLeftConnectTemp0 20
c TReturnLeftConnectTemp1 20
c TReturnLeftConnectTemp2 300
c TReturnLeftConnectTemp3 300
c TReturnLeftInvarActive0 15
c TReturnLeftInvarMat0 HPARA
c TReturnLeftInvarMat1 Invar36
c TReturnLeftInvarMat2 Void
c TReturnLeftInvarMat3 Invar36
c TReturnLeftInvarNRadii 4
c TReturnLeftInvarNSegIn 2
c TReturnLeftInvarPPt0 0 0 0
c TReturnLeftInvarPPt1 0 12.5 0
c TReturnLeftInvarPPt2 0 12.5 30
c TReturnLeftInvarRadius0 1.1
c TReturnLeftInvarRadius1 1.3
c TReturnLeftInvarRadius2 1.5
c TReturnLeftInvarRadius3 1.8
c TReturnLeftInvarTemp0 20
c TReturnLeftInvarTemp1 20
c TReturnLeftInvarTemp2 300
c TReturnLeftInvarTemp3 300
c TReturnRightAlActive0 3
c TReturnRightAlActive1 15
c TReturnRightAlMat0 HPARA
c TReturnRightAlMat1 Aluminium20K
c TReturnRightAlMat2 Void
c TReturnRightAlMat3 Aluminium
c TReturnRightAlNRadii 4
c TReturnRightAlNSegIn 3
c TReturnRightAlPPt0 -1.9 0 0
c TReturnRightAlPPt1 -1.9 2 0
c TReturnRightAlPPt2 -3.597 3 0
c TReturnRightAlPPt3 -3.597 11 0
c TReturnRightAlRadius0 1.1
c TReturnRightAlRadius1 1.3
c TReturnRightAlRadius2 1.5
c TReturnRightAlRadius3 1.8
c TReturnRightAlTemp0 20
c TReturnRightAlTemp1 20
c TReturnRightAlTemp2 300
c TReturnRightAlTemp3 300
c TReturnRightConnectActive0 15
c TReturnRightConnectMat0 HPARA
c TReturnRightConnectMat1 SS316L
c TReturnRightConnectMat2 Void
c TReturnRightConnectMat3 SS316L
c TReturnRightConnectNRadii 4
c TReturnRightConnectNSegIn 1
c TReturnRightConnectPPt0 0 0 0
c TReturnRightConnectPPt1 0 2 0
c TReturnRightConnectRadius0 1.1
c TReturnRightConnectRadius1 1.3
c TReturnRightConnectRadius2 1.5
c TReturnRightConnectRadius3 1.8
c TReturnRightConnectTemp0 20
c TReturnRightConnectTemp1 20
c TReturnRightConnectTemp2 300
c TReturnRightConnectTemp3 300
c TReturnRightInvarActive0 15
c TReturnRightInvarMat0 HPARA
c TReturnRightInvarMat1 Invar36
c TReturnRightInvarMat2 Void
c TReturnRightInvarMat3 Invar36
c TReturnRightInvarNRadii 4
c TReturnRightInvarNSegIn 2
c TReturnRightInvarPPt0 0 0 0
c TReturnRightInvarPPt1 0 12.5 0
c TReturnRightInvarPPt2 0 12.5 30
c TReturnRightInvarRadius0 1.1
c TReturnRightInvarRadius1 1.3
c TReturnRightInvarRadius2 1.5
c TReturnRightInvarRadius3 1.8
c TReturnRightInvarTemp0 20
c TReturnRightInvarTemp1 20
c TReturnRightInvarTemp2 300
c TReturnRightInvarTemp3 300
c TSupplyLeftAlActive0 3
c TSupplyLeftAlActive1 15
c TSupplyLeftAlMat0 HPARA
c TSupplyLeftAlMat1 Aluminium20K
c TSupplyLeftAlMat2 Void
c TSupplyLeftAlMat3 Aluminium
c TSupplyLeftAlNRadii 4
c TSupplyLeftAlNSegIn 3
c TSupplyLeftAlPPt0 1.9 0 0
c TSupplyLeftAlPPt1 1.9 2 0
c TSupplyLeftAlPPt2 3.597 3 0
c TSupplyLeftAlPPt3 3.597 11 0
c TSupplyLeftAlRadius0 1.1
c TSupplyLeftAlRadius1 1.3
c TSupplyLeftAlRadius2 1.5
c TSupplyLeftAlRadius3 1.8
c TSupplyLeftAlTemp0 20
c TSupplyLeftAlTemp1 20
c TSupplyLeftAlTemp2 300
c TSupplyLeftAlTemp3 300
c TSupplyLeftConnectActive0 15
c TSupplyLeftConnectMat0 HPARA
c TSupplyLeftConnectMat1 SS316L
c TSupplyLeftConnectMat2 Void
c TSupplyLeftConnectMat3 SS316L
c TSupplyLeftConnectNRadii 4
c TSupplyLeftConnectNSegIn 1
c TSupplyLeftConnectPPt0 0 0 0
c TSupplyLeftConnectPPt1 0 2 0
c TSupplyLeftConnectRadius0 1.1
c TSupplyLeftConnectRadius1 1.3
c TSupplyLeftConnectRadius2 1.5
c TSupplyLeftConnectRadius3 1.8
c TSupplyLeftConnectTemp0 20
c TSupplyLeftConnectTemp1 20
c TSupplyLeftConnectTemp2 300
c TSupplyLeftConnectTemp3 300
c TSupplyLeftInvarActive0 15
c TSupplyLeftInvarMat0 HPARA
c TSupplyLeftInvarMat1 Invar36
c TSupplyLeftInvarMat2 Void
c TSupplyLeftInvarMat3 Invar36
c TSupplyLeftInvarNRadii 4
c TSupplyLeftInvarNSegIn 2
c TSupplyLeftInvarPPt0 0 0 0
c TSupplyLeftInvarPPt1 0 12.5 0
c TSupplyLeftInvarPPt2 0 12.5 30
c TSupplyLeftInvarRadius0 1.1
c TSupplyLeftInvarRadius1 1.3
c TSupplyLeftInvarRadius2 1.5
c TSupplyLeftInvarRadius3 1.8
c TSupplyLeftInvarTemp0 20
c TSupplyLeftInvarTemp1 20
c TSupplyLeftInvarTemp2 300
c TSupplyLeftInvarTemp3 300
c TSupplyRightAlActive0 3
c TSupplyRightAlActive1 15
c TSupplyRightAlMat0 HPARA
c TSupplyRightAlMat1 Aluminium20K
c TSupplyRightAlMat2 Void
c TSupplyRightAlMat3 Aluminium
c TSupplyRightAlNRadii 4
c TSupplyRightAlNSegIn 3
c TSupplyRightAlPPt0 1.9 0 0
c TSupplyRightAlPPt1 1.9 2 0
c TSupplyRightAlPPt2 3.597 3 0
c TSupplyRightAlPPt3 3.597 11 0
c TSupplyRightAlRadius0 1.1
c TSupplyRightAlRadius1 1.3
c TSupplyRightAlRadius2 1.5
c TSupplyRightAlRadius3 1.8
c TSupplyRightAlTemp0 20
c TSupplyRightAlTemp1 20
c TSupplyRightAlTemp2 300
c TSupplyRightAlTemp3 300
c TSupplyRightConnectActive0 15
c TSupplyRightConnectMat0 HPARA
c TSupplyRightConnectMat1 SS316L
c TSupplyRightConnectMat2 Void
c TSupplyRightConnectMat3 SS316L
c TSupplyRightConnectNRadii 4
c TSupplyRightConnectNSegIn 1
c TSupplyRightConnectPPt0 0 0 0
c TSupplyRightConnectPPt1 0 2 0
c TSupplyRightConnectRadius0 1.1
c TSupplyRightConnectRadius1 1.3
c TSupplyRightConnectRadius2 1.5
c TSupplyRightConnectRadius3 1.8
c TSupplyRightConnectTemp0 20
c TSupplyRightConnectTemp1 20
c TSupplyRightConnectTemp2 300
c TSupplyRightConnectTemp3 300
c TSupplyRightInvarActive0 15
c TSupplyRightInvarMat0 HPARA
c TSupplyRightInvarMat1 Invar36
c TSupplyRightInvarMat2 Void
c TSupplyRightInvarMat3 Invar36
c TSupplyRightInvarNRadii 4
c TSupplyRightInvarNSegIn 2
c TSupplyRightInvarPPt0 0 0 0
c TSupplyRightInvarPPt1 0 12.5 0
c TSupplyRightInvarPPt2 0 12.5 30
c TSupplyRightInvarRadius0 1.1
c TSupplyRightInvarRadius1 1.3
c TSupplyRightInvarRadius2 1.5
c TSupplyRightInvarRadius3 1.8
c TSupplyRightInvarTemp0 20
c TSupplyRightInvarTemp1 20
c TSupplyRightInvarTemp2 300
c TSupplyRightInvarTemp3 300
c TopAFlightAngleXY1 60
c TopAFlightAngleXY2 60
c TopAFlightAngleZBase 1.33
c TopAFlightAngleZTop 1.1
c TopAFlightHeight 2.9
c TopAFlightLinerMat1 Aluminium
c TopAFlightLinerThick1 0.3
c TopAFlightNLiner 1
c TopAFlightNWedges 0
c TopAFlightTapSurf cone
c TopAFlightWidth 10.7
c TopAFlightXStep 0
c TopAFlightXYAngle 180
c TopAFlightZAngle 0
c TopAFlightZStep 0
c TopBFlightAngleXY1 60
c TopBFlightAngleXY2 60
c TopBFlightAngleZBase 1.33
c TopBFlightAngleZTop 1.1
c TopBFlightHeight 2.9
c TopBFlightLinerMat1 Aluminium
c TopBFlightLinerThick1 0.3
c TopBFlightNLiner 1
c TopBFlightNWedges 0
c TopBFlightWidth 10.7
c TopBFlightXStep 0
c TopBFlightXYAngle 0
c TopBFlightZAngle 0
c TopBFlightZStep 0
c TopBeRefWaterDiscDepth0 0.3
c TopBeRefWaterDiscHeight0 0.3
c TopBeRefWaterDiscHeight1 0.4
c TopCapModMat0x0 Void
c TopCapModMat1x0 Aluminium
c TopCapModMat1x1 Void
c TopCapModMat1x2 Stainless304
c TopCapModMat2x0 H2O
c TopCapModMat2x1 Aluminium
c TopCapModMat2x2 Void
c TopCapModMat2x3 Stainless304
c TopCapModMat3x0 Aluminium
c TopCapModMat3x1 Void
c TopCapModMat3x2 Stainless304
c TopCapModNLayers 4
c TopCapModRadius1x0 32.3
c TopCapModRadius1x1 32.6
c TopCapModRadius2x0 32
c TopCapModRadius2x1 32.3
c TopCapModRadius2x2 32.6
c TopCapModRadius3x0 32.3
c TopCapModRadius3x1 32.6
c TopCapModThick0 0.2
c TopCapModThick1 0.3
c TopCapModThick2 0.7
c TopCapModThick3 0.3
c TopFlyLeftLobeCorner1 0 0.45 0
c TopFlyLeftLobeCorner2 -14.4 -13.2 0
c TopFlyLeftLobeCorner3 14.4 -13.2 0
c TopFlyLeftLobeDepth1 0.3
c TopFlyLeftLobeDepth2 0.5
c TopFlyLeftLobeDepth3 0.3
c TopFlyLeftLobeHeight1 0.3
c TopFlyLeftLobeHeight2 0.5
c TopFlyLeftLobeHeight3 0.6
c TopFlyLeftLobeMat1 Aluminium
c TopFlyLeftLobeMat2 Void
c TopFlyLeftLobeMat3 Aluminium
c TopFlyLeftLobeModMat ParaOrtho%99.5
c TopFlyLeftLobeModTemp 20
c TopFlyLeftLobeNLayers 4
c TopFlyLeftLobeRadius1 5
c TopFlyLeftLobeRadius2 2.506
c TopFlyLeftLobeRadius3 2.506
c TopFlyLeftLobeTemp1 20
c TopFlyLeftLobeThick1 0.3
c TopFlyLeftLobeThick2 0.5
c TopFlyLeftLobeThick3 0.3
c TopFlyLeftLobeXStep 1
c TopFlyLeftLobeYStep 0
c TopFlyLeftWaterCutAngle 30
c TopFlyLeftWaterCutWidth 10.562
c TopFlyLeftWaterModMat H2O
c TopFlyLeftWaterModTemp 300
c TopFlyLeftWaterWallMat Aluminium
c TopFlyLeftWaterWallThick 0.347
c TopFlyLeftWaterWidth 15.76
c TopFlyMidWaterBaseThick 0.2
c TopFlyMidWaterCornerRadius 2
c TopFlyMidWaterCutLayer 3
c TopFlyMidWaterLength 10.98
c TopFlyMidWaterMidAngle 90
c TopFlyMidWaterMidYStep 4.635
c TopFlyMidWaterModMat H2O
c TopFlyMidWaterModTemp 300
c TopFlyMidWaterTopThick 0.2
c TopFlyMidWaterWallMat Aluminium
c TopFlyMidWaterWallThick 0.2
c TopFlyRightLobeCorner1 0 0.45 0
c TopFlyRightLobeCorner2 -14.4 -13.2 0
c TopFlyRightLobeCorner3 14.4 -13.2 0
c TopFlyRightLobeDepth1 0.3
c TopFlyRightLobeDepth2 0.5
c TopFlyRightLobeDepth3 0.3
c TopFlyRightLobeHeight1 0.3
c TopFlyRightLobeHeight2 0.5
c TopFlyRightLobeHeight3 0.3
c TopFlyRightLobeMat1 Aluminium20K
c TopFlyRightLobeMat2 Void
c TopFlyRightLobeMat3 Aluminium
c TopFlyRightLobeModMat ParaOrtho%99.5
c TopFlyRightLobeModTemp 20
c TopFlyRightLobeNLayers 4
c TopFlyRightLobeRadius1 5
c TopFlyRightLobeRadius2 2.506
c TopFlyRightLobeRadius3 2.506
c TopFlyRightLobeTemp1 20
c TopFlyRightLobeThick1 0.3
c TopFlyRightLobeThick2 0.5
c TopFlyRightLobeThick3 0.3
c TopFlyRightLobeXStep -1
c TopFlyRightLobeYStep 0
c TopFlyRightWaterCutAngle 30
c TopFlyRightWaterCutWidth 10.56
c TopFlyRightWaterModMat H2O
c TopFlyRightWaterModTemp 300
c TopFlyRightWaterWallMat Aluminium
c TopFlyRightWaterWallThick 0.347
c TopFlyRightWaterWidth 15.76
c TopFlyTotalHeight 5.5
c TopFlyXStep 0
c TopFlyXYAngle 90
c TopFlyYStep 0
c TopFlyZAngle 0
c TopFlyZStep 0
c TopFocusDistance 8.9
c TopFocusWidth 5.4
c TopFocusXYAngle 90
c TopFocusZStep 13.7
c TopLeftPreWingInnerDepth 2
c TopLeftPreWingInnerHeight 2
c TopLeftPreWingInnerMat1 Aluminium
c TopLeftPreWingInnerMat2 Void
c TopLeftPreWingInnerMat3 Stainless304
c TopLeftPreWingInnerRadius 10
c TopLeftPreWingInnerYCut 8
c TopLeftPreWingLayerRadius1 30
c TopLeftPreWingLayerRadius2 30.3
c TopLeftPreWingLayerRadius3 30.6
c TopLeftPreWingMat H2O
c TopLeftPreWingNLayers 4
c TopLeftPreWingOuterDepth 2.5
c TopLeftPreWingOuterHeight 2.5
c TopLeftPreWingOuterRadius 38
c TopLeftPreWingSurfMat1 Aluminium
c TopLeftPreWingSurfMat2 Void
c TopLeftPreWingSurfMat3 Stainless304
c TopLeftPreWingWallMat Aluminium
c TopLeftPreWingWallThick 0.3
c TopPreModMat0x0 Aluminium
c TopPreModMat0x1 Void
c TopPreModMat0x2 Stainless304
c TopPreModMat1x0 H2O
c TopPreModMat1x1 Aluminium
c TopPreModMat1x2 Void
c TopPreModMat1x3 Stainless304
c TopPreModNLayers 2
c TopPreModRadius0x0 30.3
c TopPreModRadius0x1 30.6
c TopPreModRadius1x0 30
c TopPreModRadius1x1 30.3
c TopPreModRadius1x2 30.6
c TopPreModThick0 0.3
c TopPreModThick1 2.85
c TopRightPreWingInnerDepth 2
c TopRightPreWingInnerHeight 2
c TopRightPreWingInnerMat1 Aluminium
c TopRightPreWingInnerMat2 Void
c TopRightPreWingInnerMat3 Stainless304
c TopRightPreWingInnerRadius 10
c TopRightPreWingInnerYCut 8
c TopRightPreWingLayerRadius1 30
c TopRightPreWingLayerRadius2 30.3
c TopRightPreWingLayerRadius3 30.6
c TopRightPreWingMat H2O
c TopRightPreWingNLayers 4
c TopRightPreWingOuterDepth 2.5
c TopRightPreWingOuterHeight 2.5
c TopRightPreWingOuterRadius 38
c TopRightPreWingSurfMat1 Aluminium
c TopRightPreWingSurfMat2 Void
c TopRightPreWingSurfMat3 Stainless304
c TopRightPreWingWallMat Aluminium
c TopRightPreWingWallThick 0.3
c TopRightPreWingXYAngle 180
c sdefEnergy 2000
c skadiADisk0NBlades 1
c skadiADisk0OpenAngle0 60
c skadiADisk0PhaseAngle0 95
c skadiADisk0Thick 0.2
c skadiADiskInnerMat Copper
c skadiADiskInnerRadius 20
c skadiADiskNDisk 1
c skadiADiskOuterMat B4C
c skadiADiskOuterRadius 35
c skadiADiskXStep 0
c skadiADiskYStep 0
c skadiADiskZStep 0
c skadiAxisXYAngle 0
c skadiAxisZAngle 0
c skadiBA0AHeight 3
c skadiBA0AWidth 3
c skadiBA0AngDir 90
c skadiBA0BHeight 3
c skadiBA0BWidth 3
c skadiBA0Length 350
c skadiBA0Radius 8400
c skadiBA0TypeID Bend
c skadiBAActiveShield 0
c skadiBABeamXYAngle 0
c skadiBABeamYStep -175
c skadiBABeamZStep 1.82
c skadiBALayerMat0 Void
c skadiBALayerMat1 Copper
c skadiBALayerThick1 0.8
c skadiBALength 350
c skadiBANShapeLayers 2
c skadiBANShapes 1
c skadiBAXStep 0
c skadiBAXYAngle 0
c skadiBAYStep -175
c skadiBAZAngle 0
c skadiBAZStep 0
c skadiBB0AHeight 3
c skadiBB0AWidth 3
c skadiBB0AngDir 90
c skadiBB0BHeight 3
c skadiBB0BWidth 3
c skadiBB0Length 40
c skadiBB0Radius 8400
c skadiBB0TypeID Bend
c skadiBBActiveShield 0
c skadiBBBeamXYAngle 0
c skadiBBBeamYStep -20
c skadiBBLayerMat0 Void
c skadiBBLayerMat1 Copper
c skadiBBLayerThick1 0.8
c skadiBBLength 40
c skadiBBNShapeLayers 2
c skadiBBNShapes 1
c skadiBBXStep 0
c skadiBBXYAngle 0
c skadiBBYStep -20
c skadiBBZAngle 0
c skadiBBZStep 0
c skadiBC0AHeight 3
c skadiBC0AWidth 3
c skadiBC0AngDir 90
c skadiBC0BHeight 3
c skadiBC0BWidth 3
c skadiBC0Length 46.5
c skadiBC0Radius 8400
c skadiBC0TypeID Bend
c skadiBCActiveShield 0
c skadiBCBeamXYAngle 0
c skadiBCBeamYStep -23.25
c skadiBCLayerMat0 Void
c skadiBCLayerMat1 Copper
c skadiBCLayerThick1 0.8
c skadiBCLength 46.5
c skadiBCNShapeLayers 2
c skadiBCNShapes 1
c skadiBCXStep 0
c skadiBCXYAngle 0
c skadiBCYStep -23.25
c skadiBCZAngle 0
c skadiBCZStep 0
c skadiBD0AHeight 3
c skadiBD0AWidth 3
c skadiBD0AngDir 270
c skadiBD0BHeight 3
c skadiBD0BWidth 3
c skadiBD0Length 323.5
c skadiBD0Radius 8400
c skadiBD0TypeID Bend
c skadiBDActiveShield 0
c skadiBDBeamXYAngle 0
c skadiBDBeamYStep 2.5
c skadiBDLayerMat0 Void
c skadiBDLayerMat1 Copper
c skadiBDLayerThick1 0.8
c skadiBDLength 323.5
c skadiBDNShapeLayers 2
c skadiBDNShapes 1
c skadiBDXStep 0
c skadiBDXYAngle 0
c skadiBDYStep 2.5
c skadiBDZAngle 0
c skadiBDZStep 0
c skadiBDisk0NBlades 1
c skadiBDisk0OpenAngle0 60
c skadiBDisk0PhaseAngle0 95
c skadiBDisk0Thick 0.2
c skadiBDiskInnerMat Copper
c skadiBDiskInnerRadius 20
c skadiBDiskNDisk 1
c skadiBDiskOuterMat B4C
c skadiBDiskOuterRadius 35
c skadiBDiskXStep 0
c skadiBDiskYStep 0
c skadiBDiskZStep 0
c skadiBE0AHeight 3
c skadiBE0AWidth 3
c skadiBE0AngDir 270
c skadiBE0BHeight 3
c skadiBE0BWidth 3
c skadiBE0Length 122
c skadiBE0Radius 8400
c skadiBE0TypeID Bend
c skadiBEActiveShield 0
c skadiBEBeamXYAngle 0
c skadiBEBeamYStep 2.5
c skadiBELayerMat0 Void
c skadiBELayerMat1 Copper
c skadiBELayerThick1 0.8
c skadiBELength 122
c skadiBENShapeLayers 2
c skadiBENShapes 1
c skadiBEXStep 0
c skadiBEXYAngle 0
c skadiBEYStep 2.5
c skadiBEZAngle 0
c skadiBEZStep 0
c skadiBInsertHeight 15
c skadiBInsertLength 200.6
c skadiBInsertMat Stainless304
c skadiBInsertNBox 1
c skadiBInsertNWall 1
c skadiBInsertWallMat Stainless304
c skadiBInsertWallThick 2.5
c skadiBInsertWidth 15
c skadiC1Disk0NBlades 1
c skadiC1Disk0OpenAngle0 60
c skadiC1Disk0PhaseAngle0 95
c skadiC1Disk0Thick 0.2
c skadiC1DiskInnerMat Copper
c skadiC1DiskInnerRadius 20
c skadiC1DiskNDisk 1
c skadiC1DiskOuterMat B4C
c skadiC1DiskOuterRadius 35
c skadiC1DiskXStep 0
c skadiC1DiskYStep 0
c skadiC1DiskZStep 0
c skadiC2Disk0NBlades 1
c skadiC2Disk0OpenAngle0 60
c skadiC2Disk0PhaseAngle0 95
c skadiC2Disk0Thick 0.2
c skadiC2DiskInnerMat Copper
c skadiC2DiskInnerRadius 20
c skadiC2DiskNDisk 1
c skadiC2DiskOuterMat B4C
c skadiC2DiskOuterRadius 35
c skadiC2DiskXStep 0
c skadiC2DiskYStep 0
c skadiC2DiskZStep 0
c skadiCInsertHeight 26
c skadiCInsertLength 203
c skadiCInsertMat Stainless304
c skadiCInsertNBox 1
c skadiCInsertNWall 1
c skadiCInsertWallMat Stainless304
c skadiCInsertWallThick 2.5
c skadiCInsertWidth 26
c skadiCaveFrontCutRadius 3.1
c skadiCaveFrontCutShape Square
c skadiCaveL1Back 0.5
c skadiCaveL1Front 0.5
c skadiCaveL1LeftWall 0.5
c skadiCaveL1Mat B4C
c skadiCaveL1RightWall 0.5
c skadiCaveL1Roof 0.5
c skadiCaveL2Back 60.5
c skadiCaveL2Front 60.5
c skadiCaveL2LeftWall 60.5
c skadiCaveL2Mat Concrete
c skadiCaveL2RightWall 60.5
c skadiCaveL2Roof 60.5
c skadiCaveL3Back 70.5
c skadiCaveL3Front 70.5
c skadiCaveL3LeftWall 70.5
c skadiCaveL3Mat Stainless304
c skadiCaveL3RightWall 70.5
c skadiCaveL3Roof 70.5
c skadiCaveVoidDepth 189.7
c skadiCaveVoidHeight 240.3
c skadiCaveVoidLength 250
c skadiCaveVoidWidth 250
c skadiCaveXStep 0
c skadiCaveYStep 357.5
c skadiCavepipeL1Mat B4C
c skadiCavepipeL1Thick 1
c skadiCavepipeL2Mat Aluminium
c skadiCavepipeL2Thick 1
c skadiCavepipeLength 2195
c skadiCavepipeRadius 100
c skadiChopperABackFlangeAngleOffset 7.5
c skadiChopperABackFlangeBoltMat ChipIRSteel
c skadiChopperABackFlangeBoltRadius 0.4
c skadiChopperABackFlangeInnerRadius 10
c skadiChopperABackFlangeMainMat Aluminium
c skadiChopperABackFlangeNBolts 24
c skadiChopperABackFlangeOuterRadius 12.65
c skadiChopperABackFlangeSealMat Poly
c skadiChopperABackFlangeSealThick 0.2
c skadiChopperABoltMat ChipIRSteel
c skadiChopperAFrontFlangeAngleOffset 7.5
c skadiChopperAFrontFlangeBoltMat ChipIRSteel
c skadiChopperAFrontFlangeBoltRadius 0.4
c skadiChopperAFrontFlangeInnerRadius 10
c skadiChopperAFrontFlangeMainMat Aluminium
c skadiChopperAFrontFlangeNBolts 24
c skadiChopperAFrontFlangeOuterRadius 12.65
c skadiChopperAFrontFlangeSealMat Poly
c skadiChopperAFrontFlangeSealThick 0.2
c skadiChopperAHeight 86.5
c skadiChopperAIPortABoltMat ChipIRSteel
c skadiChopperAIPortABoltRadius 0.3
c skadiChopperAIPortABoltStep 1
c skadiChopperAIPortAHeight 11.6
c skadiChopperAIPortALength 1
c skadiChopperAIPortAMat Aluminium
c skadiChopperAIPortANBolt 8
c skadiChopperAIPortASealMat Poly
c skadiChopperAIPortASealStep 0.5
c skadiChopperAIPortASealThick 0.3
c skadiChopperAIPortAWidth 11.6
c skadiChopperAIPortAWindow 0.3
c skadiChopperAIPortAWindowMat Aluminium
c skadiChopperAIPortAYStep -4.6375
c skadiChopperAIPortBBoltMat ChipIRSteel
c skadiChopperAIPortBBoltRadius 0.3
c skadiChopperAIPortBBoltStep 1
c skadiChopperAIPortBHeight 11.6
c skadiChopperAIPortBLength 1
c skadiChopperAIPortBMat Aluminium
c skadiChopperAIPortBNBolt 8
c skadiChopperAIPortBSealMat Poly
c skadiChopperAIPortBSealStep 0.5
c skadiChopperAIPortBSealThick 0.3
c skadiChopperAIPortBWidth 11.6
c skadiChopperAIPortBWindow 0.3
c skadiChopperAIPortBWindowMat Aluminium
c skadiChopperAIPortBYStep 4.6375
c skadiChopperALength 12
c skadiChopperAMainRadius 38.122
c skadiChopperAMainThick 6.55
c skadiChopperAMainZStep 28
c skadiChopperAMotorAxleMat Nickel
c skadiChopperAMotorAxleRadius 0.5
c skadiChopperAMotorBodyLength 5
c skadiChopperAMotorBodyMat Copper
c skadiChopperAMotorBodyRadius 10
c skadiChopperAMotorBoltMat ChipIRSteel
c skadiChopperAMotorBoltRadius 0.5
c skadiChopperAMotorInnerRadius 12
c skadiChopperAMotorMainMat Aluminium
c skadiChopperAMotorNBolts 24
c skadiChopperAMotorOuterRadius 15.2
c skadiChopperAMotorPlateMat Aluminium
c skadiChopperAMotorPlateThick 3.27
c skadiChopperAMotorReverse 196
c skadiChopperAMotorSealMat Poly
c skadiChopperAMotorSealRadius 13.6
c skadiChopperAMotorSealThick 0.2
c skadiChopperARingMat Poly
c skadiChopperARingNSection 12
c skadiChopperARingNTrack 12
c skadiChopperARingRadius 40
c skadiChopperARingThick 0.4
c skadiChopperAShortHeight 50.5
c skadiChopperAShortWidth 50.5
c skadiChopperAWallMat Aluminium
c skadiChopperAWidth 86.5
c skadiChopperAYStep 0
c skadiChopperBBackFlangeAngleOffset 7.5
c skadiChopperBBackFlangeBoltMat ChipIRSteel
c skadiChopperBBackFlangeBoltRadius 0.4
c skadiChopperBBackFlangeInnerRadius 10
c skadiChopperBBackFlangeMainMat Aluminium
c skadiChopperBBackFlangeNBolts 24
c skadiChopperBBackFlangeOuterRadius 12.65
c skadiChopperBBackFlangeSealMat Poly
c skadiChopperBBackFlangeSealThick 0.2
c skadiChopperBBoltMat ChipIRSteel
c skadiChopperBFrontFlangeAngleOffset 7.5
c skadiChopperBFrontFlangeBoltMat ChipIRSteel
c skadiChopperBFrontFlangeBoltRadius 0.4
c skadiChopperBFrontFlangeInnerRadius 10
c skadiChopperBFrontFlangeMainMat Aluminium
c skadiChopperBFrontFlangeNBolts 24
c skadiChopperBFrontFlangeOuterRadius 12.65
c skadiChopperBFrontFlangeSealMat Poly
c skadiChopperBFrontFlangeSealThick 0.2
c skadiChopperBHeight 86.5
c skadiChopperBIPortABoltMat ChipIRSteel
c skadiChopperBIPortABoltRadius 0.3
c skadiChopperBIPortABoltStep 1
c skadiChopperBIPortAHeight 11.6
c skadiChopperBIPortALength 1
c skadiChopperBIPortAMat Aluminium
c skadiChopperBIPortANBolt 8
c skadiChopperBIPortASealMat Poly
c skadiChopperBIPortASealStep 0.5
c skadiChopperBIPortASealThick 0.3
c skadiChopperBIPortAWidth 11.6
c skadiChopperBIPortAWindow 0.3
c skadiChopperBIPortAWindowMat Aluminium
c skadiChopperBIPortAYStep -4.6375
c skadiChopperBIPortBBoltMat ChipIRSteel
c skadiChopperBIPortBBoltRadius 0.3
c skadiChopperBIPortBBoltStep 1
c skadiChopperBIPortBHeight 11.6
c skadiChopperBIPortBLength 1
c skadiChopperBIPortBMat Aluminium
c skadiChopperBIPortBNBolt 8
c skadiChopperBIPortBSealMat Poly
c skadiChopperBIPortBSealStep 0.5
c skadiChopperBIPortBSealThick 0.3
c skadiChopperBIPortBWidth 11.6
c skadiChopperBIPortBWindow 0.3
c skadiChopperBIPortBWindowMat Aluminium
c skadiChopperBIPortBYStep 4.6375
c skadiChopperBLength 12
c skadiChopperBMainRadius 38.122
c skadiChopperBMainThick 6.55
c skadiChopperBMainZStep 28
c skadiChopperBMotorAxleMat Nickel
c skadiChopperBMotorAxleRadius 0.5
c skadiChopperBMotorBodyLength 5
c skadiChopperBMotorBodyMat Copper
c skadiChopperBMotorBodyRadius 10
c skadiChopperBMotorBoltMat ChipIRSteel
c skadiChopperBMotorBoltRadius 0.5
c skadiChopperBMotorInnerRadius 12
c skadiChopperBMotorMainMat Aluminium
c skadiChopperBMotorNBolts 24
c skadiChopperBMotorOuterRadius 15.2
c skadiChopperBMotorPlateMat Aluminium
c skadiChopperBMotorPlateThick 3.27
c skadiChopperBMotorReverse 196
c skadiChopperBMotorSealMat Poly
c skadiChopperBMotorSealRadius 13.6
c skadiChopperBMotorSealThick 0.2
c skadiChopperBRingMat Poly
c skadiChopperBRingNSection 12
c skadiChopperBRingNTrack 12
c skadiChopperBRingRadius 40
c skadiChopperBRingThick 0.4
c skadiChopperBShortHeight 50.5
c skadiChopperBShortWidth 50.5
c skadiChopperBWallMat Aluminium
c skadiChopperBWidth 86.5
c skadiChopperBYStep 0
c skadiChopperC1BackFlangeAngleOffset 7.5
c skadiChopperC1BackFlangeBoltMat ChipIRSteel
c skadiChopperC1BackFlangeBoltRadius 0.4
c skadiChopperC1BackFlangeInnerRadius 10
c skadiChopperC1BackFlangeMainMat Aluminium
c skadiChopperC1BackFlangeNBolts 24
c skadiChopperC1BackFlangeOuterRadius 12.65
c skadiChopperC1BackFlangeSealMat Poly
c skadiChopperC1BackFlangeSealThick 0.2
c skadiChopperC1BoltMat ChipIRSteel
c skadiChopperC1FrontFlangeAngleOffset 7.5
c skadiChopperC1FrontFlangeBoltMat ChipIRSteel
c skadiChopperC1FrontFlangeBoltRadius 0.4
c skadiChopperC1FrontFlangeInnerRadius 10
c skadiChopperC1FrontFlangeMainMat Aluminium
c skadiChopperC1FrontFlangeNBolts 24
c skadiChopperC1FrontFlangeOuterRadius 12.65
c skadiChopperC1FrontFlangeSealMat Poly
c skadiChopperC1FrontFlangeSealThick 0.2
c skadiChopperC1Height 86.5
c skadiChopperC1IPortABoltMat ChipIRSteel
c skadiChopperC1IPortABoltRadius 0.3
c skadiChopperC1IPortABoltStep 1
c skadiChopperC1IPortAHeight 11.6
c skadiChopperC1IPortALength 1
c skadiChopperC1IPortAMat Aluminium
c skadiChopperC1IPortANBolt 8
c skadiChopperC1IPortASealMat Poly
c skadiChopperC1IPortASealStep 0.5
c skadiChopperC1IPortASealThick 0.3
c skadiChopperC1IPortAWidth 11.6
c skadiChopperC1IPortAWindow 0.3
c skadiChopperC1IPortAWindowMat Aluminium
c skadiChopperC1IPortAYStep -4.6375
c skadiChopperC1IPortBBoltMat ChipIRSteel
c skadiChopperC1IPortBBoltRadius 0.3
c skadiChopperC1IPortBBoltStep 1
c skadiChopperC1IPortBHeight 11.6
c skadiChopperC1IPortBLength 1
c skadiChopperC1IPortBMat Aluminium
c skadiChopperC1IPortBNBolt 8
c skadiChopperC1IPortBSealMat Poly
c skadiChopperC1IPortBSealStep 0.5
c skadiChopperC1IPortBSealThick 0.3
c skadiChopperC1IPortBWidth 11.6
c skadiChopperC1IPortBWindow 0.3
c skadiChopperC1IPortBWindowMat Aluminium
c skadiChopperC1IPortBYStep 4.6375
c skadiChopperC1Length 12
c skadiChopperC1MainRadius 38.122
c skadiChopperC1MainThick 6.55
c skadiChopperC1MainZStep 28
c skadiChopperC1MotorAxleMat Nickel
c skadiChopperC1MotorAxleRadius 0.5
c skadiChopperC1MotorBodyLength 5
c skadiChopperC1MotorBodyMat Copper
c skadiChopperC1MotorBodyRadius 10
c skadiChopperC1MotorBoltMat ChipIRSteel
c skadiChopperC1MotorBoltRadius 0.5
c skadiChopperC1MotorInnerRadius 12
c skadiChopperC1MotorMainMat Aluminium
c skadiChopperC1MotorNBolts 24
c skadiChopperC1MotorOuterRadius 15.2
c skadiChopperC1MotorPlateMat Aluminium
c skadiChopperC1MotorPlateThick 3.27
c skadiChopperC1MotorReverse 196
c skadiChopperC1MotorSealMat Poly
c skadiChopperC1MotorSealRadius 13.6
c skadiChopperC1MotorSealThick 0.2
c skadiChopperC1RingMat Poly
c skadiChopperC1RingNSection 12
c skadiChopperC1RingNTrack 12
c skadiChopperC1RingRadius 40
c skadiChopperC1RingThick 0.4
c skadiChopperC1ShortHeight 50.5
c skadiChopperC1ShortWidth 50.5
c skadiChopperC1WallMat Aluminium
c skadiChopperC1Width 86.5
c skadiChopperC1YStep -15
c skadiChopperC2BackFlangeAngleOffset 7.5
c skadiChopperC2BackFlangeBoltMat ChipIRSteel
c skadiChopperC2BackFlangeBoltRadius 0.4
c skadiChopperC2BackFlangeInnerRadius 10
c skadiChopperC2BackFlangeMainMat Aluminium
c skadiChopperC2BackFlangeNBolts 24
c skadiChopperC2BackFlangeOuterRadius 12.65
c skadiChopperC2BackFlangeSealMat Poly
c skadiChopperC2BackFlangeSealThick 0.2
c skadiChopperC2BoltMat ChipIRSteel
c skadiChopperC2FrontFlangeAngleOffset 7.5
c skadiChopperC2FrontFlangeBoltMat ChipIRSteel
c skadiChopperC2FrontFlangeBoltRadius 0.4
c skadiChopperC2FrontFlangeInnerRadius 10
c skadiChopperC2FrontFlangeMainMat Aluminium
c skadiChopperC2FrontFlangeNBolts 24
c skadiChopperC2FrontFlangeOuterRadius 12.65
c skadiChopperC2FrontFlangeSealMat Poly
c skadiChopperC2FrontFlangeSealThick 0.2
c skadiChopperC2Height 86.5
c skadiChopperC2IPortABoltMat ChipIRSteel
c skadiChopperC2IPortABoltRadius 0.3
c skadiChopperC2IPortABoltStep 1
c skadiChopperC2IPortAHeight 11.6
c skadiChopperC2IPortALength 1
c skadiChopperC2IPortAMat Aluminium
c skadiChopperC2IPortANBolt 8
c skadiChopperC2IPortASealMat Poly
c skadiChopperC2IPortASealStep 0.5
c skadiChopperC2IPortASealThick 0.3
c skadiChopperC2IPortAWidth 11.6
c skadiChopperC2IPortAWindow 0.3
c skadiChopperC2IPortAWindowMat Aluminium
c skadiChopperC2IPortAYStep -4.6375
c skadiChopperC2IPortBBoltMat ChipIRSteel
c skadiChopperC2IPortBBoltRadius 0.3
c skadiChopperC2IPortBBoltStep 1
c skadiChopperC2IPortBHeight 11.6
c skadiChopperC2IPortBLength 1
c skadiChopperC2IPortBMat Aluminium
c skadiChopperC2IPortBNBolt 8
c skadiChopperC2IPortBSealMat Poly
c skadiChopperC2IPortBSealStep 0.5
c skadiChopperC2IPortBSealThick 0.3
c skadiChopperC2IPortBWidth 11.6
c skadiChopperC2IPortBWindow 0.3
c skadiChopperC2IPortBWindowMat Aluminium
c skadiChopperC2IPortBYStep 4.6375
c skadiChopperC2Length 12
c skadiChopperC2MainRadius 38.122
c skadiChopperC2MainThick 6.55
c skadiChopperC2MainZStep 28
c skadiChopperC2MotorAxleMat Nickel
c skadiChopperC2MotorAxleRadius 0.5
c skadiChopperC2MotorBodyLength 5
c skadiChopperC2MotorBodyMat Copper
c skadiChopperC2MotorBodyRadius 10
c skadiChopperC2MotorBoltMat ChipIRSteel
c skadiChopperC2MotorBoltRadius 0.5
c skadiChopperC2MotorInnerRadius 12
c skadiChopperC2MotorMainMat Aluminium
c skadiChopperC2MotorNBolts 24
c skadiChopperC2MotorOuterRadius 15.2
c skadiChopperC2MotorPlateMat Aluminium
c skadiChopperC2MotorPlateThick 3.27
c skadiChopperC2MotorReverse 196
c skadiChopperC2MotorSealMat Poly
c skadiChopperC2MotorSealRadius 13.6
c skadiChopperC2MotorSealThick 0.2
c skadiChopperC2RingMat Poly
c skadiChopperC2RingNSection 12
c skadiChopperC2RingNTrack 12
c skadiChopperC2RingRadius 40
c skadiChopperC2RingThick 0.4
c skadiChopperC2ShortHeight 50.5
c skadiChopperC2ShortWidth 50.5
c skadiChopperC2WallMat Aluminium
c skadiChopperC2Width 86.5
c skadiChopperC2YStep 15
c skadiCollALength 46.5
c skadiCollAMat Copper
c skadiCollAYStep 24.25
c skadiCollBLength 50
c skadiCollBMat Copper
c skadiCollBYStep 228
c skadiCollCLength 22.5
c skadiCollCMat Copper
c skadiCollCYStep 12.25
c skadiFF0Height 3
c skadiFF0Length 22.5
c skadiFF0TypeID Rectangle
c skadiFF0Width 3
c skadiFFActiveShield 0
c skadiFFBeamXYAngle 0
c skadiFFBeamYStep -11.25
c skadiFFLayerMat0 Void
c skadiFFLayerMat1 Copper
c skadiFFLayerThick1 0.8
c skadiFFLength 22.5
c skadiFFNShapeLayers 2
c skadiFFNShapes 1
c skadiFFXStep 0
c skadiFFXYAngle 0
c skadiFFYStep -11.25
c skadiFFZAngle 0
c skadiFFZStep 0
c skadiFWallA0Height 3
c skadiFWallA0Length 200.6
c skadiFWallA0TypeID Rectangle
c skadiFWallA0Width 3
c skadiFWallAActiveShield 0
c skadiFWallABeamXYAngle 0
c skadiFWallABeamYStep 0
c skadiFWallALayerMat0 Void
c skadiFWallALayerMat1 Copper
c skadiFWallALayerThick1 0.8
c skadiFWallALength 200.6
c skadiFWallANShapeLayers 2
c skadiFWallANShapes 1
c skadiFWallAXStep 0
c skadiFWallAXYAngle 0
c skadiFWallAYStep 0
c skadiFWallAZAngle 0
c skadiFWallAZStep 0
c skadiFWallB0Height 3
c skadiFWallB0Length 203
c skadiFWallB0TypeID Rectangle
c skadiFWallB0Width 3
c skadiFWallBActiveShield 0
c skadiFWallBBeamXYAngle 0
c skadiFWallBBeamYStep 0
c skadiFWallBLayerMat0 Void
c skadiFWallBLayerMat1 Copper
c skadiFWallBLayerThick1 4
c skadiFWallBLength 203
c skadiFWallBNShapeLayers 2
c skadiFWallBNShapes 1
c skadiFWallBXStep 0
c skadiFWallBXYAngle 0
c skadiFWallBYStep 0
c skadiFWallBZAngle 0
c skadiFWallBZStep 0
c skadiGOutA0Height 3
c skadiGOutA0Length 49
c skadiGOutA0TypeID Rectangle
c skadiGOutA0Width 3
c skadiGOutAActiveShield 0
c skadiGOutABeamXYAngle 0
c skadiGOutABeamYStep -24.5
c skadiGOutALayerMat0 Void
c skadiGOutALayerMat1 Copper
c skadiGOutALayerThick1 0.8
c skadiGOutALength 49
c skadiGOutANShapeLayers 2
c skadiGOutANShapes 1
c skadiGOutAXStep 0
c skadiGOutAXYAngle 0
c skadiGOutAYStep -24.5
c skadiGOutAZAngle 0
c skadiGOutAZStep 0
c skadiGOutB0Height 3
c skadiGOutB0Length 642
c skadiGOutB0TypeID Rectangle
c skadiGOutB0Width 3
c skadiGOutBActiveShield 0
c skadiGOutBBeamXYAngle 0
c skadiGOutBBeamYStep -321
c skadiGOutBLayerMat0 Void
c skadiGOutBLayerMat1 Copper
c skadiGOutBLayerThick1 0.8
c skadiGOutBLength 642
c skadiGOutBNShapeLayers 2
c skadiGOutBNShapes 1
c skadiGOutBXStep 0
c skadiGOutBXYAngle 0
c skadiGOutBYStep -321
c skadiGOutBZAngle 0
c skadiGOutBZStep 0
c skadiGOutC0Height 3
c skadiGOutC0Length 305
c skadiGOutC0TypeID Rectangle
c skadiGOutC0Width 3
c skadiGOutCActiveShield 0
c skadiGOutCBeamXYAngle 0
c skadiGOutCBeamYStep -152.5
c skadiGOutCLayerMat0 Void
c skadiGOutCLayerMat1 Copper
c skadiGOutCLayerThick1 0.8
c skadiGOutCLength 305
c skadiGOutCNShapeLayers 2
c skadiGOutCNShapes 1
c skadiGOutCXStep 0
c skadiGOutCXYAngle 0
c skadiGOutCYStep -152.5
c skadiGOutCZAngle 0
c skadiGOutCZStep 0
c skadiGOutD0Height 3
c skadiGOutD0Length 690
c skadiGOutD0TypeID Rectangle
c skadiGOutD0Width 3
c skadiGOutDActiveShield 0
c skadiGOutDBeamXYAngle 0
c skadiGOutDBeamYStep -345
c skadiGOutDLayerMat0 Void
c skadiGOutDLayerMat1 Copper
c skadiGOutDLayerThick1 0.8
c skadiGOutDLength 690
c skadiGOutDNShapeLayers 2
c skadiGOutDNShapes 1
c skadiGOutDXStep 0
c skadiGOutDXYAngle 0
c skadiGOutDYStep -345
c skadiGOutDZAngle 0
c skadiGOutDZStep 0
c skadiGOutE0Height 3
c skadiGOutE0Length 480
c skadiGOutE0TypeID Rectangle
c skadiGOutE0Width 3
c skadiGOutEActiveShield 0
c skadiGOutEBeamXYAngle 0
c skadiGOutEBeamYStep 0
c skadiGOutELayerMat0 Void
c skadiGOutELayerMat1 Copper
c skadiGOutELayerThick1 0.8
c skadiGOutELength 480
c skadiGOutENShapeLayers 2
c skadiGOutENShapes 1
c skadiGOutEXStep 0
c skadiGOutEXYAngle 0
c skadiGOutEYStep 0
c skadiGOutEZAngle 0
c skadiGOutEZStep 0
c skadiPipeBCladdingMat B4C
c skadiPipeBCladdingThick 0
c skadiPipeBFeMat Aluminium
c skadiPipeBFeThick 0.5
c skadiPipeBFlangeLength 1
c skadiPipeBFlangeRadius 11.5
c skadiPipeBLength 42
c skadiPipeBRadius 7.5
c skadiPipeBVoidMat Void
c skadiPipeBWindowActive 3
c skadiPipeBWindowBackMat Silicon300K
c skadiPipeBWindowFrontMat Silicon300K
c skadiPipeBWindowRadius 9.5
c skadiPipeBWindowThick 0.3
c skadiPipeBYStep 7
c skadiPipeCCladdingMat B4C
c skadiPipeCCladdingThick 0
c skadiPipeCFeMat Aluminium
c skadiPipeCFeThick 0.5
c skadiPipeCFlangeLength 1
c skadiPipeCFlangeRadius 11.5
c skadiPipeCLength 48.5
c skadiPipeCRadius 7.5
c skadiPipeCVoidMat Void
c skadiPipeCWindowActive 3
c skadiPipeCWindowBackMat Silicon300K
c skadiPipeCWindowFrontMat Silicon300K
c skadiPipeCWindowRadius 9.5
c skadiPipeCWindowThick 0.3
c skadiPipeCYStep 1.5
c skadiPipeDCladdingMat B4C
c skadiPipeDCladdingThick 0
c skadiPipeDFeMat Aluminium
c skadiPipeDFeThick 0.5
c skadiPipeDFlangeLength 1
c skadiPipeDFlangeRadius 11.5
c skadiPipeDLength 325.5
c skadiPipeDRadius 7.5
c skadiPipeDVoidMat Void
c skadiPipeDWindowActive 3
c skadiPipeDWindowBackMat Silicon300K
c skadiPipeDWindowFrontMat Silicon300K
c skadiPipeDWindowRadius 9.5
c skadiPipeDWindowThick 0.3
c skadiPipeDYStep 1.5
c skadiPipeDZAngle 1
c skadiPipeECladdingMat B4C
c skadiPipeECladdingThick 0
c skadiPipeEFeMat Aluminium
c skadiPipeEFeThick 0.5
c skadiPipeEFlangeLength 1
c skadiPipeEFlangeRadius 11.5
c skadiPipeELength 124
c skadiPipeERadius 7.5
c skadiPipeEVoidMat Void
c skadiPipeEWindowActive 3
c skadiPipeEWindowBackMat Silicon300K
c skadiPipeEWindowFrontMat Silicon300K
c skadiPipeEWindowRadius 9.5
c skadiPipeEWindowThick 0.3
c skadiPipeEYStep 1.5
c skadiPipeEZAngle 0.25
c skadiPipeFCladdingMat B4C
c skadiPipeFCladdingThick 0
c skadiPipeFFeMat Aluminium
c skadiPipeFFeThick 0.5
c skadiPipeFFlangeLength 1
c skadiPipeFFlangeRadius 11.5
c skadiPipeFLength 24.5
c skadiPipeFRadius 7.5
c skadiPipeFVoidMat Void
c skadiPipeFWindowActive 3
c skadiPipeFWindowBackMat Silicon300K
c skadiPipeFWindowFrontMat Silicon300K
c skadiPipeFWindowRadius 9.5
c skadiPipeFWindowThick 0.3
c skadiPipeFYStep 1.25
c skadiPipeOutACladdingMat B4C
c skadiPipeOutACladdingThick 0
c skadiPipeOutAFeMat Aluminium
c skadiPipeOutAFeThick 0.5
c skadiPipeOutAFlangeLength 1
c skadiPipeOutAFlangeRadius 11.5
c skadiPipeOutALength 51
c skadiPipeOutARadius 7.5
c skadiPipeOutAVoidMat Void
c skadiPipeOutAWindowActive 3
c skadiPipeOutAWindowBackMat Silicon300K
c skadiPipeOutAWindowFrontMat Silicon300K
c skadiPipeOutAWindowRadius 9.5
c skadiPipeOutAWindowThick 0.3
c skadiPipeOutAYStep 0.5
c skadiPipeOutBCladdingMat B4C
c skadiPipeOutBCladdingThick 0
c skadiPipeOutBFeMat Aluminium
c skadiPipeOutBFeThick 0.5
c skadiPipeOutBFlangeLength 1
c skadiPipeOutBFlangeRadius 11.5
c skadiPipeOutBLength 644
c skadiPipeOutBRadius 7.5
c skadiPipeOutBVoidMat Void
c skadiPipeOutBWindowActive 3
c skadiPipeOutBWindowBackMat Silicon300K
c skadiPipeOutBWindowFrontMat Silicon300K
c skadiPipeOutBWindowRadius 9.5
c skadiPipeOutBWindowThick 0.3
c skadiPipeOutBYStep 0.5
c skadiPipeOutCCladdingMat B4C
c skadiPipeOutCCladdingThick 0
c skadiPipeOutCFeMat Aluminium
c skadiPipeOutCFeThick 0.5
c skadiPipeOutCFlangeLength 1
c skadiPipeOutCFlangeRadius 11.5
c skadiPipeOutCLength 307
c skadiPipeOutCRadius 7.5
c skadiPipeOutCVoidMat Void
c skadiPipeOutCWindowActive 3
c skadiPipeOutCWindowBackMat Silicon300K
c skadiPipeOutCWindowFrontMat Silicon300K
c skadiPipeOutCWindowRadius 9.5
c skadiPipeOutCWindowThick 0.3
c skadiPipeOutCYStep 0.5
c skadiPipeOutDCladdingMat B4C
c skadiPipeOutDCladdingThick 0
c skadiPipeOutDFeMat Aluminium
c skadiPipeOutDFeThick 0.5
c skadiPipeOutDFlangeLength 1
c skadiPipeOutDFlangeRadius 11.5
c skadiPipeOutDLength 692
c skadiPipeOutDRadius 7.5
c skadiPipeOutDVoidMat Void
c skadiPipeOutDWindowActive 3
c skadiPipeOutDWindowBackMat Silicon300K
c skadiPipeOutDWindowFrontMat Silicon300K
c skadiPipeOutDWindowRadius 9.5
c skadiPipeOutDWindowThick 0.3
c skadiPipeOutDYStep 0.5
c skadiPitAColletDepth 5
c skadiPitAColletHeight 15
c skadiPitAColletMat Tungsten
c skadiPitAColletWidth 40
c skadiPitAConcBack 10
c skadiPitAConcDepth 10
c skadiPitAConcFront 10
c skadiPitAConcHeight 10
c skadiPitAConcMat Concrete
c skadiPitAConcWidth 10
c skadiPitACutBackRadius 5
c skadiPitACutBackShape Square
c skadiPitACutFrontRadius 5
c skadiPitACutFrontShape Square
c skadiPitAFeBack 6
c skadiPitAFeDepth 6
c skadiPitAFeFront 6
c skadiPitAFeHeight 6
c skadiPitAFeMat CastIron
c skadiPitAFeWidth 6
c skadiPitAVoidDepth 66
c skadiPitAVoidHeight 100
c skadiPitAVoidLength 25
c skadiPitAVoidWidth 160
c skadiPitAYStep 207.05
c skadiPitBColletDepth 5
c skadiPitBColletHeight 15
c skadiPitBColletMat Tungsten
c skadiPitBColletWidth 40
c skadiPitBConcBack 10
c skadiPitBConcDepth 10
c skadiPitBConcFront 10
c skadiPitBConcHeight 10
c skadiPitBConcMat Concrete
c skadiPitBConcWidth 10
c skadiPitBCutBackRadius 5
c skadiPitBCutBackShape Square
c skadiPitBCutFrontRadius 5
c skadiPitBCutFrontShape Square
c skadiPitBFeBack 6
c skadiPitBFeDepth 6
c skadiPitBFeFront 6
c skadiPitBFeHeight 6
c skadiPitBFeMat CastIron
c skadiPitBFeWidth 6
c skadiPitBVoidDepth 66
c skadiPitBVoidHeight 100
c skadiPitBVoidLength 25
c skadiPitBVoidWidth 160
c skadiPitBYStep 688
c skadiPitCColletDepth 5
c skadiPitCColletHeight 15
c skadiPitCColletMat Tungsten
c skadiPitCColletWidth 40
c skadiPitCConcBack 10
c skadiPitCConcDepth 10
c skadiPitCConcFront 10
c skadiPitCConcHeight 10
c skadiPitCConcMat Concrete
c skadiPitCConcWidth 10
c skadiPitCCutBackRadius 5
c skadiPitCCutBackShape Square
c skadiPitCCutFrontRadius 5
c skadiPitCCutFrontShape Square
c skadiPitCFeBack 6
c skadiPitCFeDepth 6
c skadiPitCFeFront 6
c skadiPitCFeHeight 6
c skadiPitCFeMat CastIron
c skadiPitCFeWidth 6
c skadiPitCVoidDepth 66
c skadiPitCVoidHeight 100
c skadiPitCVoidLength 55
c skadiPitCVoidWidth 160
c skadiPitCYStep 298
c skadiShieldADefMat Stainless304
c skadiShieldADepth 90
c skadiShieldAFloorLen1 19.5
c skadiShieldAFloorLen2 0.5
c skadiShieldAFloorLen3 5
c skadiShieldAFloorLen4 5
c skadiShieldAFloorMat1 B4C
c skadiShieldAFloorMat2 Steel71
c skadiShieldAFloorMat3 Steel71
c skadiShieldAFloorMat4 Concrete
c skadiShieldAHeight 90
c skadiShieldALeft 90
c skadiShieldALength 32
c skadiShieldANFloorLayers 5
c skadiShieldANRoofLayers 8
c skadiShieldANSeg 1
c skadiShieldANWallLayers 8
c skadiShieldARight 90
c skadiShieldARoofLen1 19.5
c skadiShieldARoofLen2 0.5
c skadiShieldARoofLen3 5
c skadiShieldARoofLen4 5
c skadiShieldARoofMat1 B4C
c skadiShieldARoofMat2 Steel71
c skadiShieldARoofMat3 Steel71
c skadiShieldARoofMat4 Concrete
c skadiShieldAWallLen1 19.5
c skadiShieldAWallLen2 0.5
c skadiShieldAWallLen3 5
c skadiShieldAWallLen4 5
c skadiShieldAWallMat1 B4C
c skadiShieldAWallMat2 Steel71
c skadiShieldAWallMat3 Steel71
c skadiShieldAWallMat4 Concrete
c skadiShieldAYStep 175
c skadiShieldBDefMat Stainless304
c skadiShieldBDepth 90
c skadiShieldBFloorLen1 19.5
c skadiShieldBFloorLen2 0.5
c skadiShieldBFloorLen3 5
c skadiShieldBFloorLen4 5
c skadiShieldBFloorMat1 B4C
c skadiShieldBFloorMat2 Steel71
c skadiShieldBFloorMat3 Steel71
c skadiShieldBFloorMat4 Concrete
c skadiShieldBHeight 90
c skadiShieldBLeft 90
c skadiShieldBLength 688
c skadiShieldBNFloorLayers 5
c skadiShieldBNRoofLayers 8
c skadiShieldBNSeg 4
c skadiShieldBNWallLayers 10
c skadiShieldBRight 90
c skadiShieldBRoofLen1 19.5
c skadiShieldBRoofLen2 0.5
c skadiShieldBRoofLen3 5
c skadiShieldBRoofLen4 5
c skadiShieldBRoofMat1 B4C
c skadiShieldBRoofMat2 Steel71
c skadiShieldBRoofMat3 Steel71
c skadiShieldBRoofMat4 Concrete
c skadiShieldBWallLen1 19.5
c skadiShieldBWallLen2 0.5
c skadiShieldBWallLen3 5
c skadiShieldBWallLen4 5
c skadiShieldBWallMat1 B4C
c skadiShieldBWallMat2 Steel71
c skadiShieldBWallMat3 Steel71
c skadiShieldBWallMat4 Concrete
c skadiShieldCDefMat Stainless304
c skadiShieldCDepth 90
c skadiShieldCFloorLen1 19.5
c skadiShieldCFloorLen2 0.5
c skadiShieldCFloorLen3 5
c skadiShieldCFloorLen4 5
c skadiShieldCFloorMat1 B4C
c skadiShieldCFloorMat2 Steel71
c skadiShieldCFloorMat3 Steel71
c skadiShieldCFloorMat4 Concrete
c skadiShieldCHeight 90
c skadiShieldCLeft 90
c skadiShieldCLength 297
c skadiShieldCNFloorLayers 5
c skadiShieldCNRoofLayers 8
c skadiShieldCNSeg 2
c skadiShieldCNWallLayers 8
c skadiShieldCRight 90
c skadiShieldCRoofLen1 19.5
c skadiShieldCRoofLen2 0.5
c skadiShieldCRoofLen3 5
c skadiShieldCRoofLen4 5
c skadiShieldCRoofMat1 B4C
c skadiShieldCRoofMat2 Steel71
c skadiShieldCRoofMat3 Steel71
c skadiShieldCRoofMat4 Concrete
c skadiShieldCWallLen1 19.5
c skadiShieldCWallLen2 0.5
c skadiShieldCWallLen3 5
c skadiShieldCWallLen4 5
c skadiShieldCWallMat1 B4C
c skadiShieldCWallMat2 Steel71
c skadiShieldCWallMat3 Steel71
c skadiShieldCWallMat4 Concrete
c skadiShieldDDefMat Stainless304
c skadiShieldDDepth 90
c skadiShieldDFloorLen1 19.5
c skadiShieldDFloorLen2 0.5
c skadiShieldDFloorLen3 5
c skadiShieldDFloorLen4 5
c skadiShieldDFloorMat1 B4C
c skadiShieldDFloorMat2 Steel71
c skadiShieldDFloorMat3 Steel71
c skadiShieldDFloorMat4 Concrete
c skadiShieldDHeight 90
c skadiShieldDLeft 90
c skadiShieldDLength 761
c skadiShieldDNFloorLayers 5
c skadiShieldDNRoofLayers 8
c skadiShieldDNSeg 5
c skadiShieldDNWallLayers 8
c skadiShieldDRight 90
c skadiShieldDRoofLen1 19.5
c skadiShieldDRoofLen2 0.5
c skadiShieldDRoofLen3 5
c skadiShieldDRoofLen4 5
c skadiShieldDRoofMat1 B4C
c skadiShieldDRoofMat2 Steel71
c skadiShieldDRoofMat3 Steel71
c skadiShieldDRoofMat4 Concrete
c skadiShieldDWallLen1 19.5
c skadiShieldDWallLen2 0.5
c skadiShieldDWallLen3 5
c skadiShieldDWallLen4 5
c skadiShieldDWallMat1 B4C
c skadiShieldDWallMat2 Steel71
c skadiShieldDWallMat3 Steel71
c skadiShieldDWallMat4 Concrete
c skadiShieldFDefMat Stainless304
c skadiShieldFDepth 190
c skadiShieldFFloorLen1 120
c skadiShieldFFloorMat1 Concrete
c skadiShieldFHeight 170
c skadiShieldFLeft 170
c skadiShieldFLength 2201.4
c skadiShieldFNFloorLayers 8
c skadiShieldFNRoofLayers 8
c skadiShieldFNSeg 5
c skadiShieldFNWallLayers 8
c skadiShieldFRight 170
c skadiShieldFRoofLen1 110
c skadiShieldFRoofMat1 Concrete
c skadiShieldFWallLen1 110
c skadiShieldFWallMat1 Concrete
c skadiShieldFYStep 678
c skadiStopPoint 0
c -------------------------------------------------------
c --------------- CELL CARDS --------------------------
c -------------------------------------------------------
1 0   1
2 0   ( -1051 : -689 : -688 : -598 : 599 : 1064 ) ( -1127 : -1129 :
        -1093 : 1094 : 1130 : 1128 ) ( -736 : -728 : -973 : 1006 : 738
        : 737 ) ( -736 : -728 : -754 : 972 : 738 : 737 ) ( -703 : -705
        : -698 : 707 : 706 : 704 ) ( -759 : -761 : -763 : 764 : 762 :
        760 ) ( -75 : -3 : -62 : -78 : 94 : 135 : 133 ) ( -129 : -70 :
        -61 : 130 : 91 : 132 ) ( -117 : -105 : -77 : -62 : 3 : 78 : 119
        : 95 ) ( -90 : -71 : -77 : -3 : -62 : 78 : 94 : 95 ) ( -58 : 62
        : 61 ) ( -171 : ( ( 183 : 180 ) ( 172 : 173 ) ( 164 : 170 ) ) )
        ( -184 : 177 : 176 ) -1 ( -150 : 151 : 162 ) ( -69 : -75 : -77
        : -3 : -62 : 78 : 76 : 79 ) ( -104 : -106 : -77 : -62 : 3 : 78
        : 107 : 79 ) ( -70 : -63 : 61 : 91 : 132 ) ( -129 : -70 : -130
        : 128 : 91 : 62 ) ( -106 : -62 : -78 : 3 : 119 : 135 : 136 ) (
        -736 : -728 : -95 : -117 : 753 : 738 : 737 ) ( -974 : -761 :
        -763 : 764 : 762 : 975 ) ( -1008 : -761 : -763 : 764 : 762 :
        1009 ) ( -736 : -728 : -1007 : 1051 : 738 : 737 ) ( -1081 :
        -1082 : -1069 : 1084 : 1083 : 1093 ) ( -1093 : 1088 : 1087 )
3 118 0.122325  -9 4 -6
4 0  -150 9 -16
5 0  162 -137 -16 150 -151
6 118 0.122325  28 -5 -6
7 3 0.0878729  ( 6 : -4 ) -9 7 -16
8 3 0.0878729  ( 6 : 5 ) 28 -8 -16
9 9 2.45e-05  -10 -2 16 -151 150 -11
10 9 2.45e-05  -12 10 -13
11 54 0.0833854  -12 10 -14 13
12 9 2.45e-05  -15 12 -13
13 54 0.0833854  -15 12 -14 13
14 0  -2 -62 15 -13
15 54 0.0833854  -2 -62 15 -14 13
16 5 0.0582256  -18 -17 151
17 0  -19 18 -17 151
18 3 0.0878729  -16 19 -17 151
19 11 0.100283  -21 -20 17
20 5 0.0582256  -18 21 -20 17
21 0  -19 18 -20 17
22 3 0.0878729  -16 19 -20 17
23 0  -16 -22 320
24 5 0.0582256  -24 -23 22
25 0  -25 24 -23 22
26 3 0.0878729  -16 25 -23 22
27 11 0.100283  -27 -26 23
28 5 0.0582256  -24 27 -26 23
29 0  -25 24 -26 23
30 3 0.0878729  -16 25 -26 23
31 5 0.0582256  -24 -28 26
32 0  -25 24 -28 26
33 3 0.0878729  -16 25 -28 26
34 0  -57 3 16 ( 32 : -38 ) ( 31 : 37 ) -30 29
35 5 0.0582256  ( -29 : 30 : ( -37 -31 ) : ( -32 38 ) ) -57 3 16 ( 36 :
        -38 ) ( 35 : 37 ) -34 33
36 0  -57 -3 16 44 43 -42 41
37 5 0.0582256  ( -41 : -43 : -44 : 42 ) -57 -3 16 48 47 -46 45
38 0  ( -569 : 588 : 568 ) ( -529 : 548 : 528 ) ( -10 : 12 : 14 ) (
        -150 : -16 : 2 : 10 : 151 : 11 ) ( -12 : 15 : 14 ) ( -15 : 2 :
        14 : 62 ) ( -33 : 34 : ( -35 -39 ) : ( 40 -36 ) ) ( -150 : 151
        : 162 ) 49 -50 -51 ( -7 : 8 : 16 ) ( -184 : 177 : 176 ) ( -171
        : ( ( 164 : 170 ) ( 172 : 173 ) ( 183 : 180 ) ) ) ( -45 : -47 :
        -48 : 46 ) ( -529 : 527 : 528 ) ( -569 : 567 : 568 )
39 121 0.0862739  ( -570 : 592 : 569 ) ( -570 : 574 : 569 ) ( -530 :
        552 : 529 ) ( -530 : 534 : 529 ) ( -10 : 12 : 14 ) ( -150 : -16
        : 2 : 10 : 151 : 11 ) ( -12 : 15 : 14 ) ( -15 : 2 : 14 : 62 ) (
        -33 : 34 : ( -35 -39 ) : ( 40 -36 ) ) ( -150 : 151 : 162 ) 52
        -53 -54 ( -49 : 50 : 51 ) ( -184 : 177 : 176 ) ( -171 : ( ( 164
        : 170 ) ( 172 : 173 ) ( 183 : 180 ) ) ) ( -45 : -47 : -48 : 46
        ) ( -529 : 527 : 528 ) ( -529 : 548 : 528 ) ( -569 : 567 : 568
        ) ( -569 : 588 : 568 )
40 121 0.0862739  ( -10 : 12 : 14 ) ( -150 : -16 : 2 : 10 : 151 : 11 )
        ( -12 : 15 : 14 ) ( -15 : 2 : 14 : 62 ) ( -33 : 34 : ( -35 -39
        ) : ( 40 -36 ) ) ( -150 : 151 : 162 ) 55 -56 -57 ( -52 : 53 :
        54 ) ( -184 : 177 : 176 ) ( -171 : ( ( 164 : 170 ) ( 172 : 173
        ) ( 183 : 180 ) ) ) ( -45 : -47 : -48 : 46 )
41 54 0.0833854  ( -10 : 12 : 14 ) ( -150 : -16 : 2 : 10 : 151 : 11 ) (
        -12 : 15 : 14 ) ( -15 : 2 : 14 : 62 ) ( -57 : -458 : -459 : 460
        : 457 : 463 : 3 ) ( -3 : -57 : -457 : -459 : 460 : 458 : 463 )
        ( -150 : 151 : 162 ) 58 -63 -60 ( -55 : 57 : 56 ) ( -184 : 177
        : 176 ) ( -171 : ( ( 164 : 170 ) ( 172 : 173 ) ( 183 : 180 ) )
        ) ( -3 : -463 : -457 : -461 : 462 : 458 : 60 ) ( -463 : -458 :
        -461 : 462 : 457 : 60 : 3 )
42 54 0.0833854  ( -171 : ( ( 183 : 180 ) ( 172 : 173 ) ( 164 : 170 ) )
        ) ( -184 : 177 : 176 ) -59 -64 63 ( -150 : 151 : 162 )
43 0  ( -10 : 12 : 14 ) ( -150 : -16 : 2 : 10 : 151 : 11 ) ( -12 : 15 :
        14 ) ( -15 : 2 : 14 : 62 ) ( -150 : 151 : 162 ) 58 -63 60 -62 (
        -184 : 177 : 176 ) ( -171 : ( ( 164 : 170 ) ( 172 : 173 ) ( 183
        : 180 ) ) )
44 0  ( -171 : ( ( 183 : 180 ) ( 172 : 173 ) ( 164 : 170 ) ) ) ( -184 :
        177 : 176 ) -62 64 -66 63 ( -150 : 151 : 162 )
45 0  ( -171 : ( ( 183 : 180 ) ( 172 : 173 ) ( 164 : 170 ) ) ) ( -184 :
        177 : 176 ) -65 64 -59 66 ( -150 : 151 : 162 )
46 0  ( -171 : ( ( 183 : 180 ) ( 172 : 173 ) ( 164 : 170 ) ) ) ( -184 :
        177 : 176 ) -65 -61 59 ( -150 : 151 : 162 )
47 3 0.0878729  ( -171 : ( ( 183 : 180 ) ( 172 : 173 ) ( 164 : 170 ) )
        ) ( -184 : 177 : 176 ) -67 66 -62 65 ( -150 : 151 : 162 )
48 0  ( -171 : ( ( 183 : 180 ) ( 172 : 173 ) ( 164 : 170 ) ) ) ( -184 :
        177 : 176 ) -68 67 -62 65 ( -150 : 151 : 162 )
49 49 0.074981  ( -171 : ( ( 183 : 180 ) ( 172 : 173 ) ( 164 : 170 ) )
        ) ( -184 : 177 : 176 ) -61 68 -62 65 ( -150 : 151 : 162 )
50 0  62 3 -73 72 -71 70 -74 69
51 71 0.0843223  62 3 -80 72 75 -70 -74 69
52 71 0.0843223  ( -90 : -71 : -77 : -3 : -62 : 78 : 94 : 95 ) 69 -74
        71 -76 72 -80 3 62
53 71 0.0843223  ( -90 : -71 : -77 : -3 : -62 : 78 : 94 : 95 ) 69 -79
        75 -76 -72 77 3 62
54 0  62 3 -73 72 -91 71 -92 90
55 71 0.0843223  62 3 -80 72 -94 91 -92 90
56 71 0.0843223  62 3 77 -72 -94 71 -95 90
57 0  62 -3 -73 72 -105 91 -74 104
58 71 0.0843223  62 -3 -80 72 106 -91 -74 104
59 71 0.0843223  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 104
        -74 105 -107 72 -80 -3 62
60 71 0.0843223  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 104
        -79 106 -107 -72 77 -3 62
61 0  ( -703 : -705 : -698 : 707 : 706 : 704 ) ( -674 : 675 : 680 ) (
        -636 : 637 : 642 ) ( -602 : 603 : 608 ) 117 -92 105 -70 72 -73
        -3 62 ( -619 : 620 : 625 ) ( -655 : 656 : 661 ) ( -472 : -696 :
        -679 : 698 : 697 : 473 )
62 71 0.0843223  62 -3 -80 72 -119 70 -92 117
63 71 0.0843223  62 -3 77 -72 -119 105 -95 117
64 49 0.074981  -128 130 -91 70 -62 129
65 49 0.074981  61 -130 -91 70 -131 129
66 0  61 -130 -91 70 -132 131
67 0  62 3 63 -66 -91 70 -131
68 0  62 3 -61 63 -91 70 -132 131
69 3 0.0878729  -131 70 -91 66 3 -67 62
70 0  -131 70 -91 3 67 -68 62
71 49 0.074981  -131 70 -91 -61 3 68 62
72 0  ( 132 : 130 ) 78 62 3 -91 70 -134 -133
73 49 0.074981  62 3 -91 70 -135 134 -133
74 49 0.074981  62 3 78 -70 75 -135 -133
75 49 0.074981  62 3 78 91 -94 -135 -133
76 0  78 62 -3 -70 91 -134 -136
77 49 0.074981  62 -3 -70 91 -135 134 -136
78 49 0.074981  62 -3 78 -91 106 -135 -136
79 49 0.074981  62 -3 78 70 -119 -135 -136
80 0  156 -145 -143 140 -285 284
81 3 0.0878729  156 -145 -143 140 -286 285  tmp=5.17040532e-08
82 0  156 -145 -143 140 -287 286
83 3 0.0878729  156 -145 -143 140 -288 287  tmp=5.17040532e-08
84 0  156 -145 -143 140 -289 288
85 3 0.0878729  156 -145 -143 140 -290 289  tmp=5.17040532e-08
86 0  156 -145 -143 140 -291 290
87 3 0.0878729  156 -145 -143 140 -292 291  tmp=5.17040532e-08
88 0  156 -145 -143 140 -293 292
89 3 0.0878729  156 -145 -143 140 -294 293  tmp=5.17040532e-08
90 0  156 -145 -143 140 -295 294
91 3 0.0878729  156 -145 -143 140 -296 295  tmp=5.17040532e-08
92 0  156 -145 -143 140 -297 296
93 3 0.0878729  156 -145 -143 140 -298 297  tmp=5.17040532e-08
94 0  156 -145 -143 140 -299 298
95 3 0.0878729  156 -145 -143 140 -300 299  tmp=5.17040532e-08
96 0  156 -145 -143 140 -301 300
97 3 0.0878729  156 -145 -143 140 -302 301  tmp=5.17040532e-08
98 0  156 -145 -143 140 -303 302
99 3 0.0878729  156 -145 -143 140 -304 303  tmp=5.17040532e-08
100 0  156 -145 -143 140 -305 304
101 3 0.0878729  156 -145 -143 140 -306 305  tmp=5.17040532e-08
102 0  156 -145 -143 140 -307 306
103 3 0.0878729  156 -145 -143 140 -308 307  tmp=5.17040532e-08
104 0  156 -145 -143 140 -309 308
105 3 0.0878729  156 -145 -143 140 -310 309  tmp=5.17040532e-08
106 0  156 -145 -143 140 -311 310
107 3 0.0878729  156 -145 -143 140 -312 311  tmp=5.17040532e-08
108 0  156 -145 -143 140 -313 312
109 3 0.0878729  156 -145 -143 140 -314 313  tmp=5.17040532e-08
110 0  156 -145 -143 140 -315 314
111 3 0.0878729  156 -145 -143 140 -316 315  tmp=5.17040532e-08
112 0  156 -145 -143 140 -317 316
113 3 0.0878729  156 -145 -143 140 -318 317  tmp=5.17040532e-08
114 0  156 -145 -143 140 -319 318
115 3 0.0878729  156 -145 -143 140 284 319  tmp=5.17040532e-08
116 0  156 -145 -143 140 285 -284
117 3 0.0878729  156 -145 -143 140 286 -285  tmp=5.17040532e-08
118 0  156 -145 -143 140 287 -286
119 3 0.0878729  156 -145 -143 140 288 -287  tmp=5.17040532e-08
120 0  156 -145 -143 140 289 -288
121 3 0.0878729  156 -145 -143 140 290 -289  tmp=5.17040532e-08
122 0  156 -145 -143 140 291 -290
123 3 0.0878729  156 -145 -143 140 292 -291  tmp=5.17040532e-08
124 0  156 -145 -143 140 293 -292
125 3 0.0878729  156 -145 -143 140 294 -293  tmp=5.17040532e-08
126 0  156 -145 -143 140 295 -294
127 3 0.0878729  156 -145 -143 140 296 -295  tmp=5.17040532e-08
128 0  156 -145 -143 140 297 -296
129 3 0.0878729  156 -145 -143 140 298 -297  tmp=5.17040532e-08
130 0  156 -145 -143 140 299 -298
131 3 0.0878729  156 -145 -143 140 300 -299  tmp=5.17040532e-08
132 0  156 -145 -143 140 301 -300
133 3 0.0878729  156 -145 -143 140 302 -301  tmp=5.17040532e-08
134 0  156 -145 -143 140 303 -302
135 3 0.0878729  156 -145 -143 140 304 -303  tmp=5.17040532e-08
136 0  156 -145 -143 140 305 -304
137 3 0.0878729  156 -145 -143 140 306 -305  tmp=5.17040532e-08
138 0  156 -145 -143 140 307 -306
139 3 0.0878729  156 -145 -143 140 308 -307  tmp=5.17040532e-08
140 0  156 -145 -143 140 309 -308
141 3 0.0878729  156 -145 -143 140 310 -309  tmp=5.17040532e-08
142 0  156 -145 -143 140 311 -310
143 3 0.0878729  156 -145 -143 140 312 -311  tmp=5.17040532e-08
144 0  156 -145 -143 140 313 -312
145 3 0.0878729  156 -145 -143 140 314 -313  tmp=5.17040532e-08
146 0  156 -145 -143 140 315 -314
147 3 0.0878729  156 -145 -143 140 316 -315  tmp=5.17040532e-08
148 0  156 -145 -143 140 317 -316
149 3 0.0878729  156 -145 -143 140 318 -317  tmp=5.17040532e-08
150 0  156 -145 -143 140 319 -318
151 3 0.0878729  156 -145 -143 140 -284 -319  tmp=5.17040532e-08
152 0  144 -155 -143 140 -285 284
153 3 0.0878729  144 -155 -143 140 -286 285  tmp=5.17040532e-08
154 0  144 -155 -143 140 -287 286
155 3 0.0878729  144 -155 -143 140 -288 287  tmp=5.17040532e-08
156 0  144 -155 -143 140 -289 288
157 3 0.0878729  144 -155 -143 140 -290 289  tmp=5.17040532e-08
158 0  144 -155 -143 140 -291 290
159 3 0.0878729  144 -155 -143 140 -292 291  tmp=5.17040532e-08
160 0  144 -155 -143 140 -293 292
161 3 0.0878729  144 -155 -143 140 -294 293  tmp=5.17040532e-08
162 0  144 -155 -143 140 -295 294
163 3 0.0878729  144 -155 -143 140 -296 295  tmp=5.17040532e-08
164 0  144 -155 -143 140 -297 296
165 3 0.0878729  144 -155 -143 140 -298 297  tmp=5.17040532e-08
166 0  144 -155 -143 140 -299 298
167 3 0.0878729  144 -155 -143 140 -300 299  tmp=5.17040532e-08
168 0  144 -155 -143 140 -301 300
169 3 0.0878729  144 -155 -143 140 -302 301  tmp=5.17040532e-08
170 0  144 -155 -143 140 -303 302
171 3 0.0878729  144 -155 -143 140 -304 303  tmp=5.17040532e-08
172 0  144 -155 -143 140 -305 304
173 3 0.0878729  144 -155 -143 140 -306 305  tmp=5.17040532e-08
174 0  144 -155 -143 140 -307 306
175 3 0.0878729  144 -155 -143 140 -308 307  tmp=5.17040532e-08
176 0  144 -155 -143 140 -309 308
177 3 0.0878729  144 -155 -143 140 -310 309  tmp=5.17040532e-08
178 0  144 -155 -143 140 -311 310
179 3 0.0878729  144 -155 -143 140 -312 311  tmp=5.17040532e-08
180 0  144 -155 -143 140 -313 312
181 3 0.0878729  144 -155 -143 140 -314 313  tmp=5.17040532e-08
182 0  144 -155 -143 140 -315 314
183 3 0.0878729  144 -155 -143 140 -316 315  tmp=5.17040532e-08
184 0  144 -155 -143 140 -317 316
185 3 0.0878729  144 -155 -143 140 -318 317  tmp=5.17040532e-08
186 0  144 -155 -143 140 -319 318
187 3 0.0878729  144 -155 -143 140 284 319  tmp=5.17040532e-08
188 0  144 -155 -143 140 285 -284
189 3 0.0878729  144 -155 -143 140 286 -285  tmp=5.17040532e-08
190 0  144 -155 -143 140 287 -286
191 3 0.0878729  144 -155 -143 140 288 -287  tmp=5.17040532e-08
192 0  144 -155 -143 140 289 -288
193 3 0.0878729  144 -155 -143 140 290 -289  tmp=5.17040532e-08
194 0  144 -155 -143 140 291 -290
195 3 0.0878729  144 -155 -143 140 292 -291  tmp=5.17040532e-08
196 0  144 -155 -143 140 293 -292
197 3 0.0878729  144 -155 -143 140 294 -293  tmp=5.17040532e-08
198 0  144 -155 -143 140 295 -294
199 3 0.0878729  144 -155 -143 140 296 -295  tmp=5.17040532e-08
200 0  144 -155 -143 140 297 -296
201 3 0.0878729  144 -155 -143 140 298 -297  tmp=5.17040532e-08
202 0  144 -155 -143 140 299 -298
203 3 0.0878729  144 -155 -143 140 300 -299  tmp=5.17040532e-08
204 0  144 -155 -143 140 301 -300
205 3 0.0878729  144 -155 -143 140 302 -301  tmp=5.17040532e-08
206 0  144 -155 -143 140 303 -302
207 3 0.0878729  144 -155 -143 140 304 -303  tmp=5.17040532e-08
208 0  144 -155 -143 140 305 -304
209 3 0.0878729  144 -155 -143 140 306 -305  tmp=5.17040532e-08
210 0  144 -155 -143 140 307 -306
211 3 0.0878729  144 -155 -143 140 308 -307  tmp=5.17040532e-08
212 0  144 -155 -143 140 309 -308
213 3 0.0878729  144 -155 -143 140 310 -309  tmp=5.17040532e-08
214 0  144 -155 -143 140 311 -310
215 3 0.0878729  144 -155 -143 140 312 -311  tmp=5.17040532e-08
216 0  144 -155 -143 140 313 -312
217 3 0.0878729  144 -155 -143 140 314 -313  tmp=5.17040532e-08
218 0  144 -155 -143 140 315 -314
219 3 0.0878729  144 -155 -143 140 316 -315  tmp=5.17040532e-08
220 0  144 -155 -143 140 317 -316
221 3 0.0878729  144 -155 -143 140 318 -317  tmp=5.17040532e-08
222 0  144 -155 -143 140 319 -318
223 3 0.0878729  144 -155 -143 140 -284 -319  tmp=5.17040532e-08
224 3 0.0878729  147 -144 -143 140  tmp=5.17040532e-08
225 3 0.0878729  -143 140 155 -282  tmp=5.17040532e-08
226 3 0.0878729  155 -143 140 -156 283  tmp=5.17040532e-08
227 0  -143 140 -283 282 -285 284
228 3 0.0878729  -143 140 -283 282 -286 285  tmp=5.17040532e-08
229 0  -143 140 -283 282 -287 286
230 3 0.0878729  -143 140 -283 282 -288 287  tmp=5.17040532e-08
231 0  -143 140 -283 282 -289 288
232 3 0.0878729  -143 140 -283 282 -290 289  tmp=5.17040532e-08
233 0  -143 140 -283 282 -291 290
234 3 0.0878729  -143 140 -283 282 -292 291  tmp=5.17040532e-08
235 0  -143 140 -283 282 -293 292
236 3 0.0878729  -143 140 -283 282 -294 293  tmp=5.17040532e-08
237 0  -143 140 -283 282 -295 294
238 3 0.0878729  -143 140 -283 282 -296 295  tmp=5.17040532e-08
239 0  -143 140 -283 282 -297 296
240 3 0.0878729  -143 140 -283 282 -298 297  tmp=5.17040532e-08
241 0  -143 140 -283 282 -299 298
242 3 0.0878729  -143 140 -283 282 -300 299  tmp=5.17040532e-08
243 0  -143 140 -283 282 -301 300
244 3 0.0878729  -143 140 -283 282 -302 301  tmp=5.17040532e-08
245 0  -143 140 -283 282 -303 302
246 3 0.0878729  -143 140 -283 282 -304 303  tmp=5.17040532e-08
247 0  -143 140 -283 282 -305 304
248 3 0.0878729  -143 140 -283 282 -306 305  tmp=5.17040532e-08
249 0  -143 140 -283 282 -307 306
250 3 0.0878729  -143 140 -283 282 -308 307  tmp=5.17040532e-08
251 0  -143 140 -283 282 -309 308
252 3 0.0878729  -143 140 -283 282 -310 309  tmp=5.17040532e-08
253 0  -143 140 -283 282 -311 310
254 3 0.0878729  -143 140 -283 282 -312 311  tmp=5.17040532e-08
255 0  -143 140 -283 282 -313 312
256 3 0.0878729  -143 140 -283 282 -314 313  tmp=5.17040532e-08
257 0  -143 140 -283 282 -315 314
258 3 0.0878729  -143 140 -283 282 -316 315  tmp=5.17040532e-08
259 0  -143 140 -283 282 -317 316
260 3 0.0878729  -143 140 -283 282 -318 317  tmp=5.17040532e-08
261 0  -143 140 -283 282 -319 318
262 3 0.0878729  -143 140 -283 282 284 319  tmp=5.17040532e-08
263 0  -143 140 -283 282 285 -284
264 3 0.0878729  -143 140 -283 282 286 -285  tmp=5.17040532e-08
265 0  -143 140 -283 282 287 -286
266 3 0.0878729  -143 140 -283 282 288 -287  tmp=5.17040532e-08
267 0  -143 140 -283 282 289 -288
268 3 0.0878729  -143 140 -283 282 290 -289  tmp=5.17040532e-08
269 0  -143 140 -283 282 291 -290
270 3 0.0878729  -143 140 -283 282 292 -291  tmp=5.17040532e-08
271 0  -143 140 -283 282 293 -292
272 3 0.0878729  -143 140 -283 282 294 -293  tmp=5.17040532e-08
273 0  -143 140 -283 282 295 -294
274 3 0.0878729  -143 140 -283 282 296 -295  tmp=5.17040532e-08
275 0  -143 140 -283 282 297 -296
276 3 0.0878729  -143 140 -283 282 298 -297  tmp=5.17040532e-08
277 0  -143 140 -283 282 299 -298
278 3 0.0878729  -143 140 -283 282 300 -299  tmp=5.17040532e-08
279 0  -143 140 -283 282 301 -300
280 3 0.0878729  -143 140 -283 282 302 -301  tmp=5.17040532e-08
281 0  -143 140 -283 282 303 -302
282 3 0.0878729  -143 140 -283 282 304 -303  tmp=5.17040532e-08
283 0  -143 140 -283 282 305 -304
284 3 0.0878729  -143 140 -283 282 306 -305  tmp=5.17040532e-08
285 0  -143 140 -283 282 307 -306
286 3 0.0878729  -143 140 -283 282 308 -307  tmp=5.17040532e-08
287 0  -143 140 -283 282 309 -308
288 3 0.0878729  -143 140 -283 282 310 -309  tmp=5.17040532e-08
289 0  -143 140 -283 282 311 -310
290 3 0.0878729  -143 140 -283 282 312 -311  tmp=5.17040532e-08
291 0  -143 140 -283 282 313 -312
292 3 0.0878729  -143 140 -283 282 314 -313  tmp=5.17040532e-08
293 0  -143 140 -283 282 315 -314
294 3 0.0878729  -143 140 -283 282 316 -315  tmp=5.17040532e-08
295 0  -143 140 -283 282 317 -316
296 3 0.0878729  -143 140 -283 282 318 -317  tmp=5.17040532e-08
297 0  -143 140 -283 282 319 -318
298 3 0.0878729  -143 140 -283 282 -284 -319  tmp=5.17040532e-08
299 3 0.0878729  -139 138 -146 157  tmp=5.17040532e-08
300 3 0.0878729  -153 152 154 -157  tmp=5.17040532e-08
301 3 0.0878729  -145 153 154 -157  tmp=5.17040532e-08
302 3 0.0878729  144 -152 154 -157  tmp=5.17040532e-08
303 3 0.0878729  147 -144 154 -157  tmp=5.17040532e-08
304 3 0.0878729  -148 145 154 -157  tmp=5.17040532e-08
305 3 0.0878729  -153 152 -154 143  tmp=5.17040532e-08
306 3 0.0878729  -156 153 -154 143  tmp=5.17040532e-08
307 3 0.0878729  155 -152 -154 143  tmp=5.17040532e-08
308 3 0.0878729  -148 156 -154 143  tmp=5.17040532e-08
309 3 0.0878729  147 -155 -154 143  tmp=5.17040532e-08
310 120 0.0494621  -139 138 -149 146  tmp=5.17040532e-08
311 0  -148 147 -160 149 -266 3  tmp=5.17040532e-08
312 0  -148 147 -160 149 -267 266  tmp=5.17040532e-08
313 0  -148 147 -160 149 -268 267  tmp=5.17040532e-08
314 0  -148 147 -160 149 -269 268  tmp=5.17040532e-08
315 0  -148 147 -160 149 -270 269  tmp=5.17040532e-08
316 0  -148 147 -160 149 -271 270  tmp=5.17040532e-08
317 0  -148 147 -160 149 -272 271  tmp=5.17040532e-08
318 0  -148 147 -160 149 -273 272  tmp=5.17040532e-08
319 0  -148 147 -160 149 -137 273  tmp=5.17040532e-08
320 0  -148 147 -160 149 -274 137  tmp=5.17040532e-08
321 0  -148 147 -160 149 -275 274  tmp=5.17040532e-08
322 0  -148 147 -160 149 -276 275  tmp=5.17040532e-08
323 0  -148 147 -160 149 -277 276  tmp=5.17040532e-08
324 0  -148 147 -160 149 -278 277  tmp=5.17040532e-08
325 0  -148 147 -160 149 -279 278  tmp=5.17040532e-08
326 0  -148 147 -160 149 -280 279  tmp=5.17040532e-08
327 0  -148 147 -160 149 -281 280  tmp=5.17040532e-08
328 0  -148 147 -160 149 3 281  tmp=5.17040532e-08
329 0  -148 147 -160 149 266 -3  tmp=5.17040532e-08
330 0  -148 147 -160 149 267 -266  tmp=5.17040532e-08
331 0  -148 147 -160 149 268 -267  tmp=5.17040532e-08
332 0  -148 147 -160 149 269 -268  tmp=5.17040532e-08
333 0  -148 147 -160 149 270 -269  tmp=5.17040532e-08
334 0  -148 147 -160 149 271 -270  tmp=5.17040532e-08
335 0  -148 147 -160 149 272 -271  tmp=5.17040532e-08
336 0  -148 147 -160 149 273 -272  tmp=5.17040532e-08
337 0  -148 147 -160 149 137 -273  tmp=5.17040532e-08
338 0  -148 147 -160 149 274 -137  tmp=5.17040532e-08
339 0  -148 147 -160 149 275 -274  tmp=5.17040532e-08
340 0  -148 147 -160 149 276 -275  tmp=5.17040532e-08
341 0  -148 147 -160 149 277 -276  tmp=5.17040532e-08
342 0  -148 147 -160 149 278 -277  tmp=5.17040532e-08
343 0  -148 147 -160 149 279 -278  tmp=5.17040532e-08
344 0  -148 147 -160 149 280 -279  tmp=5.17040532e-08
345 0  -148 147 -160 149 281 -280  tmp=5.17040532e-08
346 0  -148 147 -160 149 -3 -281  tmp=5.17040532e-08
347 0  157 -142 139 -149
348 0  157 -138 141 -149
349 3 0.0878729  157 -145 142 -149  tmp=5.17040532e-08
350 3 0.0878729  157 -141 144 -149  tmp=5.17040532e-08
351 0  157 -148 145 -149  tmp=5.17040532e-08
352 0  157 -144 147 -149  tmp=5.17040532e-08
353 3 0.0878729  -148 147 160 -161 -266 3  tmp=5.17040532e-08
354 3 0.0878729  -148 147 160 -161 -267 266  tmp=5.17040532e-08
355 3 0.0878729  -148 147 160 -161 -268 267  tmp=5.17040532e-08
356 3 0.0878729  -148 147 160 -161 -269 268  tmp=5.17040532e-08
357 3 0.0878729  -148 147 160 -161 -270 269  tmp=5.17040532e-08
358 3 0.0878729  -148 147 160 -161 -271 270  tmp=5.17040532e-08
359 3 0.0878729  -148 147 160 -161 -272 271  tmp=5.17040532e-08
360 3 0.0878729  -148 147 160 -161 -273 272  tmp=5.17040532e-08
361 3 0.0878729  -148 147 160 -161 -137 273  tmp=5.17040532e-08
362 3 0.0878729  -148 147 160 -161 -274 137  tmp=5.17040532e-08
363 3 0.0878729  -148 147 160 -161 -275 274  tmp=5.17040532e-08
364 3 0.0878729  -148 147 160 -161 -276 275  tmp=5.17040532e-08
365 3 0.0878729  -148 147 160 -161 -277 276  tmp=5.17040532e-08
366 3 0.0878729  -148 147 160 -161 -278 277  tmp=5.17040532e-08
367 3 0.0878729  -148 147 160 -161 -279 278  tmp=5.17040532e-08
368 3 0.0878729  -148 147 160 -161 -280 279  tmp=5.17040532e-08
369 3 0.0878729  -148 147 160 -161 -281 280  tmp=5.17040532e-08
370 3 0.0878729  -148 147 160 -161 3 281  tmp=5.17040532e-08
371 3 0.0878729  -148 147 160 -161 266 -3  tmp=5.17040532e-08
372 3 0.0878729  -148 147 160 -161 267 -266  tmp=5.17040532e-08
373 3 0.0878729  -148 147 160 -161 268 -267  tmp=5.17040532e-08
374 3 0.0878729  -148 147 160 -161 269 -268  tmp=5.17040532e-08
375 3 0.0878729  -148 147 160 -161 270 -269  tmp=5.17040532e-08
376 3 0.0878729  -148 147 160 -161 271 -270  tmp=5.17040532e-08
377 3 0.0878729  -148 147 160 -161 272 -271  tmp=5.17040532e-08
378 3 0.0878729  -148 147 160 -161 273 -272  tmp=5.17040532e-08
379 3 0.0878729  -148 147 160 -161 137 -273  tmp=5.17040532e-08
380 3 0.0878729  -148 147 160 -161 274 -137  tmp=5.17040532e-08
381 3 0.0878729  -148 147 160 -161 275 -274  tmp=5.17040532e-08
382 3 0.0878729  -148 147 160 -161 276 -275  tmp=5.17040532e-08
383 3 0.0878729  -148 147 160 -161 277 -276  tmp=5.17040532e-08
384 3 0.0878729  -148 147 160 -161 278 -277  tmp=5.17040532e-08
385 3 0.0878729  -148 147 160 -161 279 -278  tmp=5.17040532e-08
386 3 0.0878729  -148 147 160 -161 280 -279  tmp=5.17040532e-08
387 3 0.0878729  -148 147 160 -161 281 -280  tmp=5.17040532e-08
388 3 0.0878729  -148 147 160 -161 -3 -281  tmp=5.17040532e-08
389 3 0.0878729  148 -159 163 -161  tmp=5.17040532e-08
390 3 0.0878729  -147 158 163 -161  tmp=5.17040532e-08
391 0  ( -158 : 159 : 161 ) -162 -151 150 173 -266 3
        tmp=5.17040532e-08
392 0  ( -158 : 159 : 161 ) -162 -151 150 173 -267 266
        tmp=5.17040532e-08
393 0  ( -158 : 159 : 161 ) -162 -151 150 173 -268 267
        tmp=5.17040532e-08
394 0  ( -158 : 159 : 161 ) -162 -151 150 173 -269 268
        tmp=5.17040532e-08
395 0  ( -158 : 159 : 161 ) -162 -151 150 173 -270 269
        tmp=5.17040532e-08
396 0  ( -158 : 159 : 161 ) -162 -151 150 173 -271 270
        tmp=5.17040532e-08
397 0  ( -158 : 159 : 161 ) -162 -151 150 173 -272 271
        tmp=5.17040532e-08
398 0  ( -158 : 159 : 161 ) -162 -151 150 173 -273 272
        tmp=5.17040532e-08
399 0  ( -158 : 159 : 161 ) -162 -151 150 173 -137 273
        tmp=5.17040532e-08
400 0  ( -158 : 159 : 161 ) -162 -151 150 173 -274 137
        tmp=5.17040532e-08
401 0  ( -158 : 159 : 161 ) -162 -151 150 173 -275 274
        tmp=5.17040532e-08
402 0  ( -158 : 159 : 161 ) -162 -151 150 173 -276 275
        tmp=5.17040532e-08
403 0  ( -158 : 159 : 161 ) -162 -151 150 173 -277 276
        tmp=5.17040532e-08
404 0  ( -158 : 159 : 161 ) -162 -151 150 173 -278 277
        tmp=5.17040532e-08
405 0  ( -158 : 159 : 161 ) -162 -151 150 173 -279 278
        tmp=5.17040532e-08
406 0  ( -158 : 159 : 161 ) -162 -151 150 173 -280 279
        tmp=5.17040532e-08
407 0  ( -158 : 159 : 161 ) -162 -151 150 173 -281 280
        tmp=5.17040532e-08
408 0  ( -158 : 159 : 161 ) -162 -151 150 173 3 281  tmp=5.17040532e-08
409 0  ( -158 : 159 : 161 ) -162 -151 150 173 266 -3
        tmp=5.17040532e-08
410 0  ( -158 : 159 : 161 ) -162 -151 150 173 267 -266
        tmp=5.17040532e-08
411 0  ( -158 : 159 : 161 ) -162 -151 150 173 268 -267
        tmp=5.17040532e-08
412 0  ( -158 : 159 : 161 ) -162 -151 150 173 269 -268
        tmp=5.17040532e-08
413 0  ( -158 : 159 : 161 ) -162 -151 150 173 270 -269
        tmp=5.17040532e-08
414 0  ( -158 : 159 : 161 ) -162 -151 150 173 271 -270
        tmp=5.17040532e-08
415 0  ( -158 : 159 : 161 ) -162 -151 150 173 272 -271
        tmp=5.17040532e-08
416 0  ( -158 : 159 : 161 ) -162 -151 150 173 273 -272
        tmp=5.17040532e-08
417 0  ( -158 : 159 : 161 ) -162 -151 150 173 137 -273
        tmp=5.17040532e-08
418 0  ( -158 : 159 : 161 ) -162 -151 150 173 274 -137
        tmp=5.17040532e-08
419 0  ( -158 : 159 : 161 ) -162 -151 150 173 275 -274
        tmp=5.17040532e-08
420 0  ( -158 : 159 : 161 ) -162 -151 150 173 276 -275
        tmp=5.17040532e-08
421 0  ( -158 : 159 : 161 ) -162 -151 150 173 277 -276
        tmp=5.17040532e-08
422 0  ( -158 : 159 : 161 ) -162 -151 150 173 278 -277
        tmp=5.17040532e-08
423 0  ( -158 : 159 : 161 ) -162 -151 150 173 279 -278
        tmp=5.17040532e-08
424 0  ( -158 : 159 : 161 ) -162 -151 150 173 280 -279
        tmp=5.17040532e-08
425 0  ( -158 : 159 : 161 ) -162 -151 150 173 281 -280
        tmp=5.17040532e-08
426 0  ( -158 : 159 : 161 ) -162 -151 150 173 -3 -281
        tmp=5.17040532e-08
427 3 0.0878729  -139 138 -166  tmp=5.17040532e-08
428 3 0.0878729  -153 152 -194  tmp=5.17040532e-08
429 3 0.0878729  -153 152 -195 194  tmp=5.17040532e-08
430 0  -140 167 -153 152 -266 197 3 195  tmp=5.17040532e-08
431 3 0.0878729  -153 152 -196  tmp=5.17040532e-08
432 3 0.0878729  -153 152 -197 196  tmp=5.17040532e-08
433 0  -140 167 -153 152 -267 199 266 197  tmp=5.17040532e-08
434 3 0.0878729  -153 152 -198  tmp=5.17040532e-08
435 3 0.0878729  -153 152 -199 198  tmp=5.17040532e-08
436 0  -140 167 -153 152 -268 201 267 199  tmp=5.17040532e-08
437 3 0.0878729  -153 152 -200  tmp=5.17040532e-08
438 3 0.0878729  -153 152 -201 200  tmp=5.17040532e-08
439 0  -140 167 -153 152 -269 203 268 201  tmp=5.17040532e-08
440 3 0.0878729  -153 152 -202  tmp=5.17040532e-08
441 3 0.0878729  -153 152 -203 202  tmp=5.17040532e-08
442 0  -140 167 -153 152 -270 205 269 203  tmp=5.17040532e-08
443 3 0.0878729  -153 152 -204  tmp=5.17040532e-08
444 3 0.0878729  -153 152 -205 204  tmp=5.17040532e-08
445 0  -140 167 -153 152 -271 207 270 205  tmp=5.17040532e-08
446 3 0.0878729  -153 152 -206  tmp=5.17040532e-08
447 3 0.0878729  -153 152 -207 206  tmp=5.17040532e-08
448 0  -140 167 -153 152 -272 209 271 207  tmp=5.17040532e-08
449 3 0.0878729  -153 152 -208  tmp=5.17040532e-08
450 3 0.0878729  -153 152 -209 208  tmp=5.17040532e-08
451 0  -140 167 -153 152 -273 211 272 209  tmp=5.17040532e-08
452 3 0.0878729  -153 152 -210  tmp=5.17040532e-08
453 3 0.0878729  -153 152 -211 210  tmp=5.17040532e-08
454 0  -140 167 -153 152 -137 213 273 211  tmp=5.17040532e-08
455 3 0.0878729  -153 152 -212  tmp=5.17040532e-08
456 3 0.0878729  -153 152 -213 212  tmp=5.17040532e-08
457 0  -140 167 -153 152 -274 215 137 213  tmp=5.17040532e-08
458 3 0.0878729  -153 152 -214  tmp=5.17040532e-08
459 3 0.0878729  -153 152 -215 214  tmp=5.17040532e-08
460 0  -140 167 -153 152 -275 217 274 215  tmp=5.17040532e-08
461 3 0.0878729  -153 152 -216  tmp=5.17040532e-08
462 3 0.0878729  -153 152 -217 216  tmp=5.17040532e-08
463 0  -140 167 -153 152 -276 219 275 217  tmp=5.17040532e-08
464 3 0.0878729  -153 152 -218  tmp=5.17040532e-08
465 3 0.0878729  -153 152 -219 218  tmp=5.17040532e-08
466 0  -140 167 -153 152 -277 221 276 219  tmp=5.17040532e-08
467 3 0.0878729  -153 152 -220  tmp=5.17040532e-08
468 3 0.0878729  -153 152 -221 220  tmp=5.17040532e-08
469 0  -140 167 -153 152 -278 223 277 221  tmp=5.17040532e-08
470 3 0.0878729  -153 152 -222  tmp=5.17040532e-08
471 3 0.0878729  -153 152 -223 222  tmp=5.17040532e-08
472 0  -140 167 -153 152 -279 225 278 223  tmp=5.17040532e-08
473 3 0.0878729  -153 152 -224  tmp=5.17040532e-08
474 3 0.0878729  -153 152 -225 224  tmp=5.17040532e-08
475 0  -140 167 -153 152 -280 227 279 225  tmp=5.17040532e-08
476 3 0.0878729  -153 152 -226  tmp=5.17040532e-08
477 3 0.0878729  -153 152 -227 226  tmp=5.17040532e-08
478 0  -140 167 -153 152 -281 229 280 227  tmp=5.17040532e-08
479 3 0.0878729  -153 152 -228  tmp=5.17040532e-08
480 3 0.0878729  -153 152 -229 228  tmp=5.17040532e-08
481 0  -140 167 -153 152 3 231 281 229  tmp=5.17040532e-08
482 3 0.0878729  -153 152 -230  tmp=5.17040532e-08
483 3 0.0878729  -153 152 -231 230  tmp=5.17040532e-08
484 0  -140 167 -153 152 266 233 -3 231  tmp=5.17040532e-08
485 3 0.0878729  -153 152 -232  tmp=5.17040532e-08
486 3 0.0878729  -153 152 -233 232  tmp=5.17040532e-08
487 0  -140 167 -153 152 267 235 -266 233  tmp=5.17040532e-08
488 3 0.0878729  -153 152 -234  tmp=5.17040532e-08
489 3 0.0878729  -153 152 -235 234  tmp=5.17040532e-08
490 0  -140 167 -153 152 268 237 -267 235  tmp=5.17040532e-08
491 3 0.0878729  -153 152 -236  tmp=5.17040532e-08
492 3 0.0878729  -153 152 -237 236  tmp=5.17040532e-08
493 0  -140 167 -153 152 269 239 -268 237  tmp=5.17040532e-08
494 3 0.0878729  -153 152 -238  tmp=5.17040532e-08
495 3 0.0878729  -153 152 -239 238  tmp=5.17040532e-08
496 0  -140 167 -153 152 270 241 -269 239  tmp=5.17040532e-08
497 3 0.0878729  -153 152 -240  tmp=5.17040532e-08
498 3 0.0878729  -153 152 -241 240  tmp=5.17040532e-08
499 0  -140 167 -153 152 271 243 -270 241  tmp=5.17040532e-08
500 3 0.0878729  -153 152 -242  tmp=5.17040532e-08
501 3 0.0878729  -153 152 -243 242  tmp=5.17040532e-08
502 0  -140 167 -153 152 272 245 -271 243  tmp=5.17040532e-08
503 3 0.0878729  -153 152 -244  tmp=5.17040532e-08
504 3 0.0878729  -153 152 -245 244  tmp=5.17040532e-08
505 0  -140 167 -153 152 273 247 -272 245  tmp=5.17040532e-08
506 3 0.0878729  -153 152 -246  tmp=5.17040532e-08
507 3 0.0878729  -153 152 -247 246  tmp=5.17040532e-08
508 0  -140 167 -153 152 137 249 -273 247  tmp=5.17040532e-08
509 3 0.0878729  -153 152 -248  tmp=5.17040532e-08
510 3 0.0878729  -153 152 -249 248  tmp=5.17040532e-08
511 0  -140 167 -153 152 274 251 -137 249  tmp=5.17040532e-08
512 3 0.0878729  -153 152 -250  tmp=5.17040532e-08
513 3 0.0878729  -153 152 -251 250  tmp=5.17040532e-08
514 0  -140 167 -153 152 275 253 -274 251  tmp=5.17040532e-08
515 3 0.0878729  -153 152 -252  tmp=5.17040532e-08
516 3 0.0878729  -153 152 -253 252  tmp=5.17040532e-08
517 0  -140 167 -153 152 276 255 -275 253  tmp=5.17040532e-08
518 3 0.0878729  -153 152 -254  tmp=5.17040532e-08
519 3 0.0878729  -153 152 -255 254  tmp=5.17040532e-08
520 0  -140 167 -153 152 277 257 -276 255  tmp=5.17040532e-08
521 3 0.0878729  -153 152 -256  tmp=5.17040532e-08
522 3 0.0878729  -153 152 -257 256  tmp=5.17040532e-08
523 0  -140 167 -153 152 278 259 -277 257  tmp=5.17040532e-08
524 3 0.0878729  -153 152 -258  tmp=5.17040532e-08
525 3 0.0878729  -153 152 -259 258  tmp=5.17040532e-08
526 0  -140 167 -153 152 279 261 -278 259  tmp=5.17040532e-08
527 3 0.0878729  -153 152 -260  tmp=5.17040532e-08
528 3 0.0878729  -153 152 -261 260  tmp=5.17040532e-08
529 0  -140 167 -153 152 280 263 -279 261  tmp=5.17040532e-08
530 3 0.0878729  -153 152 -262  tmp=5.17040532e-08
531 3 0.0878729  -153 152 -263 262  tmp=5.17040532e-08
532 0  -140 167 -153 152 281 265 -280 263  tmp=5.17040532e-08
533 3 0.0878729  -153 152 -264  tmp=5.17040532e-08
534 3 0.0878729  -153 152 -265 264  tmp=5.17040532e-08
535 0  -140 167 -153 152 -3 195 -281 265  tmp=5.17040532e-08
536 3 0.0878729  -156 153 -194  tmp=5.17040532e-08
537 3 0.0878729  -156 153 -195 194  tmp=5.17040532e-08
538 3 0.0878729  167 -140 -156 153 -266 197 3 195  tmp=5.17040532e-08
539 3 0.0878729  -156 153 -196  tmp=5.17040532e-08
540 3 0.0878729  -156 153 -197 196  tmp=5.17040532e-08
541 3 0.0878729  167 -140 -156 153 -267 199 266 197  tmp=5.17040532e-08
542 3 0.0878729  -156 153 -198  tmp=5.17040532e-08
543 3 0.0878729  -156 153 -199 198  tmp=5.17040532e-08
544 3 0.0878729  167 -140 -156 153 -268 201 267 199  tmp=5.17040532e-08
545 3 0.0878729  -156 153 -200  tmp=5.17040532e-08
546 3 0.0878729  -156 153 -201 200  tmp=5.17040532e-08
547 3 0.0878729  167 -140 -156 153 -269 203 268 201  tmp=5.17040532e-08
548 3 0.0878729  -156 153 -202  tmp=5.17040532e-08
549 3 0.0878729  -156 153 -203 202  tmp=5.17040532e-08
550 3 0.0878729  167 -140 -156 153 -270 205 269 203  tmp=5.17040532e-08
551 3 0.0878729  -156 153 -204  tmp=5.17040532e-08
552 3 0.0878729  -156 153 -205 204  tmp=5.17040532e-08
553 3 0.0878729  167 -140 -156 153 -271 207 270 205  tmp=5.17040532e-08
554 3 0.0878729  -156 153 -206  tmp=5.17040532e-08
555 3 0.0878729  -156 153 -207 206  tmp=5.17040532e-08
556 3 0.0878729  167 -140 -156 153 -272 209 271 207  tmp=5.17040532e-08
557 3 0.0878729  -156 153 -208  tmp=5.17040532e-08
558 3 0.0878729  -156 153 -209 208  tmp=5.17040532e-08
559 3 0.0878729  167 -140 -156 153 -273 211 272 209  tmp=5.17040532e-08
560 3 0.0878729  -156 153 -210  tmp=5.17040532e-08
561 3 0.0878729  -156 153 -211 210  tmp=5.17040532e-08
562 3 0.0878729  167 -140 -156 153 -137 213 273 211  tmp=5.17040532e-08
563 3 0.0878729  -156 153 -212  tmp=5.17040532e-08
564 3 0.0878729  -156 153 -213 212  tmp=5.17040532e-08
565 3 0.0878729  167 -140 -156 153 -274 215 137 213  tmp=5.17040532e-08
566 3 0.0878729  -156 153 -214  tmp=5.17040532e-08
567 3 0.0878729  -156 153 -215 214  tmp=5.17040532e-08
568 3 0.0878729  167 -140 -156 153 -275 217 274 215  tmp=5.17040532e-08
569 3 0.0878729  -156 153 -216  tmp=5.17040532e-08
570 3 0.0878729  -156 153 -217 216  tmp=5.17040532e-08
571 3 0.0878729  167 -140 -156 153 -276 219 275 217  tmp=5.17040532e-08
572 3 0.0878729  -156 153 -218  tmp=5.17040532e-08
573 3 0.0878729  -156 153 -219 218  tmp=5.17040532e-08
574 3 0.0878729  167 -140 -156 153 -277 221 276 219  tmp=5.17040532e-08
575 3 0.0878729  -156 153 -220  tmp=5.17040532e-08
576 3 0.0878729  -156 153 -221 220  tmp=5.17040532e-08
577 3 0.0878729  167 -140 -156 153 -278 223 277 221  tmp=5.17040532e-08
578 3 0.0878729  -156 153 -222  tmp=5.17040532e-08
579 3 0.0878729  -156 153 -223 222  tmp=5.17040532e-08
580 3 0.0878729  167 -140 -156 153 -279 225 278 223  tmp=5.17040532e-08
581 3 0.0878729  -156 153 -224  tmp=5.17040532e-08
582 3 0.0878729  -156 153 -225 224  tmp=5.17040532e-08
583 3 0.0878729  167 -140 -156 153 -280 227 279 225  tmp=5.17040532e-08
584 3 0.0878729  -156 153 -226  tmp=5.17040532e-08
585 3 0.0878729  -156 153 -227 226  tmp=5.17040532e-08
586 3 0.0878729  167 -140 -156 153 -281 229 280 227  tmp=5.17040532e-08
587 3 0.0878729  -156 153 -228  tmp=5.17040532e-08
588 3 0.0878729  -156 153 -229 228  tmp=5.17040532e-08
589 3 0.0878729  167 -140 -156 153 3 231 281 229  tmp=5.17040532e-08
590 3 0.0878729  -156 153 -230  tmp=5.17040532e-08
591 3 0.0878729  -156 153 -231 230  tmp=5.17040532e-08
592 3 0.0878729  167 -140 -156 153 266 233 -3 231  tmp=5.17040532e-08
593 3 0.0878729  -156 153 -232  tmp=5.17040532e-08
594 3 0.0878729  -156 153 -233 232  tmp=5.17040532e-08
595 3 0.0878729  167 -140 -156 153 267 235 -266 233  tmp=5.17040532e-08
596 3 0.0878729  -156 153 -234  tmp=5.17040532e-08
597 3 0.0878729  -156 153 -235 234  tmp=5.17040532e-08
598 3 0.0878729  167 -140 -156 153 268 237 -267 235  tmp=5.17040532e-08
599 3 0.0878729  -156 153 -236  tmp=5.17040532e-08
600 3 0.0878729  -156 153 -237 236  tmp=5.17040532e-08
601 3 0.0878729  167 -140 -156 153 269 239 -268 237  tmp=5.17040532e-08
602 3 0.0878729  -156 153 -238  tmp=5.17040532e-08
603 3 0.0878729  -156 153 -239 238  tmp=5.17040532e-08
604 3 0.0878729  167 -140 -156 153 270 241 -269 239  tmp=5.17040532e-08
605 3 0.0878729  -156 153 -240  tmp=5.17040532e-08
606 3 0.0878729  -156 153 -241 240  tmp=5.17040532e-08
607 3 0.0878729  167 -140 -156 153 271 243 -270 241  tmp=5.17040532e-08
608 3 0.0878729  -156 153 -242  tmp=5.17040532e-08
609 3 0.0878729  -156 153 -243 242  tmp=5.17040532e-08
610 3 0.0878729  167 -140 -156 153 272 245 -271 243  tmp=5.17040532e-08
611 3 0.0878729  -156 153 -244  tmp=5.17040532e-08
612 3 0.0878729  -156 153 -245 244  tmp=5.17040532e-08
613 3 0.0878729  167 -140 -156 153 273 247 -272 245  tmp=5.17040532e-08
614 3 0.0878729  -156 153 -246  tmp=5.17040532e-08
615 3 0.0878729  -156 153 -247 246  tmp=5.17040532e-08
616 3 0.0878729  167 -140 -156 153 137 249 -273 247  tmp=5.17040532e-08
617 3 0.0878729  -156 153 -248  tmp=5.17040532e-08
618 3 0.0878729  -156 153 -249 248  tmp=5.17040532e-08
619 3 0.0878729  167 -140 -156 153 274 251 -137 249  tmp=5.17040532e-08
620 3 0.0878729  -156 153 -250  tmp=5.17040532e-08
621 3 0.0878729  -156 153 -251 250  tmp=5.17040532e-08
622 3 0.0878729  167 -140 -156 153 275 253 -274 251  tmp=5.17040532e-08
623 3 0.0878729  -156 153 -252  tmp=5.17040532e-08
624 3 0.0878729  -156 153 -253 252  tmp=5.17040532e-08
625 3 0.0878729  167 -140 -156 153 276 255 -275 253  tmp=5.17040532e-08
626 3 0.0878729  -156 153 -254  tmp=5.17040532e-08
627 3 0.0878729  -156 153 -255 254  tmp=5.17040532e-08
628 3 0.0878729  167 -140 -156 153 277 257 -276 255  tmp=5.17040532e-08
629 3 0.0878729  -156 153 -256  tmp=5.17040532e-08
630 3 0.0878729  -156 153 -257 256  tmp=5.17040532e-08
631 3 0.0878729  167 -140 -156 153 278 259 -277 257  tmp=5.17040532e-08
632 3 0.0878729  -156 153 -258  tmp=5.17040532e-08
633 3 0.0878729  -156 153 -259 258  tmp=5.17040532e-08
634 3 0.0878729  167 -140 -156 153 279 261 -278 259  tmp=5.17040532e-08
635 3 0.0878729  -156 153 -260  tmp=5.17040532e-08
636 3 0.0878729  -156 153 -261 260  tmp=5.17040532e-08
637 3 0.0878729  167 -140 -156 153 280 263 -279 261  tmp=5.17040532e-08
638 3 0.0878729  -156 153 -262  tmp=5.17040532e-08
639 3 0.0878729  -156 153 -263 262  tmp=5.17040532e-08
640 3 0.0878729  167 -140 -156 153 281 265 -280 263  tmp=5.17040532e-08
641 3 0.0878729  -156 153 -264  tmp=5.17040532e-08
642 3 0.0878729  -156 153 -265 264  tmp=5.17040532e-08
643 3 0.0878729  167 -140 -156 153 -3 195 -281 265  tmp=5.17040532e-08
644 3 0.0878729  155 -152 -194  tmp=5.17040532e-08
645 3 0.0878729  155 -152 -195 194  tmp=5.17040532e-08
646 3 0.0878729  167 -140 155 -152 -266 197 3 195  tmp=5.17040532e-08
647 3 0.0878729  155 -152 -196  tmp=5.17040532e-08
648 3 0.0878729  155 -152 -197 196  tmp=5.17040532e-08
649 3 0.0878729  167 -140 155 -152 -267 199 266 197  tmp=5.17040532e-08
650 3 0.0878729  155 -152 -198  tmp=5.17040532e-08
651 3 0.0878729  155 -152 -199 198  tmp=5.17040532e-08
652 3 0.0878729  167 -140 155 -152 -268 201 267 199  tmp=5.17040532e-08
653 3 0.0878729  155 -152 -200  tmp=5.17040532e-08
654 3 0.0878729  155 -152 -201 200  tmp=5.17040532e-08
655 3 0.0878729  167 -140 155 -152 -269 203 268 201  tmp=5.17040532e-08
656 3 0.0878729  155 -152 -202  tmp=5.17040532e-08
657 3 0.0878729  155 -152 -203 202  tmp=5.17040532e-08
658 3 0.0878729  167 -140 155 -152 -270 205 269 203  tmp=5.17040532e-08
659 3 0.0878729  155 -152 -204  tmp=5.17040532e-08
660 3 0.0878729  155 -152 -205 204  tmp=5.17040532e-08
661 3 0.0878729  167 -140 155 -152 -271 207 270 205  tmp=5.17040532e-08
662 3 0.0878729  155 -152 -206  tmp=5.17040532e-08
663 3 0.0878729  155 -152 -207 206  tmp=5.17040532e-08
664 3 0.0878729  167 -140 155 -152 -272 209 271 207  tmp=5.17040532e-08
665 3 0.0878729  155 -152 -208  tmp=5.17040532e-08
666 3 0.0878729  155 -152 -209 208  tmp=5.17040532e-08
667 3 0.0878729  167 -140 155 -152 -273 211 272 209  tmp=5.17040532e-08
668 3 0.0878729  155 -152 -210  tmp=5.17040532e-08
669 3 0.0878729  155 -152 -211 210  tmp=5.17040532e-08
670 3 0.0878729  167 -140 155 -152 -137 213 273 211  tmp=5.17040532e-08
671 3 0.0878729  155 -152 -212  tmp=5.17040532e-08
672 3 0.0878729  155 -152 -213 212  tmp=5.17040532e-08
673 3 0.0878729  167 -140 155 -152 -274 215 137 213  tmp=5.17040532e-08
674 3 0.0878729  155 -152 -214  tmp=5.17040532e-08
675 3 0.0878729  155 -152 -215 214  tmp=5.17040532e-08
676 3 0.0878729  167 -140 155 -152 -275 217 274 215  tmp=5.17040532e-08
677 3 0.0878729  155 -152 -216  tmp=5.17040532e-08
678 3 0.0878729  155 -152 -217 216  tmp=5.17040532e-08
679 3 0.0878729  167 -140 155 -152 -276 219 275 217  tmp=5.17040532e-08
680 3 0.0878729  155 -152 -218  tmp=5.17040532e-08
681 3 0.0878729  155 -152 -219 218  tmp=5.17040532e-08
682 3 0.0878729  167 -140 155 -152 -277 221 276 219  tmp=5.17040532e-08
683 3 0.0878729  155 -152 -220  tmp=5.17040532e-08
684 3 0.0878729  155 -152 -221 220  tmp=5.17040532e-08
685 3 0.0878729  167 -140 155 -152 -278 223 277 221  tmp=5.17040532e-08
686 3 0.0878729  155 -152 -222  tmp=5.17040532e-08
687 3 0.0878729  155 -152 -223 222  tmp=5.17040532e-08
688 3 0.0878729  167 -140 155 -152 -279 225 278 223  tmp=5.17040532e-08
689 3 0.0878729  155 -152 -224  tmp=5.17040532e-08
690 3 0.0878729  155 -152 -225 224  tmp=5.17040532e-08
691 3 0.0878729  167 -140 155 -152 -280 227 279 225  tmp=5.17040532e-08
692 3 0.0878729  155 -152 -226  tmp=5.17040532e-08
693 3 0.0878729  155 -152 -227 226  tmp=5.17040532e-08
694 3 0.0878729  167 -140 155 -152 -281 229 280 227  tmp=5.17040532e-08
695 3 0.0878729  155 -152 -228  tmp=5.17040532e-08
696 3 0.0878729  155 -152 -229 228  tmp=5.17040532e-08
697 3 0.0878729  167 -140 155 -152 3 231 281 229  tmp=5.17040532e-08
698 3 0.0878729  155 -152 -230  tmp=5.17040532e-08
699 3 0.0878729  155 -152 -231 230  tmp=5.17040532e-08
700 3 0.0878729  167 -140 155 -152 266 233 -3 231  tmp=5.17040532e-08
701 3 0.0878729  155 -152 -232  tmp=5.17040532e-08
702 3 0.0878729  155 -152 -233 232  tmp=5.17040532e-08
703 3 0.0878729  167 -140 155 -152 267 235 -266 233  tmp=5.17040532e-08
704 3 0.0878729  155 -152 -234  tmp=5.17040532e-08
705 3 0.0878729  155 -152 -235 234  tmp=5.17040532e-08
706 3 0.0878729  167 -140 155 -152 268 237 -267 235  tmp=5.17040532e-08
707 3 0.0878729  155 -152 -236  tmp=5.17040532e-08
708 3 0.0878729  155 -152 -237 236  tmp=5.17040532e-08
709 3 0.0878729  167 -140 155 -152 269 239 -268 237  tmp=5.17040532e-08
710 3 0.0878729  155 -152 -238  tmp=5.17040532e-08
711 3 0.0878729  155 -152 -239 238  tmp=5.17040532e-08
712 3 0.0878729  167 -140 155 -152 270 241 -269 239  tmp=5.17040532e-08
713 3 0.0878729  155 -152 -240  tmp=5.17040532e-08
714 3 0.0878729  155 -152 -241 240  tmp=5.17040532e-08
715 3 0.0878729  167 -140 155 -152 271 243 -270 241  tmp=5.17040532e-08
716 3 0.0878729  155 -152 -242  tmp=5.17040532e-08
717 3 0.0878729  155 -152 -243 242  tmp=5.17040532e-08
718 3 0.0878729  167 -140 155 -152 272 245 -271 243  tmp=5.17040532e-08
719 3 0.0878729  155 -152 -244  tmp=5.17040532e-08
720 3 0.0878729  155 -152 -245 244  tmp=5.17040532e-08
721 3 0.0878729  167 -140 155 -152 273 247 -272 245  tmp=5.17040532e-08
722 3 0.0878729  155 -152 -246  tmp=5.17040532e-08
723 3 0.0878729  155 -152 -247 246  tmp=5.17040532e-08
724 3 0.0878729  167 -140 155 -152 137 249 -273 247  tmp=5.17040532e-08
725 3 0.0878729  155 -152 -248  tmp=5.17040532e-08
726 3 0.0878729  155 -152 -249 248  tmp=5.17040532e-08
727 3 0.0878729  167 -140 155 -152 274 251 -137 249  tmp=5.17040532e-08
728 3 0.0878729  155 -152 -250  tmp=5.17040532e-08
729 3 0.0878729  155 -152 -251 250  tmp=5.17040532e-08
730 3 0.0878729  167 -140 155 -152 275 253 -274 251  tmp=5.17040532e-08
731 3 0.0878729  155 -152 -252  tmp=5.17040532e-08
732 3 0.0878729  155 -152 -253 252  tmp=5.17040532e-08
733 3 0.0878729  167 -140 155 -152 276 255 -275 253  tmp=5.17040532e-08
734 3 0.0878729  155 -152 -254  tmp=5.17040532e-08
735 3 0.0878729  155 -152 -255 254  tmp=5.17040532e-08
736 3 0.0878729  167 -140 155 -152 277 257 -276 255  tmp=5.17040532e-08
737 3 0.0878729  155 -152 -256  tmp=5.17040532e-08
738 3 0.0878729  155 -152 -257 256  tmp=5.17040532e-08
739 3 0.0878729  167 -140 155 -152 278 259 -277 257  tmp=5.17040532e-08
740 3 0.0878729  155 -152 -258  tmp=5.17040532e-08
741 3 0.0878729  155 -152 -259 258  tmp=5.17040532e-08
742 3 0.0878729  167 -140 155 -152 279 261 -278 259  tmp=5.17040532e-08
743 3 0.0878729  155 -152 -260  tmp=5.17040532e-08
744 3 0.0878729  155 -152 -261 260  tmp=5.17040532e-08
745 3 0.0878729  167 -140 155 -152 280 263 -279 261  tmp=5.17040532e-08
746 3 0.0878729  155 -152 -262  tmp=5.17040532e-08
747 3 0.0878729  155 -152 -263 262  tmp=5.17040532e-08
748 3 0.0878729  167 -140 155 -152 281 265 -280 263  tmp=5.17040532e-08
749 3 0.0878729  155 -152 -264  tmp=5.17040532e-08
750 3 0.0878729  155 -152 -265 264  tmp=5.17040532e-08
751 3 0.0878729  167 -140 155 -152 -3 195 -281 265  tmp=5.17040532e-08
752 0  178 -155 147 -140
753 3 0.0878729  167 -178 -155 147  tmp=5.17040532e-08
754 3 0.0878729  -167 -138 147  tmp=5.17040532e-08
755 3 0.0878729  143 148 -151 -163  tmp=5.17040532e-08
756 3 0.0878729  -147 150 -163  tmp=5.17040532e-08
757 0  176 151 -172 -163
758 0  159 -172 163 -173
759 0  -150 171 143 -163
760 3 0.0878729  -150 174 -143  tmp=5.17040532e-08
761 0  -171 174 -176 143
762 0  -158 171 163 -173
763 3 0.0878729  170 -175 145 -143 140  tmp=5.17040532e-08
764 3 0.0878729  169 -175 179 -140  tmp=5.17040532e-08
765 0  167 -179 156 -140
766 0  -166 -179 139
767 0  180 -177 175 -143
768 0  -177 151 143 -176
769 3 0.0878729  -182 181 169 -178
770 0  -181 175 169 -180
771 0  -183 181 -180 178
772 0  -183 182 169 -178
773 0  -174 184 185 -176
774 0  -174 186 193 -185
775 0  -174 186 187 -191
776 0  -174 188 -187
777 3 0.0878729  -186 184 -185  tmp=5.17040532e-08
778 3 0.0878729  -190 186 -187  tmp=5.17040532e-08
779 3 0.0878729  -188 190 189 -187  tmp=5.17040532e-08
780 0  -188 190 -189
781 3 0.0878729  -174 192 -193 191  tmp=5.17040532e-08
782 0  186 -192 -193 191
783 3 0.0878729  -164 179 -165  tmp=5.17040532e-08
784 3 0.0878729  -164 179 165 -166  tmp=5.17040532e-08
785 3 0.0878729  166 -167 138 -282  tmp=5.17040532e-08
786 3 0.0878729  138 166 -167 -164 283  tmp=5.17040532e-08
787 0  166 -167 -283 282 -285 284
788 3 0.0878729  166 -167 -283 282 -286 285  tmp=5.17040532e-08
789 0  166 -167 -283 282 -287 286
790 3 0.0878729  166 -167 -283 282 -288 287  tmp=5.17040532e-08
791 0  166 -167 -283 282 -289 288
792 3 0.0878729  166 -167 -283 282 -290 289  tmp=5.17040532e-08
793 0  166 -167 -283 282 -291 290
794 3 0.0878729  166 -167 -283 282 -292 291  tmp=5.17040532e-08
795 0  166 -167 -283 282 -293 292
796 3 0.0878729  166 -167 -283 282 -294 293  tmp=5.17040532e-08
797 0  166 -167 -283 282 -295 294
798 3 0.0878729  166 -167 -283 282 -296 295  tmp=5.17040532e-08
799 0  166 -167 -283 282 -297 296
800 3 0.0878729  166 -167 -283 282 -298 297  tmp=5.17040532e-08
801 0  166 -167 -283 282 -299 298
802 3 0.0878729  166 -167 -283 282 -300 299  tmp=5.17040532e-08
803 0  166 -167 -283 282 -301 300
804 3 0.0878729  166 -167 -283 282 -302 301  tmp=5.17040532e-08
805 0  166 -167 -283 282 -303 302
806 3 0.0878729  166 -167 -283 282 -304 303  tmp=5.17040532e-08
807 0  166 -167 -283 282 -305 304
808 3 0.0878729  166 -167 -283 282 -306 305  tmp=5.17040532e-08
809 0  166 -167 -283 282 -307 306
810 3 0.0878729  166 -167 -283 282 -308 307  tmp=5.17040532e-08
811 0  166 -167 -283 282 -309 308
812 3 0.0878729  166 -167 -283 282 -310 309  tmp=5.17040532e-08
813 0  166 -167 -283 282 -311 310
814 3 0.0878729  166 -167 -283 282 -312 311  tmp=5.17040532e-08
815 0  166 -167 -283 282 -313 312
816 3 0.0878729  166 -167 -283 282 -314 313  tmp=5.17040532e-08
817 0  166 -167 -283 282 -315 314
818 3 0.0878729  166 -167 -283 282 -316 315  tmp=5.17040532e-08
819 0  166 -167 -283 282 -317 316
820 3 0.0878729  166 -167 -283 282 -318 317  tmp=5.17040532e-08
821 0  166 -167 -283 282 -319 318
822 3 0.0878729  166 -167 -283 282 284 319  tmp=5.17040532e-08
823 0  166 -167 -283 282 285 -284
824 3 0.0878729  166 -167 -283 282 286 -285  tmp=5.17040532e-08
825 0  166 -167 -283 282 287 -286
826 3 0.0878729  166 -167 -283 282 288 -287  tmp=5.17040532e-08
827 0  166 -167 -283 282 289 -288
828 3 0.0878729  166 -167 -283 282 290 -289  tmp=5.17040532e-08
829 0  166 -167 -283 282 291 -290
830 3 0.0878729  166 -167 -283 282 292 -291  tmp=5.17040532e-08
831 0  166 -167 -283 282 293 -292
832 3 0.0878729  166 -167 -283 282 294 -293  tmp=5.17040532e-08
833 0  166 -167 -283 282 295 -294
834 3 0.0878729  166 -167 -283 282 296 -295  tmp=5.17040532e-08
835 0  166 -167 -283 282 297 -296
836 3 0.0878729  166 -167 -283 282 298 -297  tmp=5.17040532e-08
837 0  166 -167 -283 282 299 -298
838 3 0.0878729  166 -167 -283 282 300 -299  tmp=5.17040532e-08
839 0  166 -167 -283 282 301 -300
840 3 0.0878729  166 -167 -283 282 302 -301  tmp=5.17040532e-08
841 0  166 -167 -283 282 303 -302
842 3 0.0878729  166 -167 -283 282 304 -303  tmp=5.17040532e-08
843 0  166 -167 -283 282 305 -304
844 3 0.0878729  166 -167 -283 282 306 -305  tmp=5.17040532e-08
845 0  166 -167 -283 282 307 -306
846 3 0.0878729  166 -167 -283 282 308 -307  tmp=5.17040532e-08
847 0  166 -167 -283 282 309 -308
848 3 0.0878729  166 -167 -283 282 310 -309  tmp=5.17040532e-08
849 0  166 -167 -283 282 311 -310
850 3 0.0878729  166 -167 -283 282 312 -311  tmp=5.17040532e-08
851 0  166 -167 -283 282 313 -312
852 3 0.0878729  166 -167 -283 282 314 -313  tmp=5.17040532e-08
853 0  166 -167 -283 282 315 -314
854 3 0.0878729  166 -167 -283 282 316 -315  tmp=5.17040532e-08
855 0  166 -167 -283 282 317 -316
856 3 0.0878729  166 -167 -283 282 318 -317  tmp=5.17040532e-08
857 0  166 -167 -283 282 319 -318
858 3 0.0878729  166 -167 -283 282 -284 -319  tmp=5.17040532e-08
859 3 0.0878729  -164 179 167 -168  tmp=5.17040532e-08
860 3 0.0878729  -164 179 168 -169  tmp=5.17040532e-08
861 0  -164 183 169 -170  tmp=5.17040532e-08
862 0  ( 3 : 16 : ( ( 179 : 451 ) ( -450 : 452 ) ) ) ( 3 : 423 : 424 :
        ( ( 419 : ( 425 -422 ) ) ( -420 : ( 421 -422 ) ) ) ) ( -20 :
        320 : 395 : 394 : 396 : ( 399 -402 ) : ( 398 -401 ) : ( 397
        -400 ) ) ( -20 : 354 : 355 : 356 : 320 : ( 359 -362 ) : ( 358
        -361 ) : ( 357 -360 ) ) ( -395 : -443 : -435 : 436 : 444 ) ( -3
        : 430 : 431 : ( ( 426 : ( 432 -429 ) ) ( -427 : ( 428 -429 ) )
        ) ) ( -439 : -435 : -355 : 436 : 440 ) -320 20 -16 ( -3 : 16 :
        ( ( -450 : 452 ) ( 179 : 451 ) ) )
863 141 0.041975  364 -363 ( 329 : -326 ) -325 324 -323 -321
        tmp=1.72346844e-09
864 141 0.041975  3 363 ( 330 : -327 ) -325 324 -322 -321
        tmp=1.72346844e-09
865 141 0.041975  -364 -3 ( 331 : -328 ) -325 324 -323 -322
        tmp=1.72346844e-09
866 5 0.0582256  364 -363 ( -324 : 325 : 323 : 321 : ( -329 326 ) ) (
        340 : -337 ) -336 335 -334 -332  tmp=1.72346844e-09
867 5 0.0582256  ( -322 : 536 : 344 ) -332 -333 335 -336 ( 341 : -338 )
        ( -324 : 325 : 322 : 321 : ( 327 -330 ) ) 363 3
        tmp=1.72346844e-09
868 5 0.0582256  ( -322 : 514 : 344 ) -333 -334 335 -336 ( 342 : -339 )
        ( -324 : 325 : 323 : 322 : ( 328 -331 ) ) -3 -364
        tmp=1.72346844e-09
869 0  364 -363 ( -335 : 336 : 334 : 332 : ( -340 337 ) ) ( 351 : -348
        ) -347 346 -345 -343
870 0  ( -540 : 544 : 537 ) ( -322 : 536 : 344 ) -343 -344 346 -347 (
        352 : -349 ) ( -335 : 336 : 333 : 332 : ( 338 -341 ) ) 363 3 (
        -344 : -537 : 539 )
871 0  ( -518 : 522 : 515 ) ( -322 : 514 : 344 ) -344 -345 346 -347 (
        353 : -350 ) ( -335 : 336 : 334 : 333 : ( 339 -342 ) ) -3 -364
        ( -344 : -515 : 517 )
872 5 0.0582256  364 -363 ( -346 : 347 : 345 : 343 : ( -351 348 ) ) (
        360 : -357 ) -320 20 -356 -354
873 5 0.0582256  ( -540 : 544 : 537 ) 3 363 ( -346 : 347 : 344 : 343 :
        ( -352 349 ) ) ( 361 : -358 ) -320 20 -355 -354 ( -344 : -537 :
        539 )
874 5 0.0582256  ( -518 : 522 : 515 ) -364 -3 ( -346 : 347 : 345 : 344
        : ( -353 350 ) ) ( 362 : -359 ) -320 20 -356 -355 ( -344 : -515
        : 517 )
875 141 0.041975  404 -403 ( 371 : -368 ) -336 324 -367 -365
        tmp=1.72346844e-09
876 141 0.041975  -3 403 ( 372 : -369 ) -336 324 -366 -365
        tmp=1.72346844e-09
877 141 0.041975  -404 3 ( 373 : -370 ) -336 324 -367 -366
        tmp=1.72346844e-09
878 5 0.0582256  404 -403 ( -324 : 336 : 367 : 365 : ( -371 368 ) ) (
        381 : -378 ) -377 335 -376 -374  tmp=1.72346844e-09
879 5 0.0582256  ( -366 : 576 : 385 ) -374 -375 335 -377 ( 382 : -379 )
        ( -324 : 336 : 366 : 365 : ( 369 -372 ) ) 403 -3
        tmp=1.72346844e-09
880 5 0.0582256  ( -366 : 554 : 385 ) -375 -376 335 -377 ( 383 : -380 )
        ( -324 : 336 : 367 : 366 : ( 370 -373 ) ) 3 -404
        tmp=1.72346844e-09
881 0  404 -403 ( -335 : 377 : 376 : 374 : ( -381 378 ) ) ( 391 : -388
        ) -387 346 -386 -384
882 0  ( -580 : 584 : 577 ) ( -366 : 576 : 385 ) -384 -385 346 -387 (
        392 : -389 ) ( -335 : 377 : 375 : 374 : ( 379 -382 ) ) 403 -3 (
        -385 : -577 : 579 )
883 0  ( -558 : 562 : 555 ) ( -366 : 554 : 385 ) -385 -386 346 -387 (
        393 : -390 ) ( -335 : 377 : 376 : 375 : ( 380 -383 ) ) 3 -404 (
        -385 : -555 : 557 )
884 5 0.0582256  404 -403 ( -346 : 387 : 386 : 384 : ( -391 388 ) ) (
        400 : -397 ) -320 20 -396 -394
885 5 0.0582256  ( -580 : 584 : 577 ) -3 403 ( -346 : 387 : 385 : 384 :
        ( -392 389 ) ) ( 401 : -398 ) -320 20 -395 -394 ( -385 : -577 :
        579 )
886 5 0.0582256  ( -558 : 562 : 555 ) -404 3 ( -346 : 387 : 386 : 385 :
        ( -393 390 ) ) ( 402 : -399 ) -320 20 -396 -395 ( -385 : -555 :
        557 )
887 11 0.100283  20 ( 356 : 354 : ( -360 357 ) ) ( -407 : 408 ) -387
        -409 406 -3 2
888 11 0.100283  20 ( 396 : 394 : ( -400 397 ) ) ( -411 : 408 ) -387
        -410 -405 -3 -2
889 5 0.0582256  -320 ( 356 : 354 : ( -360 357 ) ) ( -407 : 408 ) 387
        -409 406 -3 2
890 5 0.0582256  -320 ( 396 : 394 : ( -400 397 ) ) ( -411 : 408 ) 387
        -410 -405 -3 -2
891 11 0.100283  20 ( 356 : 354 : ( -360 357 ) ) ( -418 : 415 ) -387
        -416 -412 3 2
892 11 0.100283  20 ( 396 : 394 : ( -400 397 ) ) ( -414 : 415 ) -387
        -417 413 3 -2
893 5 0.0582256  -320 ( 356 : 354 : ( -360 357 ) ) ( -418 : 415 ) 387
        -416 -412 3 2
894 5 0.0582256  -320 ( 396 : 394 : ( -400 397 ) ) ( -414 : 415 ) 387
        -417 413 3 -2
895 5 0.0582256  -320 20 ( 356 : 354 : ( -360 357 ) ) ( -406 : 409 : (
        -408 407 ) ) ( -421 : 422 ) -423 420 -3 2
896 5 0.0582256  -320 20 ( 396 : 394 : ( -400 397 ) ) ( 405 : 410 : (
        -408 411 ) ) ( -425 : 422 ) -424 -419 -3 -2
897 5 0.0582256  -320 20 ( 356 : 354 : ( -360 357 ) ) ( 412 : 416 : (
        -415 418 ) ) ( -432 : 429 ) -430 -426 3 2
898 5 0.0582256  -320 20 ( 396 : 394 : ( -400 397 ) ) ( -413 : 417 : (
        -415 414 ) ) ( -428 : 429 ) -431 427 3 -2
899 11 0.100283  ( -529 : 548 : 528 ) ( -523 : 548 : 540 ) ( -344 :
        -537 : 539 ) ( -528 : 527 : 523 ) ( -518 : 522 : 515 ) 355 -320
        20 -16 -438 437 -434 433 ( -344 : -515 : 517 ) ( -523 : 527 :
        518 ) ( -529 : 527 : 528 ) ( -540 : 544 : 537 ) ( -528 : 548 :
        523 )
900 5 0.0582256  -320 20 -16 437 -433 435
901 5 0.0582256  -320 20 -16 -438 -436 434
902 5 0.0582256  355 -320 20 -16 439 -437 435
903 5 0.0582256  355 -320 20 -16 -440 438 -436
904 11 0.100283  ( -569 : 588 : 568 ) ( -563 : 588 : 580 ) ( -385 :
        -577 : 579 ) ( -568 : 567 : 563 ) ( -558 : 562 : 555 ) 395 -320
        20 -16 -442 441 433 -434 ( -385 : -555 : 557 ) ( -563 : 567 :
        558 ) ( -569 : 567 : 568 ) ( -580 : 584 : 577 ) ( -568 : 588 :
        563 )
905 5 0.0582256  -320 20 -16 441 434 -436
906 5 0.0582256  -320 20 -16 -442 435 -433
907 5 0.0582256  395 -320 20 -16 443 -441 -436
908 5 0.0582256  395 -320 20 -16 -444 442 435
909 11 0.100283  20 ( 355 : 354 : ( -361 358 ) ) ( 396 : 395 : ( -402
        399 ) ) 3 ( 431 : 430 : ( ( 426 : ( 432 -429 ) ) ( -427 : ( 428
        -429 ) ) ) ) ( 440 : 436 ) ( -443 : 436 ) -447 -445
910 5 0.0582256  ( 355 : 354 : ( -361 358 ) ) ( 396 : 395 : ( -402 399
        ) ) 3 ( 431 : 430 : ( ( 426 : ( 432 -429 ) ) ( -427 : ( 428
        -429 ) ) ) ) ( 440 : 436 ) ( -443 : 436 ) -447 445 -179
911 11 0.100283  -21 447 ( 355 : 354 : ( -361 358 ) ) ( 396 : 395 : (
        -402 399 ) ) 3 ( 431 : 430 : ( ( 426 : ( 432 -429 ) ) ( -427 :
        ( 428 -429 ) ) ) ) ( 440 : 436 ) ( -443 : 436 ) ( 440 : 436 ) (
        -443 : 436 ) 20 -453 -448
912 5 0.0582256  20 -21 447 ( 355 : 354 : ( -361 358 ) ) ( 396 : 395 :
        ( -402 399 ) ) 3 ( 431 : 430 : ( ( 426 : ( 432 -429 ) ) ( -427
        : ( 428 -429 ) ) ) ) ( 440 : 436 ) ( -443 : 436 ) ( 440 : 436 )
        ( -443 : 436 ) ( 448 : 453 ) -455 -451
913 11 0.100283  -21 447 ( 355 : 354 : ( -361 358 ) ) ( 396 : 395 : (
        -402 399 ) ) 3 ( 431 : 430 : ( ( 426 : ( 432 -429 ) ) ( -427 :
        ( 428 -429 ) ) ) ) ( 440 : 436 ) ( -443 : 436 ) -320 -21 447 (
        355 : 354 : ( -361 358 ) ) ( 396 : 395 : ( -402 399 ) ) 3 ( 431
        : 430 : ( ( 426 : ( 432 -429 ) ) ( -427 : ( 428 -429 ) ) ) ) (
        440 : 436 ) ( -443 : 436 ) ( 440 : 436 ) ( -443 : 436 ) 454
        -449
914 5 0.0582256  -21 447 ( 355 : 354 : ( -361 358 ) ) ( 396 : 395 : (
        -402 399 ) ) 3 ( 431 : 430 : ( ( 426 : ( 432 -429 ) ) ( -427 :
        ( 428 -429 ) ) ) ) ( 440 : 436 ) ( -443 : 436 ) -320 ( 440 :
        436 ) ( -443 : 436 ) ( 449 : -454 ) 456 -452
915 5 0.0582256  -18 21 3 ( 440 : 436 ) ( -443 : 436 ) 20 -453 -448
916 5 0.0582256  20 -18 21 3 ( 440 : 436 ) ( -443 : 436 ) ( 448 : 453 )
        -455 -451
917 5 0.0582256  -18 21 3 -320 -18 21 3 ( 440 : 436 ) ( -443 : 436 )
        454 -449
918 5 0.0582256  -18 21 3 -320 ( 440 : 436 ) ( -443 : 436 ) ( 449 :
        -454 ) 456 -452
919 0  -19 18 3 ( 440 : 436 ) ( -443 : 436 ) 20 -453 -448
920 0  20 -19 18 3 ( 440 : 436 ) ( -443 : 436 ) ( 448 : 453 ) -455 -451
921 0  -19 18 3 -320 -19 18 3 ( 440 : 436 ) ( -443 : 436 ) 454 -449
922 0  -19 18 3 -320 ( 440 : 436 ) ( -443 : 436 ) ( 449 : -454 ) 456
        -452
923 3 0.0878729  -16 19 3 ( 440 : 436 ) ( -443 : 436 ) 20 -453 -448
924 3 0.0878729  20 -16 19 3 ( 440 : 436 ) ( -443 : 436 ) ( 448 : 453 )
        -455 -451
925 3 0.0878729  -16 19 3 -320 -16 19 3 ( 440 : 436 ) ( -443 : 436 )
        454 -449
926 3 0.0878729  -16 19 3 -320 ( 440 : 436 ) ( -443 : 436 ) ( 449 :
        -454 ) 456 -452
927 11 0.100283  -320 ( 355 : 354 : ( -361 358 ) ) ( 396 : 395 : ( -402
        399 ) ) 3 ( 431 : 430 : ( ( 426 : ( 432 -429 ) ) ( -427 : ( 428
        -429 ) ) ) ) ( 440 : 436 ) ( -443 : 436 ) -447 446
928 5 0.0582256  ( 355 : 354 : ( -361 358 ) ) ( 396 : 395 : ( -402 399
        ) ) 3 ( 431 : 430 : ( ( 426 : ( 432 -429 ) ) ( -427 : ( 428
        -429 ) ) ) ) ( 440 : 436 ) ( -443 : 436 ) -447 -446 450
929 11 0.100283  20 ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 : ( -401
        398 ) ) -3 ( 424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420 : (
        421 -422 ) ) ) ) ( -435 : -439 ) ( -435 : 444 ) -447 -445
930 5 0.0582256  ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 : ( -401 398
        ) ) -3 ( 424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420 : ( 421
        -422 ) ) ) ) ( -435 : -439 ) ( -435 : 444 ) -447 445 -179
931 11 0.100283  -21 447 ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 : (
        -401 398 ) ) -3 ( 424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420 :
        ( 421 -422 ) ) ) ) ( -435 : -439 ) ( -435 : 444 ) ( -435 : -439
        ) ( -435 : 444 ) 20 -453 -448
932 5 0.0582256  20 -21 447 ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 :
        ( -401 398 ) ) -3 ( 424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420
        : ( 421 -422 ) ) ) ) ( -435 : -439 ) ( -435 : 444 ) ( -435 :
        -439 ) ( -435 : 444 ) ( 448 : 453 ) -455 -451
933 11 0.100283  -21 447 ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 : (
        -401 398 ) ) -3 ( 424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420 :
        ( 421 -422 ) ) ) ) ( -435 : -439 ) ( -435 : 444 ) -320 -21 447
        ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 : ( -401 398 ) ) -3 (
        424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420 : ( 421 -422 ) ) )
        ) ( -435 : -439 ) ( -435 : 444 ) ( -435 : -439 ) ( -435 : 444 )
        454 -449
934 5 0.0582256  -21 447 ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 : (
        -401 398 ) ) -3 ( 424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420 :
        ( 421 -422 ) ) ) ) ( -435 : -439 ) ( -435 : 444 ) -320 ( -435 :
        -439 ) ( -435 : 444 ) ( 449 : -454 ) 456 -452
935 5 0.0582256  -18 21 -3 ( -435 : -439 ) ( -435 : 444 ) 20 -453 -448
936 5 0.0582256  20 -18 21 -3 ( -435 : -439 ) ( -435 : 444 ) ( 448 :
        453 ) -455 -451
937 5 0.0582256  -18 21 -3 -320 -18 21 -3 ( -435 : -439 ) ( -435 : 444
        ) 454 -449
938 5 0.0582256  -18 21 -3 -320 ( -435 : -439 ) ( -435 : 444 ) ( 449 :
        -454 ) 456 -452
939 0  -19 18 -3 ( -435 : -439 ) ( -435 : 444 ) 20 -453 -448
940 0  20 -19 18 -3 ( -435 : -439 ) ( -435 : 444 ) ( 448 : 453 ) -455
        -451
941 0  -19 18 -3 -320 -19 18 -3 ( -435 : -439 ) ( -435 : 444 ) 454 -449
942 0  -19 18 -3 -320 ( -435 : -439 ) ( -435 : 444 ) ( 449 : -454 ) 456
        -452
943 3 0.0878729  -16 19 -3 ( -435 : -439 ) ( -435 : 444 ) 20 -453 -448
944 3 0.0878729  20 -16 19 -3 ( -435 : -439 ) ( -435 : 444 ) ( 448 :
        453 ) -455 -451
945 3 0.0878729  -16 19 -3 -320 -16 19 -3 ( -435 : -439 ) ( -435 : 444
        ) 454 -449
946 3 0.0878729  -16 19 -3 -320 ( -435 : -439 ) ( -435 : 444 ) ( 449 :
        -454 ) 456 -452
947 11 0.100283  -320 ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 : ( -401
        398 ) ) -3 ( 424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420 : (
        421 -422 ) ) ) ) ( -435 : -439 ) ( -435 : 444 ) -447 446
948 5 0.0582256  ( 356 : 355 : ( -362 359 ) ) ( 395 : 394 : ( -401 398
        ) ) -3 ( 424 : 423 : ( ( 419 : ( 425 -422 ) ) ( -420 : ( 421
        -422 ) ) ) ) ( -435 : -439 ) ( -435 : 444 ) -447 -446 450
949 54 0.0833854  ( -150 : 151 : 162 ) -460 459 -458 457 -463 57 3 (
        -150 : 151 : 162 )
950 54 0.0833854  ( -150 : 151 : 162 ) -462 461 -458 457 -60 463 3 (
        -150 : 151 : 162 )
951 54 0.0833854  ( -57 : -472 : -464 : 465 : 463 : 473 : 3 : ( 464
        -471 -465 ) ) ( -150 : 151 : 162 ) -3 57 -463 -457 458 459 -460
        ( -150 : 151 : 162 )
952 54 0.0833854  ( -463 : -476 : -467 : 60 : 468 : 477 : 3 ) ( -150 :
        151 : 162 ) -3 463 -60 -457 458 461 -462 ( -150 : 151 : 162 )
953 0  ( -150 : 151 : 162 ) -3 57 472 -473 464 -465 -463 ( 471 : -464 :
        465 ) ( -474 : 475 : -156 : 466 )
954 3 0.0878729  ( -57 : -600 : -598 : -593 : 599 : 601 : 3 : 60 ) (
        471 : -464 : 465 ) -463 -466 156 -475 474 57 -3 ( -150 : 151 :
        162 )
955 0  ( -150 : 151 : 162 ) -3 463 476 -477 467 -468 -60 ( -478 : 479 :
        -469 : 470 )
956 3 0.0878729  ( -57 : -600 : -598 : -593 : 599 : 601 : 3 : 60 ) -60
        -470 469 -479 478 463 -3 ( -150 : 151 : 162 )
957 48 0.117208  69 -480 74 -81 75 -80 72
958 74 0.0847227  69 -481 480 -81 75 -80 72
959 48 0.117208  69 -482 481 -81 75 -80 72
960 74 0.0847227  69 -483 482 -81 75 -80 72
961 48 0.117208  69 -484 483 -81 75 -80 72
962 74 0.0847227  69 -485 484 -81 75 -80 72
963 48 0.117208  69 -486 485 -81 75 -80 72
964 74 0.0847227  69 -487 486 -81 75 -80 72
965 48 0.117208  69 -488 487 -81 75 -80 72
966 74 0.0847227  69 -489 488 -81 75 -80 72
967 48 0.117208  69 -490 489 -81 75 -80 72
968 74 0.0847227  69 -491 490 -81 75 -80 72
969 48 0.117208  69 -492 491 -81 75 -80 72
970 74 0.0847227  69 -493 492 -81 75 -80 72
971 48 0.117208  69 -79 493 -81 75 -80 72
972 48 0.117208  69 -480 74 -82 81 -80 72
973 74 0.0847227  69 -481 480 -82 81 -80 72
974 48 0.117208  69 -482 481 -82 81 -80 72
975 74 0.0847227  69 -483 482 -82 81 -80 72
976 48 0.117208  69 -484 483 -82 81 -80 72
977 74 0.0847227  69 -485 484 -82 81 -80 72
978 48 0.117208  69 -486 485 -82 81 -80 72
979 74 0.0847227  69 -487 486 -82 81 -80 72
980 48 0.117208  69 -488 487 -82 81 -80 72
981 74 0.0847227  69 -489 488 -82 81 -80 72
982 48 0.117208  69 -490 489 -82 81 -80 72
983 74 0.0847227  69 -491 490 -82 81 -80 72
984 48 0.117208  69 -492 491 -82 81 -80 72
985 74 0.0847227  69 -493 492 -82 81 -80 72
986 48 0.117208  69 -79 493 -82 81 -80 72
987 48 0.117208  69 -480 74 -83 82 -80 72
988 74 0.0847227  69 -481 480 -83 82 -80 72
989 48 0.117208  69 -482 481 -83 82 -80 72
990 74 0.0847227  69 -483 482 -83 82 -80 72
991 48 0.117208  69 -484 483 -83 82 -80 72
992 74 0.0847227  69 -485 484 -83 82 -80 72
993 48 0.117208  69 -486 485 -83 82 -80 72
994 74 0.0847227  69 -487 486 -83 82 -80 72
995 48 0.117208  69 -488 487 -83 82 -80 72
996 74 0.0847227  69 -489 488 -83 82 -80 72
997 48 0.117208  69 -490 489 -83 82 -80 72
998 74 0.0847227  69 -491 490 -83 82 -80 72
999 48 0.117208  69 -492 491 -83 82 -80 72
1000 74 0.0847227  69 -493 492 -83 82 -80 72
1001 48 0.117208  69 -79 493 -83 82 -80 72
1002 48 0.117208  69 -480 74 -84 83 -80 72
1003 74 0.0847227  69 -481 480 -84 83 -80 72
1004 48 0.117208  69 -482 481 -84 83 -80 72
1005 74 0.0847227  69 -483 482 -84 83 -80 72
1006 48 0.117208  69 -484 483 -84 83 -80 72
1007 74 0.0847227  69 -485 484 -84 83 -80 72
1008 48 0.117208  69 -486 485 -84 83 -80 72
1009 74 0.0847227  69 -487 486 -84 83 -80 72
1010 48 0.117208  69 -488 487 -84 83 -80 72
1011 74 0.0847227  69 -489 488 -84 83 -80 72
1012 48 0.117208  69 -490 489 -84 83 -80 72
1013 74 0.0847227  69 -491 490 -84 83 -80 72
1014 48 0.117208  69 -492 491 -84 83 -80 72
1015 74 0.0847227  69 -493 492 -84 83 -80 72
1016 48 0.117208  69 -79 493 -84 83 -80 72
1017 48 0.117208  69 -480 74 -85 84 -80 72
1018 74 0.0847227  69 -481 480 -85 84 -80 72
1019 48 0.117208  69 -482 481 -85 84 -80 72
1020 74 0.0847227  69 -483 482 -85 84 -80 72
1021 48 0.117208  69 -484 483 -85 84 -80 72
1022 74 0.0847227  69 -485 484 -85 84 -80 72
1023 48 0.117208  69 -486 485 -85 84 -80 72
1024 74 0.0847227  69 -487 486 -85 84 -80 72
1025 48 0.117208  69 -488 487 -85 84 -80 72
1026 74 0.0847227  69 -489 488 -85 84 -80 72
1027 48 0.117208  69 -490 489 -85 84 -80 72
1028 74 0.0847227  69 -491 490 -85 84 -80 72
1029 48 0.117208  69 -492 491 -85 84 -80 72
1030 74 0.0847227  69 -493 492 -85 84 -80 72
1031 48 0.117208  69 -79 493 -85 84 -80 72
1032 48 0.117208  69 -480 74 -86 85 -80 72
1033 74 0.0847227  69 -481 480 -86 85 -80 72
1034 48 0.117208  69 -482 481 -86 85 -80 72
1035 74 0.0847227  69 -483 482 -86 85 -80 72
1036 48 0.117208  69 -484 483 -86 85 -80 72
1037 74 0.0847227  69 -485 484 -86 85 -80 72
1038 48 0.117208  69 -486 485 -86 85 -80 72
1039 74 0.0847227  69 -487 486 -86 85 -80 72
1040 48 0.117208  69 -488 487 -86 85 -80 72
1041 74 0.0847227  69 -489 488 -86 85 -80 72
1042 48 0.117208  69 -490 489 -86 85 -80 72
1043 74 0.0847227  69 -491 490 -86 85 -80 72
1044 48 0.117208  69 -492 491 -86 85 -80 72
1045 74 0.0847227  69 -493 492 -86 85 -80 72
1046 48 0.117208  69 -79 493 -86 85 -80 72
1047 48 0.117208  69 -480 74 -87 86 -80 72
1048 74 0.0847227  69 -481 480 -87 86 -80 72
1049 48 0.117208  69 -482 481 -87 86 -80 72
1050 74 0.0847227  69 -483 482 -87 86 -80 72
1051 48 0.117208  69 -484 483 -87 86 -80 72
1052 74 0.0847227  69 -485 484 -87 86 -80 72
1053 48 0.117208  69 -486 485 -87 86 -80 72
1054 74 0.0847227  69 -487 486 -87 86 -80 72
1055 48 0.117208  69 -488 487 -87 86 -80 72
1056 74 0.0847227  69 -489 488 -87 86 -80 72
1057 48 0.117208  69 -490 489 -87 86 -80 72
1058 74 0.0847227  69 -491 490 -87 86 -80 72
1059 48 0.117208  69 -492 491 -87 86 -80 72
1060 74 0.0847227  69 -493 492 -87 86 -80 72
1061 48 0.117208  69 -79 493 -87 86 -80 72
1062 48 0.117208  69 -480 74 -88 87 -80 72
1063 74 0.0847227  69 -481 480 -88 87 -80 72
1064 48 0.117208  69 -482 481 -88 87 -80 72
1065 74 0.0847227  69 -483 482 -88 87 -80 72
1066 48 0.117208  69 -484 483 -88 87 -80 72
1067 74 0.0847227  69 -485 484 -88 87 -80 72
1068 48 0.117208  69 -486 485 -88 87 -80 72
1069 74 0.0847227  69 -487 486 -88 87 -80 72
1070 48 0.117208  69 -488 487 -88 87 -80 72
1071 74 0.0847227  69 -489 488 -88 87 -80 72
1072 48 0.117208  69 -490 489 -88 87 -80 72
1073 74 0.0847227  69 -491 490 -88 87 -80 72
1074 48 0.117208  69 -492 491 -88 87 -80 72
1075 74 0.0847227  69 -493 492 -88 87 -80 72
1076 48 0.117208  69 -79 493 -88 87 -80 72
1077 48 0.117208  69 -480 74 -89 88 -80 72
1078 74 0.0847227  69 -481 480 -89 88 -80 72
1079 48 0.117208  69 -482 481 -89 88 -80 72
1080 74 0.0847227  69 -483 482 -89 88 -80 72
1081 48 0.117208  69 -484 483 -89 88 -80 72
1082 74 0.0847227  69 -485 484 -89 88 -80 72
1083 48 0.117208  69 -486 485 -89 88 -80 72
1084 74 0.0847227  69 -487 486 -89 88 -80 72
1085 48 0.117208  69 -488 487 -89 88 -80 72
1086 74 0.0847227  69 -489 488 -89 88 -80 72
1087 48 0.117208  69 -490 489 -89 88 -80 72
1088 74 0.0847227  69 -491 490 -89 88 -80 72
1089 48 0.117208  69 -492 491 -89 88 -80 72
1090 74 0.0847227  69 -493 492 -89 88 -80 72
1091 48 0.117208  69 -79 493 -89 88 -80 72
1092 48 0.117208  69 -480 74 -76 89 -80 72
1093 74 0.0847227  69 -481 480 -76 89 -80 72
1094 48 0.117208  69 -482 481 -76 89 -80 72
1095 74 0.0847227  69 -483 482 -76 89 -80 72
1096 48 0.117208  69 -484 483 -76 89 -80 72
1097 74 0.0847227  69 -485 484 -76 89 -80 72
1098 48 0.117208  69 -486 485 -76 89 -80 72
1099 74 0.0847227  69 -487 486 -76 89 -80 72
1100 48 0.117208  69 -488 487 -76 89 -80 72
1101 74 0.0847227  69 -489 488 -76 89 -80 72
1102 48 0.117208  69 -490 489 -76 89 -80 72
1103 74 0.0847227  69 -491 490 -76 89 -80 72
1104 48 0.117208  69 -492 491 -76 89 -80 72
1105 74 0.0847227  69 -493 492 -76 89 -80 72
1106 48 0.117208  69 -79 493 -76 89 -80 72
1107 0  ( -63 : -70 : 91 : 132 ) 73 -80 75 -81 62 -79 3 ( 80 : 70 ) (
        80 : -74 ) 69
1108 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 75 -81 62 -79 3 ( 80
        : 70 ) ( 80 : -74 ) 69
1109 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 75 -81 62 -79 3 (
        80 : 70 ) ( 80 : -74 ) 69
1110 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 75 -81 62 -79 3 (
        80 : 70 ) ( 80 : -74 ) 69
1111 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 75 -81 62 -79 3 (
        80 : 70 ) ( 80 : -74 ) 69
1112 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 75 -81 62 -79 3 ( 80
        : 70 ) ( 80 : -74 ) 69
1113 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 75 -81 62 -79 3 (
        80 : 70 ) ( 80 : -74 ) 69
1114 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 75 -81 62 -79 3 ( 80
        : 70 ) ( 80 : -74 ) 69
1115 0  ( -63 : -70 : 91 : 132 ) 73 -80 81 -82 62 -79 3 ( 80 : -74 ) 69
1116 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 81 -82 62 -79 3 ( 80
        : -74 ) 69
1117 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 81 -82 62 -79 3 (
        80 : -74 ) 69
1118 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 81 -82 62 -79 3 (
        80 : -74 ) 69
1119 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 81 -82 62 -79 3 (
        80 : -74 ) 69
1120 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 81 -82 62 -79 3 ( 80
        : -74 ) 69
1121 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 81 -82 62 -79 3 (
        80 : -74 ) 69
1122 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 81 -82 62 -79 3 ( 80
        : -74 ) 69
1123 0  ( -63 : -70 : 91 : 132 ) 73 -80 82 -83 62 -79 3 ( 80 : -74 ) 69
1124 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 82 -83 62 -79 3 ( 80
        : -74 ) 69
1125 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 82 -83 62 -79 3 (
        80 : -74 ) 69
1126 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 82 -83 62 -79 3 (
        80 : -74 ) 69
1127 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 82 -83 62 -79 3 (
        80 : -74 ) 69
1128 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 82 -83 62 -79 3 ( 80
        : -74 ) 69
1129 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 82 -83 62 -79 3 (
        80 : -74 ) 69
1130 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 82 -83 62 -79 3 ( 80
        : -74 ) 69
1131 0  ( -63 : -70 : 91 : 132 ) 73 -80 83 -84 62 -79 3 ( 80 : -74 ) 69
1132 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 83 -84 62 -79 3 ( 80
        : -74 ) 69
1133 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 83 -84 62 -79 3 (
        80 : -74 ) 69
1134 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 83 -84 62 -79 3 (
        80 : -74 ) 69
1135 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 83 -84 62 -79 3 (
        80 : -74 ) 69
1136 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 83 -84 62 -79 3 ( 80
        : -74 ) 69
1137 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 83 -84 62 -79 3 (
        80 : -74 ) 69
1138 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 83 -84 62 -79 3 ( 80
        : -74 ) 69
1139 0  ( -63 : -70 : 91 : 132 ) 73 -80 84 -85 62 -79 3 ( 80 : -74 ) 69
1140 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 84 -85 62 -79 3 ( 80
        : -74 ) 69
1141 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 84 -85 62 -79 3 (
        80 : -74 ) 69
1142 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 84 -85 62 -79 3 (
        80 : -74 ) 69
1143 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 84 -85 62 -79 3 (
        80 : -74 ) 69
1144 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 84 -85 62 -79 3 ( 80
        : -74 ) 69
1145 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 84 -85 62 -79 3 (
        80 : -74 ) 69
1146 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 84 -85 62 -79 3 ( 80
        : -74 ) 69
1147 0  ( -63 : -70 : 91 : 132 ) 73 -80 85 -86 62 -79 3 ( 80 : -74 ) 69
1148 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 85 -86 62 -79 3 ( 80
        : -74 ) 69
1149 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 85 -86 62 -79 3 (
        80 : -74 ) 69
1150 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 85 -86 62 -79 3 (
        80 : -74 ) 69
1151 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 85 -86 62 -79 3 (
        80 : -74 ) 69
1152 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 85 -86 62 -79 3 ( 80
        : -74 ) 69
1153 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 85 -86 62 -79 3 (
        80 : -74 ) 69
1154 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 85 -86 62 -79 3 ( 80
        : -74 ) 69
1155 0  ( -63 : -70 : 91 : 132 ) 73 -80 86 -87 62 -79 3 ( 80 : -74 ) 69
1156 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 86 -87 62 -79 3 ( 80
        : -74 ) 69
1157 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 86 -87 62 -79 3 (
        80 : -74 ) 69
1158 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 86 -87 62 -79 3 (
        80 : -74 ) 69
1159 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 86 -87 62 -79 3 (
        80 : -74 ) 69
1160 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 86 -87 62 -79 3 ( 80
        : -74 ) 69
1161 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 86 -87 62 -79 3 (
        80 : -74 ) 69
1162 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 86 -87 62 -79 3 ( 80
        : -74 ) 69
1163 0  ( -63 : -70 : 91 : 132 ) 73 -80 87 -88 62 -79 3 ( 80 : -74 ) 69
1164 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 87 -88 62 -79 3 ( 80
        : -74 ) 69
1165 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 87 -88 62 -79 3 (
        80 : -74 ) 69
1166 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 87 -88 62 -79 3 (
        80 : -74 ) 69
1167 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 87 -88 62 -79 3 (
        80 : -74 ) 69
1168 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 87 -88 62 -79 3 ( 80
        : -74 ) 69
1169 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 87 -88 62 -79 3 (
        80 : -74 ) 69
1170 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 87 -88 62 -79 3 ( 80
        : -74 ) 69
1171 0  ( -63 : -70 : 91 : 132 ) 73 -80 88 -89 62 -79 3 ( 80 : -74 ) 69
1172 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 88 -89 62 -79 3 ( 80
        : -74 ) 69
1173 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 88 -89 62 -79 3 (
        80 : -74 ) 69
1174 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 88 -89 62 -79 3 (
        80 : -74 ) 69
1175 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 88 -89 62 -79 3 (
        80 : -74 ) 69
1176 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 88 -89 62 -79 3 ( 80
        : -74 ) 69
1177 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 88 -89 62 -79 3 (
        80 : -74 ) 69
1178 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 88 -89 62 -79 3 ( 80
        : -74 ) 69
1179 0  ( -63 : -70 : 91 : 132 ) 69 ( 80 : -74 ) ( 80 : -71 ) 3 -79 62
        -76 89 -80 73 ( -90 : -71 : -77 : -3 : -62 : 78 : 94 : 95 )
1180 48 0.117208  ( -63 : -70 : 91 : 132 ) 69 ( 80 : -74 ) ( 80 : -71 )
        3 -79 62 -76 89 -494 80 ( -90 : -71 : -77 : -3 : -62 : 78 : 94
        : 95 )
1181 74 0.0847227  ( -63 : -70 : 91 : 132 ) 69 ( 80 : -74 ) ( 80 : -71
        ) 3 -79 62 -76 89 -495 494 ( -90 : -71 : -77 : -3 : -62 : 78 :
        94 : 95 )
1182 48 0.117208  ( -63 : -70 : 91 : 132 ) 69 ( 80 : -74 ) ( 80 : -71 )
        3 -79 62 -76 89 -496 495 ( -90 : -71 : -77 : -3 : -62 : 78 : 94
        : 95 )
1183 74 0.0847227  ( -63 : -70 : 91 : 132 ) 69 ( 80 : -74 ) ( 80 : -71
        ) 3 -79 62 -76 89 -497 496 ( -90 : -71 : -77 : -3 : -62 : 78 :
        94 : 95 )
1184 48 0.117208  ( -63 : -70 : 91 : 132 ) 69 ( 80 : -74 ) ( 80 : -71 )
        3 -79 62 -76 89 -67 497 ( -90 : -71 : -77 : -3 : -62 : 78 : 94
        : 95 )
1185 74 0.0847227  ( -63 : -70 : 91 : 132 ) 69 ( 80 : -74 ) ( 80 : -71
        ) 3 -79 62 -76 89 -498 67 ( -90 : -71 : -77 : -3 : -62 : 78 :
        94 : 95 )
1186 48 0.117208  ( -63 : -70 : 91 : 132 ) 69 ( 80 : -74 ) ( 80 : -71 )
        3 -79 62 -76 89 -78 498 ( -90 : -71 : -77 : -3 : -62 : 78 : 94
        : 95 )
1187 48 0.117208  90 -499 92 -96 71 -80 72
1188 74 0.0847227  90 -500 499 -96 71 -80 72
1189 48 0.117208  90 -501 500 -96 71 -80 72
1190 74 0.0847227  90 -502 501 -96 71 -80 72
1191 48 0.117208  90 -503 502 -96 71 -80 72
1192 74 0.0847227  90 -504 503 -96 71 -80 72
1193 48 0.117208  90 -505 504 -96 71 -80 72
1194 74 0.0847227  90 -506 505 -96 71 -80 72
1195 48 0.117208  90 -507 506 -96 71 -80 72
1196 74 0.0847227  90 -508 507 -96 71 -80 72
1197 48 0.117208  90 -509 508 -96 71 -80 72
1198 74 0.0847227  90 -510 509 -96 71 -80 72
1199 48 0.117208  90 -511 510 -96 71 -80 72
1200 74 0.0847227  90 -512 511 -96 71 -80 72
1201 48 0.117208  90 -95 512 -96 71 -80 72
1202 48 0.117208  90 -499 92 -97 96 -80 72
1203 74 0.0847227  90 -500 499 -97 96 -80 72
1204 48 0.117208  90 -501 500 -97 96 -80 72
1205 74 0.0847227  90 -502 501 -97 96 -80 72
1206 48 0.117208  90 -503 502 -97 96 -80 72
1207 74 0.0847227  90 -504 503 -97 96 -80 72
1208 48 0.117208  90 -505 504 -97 96 -80 72
1209 74 0.0847227  90 -506 505 -97 96 -80 72
1210 48 0.117208  90 -507 506 -97 96 -80 72
1211 74 0.0847227  90 -508 507 -97 96 -80 72
1212 48 0.117208  90 -509 508 -97 96 -80 72
1213 74 0.0847227  90 -510 509 -97 96 -80 72
1214 48 0.117208  90 -511 510 -97 96 -80 72
1215 74 0.0847227  90 -512 511 -97 96 -80 72
1216 48 0.117208  90 -95 512 -97 96 -80 72
1217 48 0.117208  90 -499 92 -98 97 -80 72
1218 74 0.0847227  90 -500 499 -98 97 -80 72
1219 48 0.117208  90 -501 500 -98 97 -80 72
1220 74 0.0847227  90 -502 501 -98 97 -80 72
1221 48 0.117208  90 -503 502 -98 97 -80 72
1222 74 0.0847227  90 -504 503 -98 97 -80 72
1223 48 0.117208  90 -505 504 -98 97 -80 72
1224 74 0.0847227  90 -506 505 -98 97 -80 72
1225 48 0.117208  90 -507 506 -98 97 -80 72
1226 74 0.0847227  90 -508 507 -98 97 -80 72
1227 48 0.117208  90 -509 508 -98 97 -80 72
1228 74 0.0847227  90 -510 509 -98 97 -80 72
1229 48 0.117208  90 -511 510 -98 97 -80 72
1230 74 0.0847227  90 -512 511 -98 97 -80 72
1231 48 0.117208  90 -95 512 -98 97 -80 72
1232 48 0.117208  90 -499 92 -99 98 -80 72
1233 74 0.0847227  90 -500 499 -99 98 -80 72
1234 48 0.117208  90 -501 500 -99 98 -80 72
1235 74 0.0847227  90 -502 501 -99 98 -80 72
1236 48 0.117208  90 -503 502 -99 98 -80 72
1237 74 0.0847227  90 -504 503 -99 98 -80 72
1238 48 0.117208  90 -505 504 -99 98 -80 72
1239 74 0.0847227  90 -506 505 -99 98 -80 72
1240 48 0.117208  90 -507 506 -99 98 -80 72
1241 74 0.0847227  90 -508 507 -99 98 -80 72
1242 48 0.117208  90 -509 508 -99 98 -80 72
1243 74 0.0847227  90 -510 509 -99 98 -80 72
1244 48 0.117208  90 -511 510 -99 98 -80 72
1245 74 0.0847227  90 -512 511 -99 98 -80 72
1246 48 0.117208  90 -95 512 -99 98 -80 72
1247 48 0.117208  90 -499 92 -100 99 -80 72
1248 74 0.0847227  90 -500 499 -100 99 -80 72
1249 48 0.117208  90 -501 500 -100 99 -80 72
1250 74 0.0847227  90 -502 501 -100 99 -80 72
1251 48 0.117208  90 -503 502 -100 99 -80 72
1252 74 0.0847227  90 -504 503 -100 99 -80 72
1253 48 0.117208  90 -505 504 -100 99 -80 72
1254 74 0.0847227  90 -506 505 -100 99 -80 72
1255 48 0.117208  90 -507 506 -100 99 -80 72
1256 74 0.0847227  90 -508 507 -100 99 -80 72
1257 48 0.117208  90 -509 508 -100 99 -80 72
1258 74 0.0847227  90 -510 509 -100 99 -80 72
1259 48 0.117208  90 -511 510 -100 99 -80 72
1260 74 0.0847227  90 -512 511 -100 99 -80 72
1261 48 0.117208  90 -95 512 -100 99 -80 72
1262 48 0.117208  90 -499 92 -101 100 -80 72
1263 74 0.0847227  90 -500 499 -101 100 -80 72
1264 48 0.117208  90 -501 500 -101 100 -80 72
1265 74 0.0847227  90 -502 501 -101 100 -80 72
1266 48 0.117208  90 -503 502 -101 100 -80 72
1267 74 0.0847227  90 -504 503 -101 100 -80 72
1268 48 0.117208  90 -505 504 -101 100 -80 72
1269 74 0.0847227  90 -506 505 -101 100 -80 72
1270 48 0.117208  90 -507 506 -101 100 -80 72
1271 74 0.0847227  90 -508 507 -101 100 -80 72
1272 48 0.117208  90 -509 508 -101 100 -80 72
1273 74 0.0847227  90 -510 509 -101 100 -80 72
1274 48 0.117208  90 -511 510 -101 100 -80 72
1275 74 0.0847227  90 -512 511 -101 100 -80 72
1276 48 0.117208  90 -95 512 -101 100 -80 72
1277 48 0.117208  90 -499 92 -102 101 -80 72
1278 74 0.0847227  90 -500 499 -102 101 -80 72
1279 48 0.117208  90 -501 500 -102 101 -80 72
1280 74 0.0847227  90 -502 501 -102 101 -80 72
1281 48 0.117208  90 -503 502 -102 101 -80 72
1282 74 0.0847227  90 -504 503 -102 101 -80 72
1283 48 0.117208  90 -505 504 -102 101 -80 72
1284 74 0.0847227  90 -506 505 -102 101 -80 72
1285 48 0.117208  90 -507 506 -102 101 -80 72
1286 74 0.0847227  90 -508 507 -102 101 -80 72
1287 48 0.117208  90 -509 508 -102 101 -80 72
1288 74 0.0847227  90 -510 509 -102 101 -80 72
1289 48 0.117208  90 -511 510 -102 101 -80 72
1290 74 0.0847227  90 -512 511 -102 101 -80 72
1291 48 0.117208  90 -95 512 -102 101 -80 72
1292 48 0.117208  90 -499 92 -103 102 -80 72
1293 74 0.0847227  90 -500 499 -103 102 -80 72
1294 48 0.117208  90 -501 500 -103 102 -80 72
1295 74 0.0847227  90 -502 501 -103 102 -80 72
1296 48 0.117208  90 -503 502 -103 102 -80 72
1297 74 0.0847227  90 -504 503 -103 102 -80 72
1298 48 0.117208  90 -505 504 -103 102 -80 72
1299 74 0.0847227  90 -506 505 -103 102 -80 72
1300 48 0.117208  90 -507 506 -103 102 -80 72
1301 74 0.0847227  90 -508 507 -103 102 -80 72
1302 48 0.117208  90 -509 508 -103 102 -80 72
1303 74 0.0847227  90 -510 509 -103 102 -80 72
1304 48 0.117208  90 -511 510 -103 102 -80 72
1305 74 0.0847227  90 -512 511 -103 102 -80 72
1306 48 0.117208  90 -95 512 -103 102 -80 72
1307 48 0.117208  90 -499 92 -94 103 -80 72
1308 74 0.0847227  90 -500 499 -94 103 -80 72
1309 48 0.117208  90 -501 500 -94 103 -80 72
1310 74 0.0847227  90 -502 501 -94 103 -80 72
1311 48 0.117208  90 -503 502 -94 103 -80 72
1312 74 0.0847227  90 -504 503 -94 103 -80 72
1313 48 0.117208  90 -505 504 -94 103 -80 72
1314 74 0.0847227  90 -506 505 -94 103 -80 72
1315 48 0.117208  90 -507 506 -94 103 -80 72
1316 74 0.0847227  90 -508 507 -94 103 -80 72
1317 48 0.117208  90 -509 508 -94 103 -80 72
1318 74 0.0847227  90 -510 509 -94 103 -80 72
1319 48 0.117208  90 -511 510 -94 103 -80 72
1320 74 0.0847227  90 -512 511 -94 103 -80 72
1321 48 0.117208  90 -95 512 -94 103 -80 72
1322 0  ( -63 : -70 : 91 : 132 ) 73 -80 71 -96 62 -95 3 93 ( 80 : -92 )
        90
1323 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 71 -96 62 -95 3 93 (
        80 : -92 ) 90
1324 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 71 -96 62 -95 3 93
        ( 80 : -92 ) 90
1325 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 71 -96 62 -95 3 93
        ( 80 : -92 ) 90
1326 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 71 -96 62 -95 3 93
        ( 80 : -92 ) 90
1327 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 71 -96 62 -95 3 93 (
        80 : -92 ) 90
1328 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 71 -96 62 -95 3 93
        ( 80 : -92 ) 90
1329 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 71 -96 62 -95 3 93 (
        80 : -92 ) 90
1330 0  ( -63 : -70 : 91 : 132 ) 73 -80 96 -97 62 -95 3 ( 80 : -92 ) 90
1331 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 96 -97 62 -95 3 ( 80
        : -92 ) 90
1332 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 96 -97 62 -95 3 (
        80 : -92 ) 90
1333 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 96 -97 62 -95 3 (
        80 : -92 ) 90
1334 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 96 -97 62 -95 3 (
        80 : -92 ) 90
1335 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 96 -97 62 -95 3 ( 80
        : -92 ) 90
1336 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 96 -97 62 -95 3 (
        80 : -92 ) 90
1337 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 96 -97 62 -95 3 ( 80
        : -92 ) 90
1338 0  ( -63 : -70 : 91 : 132 ) 73 -80 97 -98 62 -95 3 ( 80 : -92 ) 90
1339 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 97 -98 62 -95 3 ( 80
        : -92 ) 90
1340 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 97 -98 62 -95 3 (
        80 : -92 ) 90
1341 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 97 -98 62 -95 3 (
        80 : -92 ) 90
1342 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 97 -98 62 -95 3 (
        80 : -92 ) 90
1343 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 97 -98 62 -95 3 ( 80
        : -92 ) 90
1344 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 97 -98 62 -95 3 (
        80 : -92 ) 90
1345 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 97 -98 62 -95 3 ( 80
        : -92 ) 90
1346 0  ( -63 : -70 : 91 : 132 ) 73 -80 98 -99 62 -95 3 ( 80 : -92 ) 90
1347 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 98 -99 62 -95 3 ( 80
        : -92 ) 90
1348 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 98 -99 62 -95 3 (
        80 : -92 ) 90
1349 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 98 -99 62 -95 3 (
        80 : -92 ) 90
1350 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 98 -99 62 -95 3 (
        80 : -92 ) 90
1351 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 98 -99 62 -95 3 ( 80
        : -92 ) 90
1352 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 98 -99 62 -95 3 (
        80 : -92 ) 90
1353 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 98 -99 62 -95 3 ( 80
        : -92 ) 90
1354 0  ( -63 : -70 : 91 : 132 ) 73 -80 99 -100 62 -95 3 ( 80 : -92 )
        90
1355 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 99 -100 62 -95 3 (
        80 : -92 ) 90
1356 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 99 -100 62 -95 3 (
        80 : -92 ) 90
1357 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 99 -100 62 -95 3 (
        80 : -92 ) 90
1358 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 99 -100 62 -95 3 (
        80 : -92 ) 90
1359 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 99 -100 62 -95 3 (
        80 : -92 ) 90
1360 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 99 -100 62 -95 3 (
        80 : -92 ) 90
1361 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 99 -100 62 -95 3 (
        80 : -92 ) 90
1362 0  ( -63 : -70 : 91 : 132 ) 73 -80 100 -101 62 -95 3 ( 80 : -92 )
        90
1363 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 100 -101 62 -95 3 (
        80 : -92 ) 90
1364 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 100 -101 62 -95 3
        ( 80 : -92 ) 90
1365 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 100 -101 62 -95 3 (
        80 : -92 ) 90
1366 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 100 -101 62 -95 3
        ( 80 : -92 ) 90
1367 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 100 -101 62 -95 3 (
        80 : -92 ) 90
1368 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 100 -101 62 -95 3 (
        80 : -92 ) 90
1369 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 100 -101 62 -95 3 (
        80 : -92 ) 90
1370 0  ( -63 : -70 : 91 : 132 ) 73 -80 101 -102 62 -95 3 ( 80 : -92 )
        90
1371 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 101 -102 62 -95 3 (
        80 : -92 ) 90
1372 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 101 -102 62 -95 3
        ( 80 : -92 ) 90
1373 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 101 -102 62 -95 3 (
        80 : -92 ) 90
1374 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 101 -102 62 -95 3
        ( 80 : -92 ) 90
1375 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 101 -102 62 -95 3 (
        80 : -92 ) 90
1376 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 101 -102 62 -95 3 (
        80 : -92 ) 90
1377 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 101 -102 62 -95 3 (
        80 : -92 ) 90
1378 0  ( -63 : -70 : 91 : 132 ) 73 -80 102 -103 62 -95 3 ( 80 : -92 )
        90
1379 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 102 -103 62 -95 3 (
        80 : -92 ) 90
1380 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 102 -103 62 -95 3
        ( 80 : -92 ) 90
1381 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 102 -103 62 -95 3 (
        80 : -92 ) 90
1382 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 102 -103 62 -95 3
        ( 80 : -92 ) 90
1383 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 102 -103 62 -95 3 (
        80 : -92 ) 90
1384 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 102 -103 62 -95 3 (
        80 : -92 ) 90
1385 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 102 -103 62 -95 3 (
        80 : -92 ) 90
1386 0  ( -63 : -70 : 91 : 132 ) 73 -80 103 -94 62 -95 3 ( 80 : -91 ) (
        80 : -92 ) 90
1387 48 0.117208  ( -63 : -70 : 91 : 132 ) 80 -494 103 -94 62 -95 3 (
        80 : -91 ) ( 80 : -92 ) 90
1388 74 0.0847227  ( -63 : -70 : 91 : 132 ) 494 -495 103 -94 62 -95 3 (
        80 : -91 ) ( 80 : -92 ) 90
1389 48 0.117208  ( -63 : -70 : 91 : 132 ) 495 -496 103 -94 62 -95 3 (
        80 : -91 ) ( 80 : -92 ) 90
1390 74 0.0847227  ( -63 : -70 : 91 : 132 ) 496 -497 103 -94 62 -95 3 (
        80 : -91 ) ( 80 : -92 ) 90
1391 48 0.117208  ( -63 : -70 : 91 : 132 ) 497 -67 103 -94 62 -95 3 (
        80 : -91 ) ( 80 : -92 ) 90
1392 74 0.0847227  ( -63 : -70 : 91 : 132 ) 67 -498 103 -94 62 -95 3 (
        80 : -91 ) ( 80 : -92 ) 90
1393 48 0.117208  ( -63 : -70 : 91 : 132 ) 498 -78 103 -94 62 -95 3 (
        80 : -91 ) ( 80 : -92 ) 90
1394 48 0.117208  104 -480 74 -108 106 -80 72
1395 74 0.0847227  104 -481 480 -108 106 -80 72
1396 48 0.117208  104 -482 481 -108 106 -80 72
1397 74 0.0847227  104 -483 482 -108 106 -80 72
1398 48 0.117208  104 -484 483 -108 106 -80 72
1399 74 0.0847227  104 -485 484 -108 106 -80 72
1400 48 0.117208  104 -486 485 -108 106 -80 72
1401 74 0.0847227  104 -487 486 -108 106 -80 72
1402 48 0.117208  104 -488 487 -108 106 -80 72
1403 74 0.0847227  104 -489 488 -108 106 -80 72
1404 48 0.117208  104 -490 489 -108 106 -80 72
1405 74 0.0847227  104 -491 490 -108 106 -80 72
1406 48 0.117208  104 -492 491 -108 106 -80 72
1407 74 0.0847227  104 -493 492 -108 106 -80 72
1408 48 0.117208  104 -79 493 -108 106 -80 72
1409 48 0.117208  104 -480 74 -109 108 -80 72
1410 74 0.0847227  104 -481 480 -109 108 -80 72
1411 48 0.117208  104 -482 481 -109 108 -80 72
1412 74 0.0847227  104 -483 482 -109 108 -80 72
1413 48 0.117208  104 -484 483 -109 108 -80 72
1414 74 0.0847227  104 -485 484 -109 108 -80 72
1415 48 0.117208  104 -486 485 -109 108 -80 72
1416 74 0.0847227  104 -487 486 -109 108 -80 72
1417 48 0.117208  104 -488 487 -109 108 -80 72
1418 74 0.0847227  104 -489 488 -109 108 -80 72
1419 48 0.117208  104 -490 489 -109 108 -80 72
1420 74 0.0847227  104 -491 490 -109 108 -80 72
1421 48 0.117208  104 -492 491 -109 108 -80 72
1422 74 0.0847227  104 -493 492 -109 108 -80 72
1423 48 0.117208  104 -79 493 -109 108 -80 72
1424 48 0.117208  104 -480 74 -110 109 -80 72
1425 74 0.0847227  104 -481 480 -110 109 -80 72
1426 48 0.117208  104 -482 481 -110 109 -80 72
1427 74 0.0847227  104 -483 482 -110 109 -80 72
1428 48 0.117208  104 -484 483 -110 109 -80 72
1429 74 0.0847227  104 -485 484 -110 109 -80 72
1430 48 0.117208  104 -486 485 -110 109 -80 72
1431 74 0.0847227  104 -487 486 -110 109 -80 72
1432 48 0.117208  104 -488 487 -110 109 -80 72
1433 74 0.0847227  104 -489 488 -110 109 -80 72
1434 48 0.117208  104 -490 489 -110 109 -80 72
1435 74 0.0847227  104 -491 490 -110 109 -80 72
1436 48 0.117208  104 -492 491 -110 109 -80 72
1437 74 0.0847227  104 -493 492 -110 109 -80 72
1438 48 0.117208  104 -79 493 -110 109 -80 72
1439 48 0.117208  104 -480 74 -111 110 -80 72
1440 74 0.0847227  104 -481 480 -111 110 -80 72
1441 48 0.117208  104 -482 481 -111 110 -80 72
1442 74 0.0847227  104 -483 482 -111 110 -80 72
1443 48 0.117208  104 -484 483 -111 110 -80 72
1444 74 0.0847227  104 -485 484 -111 110 -80 72
1445 48 0.117208  104 -486 485 -111 110 -80 72
1446 74 0.0847227  104 -487 486 -111 110 -80 72
1447 48 0.117208  104 -488 487 -111 110 -80 72
1448 74 0.0847227  104 -489 488 -111 110 -80 72
1449 48 0.117208  104 -490 489 -111 110 -80 72
1450 74 0.0847227  104 -491 490 -111 110 -80 72
1451 48 0.117208  104 -492 491 -111 110 -80 72
1452 74 0.0847227  104 -493 492 -111 110 -80 72
1453 48 0.117208  104 -79 493 -111 110 -80 72
1454 48 0.117208  104 -480 74 -112 111 -80 72
1455 74 0.0847227  104 -481 480 -112 111 -80 72
1456 48 0.117208  104 -482 481 -112 111 -80 72
1457 74 0.0847227  104 -483 482 -112 111 -80 72
1458 48 0.117208  104 -484 483 -112 111 -80 72
1459 74 0.0847227  104 -485 484 -112 111 -80 72
1460 48 0.117208  104 -486 485 -112 111 -80 72
1461 74 0.0847227  104 -487 486 -112 111 -80 72
1462 48 0.117208  104 -488 487 -112 111 -80 72
1463 74 0.0847227  104 -489 488 -112 111 -80 72
1464 48 0.117208  104 -490 489 -112 111 -80 72
1465 74 0.0847227  104 -491 490 -112 111 -80 72
1466 48 0.117208  104 -492 491 -112 111 -80 72
1467 74 0.0847227  104 -493 492 -112 111 -80 72
1468 48 0.117208  104 -79 493 -112 111 -80 72
1469 48 0.117208  104 -480 74 -113 112 -80 72
1470 74 0.0847227  104 -481 480 -113 112 -80 72
1471 48 0.117208  104 -482 481 -113 112 -80 72
1472 74 0.0847227  104 -483 482 -113 112 -80 72
1473 48 0.117208  104 -484 483 -113 112 -80 72
1474 74 0.0847227  104 -485 484 -113 112 -80 72
1475 48 0.117208  104 -486 485 -113 112 -80 72
1476 74 0.0847227  104 -487 486 -113 112 -80 72
1477 48 0.117208  104 -488 487 -113 112 -80 72
1478 74 0.0847227  104 -489 488 -113 112 -80 72
1479 48 0.117208  104 -490 489 -113 112 -80 72
1480 74 0.0847227  104 -491 490 -113 112 -80 72
1481 48 0.117208  104 -492 491 -113 112 -80 72
1482 74 0.0847227  104 -493 492 -113 112 -80 72
1483 48 0.117208  104 -79 493 -113 112 -80 72
1484 48 0.117208  104 -480 74 -114 113 -80 72
1485 74 0.0847227  104 -481 480 -114 113 -80 72
1486 48 0.117208  104 -482 481 -114 113 -80 72
1487 74 0.0847227  104 -483 482 -114 113 -80 72
1488 48 0.117208  104 -484 483 -114 113 -80 72
1489 74 0.0847227  104 -485 484 -114 113 -80 72
1490 48 0.117208  104 -486 485 -114 113 -80 72
1491 74 0.0847227  104 -487 486 -114 113 -80 72
1492 48 0.117208  104 -488 487 -114 113 -80 72
1493 74 0.0847227  104 -489 488 -114 113 -80 72
1494 48 0.117208  104 -490 489 -114 113 -80 72
1495 74 0.0847227  104 -491 490 -114 113 -80 72
1496 48 0.117208  104 -492 491 -114 113 -80 72
1497 74 0.0847227  104 -493 492 -114 113 -80 72
1498 48 0.117208  104 -79 493 -114 113 -80 72
1499 48 0.117208  104 -480 74 -115 114 -80 72
1500 74 0.0847227  104 -481 480 -115 114 -80 72
1501 48 0.117208  104 -482 481 -115 114 -80 72
1502 74 0.0847227  104 -483 482 -115 114 -80 72
1503 48 0.117208  104 -484 483 -115 114 -80 72
1504 74 0.0847227  104 -485 484 -115 114 -80 72
1505 48 0.117208  104 -486 485 -115 114 -80 72
1506 74 0.0847227  104 -487 486 -115 114 -80 72
1507 48 0.117208  104 -488 487 -115 114 -80 72
1508 74 0.0847227  104 -489 488 -115 114 -80 72
1509 48 0.117208  104 -490 489 -115 114 -80 72
1510 74 0.0847227  104 -491 490 -115 114 -80 72
1511 48 0.117208  104 -492 491 -115 114 -80 72
1512 74 0.0847227  104 -493 492 -115 114 -80 72
1513 48 0.117208  104 -79 493 -115 114 -80 72
1514 48 0.117208  104 -480 74 -116 115 -80 72
1515 74 0.0847227  104 -481 480 -116 115 -80 72
1516 48 0.117208  104 -482 481 -116 115 -80 72
1517 74 0.0847227  104 -483 482 -116 115 -80 72
1518 48 0.117208  104 -484 483 -116 115 -80 72
1519 74 0.0847227  104 -485 484 -116 115 -80 72
1520 48 0.117208  104 -486 485 -116 115 -80 72
1521 74 0.0847227  104 -487 486 -116 115 -80 72
1522 48 0.117208  104 -488 487 -116 115 -80 72
1523 74 0.0847227  104 -489 488 -116 115 -80 72
1524 48 0.117208  104 -490 489 -116 115 -80 72
1525 74 0.0847227  104 -491 490 -116 115 -80 72
1526 48 0.117208  104 -492 491 -116 115 -80 72
1527 74 0.0847227  104 -493 492 -116 115 -80 72
1528 48 0.117208  104 -79 493 -116 115 -80 72
1529 48 0.117208  104 -480 74 -107 116 -80 72
1530 74 0.0847227  104 -481 480 -107 116 -80 72
1531 48 0.117208  104 -482 481 -107 116 -80 72
1532 74 0.0847227  104 -483 482 -107 116 -80 72
1533 48 0.117208  104 -484 483 -107 116 -80 72
1534 74 0.0847227  104 -485 484 -107 116 -80 72
1535 48 0.117208  104 -486 485 -107 116 -80 72
1536 74 0.0847227  104 -487 486 -107 116 -80 72
1537 48 0.117208  104 -488 487 -107 116 -80 72
1538 74 0.0847227  104 -489 488 -107 116 -80 72
1539 48 0.117208  104 -490 489 -107 116 -80 72
1540 74 0.0847227  104 -491 490 -107 116 -80 72
1541 48 0.117208  104 -492 491 -107 116 -80 72
1542 74 0.0847227  104 -493 492 -107 116 -80 72
1543 48 0.117208  104 -79 493 -107 116 -80 72
1544 0  104 ( 80 : -74 ) ( 80 : 91 ) -3 -79 62 -108 106 -80 73
1545 48 0.117208  104 ( 80 : -74 ) ( 80 : 91 ) -3 -79 62 -108 106 -494
        80
1546 74 0.0847227  104 ( 80 : -74 ) ( 80 : 91 ) -3 -79 62 -108 106 -495
        494
1547 48 0.117208  104 ( 80 : -74 ) ( 80 : 91 ) -3 -79 62 -108 106 -496
        495
1548 74 0.0847227  104 ( 80 : -74 ) ( 80 : 91 ) -3 -79 62 -108 106 -497
        496
1549 48 0.117208  104 ( 80 : -74 ) ( 80 : 91 ) -3 -79 62 -108 106 -67
        497
1550 74 0.0847227  104 ( 80 : -74 ) ( 80 : 91 ) -3 -79 62 -108 106 -498
        67
1551 48 0.117208  104 ( 80 : -74 ) ( 80 : 91 ) -3 -79 62 -108 106 -78
        498
1552 0  104 ( 80 : -74 ) -3 -79 62 -109 108 -80 73
1553 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -109 108 -494 80
1554 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -109 108 -495 494
1555 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -109 108 -496 495
1556 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -109 108 -497 496
1557 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -109 108 -67 497
1558 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -109 108 -498 67
1559 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -109 108 -78 498
1560 0  104 ( 80 : -74 ) -3 -79 62 -110 109 -80 73
1561 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -110 109 -494 80
1562 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -110 109 -495 494
1563 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -110 109 -496 495
1564 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -110 109 -497 496
1565 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -110 109 -67 497
1566 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -110 109 -498 67
1567 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -110 109 -78 498
1568 0  104 ( 80 : -74 ) -3 -79 62 -111 110 -80 73
1569 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -111 110 -494 80
1570 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -111 110 -495 494
1571 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -111 110 -496 495
1572 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -111 110 -497 496
1573 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -111 110 -67 497
1574 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -111 110 -498 67
1575 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -111 110 -78 498
1576 0  104 ( 80 : -74 ) -3 -79 62 -112 111 -80 73
1577 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -112 111 -494 80
1578 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -112 111 -495 494
1579 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -112 111 -496 495
1580 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -112 111 -497 496
1581 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -112 111 -67 497
1582 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -112 111 -498 67
1583 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -112 111 -78 498
1584 0  104 ( 80 : -74 ) -3 -79 62 -113 112 -80 73
1585 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -113 112 -494 80
1586 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -113 112 -495 494
1587 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -113 112 -496 495
1588 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -113 112 -497 496
1589 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -113 112 -67 497
1590 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -113 112 -498 67
1591 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -113 112 -78 498
1592 0  104 ( 80 : -74 ) -3 -79 62 -114 113 -80 73
1593 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -114 113 -494 80
1594 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -114 113 -495 494
1595 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -114 113 -496 495
1596 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -114 113 -497 496
1597 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -114 113 -67 497
1598 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -114 113 -498 67
1599 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -114 113 -78 498
1600 0  104 ( 80 : -74 ) -3 -79 62 -115 114 -80 73
1601 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -115 114 -494 80
1602 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -115 114 -495 494
1603 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -115 114 -496 495
1604 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -115 114 -497 496
1605 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -115 114 -67 497
1606 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -115 114 -498 67
1607 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -115 114 -78 498
1608 0  104 ( 80 : -74 ) -3 -79 62 -116 115 -80 73
1609 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -116 115 -494 80
1610 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -116 115 -495 494
1611 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -116 115 -496 495
1612 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -116 115 -497 496
1613 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -116 115 -67 497
1614 74 0.0847227  104 ( 80 : -74 ) -3 -79 62 -116 115 -498 67
1615 48 0.117208  104 ( 80 : -74 ) -3 -79 62 -116 115 -78 498
1616 0  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 73 -80 116 -107
        62 -79 -3 ( 80 : -105 ) ( 80 : -74 ) 104
1617 48 0.117208  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 80
        -494 116 -107 62 -79 -3 ( 80 : -105 ) ( 80 : -74 ) 104
1618 74 0.0847227  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 494
        -495 116 -107 62 -79 -3 ( 80 : -105 ) ( 80 : -74 ) 104
1619 48 0.117208  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 495
        -496 116 -107 62 -79 -3 ( 80 : -105 ) ( 80 : -74 ) 104
1620 74 0.0847227  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 496
        -497 116 -107 62 -79 -3 ( 80 : -105 ) ( 80 : -74 ) 104
1621 48 0.117208  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 497
        -67 116 -107 62 -79 -3 ( 80 : -105 ) ( 80 : -74 ) 104
1622 74 0.0847227  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 67
        -498 116 -107 62 -79 -3 ( 80 : -105 ) ( 80 : -74 ) 104
1623 48 0.117208  ( -117 : -105 : -77 : -62 : 3 : 78 : 119 : 95 ) 498
        -78 116 -107 62 -79 -3 ( 80 : -105 ) ( 80 : -74 ) 104
1624 48 0.117208  117 -499 92 -120 105 -80 72
1625 74 0.0847227  117 -500 499 -120 105 -80 72
1626 48 0.117208  117 -501 500 -120 105 -80 72
1627 74 0.0847227  117 -502 501 -120 105 -80 72
1628 48 0.117208  117 -503 502 -120 105 -80 72
1629 74 0.0847227  117 -504 503 -120 105 -80 72
1630 48 0.117208  117 -505 504 -120 105 -80 72
1631 74 0.0847227  117 -506 505 -120 105 -80 72
1632 48 0.117208  117 -507 506 -120 105 -80 72
1633 74 0.0847227  117 -508 507 -120 105 -80 72
1634 48 0.117208  117 -509 508 -120 105 -80 72
1635 74 0.0847227  117 -510 509 -120 105 -80 72
1636 48 0.117208  117 -511 510 -120 105 -80 72
1637 74 0.0847227  117 -512 511 -120 105 -80 72
1638 48 0.117208  117 -95 512 -120 105 -80 72
1639 48 0.117208  117 -499 92 -121 120 -80 72
1640 74 0.0847227  117 -500 499 -121 120 -80 72
1641 48 0.117208  117 -501 500 -121 120 -80 72
1642 74 0.0847227  117 -502 501 -121 120 -80 72
1643 48 0.117208  117 -503 502 -121 120 -80 72
1644 74 0.0847227  117 -504 503 -121 120 -80 72
1645 48 0.117208  117 -505 504 -121 120 -80 72
1646 74 0.0847227  117 -506 505 -121 120 -80 72
1647 48 0.117208  117 -507 506 -121 120 -80 72
1648 74 0.0847227  117 -508 507 -121 120 -80 72
1649 48 0.117208  117 -509 508 -121 120 -80 72
1650 74 0.0847227  117 -510 509 -121 120 -80 72
1651 48 0.117208  117 -511 510 -121 120 -80 72
1652 74 0.0847227  117 -512 511 -121 120 -80 72
1653 48 0.117208  117 -95 512 -121 120 -80 72
1654 48 0.117208  117 -499 92 -122 121 -80 72
1655 74 0.0847227  117 -500 499 -122 121 -80 72
1656 48 0.117208  117 -501 500 -122 121 -80 72
1657 74 0.0847227  117 -502 501 -122 121 -80 72
1658 48 0.117208  117 -503 502 -122 121 -80 72
1659 74 0.0847227  117 -504 503 -122 121 -80 72
1660 48 0.117208  117 -505 504 -122 121 -80 72
1661 74 0.0847227  117 -506 505 -122 121 -80 72
1662 48 0.117208  117 -507 506 -122 121 -80 72
1663 74 0.0847227  117 -508 507 -122 121 -80 72
1664 48 0.117208  117 -509 508 -122 121 -80 72
1665 74 0.0847227  117 -510 509 -122 121 -80 72
1666 48 0.117208  117 -511 510 -122 121 -80 72
1667 74 0.0847227  117 -512 511 -122 121 -80 72
1668 48 0.117208  117 -95 512 -122 121 -80 72
1669 48 0.117208  117 -499 92 -123 122 -80 72
1670 74 0.0847227  117 -500 499 -123 122 -80 72
1671 48 0.117208  117 -501 500 -123 122 -80 72
1672 74 0.0847227  117 -502 501 -123 122 -80 72
1673 48 0.117208  117 -503 502 -123 122 -80 72
1674 74 0.0847227  117 -504 503 -123 122 -80 72
1675 48 0.117208  117 -505 504 -123 122 -80 72
1676 74 0.0847227  117 -506 505 -123 122 -80 72
1677 48 0.117208  117 -507 506 -123 122 -80 72
1678 74 0.0847227  117 -508 507 -123 122 -80 72
1679 48 0.117208  117 -509 508 -123 122 -80 72
1680 74 0.0847227  117 -510 509 -123 122 -80 72
1681 48 0.117208  117 -511 510 -123 122 -80 72
1682 74 0.0847227  117 -512 511 -123 122 -80 72
1683 48 0.117208  117 -95 512 -123 122 -80 72
1684 48 0.117208  117 -499 92 -124 123 -80 72
1685 74 0.0847227  117 -500 499 -124 123 -80 72
1686 48 0.117208  117 -501 500 -124 123 -80 72
1687 74 0.0847227  117 -502 501 -124 123 -80 72
1688 48 0.117208  117 -503 502 -124 123 -80 72
1689 74 0.0847227  117 -504 503 -124 123 -80 72
1690 48 0.117208  117 -505 504 -124 123 -80 72
1691 74 0.0847227  117 -506 505 -124 123 -80 72
1692 48 0.117208  117 -507 506 -124 123 -80 72
1693 74 0.0847227  117 -508 507 -124 123 -80 72
1694 48 0.117208  117 -509 508 -124 123 -80 72
1695 74 0.0847227  117 -510 509 -124 123 -80 72
1696 48 0.117208  117 -511 510 -124 123 -80 72
1697 74 0.0847227  117 -512 511 -124 123 -80 72
1698 48 0.117208  117 -95 512 -124 123 -80 72
1699 48 0.117208  117 -499 92 -125 124 -80 72
1700 74 0.0847227  117 -500 499 -125 124 -80 72
1701 48 0.117208  117 -501 500 -125 124 -80 72
1702 74 0.0847227  117 -502 501 -125 124 -80 72
1703 48 0.117208  117 -503 502 -125 124 -80 72
1704 74 0.0847227  117 -504 503 -125 124 -80 72
1705 48 0.117208  117 -505 504 -125 124 -80 72
1706 74 0.0847227  117 -506 505 -125 124 -80 72
1707 48 0.117208  117 -507 506 -125 124 -80 72
1708 74 0.0847227  117 -508 507 -125 124 -80 72
1709 48 0.117208  117 -509 508 -125 124 -80 72
1710 74 0.0847227  117 -510 509 -125 124 -80 72
1711 48 0.117208  117 -511 510 -125 124 -80 72
1712 74 0.0847227  117 -512 511 -125 124 -80 72
1713 48 0.117208  117 -95 512 -125 124 -80 72
1714 48 0.117208  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 125
        -126 92 -499 117
1715 74 0.0847227  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 125
        -126 499 -500 117
1716 48 0.117208  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 125
        -126 500 -501 117
1717 74 0.0847227  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 125
        -126 501 -502 117
1718 48 0.117208  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 125
        -126 502 -503 117
1719 74 0.0847227  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 125
        -126 503 -504 117
1720 48 0.117208  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 125
        -126 504 -505 117
1721 74 0.0847227  ( -703 : -705 : -698 : 707 : 706 : 704 ) 117 -506
        505 -126 125 -80 72 ( -472 : -696 : -679 : 698 : 697 : 473 )
1722 48 0.117208  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 125
        -126 506 -507 117
1723 74 0.0847227  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 125
        -126 507 -508 117
1724 48 0.117208  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 125
        -126 508 -509 117
1725 74 0.0847227  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 125
        -126 509 -510 117
1726 48 0.117208  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 125
        -126 510 -511 117
1727 74 0.0847227  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 125
        -126 511 -512 117
1728 48 0.117208  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 125
        -126 512 -95 117
1729 48 0.117208  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 126
        -127 92 -499 117
1730 74 0.0847227  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 126
        -127 499 -500 117
1731 48 0.117208  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 126
        -127 500 -501 117
1732 74 0.0847227  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 126
        -127 501 -502 117
1733 48 0.117208  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 126
        -127 502 -503 117
1734 74 0.0847227  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 126
        -127 503 -504 117
1735 48 0.117208  ( -472 : -696 : -679 : 698 : 697 : 473 ) 72 -80 126
        -127 504 -505 117
1736 74 0.0847227  ( -703 : -705 : -698 : 707 : 706 : 704 ) 117 -506
        505 -127 126 -80 72 ( -472 : -696 : -679 : 698 : 697 : 473 )
1737 48 0.117208  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 126
        -127 506 -507 117
1738 74 0.0847227  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 126
        -127 507 -508 117
1739 48 0.117208  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 126
        -127 508 -509 117
1740 74 0.0847227  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 126
        -127 509 -510 117
1741 48 0.117208  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 126
        -127 510 -511 117
1742 74 0.0847227  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 126
        -127 511 -512 117
1743 48 0.117208  ( -703 : -705 : -698 : 707 : 706 : 704 ) 72 -80 126
        -127 512 -95 117
1744 48 0.117208  117 -499 92 -119 127 -80 72
1745 74 0.0847227  117 -500 499 -119 127 -80 72
1746 48 0.117208  117 -501 500 -119 127 -80 72
1747 74 0.0847227  117 -502 501 -119 127 -80 72
1748 48 0.117208  117 -503 502 -119 127 -80 72
1749 74 0.0847227  117 -504 503 -119 127 -80 72
1750 48 0.117208  117 -505 504 -119 127 -80 72
1751 74 0.0847227  117 -506 505 -119 127 -80 72
1752 48 0.117208  117 -507 506 -119 127 -80 72
1753 74 0.0847227  117 -508 507 -119 127 -80 72
1754 48 0.117208  117 -509 508 -119 127 -80 72
1755 74 0.0847227  117 -510 509 -119 127 -80 72
1756 48 0.117208  117 -511 510 -119 127 -80 72
1757 74 0.0847227  117 -512 511 -119 127 -80 72
1758 48 0.117208  117 -95 512 -119 127 -80 72
1759 0  117 ( 80 : -92 ) 118 -3 -95 62 -120 105 -80 73
1760 48 0.117208  117 ( 80 : -92 ) 118 -3 -95 62 -120 105 -494 80
1761 74 0.0847227  117 ( 80 : -92 ) 118 -3 -95 62 -120 105 -495 494
1762 48 0.117208  117 ( 80 : -92 ) 118 -3 -95 62 -120 105 -496 495
1763 74 0.0847227  117 ( 80 : -92 ) 118 -3 -95 62 -120 105 -497 496
1764 48 0.117208  117 ( 80 : -92 ) 118 -3 -95 62 -120 105 -67 497
1765 74 0.0847227  117 ( 80 : -92 ) 118 -3 -95 62 -120 105 -498 67
1766 48 0.117208  117 ( 80 : -92 ) 118 -3 -95 62 -120 105 -78 498
1767 0  117 ( 80 : -92 ) -3 -95 62 -121 120 -80 73
1768 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -121 120 -494 80
1769 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -121 120 -495 494
1770 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -121 120 -496 495
1771 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -121 120 -497 496
1772 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -121 120 -67 497
1773 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -121 120 -498 67
1774 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -121 120 -78 498
1775 0  117 ( 80 : -92 ) -3 -95 62 -122 121 -80 73
1776 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -122 121 -494 80
1777 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -122 121 -495 494
1778 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -122 121 -496 495
1779 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -122 121 -497 496
1780 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -122 121 -67 497
1781 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -122 121 -498 67
1782 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -122 121 -78 498
1783 0  117 ( 80 : -92 ) -3 -95 62 -123 122 -80 73
1784 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -123 122 -494 80
1785 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -123 122 -495 494
1786 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -123 122 -496 495
1787 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -123 122 -497 496
1788 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -123 122 -67 497
1789 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -123 122 -498 67
1790 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -123 122 -78 498
1791 0  117 ( 80 : -92 ) -3 -95 62 -124 123 -80 73
1792 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -124 123 -494 80
1793 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -124 123 -495 494
1794 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -124 123 -496 495
1795 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -124 123 -497 496
1796 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -124 123 -67 497
1797 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -124 123 -498 67
1798 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -124 123 -78 498
1799 0  117 ( 80 : -92 ) -3 -95 62 -125 124 -80 73
1800 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -125 124 -494 80
1801 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -125 124 -495 494
1802 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -125 124 -496 495
1803 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -125 124 -497 496
1804 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -125 124 -67 497
1805 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -125 124 -498 67
1806 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -125 124 -78 498
1807 0  117 ( 80 : -92 ) -3 -95 62 -126 125 -80 73
1808 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -126 125 -494 80
1809 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -126 125 -495 494
1810 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -126 125 -496 495
1811 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -126 125 -497 496
1812 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -126 125 -67 497
1813 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -126 125 -498 67
1814 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -126 125 -78 498
1815 0  117 ( 80 : -92 ) -3 -95 62 -127 126 -80 73
1816 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -127 126 -494 80
1817 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -127 126 -495 494
1818 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -127 126 -496 495
1819 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -127 126 -497 496
1820 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -127 126 -67 497
1821 74 0.0847227  117 ( 80 : -92 ) -3 -95 62 -127 126 -498 67
1822 48 0.117208  117 ( 80 : -92 ) -3 -95 62 -127 126 -78 498
1823 0  117 ( 80 : -92 ) ( 80 : -70 ) -3 -95 62 -119 127 -80 73
1824 48 0.117208  117 ( 80 : -92 ) ( 80 : -70 ) -3 -95 62 -119 127 -494
        80
1825 74 0.0847227  117 ( 80 : -92 ) ( 80 : -70 ) -3 -95 62 -119 127
        -495 494
1826 48 0.117208  117 ( 80 : -92 ) ( 80 : -70 ) -3 -95 62 -119 127 -496
        495
1827 74 0.0847227  117 ( 80 : -92 ) ( 80 : -70 ) -3 -95 62 -119 127
        -497 496
1828 48 0.117208  117 ( 80 : -92 ) ( 80 : -70 ) -3 -95 62 -119 127 -67
        497
1829 74 0.0847227  117 ( 80 : -92 ) ( 80 : -70 ) -3 -95 62 -119 127
        -498 67
1830 48 0.117208  117 ( 80 : -92 ) ( 80 : -70 ) -3 -95 62 -119 127 -78
        498
1831 25 0.041957  ( -518 : 522 : 515 ) -513 -344 322 ( -344 : -515 :
        517 )  tmp=1.72346844e-09
1832 5 0.0582256  ( -518 : 522 : 515 ) 322 -344 -514 513
        tmp=1.72346844e-09
1833 25 0.041957  -513 515 344  tmp=1.72346844e-09
1834 5 0.0582256  513 -514 515 344  tmp=1.72346844e-09
1835 0  ( -518 : 522 : 515 ) 344 515 -516 514
1836 5 0.0582256  ( -518 : 522 : 515 ) 344 515 -517 516
1837 25 0.041957  ( -523 : 527 : 518 ) -515 518 -519
        tmp=1.72346844e-09
1838 5 0.0582256  ( -523 : 527 : 518 ) -515 518 -520 519
        tmp=1.72346844e-09
1839 0  ( -523 : 527 : 518 ) -515 518 -521 520
1840 5 0.0582256  ( -523 : 527 : 518 ) -515 518 -522 521
1841 25 0.041957  -524 523 -518  tmp=1.72346844e-09
1842 5 0.0582256  524 -525 523 -518  tmp=1.72346844e-09
1843 0  525 -526 523 -518
1844 5 0.0582256  526 -527 523 -518
1845 25 0.041957  -524 528 -523  tmp=1.72346844e-09
1846 3 0.0878729  524 -525 528 -523  tmp=1.72346844e-09
1847 0  525 -526 528 -523
1848 3 0.0878729  526 -527 528 -523
1849 25 0.041957  ( -530 : 534 : 529 ) -528 529 -524
        tmp=1.72346844e-09
1850 127 0.0860477  ( -530 : 534 : 529 ) -528 529 -525 524
        tmp=1.72346844e-09
1851 0  ( -530 : 534 : 529 ) -528 529 -526 525
1852 127 0.0860477  ( -530 : 534 : 529 ) -528 529 -527 526
1853 25 0.041957  -531 530 -529  tmp=1.72346844e-09
1854 127 0.0860477  531 -532 530 -529  tmp=1.72346844e-09
1855 0  532 -533 530 -529
1856 127 0.0860477  533 -534 530 -529
1857 25 0.041957  ( -540 : 544 : 537 ) -535 -344 322 ( -344 : -537 :
        539 )  tmp=1.72346844e-09
1858 5 0.0582256  ( -540 : 544 : 537 ) 322 -344 -536 535
        tmp=1.72346844e-09
1859 25 0.041957  -535 537 344  tmp=1.72346844e-09
1860 5 0.0582256  535 -536 537 344  tmp=1.72346844e-09
1861 0  ( -540 : 544 : 537 ) 344 537 -538 536
1862 5 0.0582256  ( -540 : 544 : 537 ) 344 537 -539 538
1863 25 0.041957  ( -523 : 548 : 540 ) -537 540 -541
        tmp=1.72346844e-09
1864 5 0.0582256  ( -523 : 548 : 540 ) -537 540 -542 541
        tmp=1.72346844e-09
1865 0  ( -523 : 548 : 540 ) -537 540 -543 542
1866 5 0.0582256  ( -523 : 548 : 540 ) -537 540 -544 543
1867 25 0.041957  -545 523 -540  tmp=1.72346844e-09
1868 5 0.0582256  545 -546 523 -540  tmp=1.72346844e-09
1869 0  546 -547 523 -540
1870 5 0.0582256  547 -548 523 -540
1871 25 0.041957  -545 528 -523  tmp=1.72346844e-09
1872 3 0.0878729  545 -546 528 -523  tmp=1.72346844e-09
1873 0  546 -547 528 -523
1874 3 0.0878729  547 -548 528 -523
1875 25 0.041957  ( -530 : 552 : 529 ) -528 529 -545
        tmp=1.72346844e-09
1876 127 0.0860477  ( -530 : 552 : 529 ) -528 529 -546 545
        tmp=1.72346844e-09
1877 0  ( -530 : 552 : 529 ) -528 529 -547 546
1878 127 0.0860477  ( -530 : 552 : 529 ) -528 529 -548 547
1879 25 0.041957  -549 530 -529  tmp=1.72346844e-09
1880 127 0.0860477  549 -550 530 -529  tmp=1.72346844e-09
1881 0  550 -551 530 -529
1882 127 0.0860477  551 -552 530 -529
1883 25 0.041957  ( -558 : 562 : 555 ) -553 -385 366 ( -385 : -555 :
        557 )  tmp=1.72346844e-09
1884 5 0.0582256  ( -558 : 562 : 555 ) 366 -385 -554 553
        tmp=1.72346844e-09
1885 25 0.041957  -553 555 385  tmp=1.72346844e-09
1886 5 0.0582256  553 -554 555 385  tmp=1.72346844e-09
1887 0  ( -558 : 562 : 555 ) 385 555 -556 554
1888 5 0.0582256  ( -558 : 562 : 555 ) 385 555 -557 556
1889 25 0.041957  ( -563 : 567 : 558 ) -555 558 -559
        tmp=1.72346844e-09
1890 5 0.0582256  ( -563 : 567 : 558 ) -555 558 -560 559
        tmp=1.72346844e-09
1891 0  ( -563 : 567 : 558 ) -555 558 -561 560
1892 5 0.0582256  ( -563 : 567 : 558 ) -555 558 -562 561
1893 25 0.041957  -564 563 -558  tmp=1.72346844e-09
1894 5 0.0582256  564 -565 563 -558  tmp=1.72346844e-09
1895 0  565 -566 563 -558
1896 5 0.0582256  566 -567 563 -558
1897 25 0.041957  -564 568 -563  tmp=1.72346844e-09
1898 3 0.0878729  564 -565 568 -563  tmp=1.72346844e-09
1899 0  565 -566 568 -563
1900 3 0.0878729  566 -567 568 -563
1901 25 0.041957  ( -570 : 574 : 569 ) -568 569 -564
        tmp=1.72346844e-09
1902 127 0.0860477  ( -570 : 574 : 569 ) -568 569 -565 564
        tmp=1.72346844e-09
1903 0  ( -570 : 574 : 569 ) -568 569 -566 565
1904 127 0.0860477  ( -570 : 574 : 569 ) -568 569 -567 566
1905 25 0.041957  -571 570 -569  tmp=1.72346844e-09
1906 127 0.0860477  571 -572 570 -569  tmp=1.72346844e-09
1907 0  572 -573 570 -569
1908 127 0.0860477  573 -574 570 -569
1909 25 0.041957  ( -580 : 584 : 577 ) -575 -385 366 ( -385 : -577 :
        579 )  tmp=1.72346844e-09
1910 5 0.0582256  ( -580 : 584 : 577 ) 366 -385 -576 575
        tmp=1.72346844e-09
1911 25 0.041957  -575 577 385  tmp=1.72346844e-09
1912 5 0.0582256  575 -576 577 385  tmp=1.72346844e-09
1913 0  ( -580 : 584 : 577 ) 385 577 -578 576
1914 5 0.0582256  ( -580 : 584 : 577 ) 385 577 -579 578
1915 25 0.041957  ( -563 : 588 : 580 ) -577 580 -581
        tmp=1.72346844e-09
1916 5 0.0582256  ( -563 : 588 : 580 ) -577 580 -582 581
        tmp=1.72346844e-09
1917 0  ( -563 : 588 : 580 ) -577 580 -583 582
1918 5 0.0582256  ( -563 : 588 : 580 ) -577 580 -584 583
1919 25 0.041957  -585 563 -580  tmp=1.72346844e-09
1920 5 0.0582256  585 -586 563 -580  tmp=1.72346844e-09
1921 0  586 -587 563 -580
1922 5 0.0582256  587 -588 563 -580
1923 25 0.041957  -585 568 -563  tmp=1.72346844e-09
1924 3 0.0878729  585 -586 568 -563  tmp=1.72346844e-09
1925 0  586 -587 568 -563
1926 3 0.0878729  587 -588 568 -563
1927 25 0.041957  ( -570 : 592 : 569 ) -568 569 -585
        tmp=1.72346844e-09
1928 127 0.0860477  ( -570 : 592 : 569 ) -568 569 -586 585
        tmp=1.72346844e-09
1929 0  ( -570 : 592 : 569 ) -568 569 -587 586
1930 127 0.0860477  ( -570 : 592 : 569 ) -568 569 -588 587
1931 25 0.041957  -589 570 -569  tmp=1.72346844e-09
1932 127 0.0860477  589 -590 570 -569  tmp=1.72346844e-09
1933 0  590 -591 570 -569
1934 127 0.0860477  591 -592 570 -569
1935 0  -3 -60 -3 57 -597 596 -595 594 593
1936 73 0.0844385  ( -594 : 595 : -596 : 597 ) -3 -60 -3 57 -601 600
        -599 598 593
1937 83 0.0499651  -610 609 -611
1938 83 0.0499651  -612 613 -611
1939 0  ( -606 : -617 : -598 : -614 : 599 : 618 : 607 ) -604 602 -603 (
        -609 : 610 : 611 ) ( -613 : 612 : 611 )
1940 5 0.0582256  604 -605 -607 606
1941 5 0.0582256  ( -609 : 610 : 611 ) 602 604 -608 -606
1942 5 0.0582256  ( -613 : 612 : 611 ) -603 604 -608 607
1943 0  605 -608 -607 606
1944 0  -607 606 -616 615 -595 594 614
1945 73 0.0844385  ( -594 : 595 : -615 : 616 ) -607 606 -618 617 -599
        598 614
1946 83 0.0499651  -627 626 -628
1947 83 0.0499651  -629 630 -628
1948 0  ( -623 : 624 ) ( -630 : 629 : 628 ) ( -626 : 627 : 628 ) -620
        619 -621 ( -623 : -634 : -598 : -631 : 599 : 635 : 624 )
1949 5 0.0582256  621 -622 -624 623
1950 5 0.0582256  ( -626 : 627 : 628 ) 619 621 -625 -623
1951 5 0.0582256  ( -630 : 629 : 628 ) -620 621 -625 624
1952 0  622 -625 -624 623
1953 0  -624 623 -633 632 -595 594 631
1954 73 0.0844385  ( -594 : 595 : -632 : 633 ) -624 623 -635 634 -599
        598 631
1955 83 0.0499651  -644 643 -645
1956 83 0.0499651  -646 647 -645
1957 0  ( -690 : 691 ) ( -647 : 646 : 645 ) ( -643 : 644 : 645 ) -637
        636 -638 ( -648 : -653 : -598 : -650 : 599 : 654 : 649 )
1958 5 0.0582256  638 -639 -641 640
1959 5 0.0582256  ( -643 : 644 : 645 ) 636 638 -642 -640
1960 5 0.0582256  ( -647 : 646 : 645 ) -637 638 -642 641
1961 0  639 -642 -641 640
1962 0  -649 648 -652 651 594 -595 650
1963 73 0.0844385  ( 595 : -594 : -651 : 652 ) -649 648 -654 653 598
        -599 650
1964 83 0.0499651  -663 662 -664
1965 83 0.0499651  -665 666 -664
1966 0  ( -667 : -672 : -598 : -669 : 599 : 673 : 668 ) -657 655 -656 (
        -662 : 663 : 664 ) ( -666 : 665 : 664 )
1967 5 0.0582256  657 -658 -660 659
1968 5 0.0582256  ( -662 : 663 : 664 ) 655 657 -661 -659
1969 5 0.0582256  ( -666 : 665 : 664 ) -656 657 -661 660
1970 0  658 -661 -660 659
1971 0  -668 667 -671 670 594 -595 669
1972 73 0.0844385  ( 595 : -594 : -670 : 671 ) -668 667 -673 672 598
        -599 669
1973 83 0.0499651  -682 681 -683
1974 83 0.0499651  -684 685 -683
1975 0  ( -678 : 679 ) ( -685 : 684 : 683 ) ( -681 : 682 : 683 ) -675
        674 -676 ( -678 : -689 : -688 : -598 : 599 : 679 )
1976 5 0.0582256  676 -677 -679 678
1977 5 0.0582256  ( -681 : 682 : 683 ) 674 676 -680 -678
1978 5 0.0582256  ( -685 : 684 : 683 ) -675 676 -680 679
1979 0  677 -680 -679 678
1980 0  -679 678 687 -595 686 594
1981 73 0.0844385  ( -594 : -686 : -687 : 595 ) -679 678 689 -599 688
        598
1982 73 0.0844385  -621 ( -631 : -598 : -634 : 635 : 599 ) -624 623
1983 73 0.0844385  -638 ( -650 : -598 : -653 : 654 : 599 ) -691 690
1984 73 0.0844385  -676 ( -598 : -688 : -689 : 599 ) -679 678
1985 3 0.0878729  ( -679 : -689 : -688 : -598 : 599 : 698 ) 679 -698
        692 -693 694 -695
1986 3 0.0878729  ( -692 : -694 : 695 : 693 ) -697 696 -473 472 -698
        679
1987 0  -698 679 687 -595 686 594
1988 73 0.0844385  ( -594 : -686 : -687 : 595 ) -698 679 689 -599 688
        598
1989 3 0.0878729  ( -698 : -711 : -710 : -709 : -708 : 707 ) 698 -707
        699 -700 701 -702
1990 3 0.0878729  ( -699 : -701 : 702 : 700 ) -706 705 -704 703 -707
        698
1991 0  -707 698 687 -595 686 594
1992 73 0.0844385  ( -594 : -686 : -687 : 595 ) -707 698 711 710 709
        708
1993 0  ( -703 : -705 : -698 : 707 : 706 : 704 ) 712 -713 714 -715 95
        117 -753
1994 47 0.136566  -753 117 95 -715 714 -712 766
1995 47 0.136566  -753 117 95 -715 714 -767 713
1996 71 0.0843223  -753 117 95 -715 714 -766 718
1997 71 0.0843223  -753 117 95 -715 714 -719 767
1998 71 0.0843223  -753 117 95 -715 714 -718 722
1999 71 0.0843223  -753 117 95 -715 714 -723 719
2000 49 0.074981  -753 117 95 -715 714 -722 726
2001 49 0.074981  -753 117 95 -715 714 -727 723
2002 49 0.074981  -753 117 95 -715 714 -726 730
2003 49 0.074981  -753 117 95 -715 714 -731 727
2004 49 0.074981  -753 117 95 -715 714 -730 733
2005 49 0.074981  -753 117 95 -715 714 -734 731
2006 49 0.074981  -753 117 95 -715 714 -733 736
2007 49 0.074981  -753 117 95 -715 714 -737 734
2008 47 0.136566  -753 117 95 715 -717 -713 712
2009 71 0.0843223  -753 117 95 717 -721 -713 712
2010 71 0.0843223  -753 117 95 721 -725 -713 712
2011 49 0.074981  -753 117 95 725 -729 -713 712
2012 49 0.074981  -753 117 95 729 -732 -713 712
2013 49 0.074981  -753 117 95 732 -735 -713 712
2014 49 0.074981  -753 117 95 735 -738 -713 712
2015 47 0.136566  -753 117 95 716 -714 -737 736
2016 71 0.0843223  -753 117 95 720 -716 -737 736
2017 71 0.0843223  -753 117 95 724 -720 -737 736
2018 49 0.074981  -753 117 95 728 -724 -737 736
2019 47 0.136566  -753 117 95 -717 715 766 -712
2020 47 0.136566  -753 117 95 -717 715 -767 713
2021 71 0.0843223  -753 117 95 -717 715 718 -766
2022 71 0.0843223  -753 117 95 -717 715 -719 767
2023 71 0.0843223  -753 117 95 -717 715 722 -718
2024 71 0.0843223  -753 117 95 -717 715 -723 719
2025 49 0.074981  -753 117 95 -717 715 726 -722
2026 49 0.074981  -753 117 95 -717 715 -727 723
2027 49 0.074981  -753 117 95 -717 715 730 -726
2028 49 0.074981  -753 117 95 -717 715 -731 727
2029 49 0.074981  -753 117 95 -717 715 733 -730
2030 49 0.074981  -753 117 95 -717 715 -734 731
2031 49 0.074981  -753 117 95 -717 715 736 -733
2032 49 0.074981  -753 117 95 -717 715 -737 734
2033 71 0.0843223  -753 117 95 -721 717 766 -712
2034 71 0.0843223  -753 117 95 -721 717 -767 713
2035 71 0.0843223  -753 117 95 -721 717 718 -766
2036 71 0.0843223  -753 117 95 -721 717 -719 767
2037 71 0.0843223  -753 117 95 -721 717 722 -718
2038 71 0.0843223  -753 117 95 -721 717 -723 719
2039 49 0.074981  -753 117 95 -721 717 726 -722
2040 49 0.074981  -753 117 95 -721 717 -727 723
2041 49 0.074981  -753 117 95 -721 717 730 -726
2042 49 0.074981  -753 117 95 -721 717 -731 727
2043 49 0.074981  -753 117 95 -721 717 733 -730
2044 49 0.074981  -753 117 95 -721 717 -734 731
2045 49 0.074981  -753 117 95 -721 717 736 -733
2046 49 0.074981  -753 117 95 -721 717 -737 734
2047 71 0.0843223  -753 117 95 -725 721 766 -712
2048 71 0.0843223  -753 117 95 -725 721 -767 713
2049 71 0.0843223  -753 117 95 -725 721 718 -766
2050 71 0.0843223  -753 117 95 -725 721 -719 767
2051 71 0.0843223  -753 117 95 -725 721 722 -718
2052 71 0.0843223  -753 117 95 -725 721 -723 719
2053 49 0.074981  -753 117 95 -725 721 726 -722
2054 49 0.074981  -753 117 95 -725 721 -727 723
2055 49 0.074981  -753 117 95 -725 721 730 -726
2056 49 0.074981  -753 117 95 -725 721 -731 727
2057 49 0.074981  -753 117 95 -725 721 733 -730
2058 49 0.074981  -753 117 95 -725 721 -734 731
2059 49 0.074981  -753 117 95 -725 721 736 -733
2060 49 0.074981  -753 117 95 -725 721 -737 734
2061 49 0.074981  -753 117 95 -729 725 766 -712
2062 49 0.074981  -753 117 95 -729 725 -767 713
2063 49 0.074981  -753 117 95 -729 725 718 -766
2064 49 0.074981  -753 117 95 -729 725 -719 767
2065 49 0.074981  -753 117 95 -729 725 722 -718
2066 49 0.074981  -753 117 95 -729 725 -723 719
2067 49 0.074981  -753 117 95 -729 725 726 -722
2068 49 0.074981  -753 117 95 -729 725 -727 723
2069 49 0.074981  -753 117 95 -729 725 730 -726
2070 49 0.074981  -753 117 95 -729 725 -731 727
2071 49 0.074981  -753 117 95 -729 725 733 -730
2072 49 0.074981  -753 117 95 -729 725 -734 731
2073 49 0.074981  -753 117 95 -729 725 736 -733
2074 49 0.074981  -753 117 95 -729 725 -737 734
2075 49 0.074981  -753 117 95 -732 729 766 -712
2076 49 0.074981  -753 117 95 -732 729 -767 713
2077 49 0.074981  -753 117 95 -732 729 718 -766
2078 49 0.074981  -753 117 95 -732 729 -719 767
2079 49 0.074981  -753 117 95 -732 729 722 -718
2080 49 0.074981  -753 117 95 -732 729 -723 719
2081 49 0.074981  -753 117 95 -732 729 726 -722
2082 49 0.074981  -753 117 95 -732 729 -727 723
2083 49 0.074981  -753 117 95 -732 729 730 -726
2084 49 0.074981  -753 117 95 -732 729 -731 727
2085 49 0.074981  -753 117 95 -732 729 733 -730
2086 49 0.074981  -753 117 95 -732 729 -734 731
2087 49 0.074981  -753 117 95 -732 729 736 -733
2088 49 0.074981  -753 117 95 -732 729 -737 734
2089 49 0.074981  -753 117 95 -735 732 766 -712
2090 49 0.074981  -753 117 95 -735 732 -767 713
2091 49 0.074981  -753 117 95 -735 732 718 -766
2092 49 0.074981  -753 117 95 -735 732 -719 767
2093 49 0.074981  -753 117 95 -735 732 722 -718
2094 49 0.074981  -753 117 95 -735 732 -723 719
2095 49 0.074981  -753 117 95 -735 732 726 -722
2096 49 0.074981  -753 117 95 -735 732 -727 723
2097 49 0.074981  -753 117 95 -735 732 730 -726
2098 49 0.074981  -753 117 95 -735 732 -731 727
2099 49 0.074981  -753 117 95 -735 732 733 -730
2100 49 0.074981  -753 117 95 -735 732 -734 731
2101 49 0.074981  -753 117 95 -735 732 736 -733
2102 49 0.074981  -753 117 95 -735 732 -737 734
2103 49 0.074981  -753 117 95 -738 735 766 -712
2104 49 0.074981  -753 117 95 -738 735 -767 713
2105 49 0.074981  -753 117 95 -738 735 718 -766
2106 49 0.074981  -753 117 95 -738 735 -719 767
2107 49 0.074981  -753 117 95 -738 735 722 -718
2108 49 0.074981  -753 117 95 -738 735 -723 719
2109 49 0.074981  -753 117 95 -738 735 726 -722
2110 49 0.074981  -753 117 95 -738 735 -727 723
2111 49 0.074981  -753 117 95 -738 735 730 -726
2112 49 0.074981  -753 117 95 -738 735 -731 727
2113 49 0.074981  -753 117 95 -738 735 733 -730
2114 49 0.074981  -753 117 95 -738 735 -734 731
2115 49 0.074981  -753 117 95 -738 735 736 -733
2116 49 0.074981  -753 117 95 -738 735 -737 734
2117 83 0.0499651  -744 743 -683
2118 83 0.0499651  -745 746 -683
2119 0  ( -741 : -689 : -688 : -598 : 599 : 742 ) -676 739 -740 ( -743
        : 744 : 683 ) ( -746 : 745 : 683 )
2120 5 0.0582256  676 -677 -742 741
2121 5 0.0582256  ( -743 : 744 : 683 ) 739 676 -680 -741
2122 5 0.0582256  ( -746 : 745 : 683 ) -740 676 -680 742
2123 0  677 -680 -742 741
2124 0  -742 741 687 -595 686 594
2125 73 0.0844385  ( -594 : -686 : -687 : 595 ) -742 741 689 -599 688
        598
2126 0  ( -787 : 784 : 793 ) ( -788 : 791 : 790 ) ( -785 : 793 : 788 )
        -752 751 -750 749 -748 747 ( -774 : -776 : -778 : -780 : -781 :
        -782 : -783 : 779 : 777 : 775 )
2127 38 0.06359  ( -770 : -772 : 773 : 771 ) 748 -765 766 -767 768 -769
2128 54 0.0833854  ( -770 : -772 : 773 : 771 ) 753 -747 749 -750 751
        -752
2129 54 0.0833854  ( -749 : 750 : -751 : 752 ) -758 757 -756 755 -754
        753
2130 54 0.0833854  ( -770 : -772 : 773 : 771 ) 748 -754 749 -750 751
        -752 ( -748 : -766 : -768 : 769 : 767 : 765 )
2131 49 0.074981  ( -736 : -728 : -754 : 972 : 738 : 737 ) ( -753 : 754
        : -755 : 756 : -757 : 758 ) -764 763 -762 761 -760 759 ( -736 :
        -728 : -95 : -117 : 753 : 738 : 737 )
2132 0  -747 753 -773 772 -771 770
2133 0  748 -754 -773 772 -771 770
2134 0  ( -871 : 830 : 952 ) -786 -785 784 789
2135 5 0.0582256  ( -787 : 784 : 793 ) ( -785 : 793 : 788 ) ( -785 :
        775 : 834 ) ( -784 : 785 : 786 ) 783 782 781 780 -779 778 -777
        776 -775 774 ( -774 : 784 : 834 ) ( -871 : -874 : 876 : 872 )
2136 0  ( -881 : 882 ) 774 -784 -833
2137 0  ( -945 : 946 ) 785 -775 -833
2138 5 0.0582256  789 -784 787 -792
2139 5 0.0582256  789 -788 785 -792
2140 33 0.0913215  -789 -788 787
2141 73 0.0844385  -791 788 -790
2142 74 0.0847227  -784 787 -795
2143 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 795
        -794 -816
2144 48 0.117208  -832 831 -830 830 -794 -816
2145 74 0.0847227  -784 787 -797
2146 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 797
        -796 794
2147 48 0.117208  -832 831 -830 830 -796 794
2148 74 0.0847227  -784 787 -799
2149 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 799
        -798 796
2150 48 0.117208  -832 831 -830 830 -798 796
2151 74 0.0847227  -784 787 -801
2152 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 801
        -800 798
2153 48 0.117208  -832 831 -830 830 -800 798
2154 74 0.0847227  -784 787 -803
2155 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 803
        -802 800
2156 48 0.117208  -832 831 -830 830 -802 800
2157 74 0.0847227  -784 787 -805
2158 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 805
        -804 802
2159 48 0.117208  -832 831 -830 830 -804 802
2160 74 0.0847227  -784 787 -807
2161 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 807
        -806 804
2162 48 0.117208  -832 831 -830 830 -806 804
2163 74 0.0847227  -784 787 -809
2164 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 809
        -808 806
2165 48 0.117208  -832 831 -830 830 -808 806
2166 74 0.0847227  -784 787 -811
2167 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 811
        -810 808
2168 48 0.117208  -832 831 -830 830 -810 808
2169 74 0.0847227  -784 787 -813
2170 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 813
        -812 810
2171 48 0.117208  -832 831 -830 830 -812 810
2172 74 0.0847227  -784 787 -815
2173 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 815
        -814 812
2174 48 0.117208  -832 831 -830 830 -814 812
2175 74 0.0847227  -784 787 -817
2176 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 817
        -816 814
2177 48 0.117208  -832 831 -830 830 -816 814
2178 74 0.0847227  -784 787 -818
2179 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 818 794
        816
2180 48 0.117208  -832 831 -830 830 794 816
2181 74 0.0847227  -784 787 -819
2182 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 819 796
        -794
2183 48 0.117208  -832 831 -830 830 796 -794
2184 74 0.0847227  -784 787 -820
2185 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 820 798
        -796
2186 48 0.117208  -832 831 -830 830 798 -796
2187 74 0.0847227  -784 787 -821
2188 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 821 800
        -798
2189 48 0.117208  -832 831 -830 830 800 -798
2190 74 0.0847227  -784 787 -822
2191 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 822 802
        -800
2192 48 0.117208  -832 831 -830 830 802 -800
2193 74 0.0847227  -784 787 -823
2194 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 823 804
        -802
2195 48 0.117208  -832 831 -830 830 804 -802
2196 74 0.0847227  -784 787 -824
2197 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 824 806
        -804
2198 48 0.117208  -832 831 -830 830 806 -804
2199 74 0.0847227  -784 787 -825
2200 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 825 808
        -806
2201 48 0.117208  -832 831 -830 830 808 -806
2202 74 0.0847227  -784 787 -826
2203 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 826 810
        -808
2204 48 0.117208  -832 831 -830 830 810 -808
2205 74 0.0847227  -784 787 -827
2206 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 827 812
        -810
2207 48 0.117208  -832 831 -830 830 812 -810
2208 74 0.0847227  -784 787 -828
2209 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 828 814
        -812
2210 48 0.117208  -832 831 -830 830 814 -812
2211 74 0.0847227  -784 787 -829
2212 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -784 787 829 816
        -814
2213 48 0.117208  -832 831 -830 830 816 -814
2214 74 0.0847227  -788 785 -795
2215 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 795
        -794 -816
2216 48 0.117208  -832 831 -830 830 -794 -816
2217 74 0.0847227  -788 785 -797
2218 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 797
        -796 794
2219 48 0.117208  -832 831 -830 830 -796 794
2220 74 0.0847227  -788 785 -799
2221 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 799
        -798 796
2222 48 0.117208  -832 831 -830 830 -798 796
2223 74 0.0847227  -788 785 -801
2224 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 801
        -800 798
2225 48 0.117208  -832 831 -830 830 -800 798
2226 74 0.0847227  -788 785 -803
2227 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 803
        -802 800
2228 48 0.117208  -832 831 -830 830 -802 800
2229 74 0.0847227  -788 785 -805
2230 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 805
        -804 802
2231 48 0.117208  -832 831 -830 830 -804 802
2232 74 0.0847227  -788 785 -807
2233 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 807
        -806 804
2234 48 0.117208  -832 831 -830 830 -806 804
2235 74 0.0847227  -788 785 -809
2236 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 809
        -808 806
2237 48 0.117208  -832 831 -830 830 -808 806
2238 74 0.0847227  -788 785 -811
2239 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 811
        -810 808
2240 48 0.117208  -832 831 -830 830 -810 808
2241 74 0.0847227  -788 785 -813
2242 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 813
        -812 810
2243 48 0.117208  -832 831 -830 830 -812 810
2244 74 0.0847227  -788 785 -815
2245 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 815
        -814 812
2246 48 0.117208  -832 831 -830 830 -814 812
2247 74 0.0847227  -788 785 -817
2248 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 817
        -816 814
2249 48 0.117208  -832 831 -830 830 -816 814
2250 74 0.0847227  -788 785 -818
2251 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 818 794
        816
2252 48 0.117208  -832 831 -830 830 794 816
2253 74 0.0847227  -788 785 -819
2254 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 819 796
        -794
2255 48 0.117208  -832 831 -830 830 796 -794
2256 74 0.0847227  -788 785 -820
2257 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 820 798
        -796
2258 48 0.117208  -832 831 -830 830 798 -796
2259 74 0.0847227  -788 785 -821
2260 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 821 800
        -798
2261 48 0.117208  -832 831 -830 830 800 -798
2262 74 0.0847227  -788 785 -822
2263 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 822 802
        -800
2264 48 0.117208  -832 831 -830 830 802 -800
2265 74 0.0847227  -788 785 -823
2266 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 823 804
        -802
2267 48 0.117208  -832 831 -830 830 804 -802
2268 74 0.0847227  -788 785 -824
2269 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 824 806
        -804
2270 48 0.117208  -832 831 -830 830 806 -804
2271 74 0.0847227  -788 785 -825
2272 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 825 808
        -806
2273 48 0.117208  -832 831 -830 830 808 -806
2274 74 0.0847227  -788 785 -826
2275 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 826 810
        -808
2276 48 0.117208  -832 831 -830 830 810 -808
2277 74 0.0847227  -788 785 -827
2278 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 827 812
        -810
2279 48 0.117208  -832 831 -830 830 812 -810
2280 74 0.0847227  -788 785 -828
2281 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 828 814
        -812
2282 48 0.117208  -832 831 -830 830 814 -812
2283 74 0.0847227  -788 785 -829
2284 5 0.0582256  ( -830 : -831 : 832 : 830 ) -793 792 -788 785 829 816
        -814
2285 48 0.117208  -832 831 -830 830 816 -814
2286 74 0.0847227  -784 774 -836
2287 5 0.0582256  -834 833 -784 774 836 -835 -857
2288 74 0.0847227  -784 774 -838
2289 5 0.0582256  -834 833 -784 774 838 -837 835
2290 74 0.0847227  -784 774 -840
2291 5 0.0582256  -834 833 -784 774 840 -839 837
2292 74 0.0847227  -784 774 -842
2293 5 0.0582256  -834 833 -784 774 842 -841 839
2294 74 0.0847227  -784 774 -844
2295 5 0.0582256  -834 833 -784 774 844 -843 841
2296 74 0.0847227  -784 774 -846
2297 5 0.0582256  -834 833 -784 774 846 -845 843
2298 74 0.0847227  -784 774 -848
2299 5 0.0582256  -834 833 -784 774 848 -847 845
2300 74 0.0847227  -784 774 -850
2301 5 0.0582256  -834 833 -784 774 850 -849 847
2302 74 0.0847227  -784 774 -852
2303 5 0.0582256  -834 833 -784 774 852 -851 849
2304 74 0.0847227  -784 774 -854
2305 5 0.0582256  -834 833 -784 774 854 -853 851
2306 74 0.0847227  -784 774 -856
2307 5 0.0582256  -834 833 -784 774 856 -855 853
2308 74 0.0847227  -784 774 -858
2309 5 0.0582256  -834 833 -784 774 858 -857 855
2310 74 0.0847227  -784 774 -859
2311 5 0.0582256  -834 833 -784 774 859 835 857
2312 74 0.0847227  -784 774 -860
2313 5 0.0582256  -834 833 -784 774 860 837 -835
2314 74 0.0847227  -784 774 -861
2315 5 0.0582256  -834 833 -784 774 861 839 -837
2316 74 0.0847227  -784 774 -862
2317 5 0.0582256  -834 833 -784 774 862 841 -839
2318 74 0.0847227  -784 774 -863
2319 5 0.0582256  -834 833 -784 774 863 843 -841
2320 74 0.0847227  -784 774 -864
2321 5 0.0582256  -834 833 -784 774 864 845 -843
2322 74 0.0847227  -784 774 -865
2323 5 0.0582256  -834 833 -784 774 865 847 -845
2324 74 0.0847227  -784 774 -866
2325 5 0.0582256  -834 833 -784 774 866 849 -847
2326 74 0.0847227  -784 774 -867
2327 5 0.0582256  -834 833 -784 774 867 851 -849
2328 74 0.0847227  -784 774 -868
2329 5 0.0582256  -834 833 -784 774 868 853 -851
2330 74 0.0847227  -784 774 -869
2331 5 0.0582256  -834 833 -784 774 869 855 -853
2332 74 0.0847227  -784 774 -870
2333 5 0.0582256  -834 833 -784 774 870 857 -855
2334 74 0.0847227  -775 785 -836
2335 5 0.0582256  -834 833 -775 785 836 -835 -857
2336 74 0.0847227  -775 785 -838
2337 5 0.0582256  -834 833 -775 785 838 -837 835
2338 74 0.0847227  -775 785 -840
2339 5 0.0582256  -834 833 -775 785 840 -839 837
2340 74 0.0847227  -775 785 -842
2341 5 0.0582256  -834 833 -775 785 842 -841 839
2342 74 0.0847227  -775 785 -844
2343 5 0.0582256  -834 833 -775 785 844 -843 841
2344 74 0.0847227  -775 785 -846
2345 5 0.0582256  -834 833 -775 785 846 -845 843
2346 74 0.0847227  -775 785 -848
2347 5 0.0582256  -834 833 -775 785 848 -847 845
2348 74 0.0847227  -775 785 -850
2349 5 0.0582256  -834 833 -775 785 850 -849 847
2350 74 0.0847227  -775 785 -852
2351 5 0.0582256  -834 833 -775 785 852 -851 849
2352 74 0.0847227  -775 785 -854
2353 5 0.0582256  -834 833 -775 785 854 -853 851
2354 74 0.0847227  -775 785 -856
2355 5 0.0582256  -834 833 -775 785 856 -855 853
2356 74 0.0847227  -775 785 -858
2357 5 0.0582256  -834 833 -775 785 858 -857 855
2358 74 0.0847227  -775 785 -859
2359 5 0.0582256  -834 833 -775 785 859 835 857
2360 74 0.0847227  -775 785 -860
2361 5 0.0582256  -834 833 -775 785 860 837 -835
2362 74 0.0847227  -775 785 -861
2363 5 0.0582256  -834 833 -775 785 861 839 -837
2364 74 0.0847227  -775 785 -862
2365 5 0.0582256  -834 833 -775 785 862 841 -839
2366 74 0.0847227  -775 785 -863
2367 5 0.0582256  -834 833 -775 785 863 843 -841
2368 74 0.0847227  -775 785 -864
2369 5 0.0582256  -834 833 -775 785 864 845 -843
2370 74 0.0847227  -775 785 -865
2371 5 0.0582256  -834 833 -775 785 865 847 -845
2372 74 0.0847227  -775 785 -866
2373 5 0.0582256  -834 833 -775 785 866 849 -847
2374 74 0.0847227  -775 785 -867
2375 5 0.0582256  -834 833 -775 785 867 851 -849
2376 74 0.0847227  -775 785 -868
2377 5 0.0582256  -834 833 -775 785 868 853 -851
2378 74 0.0847227  -775 785 -869
2379 5 0.0582256  -834 833 -775 785 869 855 -853
2380 74 0.0847227  -775 785 -870
2381 5 0.0582256  -834 833 -775 785 870 857 -855
2382 48 0.117208  -876 874 -872 871 -875 873
2383 48 0.117208  -876 874 -872 871 -877 875
2384 48 0.117208  -876 874 -872 871 -878 877
2385 48 0.117208  -876 874 -872 871 -879 878
2386 48 0.117208  -876 874 -872 871 -880 879
2387 48 0.117208  -876 874 -872 871 873 880
2388 48 0.117208  -876 874 -872 871 875 -873
2389 48 0.117208  -876 874 -872 871 877 -875
2390 48 0.117208  -876 874 -872 871 878 -877
2391 48 0.117208  -876 874 -872 871 879 -878
2392 48 0.117208  -876 874 -872 871 880 -879
2393 48 0.117208  -876 874 -872 871 -873 -880
2394 5 0.0582256  -886 885 -884 883 -900 899
2395 0  ( -899 : 900 ) -886 885 -884 883 -882 881
2396 48 0.117208  -894 893 -887 891 -900 899
2397 48 0.117208  -894 893 -892 888 -900 899
2398 48 0.117208  893 -889 -888 887 -900 899
2399 48 0.117208  -894 890 -888 887 -900 899
2400 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -899
        881
2401 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -882
        900
2402 5 0.0582256  ( -883 : 884 : -885 : 886 ) -890 889 -888 887 -882
        881
2403 5 0.0582256  774 -784 -833 ( -895 : 896 : -897 : 898 ) -882 881
2404 74 0.0847227  -913 -882 881
2405 5 0.0582256  895 -891 774 -784 -833 913 -882 881 -902 897
2406 74 0.0847227  -914 -882 881
2407 5 0.0582256  895 -891 774 -784 -833 914 -882 881 -904 902
2408 74 0.0847227  -915 -882 881
2409 5 0.0582256  895 -891 774 -784 -833 915 -882 881 -906 904
2410 74 0.0847227  -916 -882 881
2411 5 0.0582256  895 -891 774 -784 -833 916 -882 881 845 906
2412 74 0.0847227  -917 -882 881
2413 5 0.0582256  895 -891 774 -784 -833 917 -882 881 -908 -845
2414 74 0.0847227  -918 -882 881
2415 5 0.0582256  895 -891 774 -784 -833 918 -882 881 -910 908
2416 74 0.0847227  -919 -882 881
2417 5 0.0582256  895 -891 774 -784 -833 919 -882 881 -912 910
2418 74 0.0847227  -920 -882 881
2419 5 0.0582256  895 -891 774 -784 -833 920 -882 881 -898 912
2420 74 0.0847227  -921 -882 881
2421 5 0.0582256  -898 894 774 -784 -833 921 -882 881 -901 891
2422 74 0.0847227  -922 -882 881
2423 5 0.0582256  -898 894 774 -784 -833 922 -882 881 -903 901
2424 74 0.0847227  -923 -882 881
2425 5 0.0582256  -898 894 774 -784 -833 923 -882 881 -905 903
2426 74 0.0847227  -924 -882 881
2427 5 0.0582256  -898 894 774 -784 -833 924 -882 881 857 905
2428 74 0.0847227  -925 -882 881
2429 5 0.0582256  -898 894 774 -784 -833 925 -882 881 -907 -857
2430 74 0.0847227  -926 -882 881
2431 5 0.0582256  -898 894 774 -784 -833 926 -882 881 -909 907
2432 74 0.0847227  -927 -882 881
2433 5 0.0582256  -898 894 774 -784 -833 927 -882 881 -911 909
2434 74 0.0847227  -928 -882 881
2435 5 0.0582256  -898 894 774 -784 -833 928 -882 881 -892 911
2436 74 0.0847227  -929 -882 881
2437 5 0.0582256  -896 892 774 -784 -833 929 -882 881 912 -898
2438 74 0.0847227  -930 -882 881
2439 5 0.0582256  -896 892 774 -784 -833 930 -882 881 910 -912
2440 74 0.0847227  -931 -882 881
2441 5 0.0582256  -896 892 774 -784 -833 931 -882 881 908 -910
2442 74 0.0847227  -932 -882 881
2443 5 0.0582256  -896 892 774 -784 -833 932 -882 881 -845 -908
2444 74 0.0847227  -933 -882 881
2445 5 0.0582256  -896 892 774 -784 -833 933 -882 881 906 845
2446 74 0.0847227  -934 -882 881
2447 5 0.0582256  -896 892 774 -784 -833 934 -882 881 904 -906
2448 74 0.0847227  -935 -882 881
2449 5 0.0582256  -896 892 774 -784 -833 935 -882 881 902 -904
2450 74 0.0847227  -936 -882 881
2451 5 0.0582256  -896 892 774 -784 -833 936 -882 881 897 -902
2452 74 0.0847227  -937 -882 881
2453 5 0.0582256  897 -893 774 -784 -833 937 -882 881 911 -892
2454 74 0.0847227  -938 -882 881
2455 5 0.0582256  897 -893 774 -784 -833 938 -882 881 909 -911
2456 74 0.0847227  -939 -882 881
2457 5 0.0582256  897 -893 774 -784 -833 939 -882 881 907 -909
2458 74 0.0847227  -940 -882 881
2459 5 0.0582256  897 -893 774 -784 -833 940 -882 881 -857 -907
2460 74 0.0847227  -941 -882 881
2461 5 0.0582256  897 -893 774 -784 -833 941 -882 881 905 857
2462 74 0.0847227  -942 -882 881
2463 5 0.0582256  897 -893 774 -784 -833 942 -882 881 903 -905
2464 74 0.0847227  -943 -882 881
2465 5 0.0582256  897 -893 774 -784 -833 943 -882 881 901 -903
2466 74 0.0847227  -944 -882 881
2467 5 0.0582256  897 -893 774 -784 -833 944 -882 881 891 -901
2468 5 0.0582256  -886 885 -884 883 -948 947
2469 0  ( -947 : 948 ) -886 885 -884 883 -946 945
2470 48 0.117208  -894 893 -887 891 -948 947
2471 48 0.117208  -894 893 -892 888 -948 947
2472 48 0.117208  893 -889 -888 887 -948 947
2473 48 0.117208  -894 890 -888 887 -948 947
2474 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -947
        945
2475 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -946
        948
2476 5 0.0582256  ( -883 : 884 : -885 : 886 ) -890 889 -888 887 -946
        945
2477 5 0.0582256  785 -775 -833 ( -895 : 896 : -897 : 898 ) -946 945
2478 74 0.0847227  -913 -946 945
2479 5 0.0582256  895 -891 785 -775 -833 913 -946 945 -902 897
2480 74 0.0847227  -914 -946 945
2481 5 0.0582256  895 -891 785 -775 -833 914 -946 945 -904 902
2482 74 0.0847227  -915 -946 945
2483 5 0.0582256  895 -891 785 -775 -833 915 -946 945 -906 904
2484 74 0.0847227  -916 -946 945
2485 5 0.0582256  895 -891 785 -775 -833 916 -946 945 845 906
2486 74 0.0847227  -917 -946 945
2487 5 0.0582256  895 -891 785 -775 -833 917 -946 945 -908 -845
2488 74 0.0847227  -918 -946 945
2489 5 0.0582256  895 -891 785 -775 -833 918 -946 945 -910 908
2490 74 0.0847227  -919 -946 945
2491 5 0.0582256  895 -891 785 -775 -833 919 -946 945 -912 910
2492 74 0.0847227  -920 -946 945
2493 5 0.0582256  895 -891 785 -775 -833 920 -946 945 -898 912
2494 74 0.0847227  -921 -946 945
2495 5 0.0582256  -898 894 785 -775 -833 921 -946 945 -901 891
2496 74 0.0847227  -922 -946 945
2497 5 0.0582256  -898 894 785 -775 -833 922 -946 945 -903 901
2498 74 0.0847227  -923 -946 945
2499 5 0.0582256  -898 894 785 -775 -833 923 -946 945 -905 903
2500 74 0.0847227  -924 -946 945
2501 5 0.0582256  -898 894 785 -775 -833 924 -946 945 857 905
2502 74 0.0847227  -925 -946 945
2503 5 0.0582256  -898 894 785 -775 -833 925 -946 945 -907 -857
2504 74 0.0847227  -926 -946 945
2505 5 0.0582256  -898 894 785 -775 -833 926 -946 945 -909 907
2506 74 0.0847227  -927 -946 945
2507 5 0.0582256  -898 894 785 -775 -833 927 -946 945 -911 909
2508 74 0.0847227  -928 -946 945
2509 5 0.0582256  -898 894 785 -775 -833 928 -946 945 -892 911
2510 74 0.0847227  -929 -946 945
2511 5 0.0582256  -896 892 785 -775 -833 929 -946 945 912 -898
2512 74 0.0847227  -930 -946 945
2513 5 0.0582256  -896 892 785 -775 -833 930 -946 945 910 -912
2514 74 0.0847227  -931 -946 945
2515 5 0.0582256  -896 892 785 -775 -833 931 -946 945 908 -910
2516 74 0.0847227  -932 -946 945
2517 5 0.0582256  -896 892 785 -775 -833 932 -946 945 -845 -908
2518 74 0.0847227  -933 -946 945
2519 5 0.0582256  -896 892 785 -775 -833 933 -946 945 906 845
2520 74 0.0847227  -934 -946 945
2521 5 0.0582256  -896 892 785 -775 -833 934 -946 945 904 -906
2522 74 0.0847227  -935 -946 945
2523 5 0.0582256  -896 892 785 -775 -833 935 -946 945 902 -904
2524 74 0.0847227  -936 -946 945
2525 5 0.0582256  -896 892 785 -775 -833 936 -946 945 897 -902
2526 74 0.0847227  -937 -946 945
2527 5 0.0582256  897 -893 785 -775 -833 937 -946 945 911 -892
2528 74 0.0847227  -938 -946 945
2529 5 0.0582256  897 -893 785 -775 -833 938 -946 945 909 -911
2530 74 0.0847227  -939 -946 945
2531 5 0.0582256  897 -893 785 -775 -833 939 -946 945 907 -909
2532 74 0.0847227  -940 -946 945
2533 5 0.0582256  897 -893 785 -775 -833 940 -946 945 -857 -907
2534 74 0.0847227  -941 -946 945
2535 5 0.0582256  897 -893 785 -775 -833 941 -946 945 905 857
2536 74 0.0847227  -942 -946 945
2537 5 0.0582256  897 -893 785 -775 -833 942 -946 945 903 -905
2538 74 0.0847227  -943 -946 945
2539 5 0.0582256  897 -893 785 -775 -833 943 -946 945 901 -903
2540 74 0.0847227  -944 -946 945
2541 5 0.0582256  897 -893 785 -775 -833 944 -946 945 891 -901
2542 73 0.0844385  789 871 -830 -951
2543 0  -952 951 -830 871 -950 949
2544 47 0.136566  -952 951 -830 871 ( 950 : -949 )
2545 0  ( -962 : 963 : 680 ) -972 754 -715 714 -713 712 ( -739 : 740 :
        680 )
2546 47 0.136566  -953 754 -715 714 -712 766
2547 47 0.136566  -953 754 -715 714 -767 713
2548 71 0.0843223  -953 754 -715 714 -766 718
2549 71 0.0843223  -953 754 -715 714 -719 767
2550 71 0.0843223  -953 754 -715 714 -718 722
2551 71 0.0843223  -953 754 -715 714 -723 719
2552 49 0.074981  -953 754 -715 714 -722 956
2553 49 0.074981  -953 754 -715 714 -957 723
2554 49 0.074981  -953 754 -715 714 -956 958
2555 49 0.074981  -953 754 -715 714 -959 957
2556 49 0.074981  -953 754 -715 714 -958 730
2557 49 0.074981  -953 754 -715 714 -731 959
2558 49 0.074981  -953 754 -715 714 -730 960
2559 49 0.074981  -953 754 -715 714 -961 731
2560 49 0.074981  -953 754 -715 714 -960 749
2561 49 0.074981  -953 754 -715 714 -750 961
2562 49 0.074981  -953 754 -715 714 -749 736
2563 49 0.074981  -953 754 -715 714 -737 750
2564 47 0.136566  -953 754 715 -717 -713 712
2565 71 0.0843223  -953 754 717 -721 -713 712
2566 71 0.0843223  -953 754 721 -725 -713 712
2567 49 0.074981  -953 754 725 -729 -713 712
2568 49 0.074981  -953 754 729 -732 -713 712
2569 49 0.074981  -953 754 732 -735 -713 712
2570 49 0.074981  -953 754 735 -738 -713 712
2571 47 0.136566  -953 754 716 -714 -737 736
2572 71 0.0843223  -953 754 720 -716 -737 736
2573 71 0.0843223  -953 754 724 -720 -737 736
2574 49 0.074981  -953 754 728 -724 -737 736
2575 47 0.136566  -953 754 -717 715 766 -712
2576 47 0.136566  -953 754 -717 715 -767 713
2577 71 0.0843223  -953 754 -717 715 718 -766
2578 71 0.0843223  -953 754 -717 715 -719 767
2579 71 0.0843223  -953 754 -717 715 722 -718
2580 71 0.0843223  -953 754 -717 715 -723 719
2581 49 0.074981  -953 754 -717 715 956 -722
2582 49 0.074981  -953 754 -717 715 -957 723
2583 49 0.074981  -953 754 -717 715 958 -956
2584 49 0.074981  -953 754 -717 715 -959 957
2585 49 0.074981  -953 754 -717 715 730 -958
2586 49 0.074981  -953 754 -717 715 -731 959
2587 49 0.074981  -953 754 -717 715 960 -730
2588 49 0.074981  -953 754 -717 715 -961 731
2589 49 0.074981  -953 754 -717 715 749 -960
2590 49 0.074981  -953 754 -717 715 -750 961
2591 49 0.074981  -953 754 -717 715 736 -749
2592 49 0.074981  -953 754 -717 715 -737 750
2593 71 0.0843223  -953 754 -721 717 766 -712
2594 71 0.0843223  -953 754 -721 717 -767 713
2595 71 0.0843223  -953 754 -721 717 718 -766
2596 71 0.0843223  -953 754 -721 717 -719 767
2597 71 0.0843223  -953 754 -721 717 722 -718
2598 71 0.0843223  -953 754 -721 717 -723 719
2599 49 0.074981  -953 754 -721 717 956 -722
2600 49 0.074981  -953 754 -721 717 -957 723
2601 49 0.074981  -953 754 -721 717 958 -956
2602 49 0.074981  -953 754 -721 717 -959 957
2603 49 0.074981  -953 754 -721 717 730 -958
2604 49 0.074981  -953 754 -721 717 -731 959
2605 49 0.074981  -953 754 -721 717 960 -730
2606 49 0.074981  -953 754 -721 717 -961 731
2607 49 0.074981  -953 754 -721 717 749 -960
2608 49 0.074981  -953 754 -721 717 -750 961
2609 49 0.074981  -953 754 -721 717 736 -749
2610 49 0.074981  -953 754 -721 717 -737 750
2611 71 0.0843223  -953 754 -725 721 766 -712
2612 71 0.0843223  -953 754 -725 721 -767 713
2613 71 0.0843223  -953 754 -725 721 718 -766
2614 71 0.0843223  -953 754 -725 721 -719 767
2615 71 0.0843223  -953 754 -725 721 722 -718
2616 71 0.0843223  -953 754 -725 721 -723 719
2617 49 0.074981  -953 754 -725 721 956 -722
2618 49 0.074981  -953 754 -725 721 -957 723
2619 49 0.074981  -953 754 -725 721 958 -956
2620 49 0.074981  -953 754 -725 721 -959 957
2621 49 0.074981  -953 754 -725 721 730 -958
2622 49 0.074981  -953 754 -725 721 -731 959
2623 49 0.074981  -953 754 -725 721 960 -730
2624 49 0.074981  -953 754 -725 721 -961 731
2625 49 0.074981  -953 754 -725 721 749 -960
2626 49 0.074981  -953 754 -725 721 -750 961
2627 49 0.074981  -953 754 -725 721 736 -749
2628 49 0.074981  -953 754 -725 721 -737 750
2629 49 0.074981  -953 754 -729 725 766 -712
2630 49 0.074981  -953 754 -729 725 -767 713
2631 49 0.074981  -953 754 -729 725 718 -766
2632 49 0.074981  -953 754 -729 725 -719 767
2633 49 0.074981  -953 754 -729 725 722 -718
2634 49 0.074981  -953 754 -729 725 -723 719
2635 49 0.074981  -953 754 -729 725 956 -722
2636 49 0.074981  -953 754 -729 725 -957 723
2637 49 0.074981  -953 754 -729 725 958 -956
2638 49 0.074981  -953 754 -729 725 -959 957
2639 49 0.074981  -953 754 -729 725 730 -958
2640 49 0.074981  -953 754 -729 725 -731 959
2641 49 0.074981  -953 754 -729 725 960 -730
2642 49 0.074981  -953 754 -729 725 -961 731
2643 49 0.074981  -953 754 -729 725 749 -960
2644 49 0.074981  -953 754 -729 725 -750 961
2645 49 0.074981  -953 754 -729 725 736 -749
2646 49 0.074981  -953 754 -729 725 -737 750
2647 49 0.074981  -953 754 -732 729 766 -712
2648 49 0.074981  -953 754 -732 729 -767 713
2649 49 0.074981  -953 754 -732 729 718 -766
2650 49 0.074981  -953 754 -732 729 -719 767
2651 49 0.074981  -953 754 -732 729 722 -718
2652 49 0.074981  -953 754 -732 729 -723 719
2653 49 0.074981  -953 754 -732 729 956 -722
2654 49 0.074981  -953 754 -732 729 -957 723
2655 49 0.074981  -953 754 -732 729 958 -956
2656 49 0.074981  -953 754 -732 729 -959 957
2657 49 0.074981  -953 754 -732 729 730 -958
2658 49 0.074981  -953 754 -732 729 -731 959
2659 49 0.074981  -953 754 -732 729 960 -730
2660 49 0.074981  -953 754 -732 729 -961 731
2661 49 0.074981  -953 754 -732 729 749 -960
2662 49 0.074981  -953 754 -732 729 -750 961
2663 49 0.074981  -953 754 -732 729 736 -749
2664 49 0.074981  -953 754 -732 729 -737 750
2665 49 0.074981  -953 754 -735 732 766 -712
2666 49 0.074981  -953 754 -735 732 -767 713
2667 49 0.074981  -953 754 -735 732 718 -766
2668 49 0.074981  -953 754 -735 732 -719 767
2669 49 0.074981  -953 754 -735 732 722 -718
2670 49 0.074981  -953 754 -735 732 -723 719
2671 49 0.074981  -953 754 -735 732 956 -722
2672 49 0.074981  -953 754 -735 732 -957 723
2673 49 0.074981  -953 754 -735 732 958 -956
2674 49 0.074981  -953 754 -735 732 -959 957
2675 49 0.074981  -953 754 -735 732 730 -958
2676 49 0.074981  -953 754 -735 732 -731 959
2677 49 0.074981  -953 754 -735 732 960 -730
2678 49 0.074981  -953 754 -735 732 -961 731
2679 49 0.074981  -953 754 -735 732 749 -960
2680 49 0.074981  -953 754 -735 732 -750 961
2681 49 0.074981  -953 754 -735 732 736 -749
2682 49 0.074981  -953 754 -735 732 -737 750
2683 49 0.074981  -953 754 -738 735 766 -712
2684 49 0.074981  -953 754 -738 735 -767 713
2685 49 0.074981  -953 754 -738 735 718 -766
2686 49 0.074981  -953 754 -738 735 -719 767
2687 49 0.074981  -953 754 -738 735 722 -718
2688 49 0.074981  -953 754 -738 735 -723 719
2689 49 0.074981  -953 754 -738 735 956 -722
2690 49 0.074981  -953 754 -738 735 -957 723
2691 49 0.074981  -953 754 -738 735 958 -956
2692 49 0.074981  -953 754 -738 735 -959 957
2693 49 0.074981  -953 754 -738 735 730 -958
2694 49 0.074981  -953 754 -738 735 -731 959
2695 49 0.074981  -953 754 -738 735 960 -730
2696 49 0.074981  -953 754 -738 735 -961 731
2697 49 0.074981  -953 754 -738 735 749 -960
2698 49 0.074981  -953 754 -738 735 -750 961
2699 49 0.074981  -953 754 -738 735 736 -749
2700 49 0.074981  -953 754 -738 735 -737 750
2701 47 0.136566  -954 953 -715 714 -712 766
2702 47 0.136566  -954 953 -715 714 -767 713
2703 71 0.0843223  -954 953 -715 714 -766 718
2704 71 0.0843223  -954 953 -715 714 -719 767
2705 71 0.0843223  -954 953 -715 714 -718 722
2706 71 0.0843223  -954 953 -715 714 -723 719
2707 49 0.074981  -954 953 -715 714 -722 956
2708 49 0.074981  -954 953 -715 714 -957 723
2709 49 0.074981  -954 953 -715 714 -956 958
2710 49 0.074981  -954 953 -715 714 -959 957
2711 49 0.074981  -954 953 -715 714 -958 730
2712 49 0.074981  -954 953 -715 714 -731 959
2713 49 0.074981  -954 953 -715 714 -730 960
2714 49 0.074981  -954 953 -715 714 -961 731
2715 49 0.074981  -954 953 -715 714 -960 749
2716 49 0.074981  -954 953 -715 714 -750 961
2717 49 0.074981  -954 953 -715 714 -749 736
2718 49 0.074981  -954 953 -715 714 -737 750
2719 47 0.136566  -954 953 715 -717 -713 712
2720 71 0.0843223  -954 953 717 -721 -713 712
2721 71 0.0843223  -954 953 721 -725 -713 712
2722 49 0.074981  -954 953 725 -729 -713 712
2723 49 0.074981  -954 953 729 -732 -713 712
2724 49 0.074981  -954 953 732 -735 -713 712
2725 49 0.074981  -954 953 735 -738 -713 712
2726 47 0.136566  -954 953 716 -714 -737 736
2727 71 0.0843223  -954 953 720 -716 -737 736
2728 71 0.0843223  -954 953 724 -720 -737 736
2729 49 0.074981  -954 953 728 -724 -737 736
2730 47 0.136566  -954 953 -717 715 766 -712
2731 47 0.136566  -954 953 -717 715 -767 713
2732 71 0.0843223  -954 953 -717 715 718 -766
2733 71 0.0843223  -954 953 -717 715 -719 767
2734 71 0.0843223  -954 953 -717 715 722 -718
2735 71 0.0843223  -954 953 -717 715 -723 719
2736 49 0.074981  -954 953 -717 715 956 -722
2737 49 0.074981  -954 953 -717 715 -957 723
2738 49 0.074981  -954 953 -717 715 958 -956
2739 49 0.074981  -954 953 -717 715 -959 957
2740 49 0.074981  -954 953 -717 715 730 -958
2741 49 0.074981  -954 953 -717 715 -731 959
2742 49 0.074981  -954 953 -717 715 960 -730
2743 49 0.074981  -954 953 -717 715 -961 731
2744 49 0.074981  -954 953 -717 715 749 -960
2745 49 0.074981  -954 953 -717 715 -750 961
2746 49 0.074981  -954 953 -717 715 736 -749
2747 49 0.074981  -954 953 -717 715 -737 750
2748 71 0.0843223  -954 953 -721 717 766 -712
2749 71 0.0843223  -954 953 -721 717 -767 713
2750 71 0.0843223  -954 953 -721 717 718 -766
2751 71 0.0843223  -954 953 -721 717 -719 767
2752 71 0.0843223  -954 953 -721 717 722 -718
2753 71 0.0843223  -954 953 -721 717 -723 719
2754 49 0.074981  -954 953 -721 717 956 -722
2755 49 0.074981  -954 953 -721 717 -957 723
2756 49 0.074981  -954 953 -721 717 958 -956
2757 49 0.074981  -954 953 -721 717 -959 957
2758 49 0.074981  -954 953 -721 717 730 -958
2759 49 0.074981  -954 953 -721 717 -731 959
2760 49 0.074981  -954 953 -721 717 960 -730
2761 49 0.074981  -954 953 -721 717 -961 731
2762 49 0.074981  -954 953 -721 717 749 -960
2763 49 0.074981  -954 953 -721 717 -750 961
2764 49 0.074981  -954 953 -721 717 736 -749
2765 49 0.074981  -954 953 -721 717 -737 750
2766 71 0.0843223  -954 953 -725 721 766 -712
2767 71 0.0843223  -954 953 -725 721 -767 713
2768 71 0.0843223  -954 953 -725 721 718 -766
2769 71 0.0843223  -954 953 -725 721 -719 767
2770 71 0.0843223  -954 953 -725 721 722 -718
2771 71 0.0843223  -954 953 -725 721 -723 719
2772 49 0.074981  -954 953 -725 721 956 -722
2773 49 0.074981  -954 953 -725 721 -957 723
2774 49 0.074981  -954 953 -725 721 958 -956
2775 49 0.074981  -954 953 -725 721 -959 957
2776 49 0.074981  -954 953 -725 721 730 -958
2777 49 0.074981  -954 953 -725 721 -731 959
2778 49 0.074981  -954 953 -725 721 960 -730
2779 49 0.074981  -954 953 -725 721 -961 731
2780 49 0.074981  -954 953 -725 721 749 -960
2781 49 0.074981  -954 953 -725 721 -750 961
2782 49 0.074981  -954 953 -725 721 736 -749
2783 49 0.074981  -954 953 -725 721 -737 750
2784 49 0.074981  -954 953 -729 725 766 -712
2785 49 0.074981  -954 953 -729 725 -767 713
2786 49 0.074981  -954 953 -729 725 718 -766
2787 49 0.074981  -954 953 -729 725 -719 767
2788 49 0.074981  -954 953 -729 725 722 -718
2789 49 0.074981  -954 953 -729 725 -723 719
2790 49 0.074981  -954 953 -729 725 956 -722
2791 49 0.074981  -954 953 -729 725 -957 723
2792 49 0.074981  -954 953 -729 725 958 -956
2793 49 0.074981  -954 953 -729 725 -959 957
2794 49 0.074981  -954 953 -729 725 730 -958
2795 49 0.074981  -954 953 -729 725 -731 959
2796 49 0.074981  -954 953 -729 725 960 -730
2797 49 0.074981  -954 953 -729 725 -961 731
2798 49 0.074981  -954 953 -729 725 749 -960
2799 49 0.074981  -954 953 -729 725 -750 961
2800 49 0.074981  -954 953 -729 725 736 -749
2801 49 0.074981  -954 953 -729 725 -737 750
2802 49 0.074981  -954 953 -732 729 766 -712
2803 49 0.074981  -954 953 -732 729 -767 713
2804 49 0.074981  -954 953 -732 729 718 -766
2805 49 0.074981  -954 953 -732 729 -719 767
2806 49 0.074981  -954 953 -732 729 722 -718
2807 49 0.074981  -954 953 -732 729 -723 719
2808 49 0.074981  -954 953 -732 729 956 -722
2809 49 0.074981  -954 953 -732 729 -957 723
2810 49 0.074981  -954 953 -732 729 958 -956
2811 49 0.074981  -954 953 -732 729 -959 957
2812 49 0.074981  -954 953 -732 729 730 -958
2813 49 0.074981  -954 953 -732 729 -731 959
2814 49 0.074981  -954 953 -732 729 960 -730
2815 49 0.074981  -954 953 -732 729 -961 731
2816 49 0.074981  -954 953 -732 729 749 -960
2817 49 0.074981  -954 953 -732 729 -750 961
2818 49 0.074981  -954 953 -732 729 736 -749
2819 49 0.074981  -954 953 -732 729 -737 750
2820 49 0.074981  -954 953 -735 732 766 -712
2821 49 0.074981  -954 953 -735 732 -767 713
2822 49 0.074981  -954 953 -735 732 718 -766
2823 49 0.074981  -954 953 -735 732 -719 767
2824 49 0.074981  -954 953 -735 732 722 -718
2825 49 0.074981  -954 953 -735 732 -723 719
2826 49 0.074981  -954 953 -735 732 956 -722
2827 49 0.074981  -954 953 -735 732 -957 723
2828 49 0.074981  -954 953 -735 732 958 -956
2829 49 0.074981  -954 953 -735 732 -959 957
2830 49 0.074981  -954 953 -735 732 730 -958
2831 49 0.074981  -954 953 -735 732 -731 959
2832 49 0.074981  -954 953 -735 732 960 -730
2833 49 0.074981  -954 953 -735 732 -961 731
2834 49 0.074981  -954 953 -735 732 749 -960
2835 49 0.074981  -954 953 -735 732 -750 961
2836 49 0.074981  -954 953 -735 732 736 -749
2837 49 0.074981  -954 953 -735 732 -737 750
2838 49 0.074981  -954 953 -738 735 766 -712
2839 49 0.074981  -954 953 -738 735 -767 713
2840 49 0.074981  -954 953 -738 735 718 -766
2841 49 0.074981  -954 953 -738 735 -719 767
2842 49 0.074981  -954 953 -738 735 722 -718
2843 49 0.074981  -954 953 -738 735 -723 719
2844 49 0.074981  -954 953 -738 735 956 -722
2845 49 0.074981  -954 953 -738 735 -957 723
2846 49 0.074981  -954 953 -738 735 958 -956
2847 49 0.074981  -954 953 -738 735 -959 957
2848 49 0.074981  -954 953 -738 735 730 -958
2849 49 0.074981  -954 953 -738 735 -731 959
2850 49 0.074981  -954 953 -738 735 960 -730
2851 49 0.074981  -954 953 -738 735 -961 731
2852 49 0.074981  -954 953 -738 735 749 -960
2853 49 0.074981  -954 953 -738 735 -750 961
2854 49 0.074981  -954 953 -738 735 736 -749
2855 49 0.074981  -954 953 -738 735 -737 750
2856 47 0.136566  -955 954 -715 714 -712 766
2857 47 0.136566  -955 954 -715 714 -767 713
2858 71 0.0843223  -955 954 -715 714 -766 718
2859 71 0.0843223  -955 954 -715 714 -719 767
2860 71 0.0843223  -955 954 -715 714 -718 722
2861 71 0.0843223  -955 954 -715 714 -723 719
2862 49 0.074981  -955 954 -715 714 -722 956
2863 49 0.074981  -955 954 -715 714 -957 723
2864 49 0.074981  -955 954 -715 714 -956 958
2865 49 0.074981  -955 954 -715 714 -959 957
2866 49 0.074981  -955 954 -715 714 -958 730
2867 49 0.074981  -955 954 -715 714 -731 959
2868 49 0.074981  -955 954 -715 714 -730 960
2869 49 0.074981  -955 954 -715 714 -961 731
2870 49 0.074981  -955 954 -715 714 -960 749
2871 49 0.074981  -955 954 -715 714 -750 961
2872 49 0.074981  -955 954 -715 714 -749 736
2873 49 0.074981  -955 954 -715 714 -737 750
2874 47 0.136566  -955 954 715 -717 -713 712
2875 71 0.0843223  -955 954 717 -721 -713 712
2876 71 0.0843223  -955 954 721 -725 -713 712
2877 49 0.074981  -955 954 725 -729 -713 712
2878 49 0.074981  -955 954 729 -732 -713 712
2879 49 0.074981  -955 954 732 -735 -713 712
2880 49 0.074981  -955 954 735 -738 -713 712
2881 47 0.136566  -955 954 716 -714 -737 736
2882 71 0.0843223  -955 954 720 -716 -737 736
2883 71 0.0843223  -955 954 724 -720 -737 736
2884 49 0.074981  -955 954 728 -724 -737 736
2885 47 0.136566  -955 954 -717 715 766 -712
2886 47 0.136566  -955 954 -717 715 -767 713
2887 71 0.0843223  -955 954 -717 715 718 -766
2888 71 0.0843223  -955 954 -717 715 -719 767
2889 71 0.0843223  -955 954 -717 715 722 -718
2890 71 0.0843223  -955 954 -717 715 -723 719
2891 49 0.074981  -955 954 -717 715 956 -722
2892 49 0.074981  -955 954 -717 715 -957 723
2893 49 0.074981  -955 954 -717 715 958 -956
2894 49 0.074981  -955 954 -717 715 -959 957
2895 49 0.074981  -955 954 -717 715 730 -958
2896 49 0.074981  -955 954 -717 715 -731 959
2897 49 0.074981  -955 954 -717 715 960 -730
2898 49 0.074981  -955 954 -717 715 -961 731
2899 49 0.074981  -955 954 -717 715 749 -960
2900 49 0.074981  -955 954 -717 715 -750 961
2901 49 0.074981  -955 954 -717 715 736 -749
2902 49 0.074981  -955 954 -717 715 -737 750
2903 71 0.0843223  -955 954 -721 717 766 -712
2904 71 0.0843223  -955 954 -721 717 -767 713
2905 71 0.0843223  -955 954 -721 717 718 -766
2906 71 0.0843223  -955 954 -721 717 -719 767
2907 71 0.0843223  -955 954 -721 717 722 -718
2908 71 0.0843223  -955 954 -721 717 -723 719
2909 49 0.074981  -955 954 -721 717 956 -722
2910 49 0.074981  -955 954 -721 717 -957 723
2911 49 0.074981  -955 954 -721 717 958 -956
2912 49 0.074981  -955 954 -721 717 -959 957
2913 49 0.074981  -955 954 -721 717 730 -958
2914 49 0.074981  -955 954 -721 717 -731 959
2915 49 0.074981  -955 954 -721 717 960 -730
2916 49 0.074981  -955 954 -721 717 -961 731
2917 49 0.074981  -955 954 -721 717 749 -960
2918 49 0.074981  -955 954 -721 717 -750 961
2919 49 0.074981  -955 954 -721 717 736 -749
2920 49 0.074981  -955 954 -721 717 -737 750
2921 71 0.0843223  -955 954 -725 721 766 -712
2922 71 0.0843223  -955 954 -725 721 -767 713
2923 71 0.0843223  -955 954 -725 721 718 -766
2924 71 0.0843223  -955 954 -725 721 -719 767
2925 71 0.0843223  -955 954 -725 721 722 -718
2926 71 0.0843223  -955 954 -725 721 -723 719
2927 49 0.074981  -955 954 -725 721 956 -722
2928 49 0.074981  -955 954 -725 721 -957 723
2929 49 0.074981  -955 954 -725 721 958 -956
2930 49 0.074981  -955 954 -725 721 -959 957
2931 49 0.074981  -955 954 -725 721 730 -958
2932 49 0.074981  -955 954 -725 721 -731 959
2933 49 0.074981  -955 954 -725 721 960 -730
2934 49 0.074981  -955 954 -725 721 -961 731
2935 49 0.074981  -955 954 -725 721 749 -960
2936 49 0.074981  -955 954 -725 721 -750 961
2937 49 0.074981  -955 954 -725 721 736 -749
2938 49 0.074981  -955 954 -725 721 -737 750
2939 49 0.074981  -955 954 -729 725 766 -712
2940 49 0.074981  -955 954 -729 725 -767 713
2941 49 0.074981  -955 954 -729 725 718 -766
2942 49 0.074981  -955 954 -729 725 -719 767
2943 49 0.074981  -955 954 -729 725 722 -718
2944 49 0.074981  -955 954 -729 725 -723 719
2945 49 0.074981  -955 954 -729 725 956 -722
2946 49 0.074981  -955 954 -729 725 -957 723
2947 49 0.074981  -955 954 -729 725 958 -956
2948 49 0.074981  -955 954 -729 725 -959 957
2949 49 0.074981  -955 954 -729 725 730 -958
2950 49 0.074981  -955 954 -729 725 -731 959
2951 49 0.074981  -955 954 -729 725 960 -730
2952 49 0.074981  -955 954 -729 725 -961 731
2953 49 0.074981  -955 954 -729 725 749 -960
2954 49 0.074981  -955 954 -729 725 -750 961
2955 49 0.074981  -955 954 -729 725 736 -749
2956 49 0.074981  -955 954 -729 725 -737 750
2957 49 0.074981  -955 954 -732 729 766 -712
2958 49 0.074981  -955 954 -732 729 -767 713
2959 49 0.074981  -955 954 -732 729 718 -766
2960 49 0.074981  -955 954 -732 729 -719 767
2961 49 0.074981  -955 954 -732 729 722 -718
2962 49 0.074981  -955 954 -732 729 -723 719
2963 49 0.074981  -955 954 -732 729 956 -722
2964 49 0.074981  -955 954 -732 729 -957 723
2965 49 0.074981  -955 954 -732 729 958 -956
2966 49 0.074981  -955 954 -732 729 -959 957
2967 49 0.074981  -955 954 -732 729 730 -958
2968 49 0.074981  -955 954 -732 729 -731 959
2969 49 0.074981  -955 954 -732 729 960 -730
2970 49 0.074981  -955 954 -732 729 -961 731
2971 49 0.074981  -955 954 -732 729 749 -960
2972 49 0.074981  -955 954 -732 729 -750 961
2973 49 0.074981  -955 954 -732 729 736 -749
2974 49 0.074981  -955 954 -732 729 -737 750
2975 49 0.074981  -955 954 -735 732 766 -712
2976 49 0.074981  -955 954 -735 732 -767 713
2977 49 0.074981  -955 954 -735 732 718 -766
2978 49 0.074981  -955 954 -735 732 -719 767
2979 49 0.074981  -955 954 -735 732 722 -718
2980 49 0.074981  -955 954 -735 732 -723 719
2981 49 0.074981  -955 954 -735 732 956 -722
2982 49 0.074981  -955 954 -735 732 -957 723
2983 49 0.074981  -955 954 -735 732 958 -956
2984 49 0.074981  -955 954 -735 732 -959 957
2985 49 0.074981  -955 954 -735 732 730 -958
2986 49 0.074981  -955 954 -735 732 -731 959
2987 49 0.074981  -955 954 -735 732 960 -730
2988 49 0.074981  -955 954 -735 732 -961 731
2989 49 0.074981  -955 954 -735 732 749 -960
2990 49 0.074981  -955 954 -735 732 -750 961
2991 49 0.074981  -955 954 -735 732 736 -749
2992 49 0.074981  -955 954 -735 732 -737 750
2993 49 0.074981  -955 954 -738 735 766 -712
2994 49 0.074981  -955 954 -738 735 -767 713
2995 49 0.074981  -955 954 -738 735 718 -766
2996 49 0.074981  -955 954 -738 735 -719 767
2997 49 0.074981  -955 954 -738 735 722 -718
2998 49 0.074981  -955 954 -738 735 -723 719
2999 49 0.074981  -955 954 -738 735 956 -722
3000 49 0.074981  -955 954 -738 735 -957 723
3001 49 0.074981  -955 954 -738 735 958 -956
3002 49 0.074981  -955 954 -738 735 -959 957
3003 49 0.074981  -955 954 -738 735 730 -958
3004 49 0.074981  -955 954 -738 735 -731 959
3005 49 0.074981  -955 954 -738 735 960 -730
3006 49 0.074981  -955 954 -738 735 -961 731
3007 49 0.074981  -955 954 -738 735 749 -960
3008 49 0.074981  -955 954 -738 735 -750 961
3009 49 0.074981  -955 954 -738 735 736 -749
3010 49 0.074981  -955 954 -738 735 -737 750
3011 47 0.136566  -972 955 -715 714 -712 766
3012 47 0.136566  -972 955 -715 714 -767 713
3013 71 0.0843223  -972 955 -715 714 -766 718
3014 71 0.0843223  -972 955 -715 714 -719 767
3015 71 0.0843223  -972 955 -715 714 -718 722
3016 71 0.0843223  -972 955 -715 714 -723 719
3017 49 0.074981  -972 955 -715 714 -722 956
3018 49 0.074981  -972 955 -715 714 -957 723
3019 49 0.074981  -972 955 -715 714 -956 958
3020 49 0.074981  -972 955 -715 714 -959 957
3021 49 0.074981  -972 955 -715 714 -958 730
3022 49 0.074981  -972 955 -715 714 -731 959
3023 49 0.074981  -972 955 -715 714 -730 960
3024 49 0.074981  -972 955 -715 714 -961 731
3025 49 0.074981  -972 955 -715 714 -960 749
3026 49 0.074981  -972 955 -715 714 -750 961
3027 49 0.074981  -972 955 -715 714 -749 736
3028 49 0.074981  -972 955 -715 714 -737 750
3029 47 0.136566  -972 955 715 -717 -713 712
3030 71 0.0843223  -972 955 717 -721 -713 712
3031 71 0.0843223  -972 955 721 -725 -713 712
3032 49 0.074981  -972 955 725 -729 -713 712
3033 49 0.074981  -972 955 729 -732 -713 712
3034 49 0.074981  -972 955 732 -735 -713 712
3035 49 0.074981  -972 955 735 -738 -713 712
3036 47 0.136566  -972 955 716 -714 -737 736
3037 71 0.0843223  -972 955 720 -716 -737 736
3038 71 0.0843223  -972 955 724 -720 -737 736
3039 49 0.074981  -972 955 728 -724 -737 736
3040 47 0.136566  -972 955 -717 715 766 -712
3041 47 0.136566  -972 955 -717 715 -767 713
3042 71 0.0843223  -972 955 -717 715 718 -766
3043 71 0.0843223  -972 955 -717 715 -719 767
3044 71 0.0843223  -972 955 -717 715 722 -718
3045 71 0.0843223  -972 955 -717 715 -723 719
3046 49 0.074981  -972 955 -717 715 956 -722
3047 49 0.074981  -972 955 -717 715 -957 723
3048 49 0.074981  -972 955 -717 715 958 -956
3049 49 0.074981  -972 955 -717 715 -959 957
3050 49 0.074981  -972 955 -717 715 730 -958
3051 49 0.074981  -972 955 -717 715 -731 959
3052 49 0.074981  -972 955 -717 715 960 -730
3053 49 0.074981  -972 955 -717 715 -961 731
3054 49 0.074981  -972 955 -717 715 749 -960
3055 49 0.074981  -972 955 -717 715 -750 961
3056 49 0.074981  -972 955 -717 715 736 -749
3057 49 0.074981  -972 955 -717 715 -737 750
3058 71 0.0843223  -972 955 -721 717 766 -712
3059 71 0.0843223  -972 955 -721 717 -767 713
3060 71 0.0843223  -972 955 -721 717 718 -766
3061 71 0.0843223  -972 955 -721 717 -719 767
3062 71 0.0843223  -972 955 -721 717 722 -718
3063 71 0.0843223  -972 955 -721 717 -723 719
3064 49 0.074981  -972 955 -721 717 956 -722
3065 49 0.074981  -972 955 -721 717 -957 723
3066 49 0.074981  -972 955 -721 717 958 -956
3067 49 0.074981  -972 955 -721 717 -959 957
3068 49 0.074981  -972 955 -721 717 730 -958
3069 49 0.074981  -972 955 -721 717 -731 959
3070 49 0.074981  -972 955 -721 717 960 -730
3071 49 0.074981  -972 955 -721 717 -961 731
3072 49 0.074981  -972 955 -721 717 749 -960
3073 49 0.074981  -972 955 -721 717 -750 961
3074 49 0.074981  -972 955 -721 717 736 -749
3075 49 0.074981  -972 955 -721 717 -737 750
3076 71 0.0843223  -972 955 -725 721 766 -712
3077 71 0.0843223  -972 955 -725 721 -767 713
3078 71 0.0843223  -972 955 -725 721 718 -766
3079 71 0.0843223  -972 955 -725 721 -719 767
3080 71 0.0843223  -972 955 -725 721 722 -718
3081 71 0.0843223  -972 955 -725 721 -723 719
3082 49 0.074981  -972 955 -725 721 956 -722
3083 49 0.074981  -972 955 -725 721 -957 723
3084 49 0.074981  -972 955 -725 721 958 -956
3085 49 0.074981  -972 955 -725 721 -959 957
3086 49 0.074981  -972 955 -725 721 730 -958
3087 49 0.074981  -972 955 -725 721 -731 959
3088 49 0.074981  -972 955 -725 721 960 -730
3089 49 0.074981  -972 955 -725 721 -961 731
3090 49 0.074981  -972 955 -725 721 749 -960
3091 49 0.074981  -972 955 -725 721 -750 961
3092 49 0.074981  -972 955 -725 721 736 -749
3093 49 0.074981  -972 955 -725 721 -737 750
3094 49 0.074981  -972 955 -729 725 766 -712
3095 49 0.074981  -972 955 -729 725 -767 713
3096 49 0.074981  -972 955 -729 725 718 -766
3097 49 0.074981  -972 955 -729 725 -719 767
3098 49 0.074981  -972 955 -729 725 722 -718
3099 49 0.074981  -972 955 -729 725 -723 719
3100 49 0.074981  -972 955 -729 725 956 -722
3101 49 0.074981  -972 955 -729 725 -957 723
3102 49 0.074981  -972 955 -729 725 958 -956
3103 49 0.074981  -972 955 -729 725 -959 957
3104 49 0.074981  -972 955 -729 725 730 -958
3105 49 0.074981  -972 955 -729 725 -731 959
3106 49 0.074981  -972 955 -729 725 960 -730
3107 49 0.074981  -972 955 -729 725 -961 731
3108 49 0.074981  -972 955 -729 725 749 -960
3109 49 0.074981  -972 955 -729 725 -750 961
3110 49 0.074981  -972 955 -729 725 736 -749
3111 49 0.074981  -972 955 -729 725 -737 750
3112 49 0.074981  -972 955 -732 729 766 -712
3113 49 0.074981  -972 955 -732 729 -767 713
3114 49 0.074981  -972 955 -732 729 718 -766
3115 49 0.074981  -972 955 -732 729 -719 767
3116 49 0.074981  -972 955 -732 729 722 -718
3117 49 0.074981  -972 955 -732 729 -723 719
3118 49 0.074981  -972 955 -732 729 956 -722
3119 49 0.074981  -972 955 -732 729 -957 723
3120 49 0.074981  -972 955 -732 729 958 -956
3121 49 0.074981  -972 955 -732 729 -959 957
3122 49 0.074981  -972 955 -732 729 730 -958
3123 49 0.074981  -972 955 -732 729 -731 959
3124 49 0.074981  -972 955 -732 729 960 -730
3125 49 0.074981  -972 955 -732 729 -961 731
3126 49 0.074981  -972 955 -732 729 749 -960
3127 49 0.074981  -972 955 -732 729 -750 961
3128 49 0.074981  -972 955 -732 729 736 -749
3129 49 0.074981  -972 955 -732 729 -737 750
3130 49 0.074981  -972 955 -735 732 766 -712
3131 49 0.074981  -972 955 -735 732 -767 713
3132 49 0.074981  -972 955 -735 732 718 -766
3133 49 0.074981  -972 955 -735 732 -719 767
3134 49 0.074981  -972 955 -735 732 722 -718
3135 49 0.074981  -972 955 -735 732 -723 719
3136 49 0.074981  -972 955 -735 732 956 -722
3137 49 0.074981  -972 955 -735 732 -957 723
3138 49 0.074981  -972 955 -735 732 958 -956
3139 49 0.074981  -972 955 -735 732 -959 957
3140 49 0.074981  -972 955 -735 732 730 -958
3141 49 0.074981  -972 955 -735 732 -731 959
3142 49 0.074981  -972 955 -735 732 960 -730
3143 49 0.074981  -972 955 -735 732 -961 731
3144 49 0.074981  -972 955 -735 732 749 -960
3145 49 0.074981  -972 955 -735 732 -750 961
3146 49 0.074981  -972 955 -735 732 736 -749
3147 49 0.074981  -972 955 -735 732 -737 750
3148 49 0.074981  -972 955 -738 735 766 -712
3149 49 0.074981  -972 955 -738 735 -767 713
3150 49 0.074981  -972 955 -738 735 718 -766
3151 49 0.074981  -972 955 -738 735 -719 767
3152 49 0.074981  -972 955 -738 735 722 -718
3153 49 0.074981  -972 955 -738 735 -723 719
3154 49 0.074981  -972 955 -738 735 956 -722
3155 49 0.074981  -972 955 -738 735 -957 723
3156 49 0.074981  -972 955 -738 735 958 -956
3157 49 0.074981  -972 955 -738 735 -959 957
3158 49 0.074981  -972 955 -738 735 730 -958
3159 49 0.074981  -972 955 -738 735 -731 959
3160 49 0.074981  -972 955 -738 735 960 -730
3161 49 0.074981  -972 955 -738 735 -961 731
3162 49 0.074981  -972 955 -738 735 749 -960
3163 49 0.074981  -972 955 -738 735 -750 961
3164 49 0.074981  -972 955 -738 735 736 -749
3165 49 0.074981  -972 955 -738 735 -737 750
3166 83 0.0499651  -967 966 -683
3167 83 0.0499651  -968 969 -683
3168 0  ( -964 : -689 : -688 : -598 : 599 : 965 ) -676 962 -963 ( -966
        : 967 : 683 ) ( -969 : 968 : 683 )
3169 5 0.0582256  676 -677 -965 964
3170 5 0.0582256  ( -966 : 967 : 683 ) 962 676 -680 -964
3171 5 0.0582256  ( -969 : 968 : 683 ) -963 676 -680 965
3172 0  677 -680 -965 964
3173 0  -965 964 687 -595 686 594
3174 73 0.0844385  ( -594 : -686 : -687 : 595 ) -965 964 689 -599 688
        598
3175 0  ( -981 : 979 : 793 ) ( -982 : 791 : 983 ) ( -980 : 793 : 982 )
        -752 751 -750 749 -971 970 ( -977 : -776 : -778 : -780 : -781 :
        -782 : -783 : 779 : 777 : 978 )
3176 38 0.06359  ( -770 : -772 : 773 : 771 ) 971 -976 766 -767 768 -769
3177 54 0.0833854  ( -770 : -772 : 773 : 771 ) 972 -970 749 -750 751
        -752
3178 54 0.0833854  ( -749 : 750 : -751 : 752 ) -758 757 -756 755 -973
        972
3179 54 0.0833854  ( -770 : -772 : 773 : 771 ) 971 -973 749 -750 751
        -752 ( -971 : -766 : -768 : 769 : 767 : 976 )
3180 49 0.074981  ( -736 : -728 : -973 : 1006 : 738 : 737 ) ( -972 :
        973 : -755 : 756 : -757 : 758 ) -764 763 -762 761 -975 974 (
        -736 : -728 : -754 : 972 : 738 : 737 )
3181 0  -970 972 -773 772 -771 770
3182 0  971 -973 -773 772 -771 770
3183 0  ( -985 : 984 : 952 ) -786 -980 979 789
3184 5 0.0582256  ( -981 : 979 : 793 ) ( -980 : 793 : 982 ) ( -980 :
        978 : 834 ) ( -979 : 980 : 786 ) 783 782 781 780 -779 778 -777
        776 -978 977 ( -977 : 979 : 834 ) ( -985 : -874 : 876 : 986 )
3185 0  ( -987 : 988 ) 977 -979 -833
3186 0  ( -991 : 992 ) 980 -978 -833
3187 5 0.0582256  789 -979 981 -792
3188 5 0.0582256  789 -982 980 -792
3189 33 0.0913215  -789 -982 981
3190 73 0.0844385  -791 982 -983
3191 74 0.0847227  -979 981 -795
3192 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 795
        -794 -816
3193 48 0.117208  -832 831 -984 984 -794 -816
3194 74 0.0847227  -979 981 -797
3195 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 797
        -796 794
3196 48 0.117208  -832 831 -984 984 -796 794
3197 74 0.0847227  -979 981 -799
3198 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 799
        -798 796
3199 48 0.117208  -832 831 -984 984 -798 796
3200 74 0.0847227  -979 981 -801
3201 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 801
        -800 798
3202 48 0.117208  -832 831 -984 984 -800 798
3203 74 0.0847227  -979 981 -803
3204 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 803
        -802 800
3205 48 0.117208  -832 831 -984 984 -802 800
3206 74 0.0847227  -979 981 -805
3207 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 805
        -804 802
3208 48 0.117208  -832 831 -984 984 -804 802
3209 74 0.0847227  -979 981 -807
3210 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 807
        -806 804
3211 48 0.117208  -832 831 -984 984 -806 804
3212 74 0.0847227  -979 981 -809
3213 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 809
        -808 806
3214 48 0.117208  -832 831 -984 984 -808 806
3215 74 0.0847227  -979 981 -811
3216 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 811
        -810 808
3217 48 0.117208  -832 831 -984 984 -810 808
3218 74 0.0847227  -979 981 -813
3219 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 813
        -812 810
3220 48 0.117208  -832 831 -984 984 -812 810
3221 74 0.0847227  -979 981 -815
3222 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 815
        -814 812
3223 48 0.117208  -832 831 -984 984 -814 812
3224 74 0.0847227  -979 981 -817
3225 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 817
        -816 814
3226 48 0.117208  -832 831 -984 984 -816 814
3227 74 0.0847227  -979 981 -818
3228 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 818 794
        816
3229 48 0.117208  -832 831 -984 984 794 816
3230 74 0.0847227  -979 981 -819
3231 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 819 796
        -794
3232 48 0.117208  -832 831 -984 984 796 -794
3233 74 0.0847227  -979 981 -820
3234 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 820 798
        -796
3235 48 0.117208  -832 831 -984 984 798 -796
3236 74 0.0847227  -979 981 -821
3237 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 821 800
        -798
3238 48 0.117208  -832 831 -984 984 800 -798
3239 74 0.0847227  -979 981 -822
3240 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 822 802
        -800
3241 48 0.117208  -832 831 -984 984 802 -800
3242 74 0.0847227  -979 981 -823
3243 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 823 804
        -802
3244 48 0.117208  -832 831 -984 984 804 -802
3245 74 0.0847227  -979 981 -824
3246 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 824 806
        -804
3247 48 0.117208  -832 831 -984 984 806 -804
3248 74 0.0847227  -979 981 -825
3249 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 825 808
        -806
3250 48 0.117208  -832 831 -984 984 808 -806
3251 74 0.0847227  -979 981 -826
3252 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 826 810
        -808
3253 48 0.117208  -832 831 -984 984 810 -808
3254 74 0.0847227  -979 981 -827
3255 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 827 812
        -810
3256 48 0.117208  -832 831 -984 984 812 -810
3257 74 0.0847227  -979 981 -828
3258 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 828 814
        -812
3259 48 0.117208  -832 831 -984 984 814 -812
3260 74 0.0847227  -979 981 -829
3261 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -979 981 829 816
        -814
3262 48 0.117208  -832 831 -984 984 816 -814
3263 74 0.0847227  -982 980 -795
3264 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 795
        -794 -816
3265 48 0.117208  -832 831 -984 984 -794 -816
3266 74 0.0847227  -982 980 -797
3267 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 797
        -796 794
3268 48 0.117208  -832 831 -984 984 -796 794
3269 74 0.0847227  -982 980 -799
3270 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 799
        -798 796
3271 48 0.117208  -832 831 -984 984 -798 796
3272 74 0.0847227  -982 980 -801
3273 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 801
        -800 798
3274 48 0.117208  -832 831 -984 984 -800 798
3275 74 0.0847227  -982 980 -803
3276 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 803
        -802 800
3277 48 0.117208  -832 831 -984 984 -802 800
3278 74 0.0847227  -982 980 -805
3279 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 805
        -804 802
3280 48 0.117208  -832 831 -984 984 -804 802
3281 74 0.0847227  -982 980 -807
3282 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 807
        -806 804
3283 48 0.117208  -832 831 -984 984 -806 804
3284 74 0.0847227  -982 980 -809
3285 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 809
        -808 806
3286 48 0.117208  -832 831 -984 984 -808 806
3287 74 0.0847227  -982 980 -811
3288 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 811
        -810 808
3289 48 0.117208  -832 831 -984 984 -810 808
3290 74 0.0847227  -982 980 -813
3291 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 813
        -812 810
3292 48 0.117208  -832 831 -984 984 -812 810
3293 74 0.0847227  -982 980 -815
3294 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 815
        -814 812
3295 48 0.117208  -832 831 -984 984 -814 812
3296 74 0.0847227  -982 980 -817
3297 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 817
        -816 814
3298 48 0.117208  -832 831 -984 984 -816 814
3299 74 0.0847227  -982 980 -818
3300 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 818 794
        816
3301 48 0.117208  -832 831 -984 984 794 816
3302 74 0.0847227  -982 980 -819
3303 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 819 796
        -794
3304 48 0.117208  -832 831 -984 984 796 -794
3305 74 0.0847227  -982 980 -820
3306 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 820 798
        -796
3307 48 0.117208  -832 831 -984 984 798 -796
3308 74 0.0847227  -982 980 -821
3309 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 821 800
        -798
3310 48 0.117208  -832 831 -984 984 800 -798
3311 74 0.0847227  -982 980 -822
3312 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 822 802
        -800
3313 48 0.117208  -832 831 -984 984 802 -800
3314 74 0.0847227  -982 980 -823
3315 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 823 804
        -802
3316 48 0.117208  -832 831 -984 984 804 -802
3317 74 0.0847227  -982 980 -824
3318 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 824 806
        -804
3319 48 0.117208  -832 831 -984 984 806 -804
3320 74 0.0847227  -982 980 -825
3321 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 825 808
        -806
3322 48 0.117208  -832 831 -984 984 808 -806
3323 74 0.0847227  -982 980 -826
3324 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 826 810
        -808
3325 48 0.117208  -832 831 -984 984 810 -808
3326 74 0.0847227  -982 980 -827
3327 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 827 812
        -810
3328 48 0.117208  -832 831 -984 984 812 -810
3329 74 0.0847227  -982 980 -828
3330 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 828 814
        -812
3331 48 0.117208  -832 831 -984 984 814 -812
3332 74 0.0847227  -982 980 -829
3333 5 0.0582256  ( -984 : -831 : 832 : 984 ) -793 792 -982 980 829 816
        -814
3334 48 0.117208  -832 831 -984 984 816 -814
3335 74 0.0847227  -979 977 -836
3336 5 0.0582256  -834 833 -979 977 836 -835 -857
3337 74 0.0847227  -979 977 -838
3338 5 0.0582256  -834 833 -979 977 838 -837 835
3339 74 0.0847227  -979 977 -840
3340 5 0.0582256  -834 833 -979 977 840 -839 837
3341 74 0.0847227  -979 977 -842
3342 5 0.0582256  -834 833 -979 977 842 -841 839
3343 74 0.0847227  -979 977 -844
3344 5 0.0582256  -834 833 -979 977 844 -843 841
3345 74 0.0847227  -979 977 -846
3346 5 0.0582256  -834 833 -979 977 846 -845 843
3347 74 0.0847227  -979 977 -848
3348 5 0.0582256  -834 833 -979 977 848 -847 845
3349 74 0.0847227  -979 977 -850
3350 5 0.0582256  -834 833 -979 977 850 -849 847
3351 74 0.0847227  -979 977 -852
3352 5 0.0582256  -834 833 -979 977 852 -851 849
3353 74 0.0847227  -979 977 -854
3354 5 0.0582256  -834 833 -979 977 854 -853 851
3355 74 0.0847227  -979 977 -856
3356 5 0.0582256  -834 833 -979 977 856 -855 853
3357 74 0.0847227  -979 977 -858
3358 5 0.0582256  -834 833 -979 977 858 -857 855
3359 74 0.0847227  -979 977 -859
3360 5 0.0582256  -834 833 -979 977 859 835 857
3361 74 0.0847227  -979 977 -860
3362 5 0.0582256  -834 833 -979 977 860 837 -835
3363 74 0.0847227  -979 977 -861
3364 5 0.0582256  -834 833 -979 977 861 839 -837
3365 74 0.0847227  -979 977 -862
3366 5 0.0582256  -834 833 -979 977 862 841 -839
3367 74 0.0847227  -979 977 -863
3368 5 0.0582256  -834 833 -979 977 863 843 -841
3369 74 0.0847227  -979 977 -864
3370 5 0.0582256  -834 833 -979 977 864 845 -843
3371 74 0.0847227  -979 977 -865
3372 5 0.0582256  -834 833 -979 977 865 847 -845
3373 74 0.0847227  -979 977 -866
3374 5 0.0582256  -834 833 -979 977 866 849 -847
3375 74 0.0847227  -979 977 -867
3376 5 0.0582256  -834 833 -979 977 867 851 -849
3377 74 0.0847227  -979 977 -868
3378 5 0.0582256  -834 833 -979 977 868 853 -851
3379 74 0.0847227  -979 977 -869
3380 5 0.0582256  -834 833 -979 977 869 855 -853
3381 74 0.0847227  -979 977 -870
3382 5 0.0582256  -834 833 -979 977 870 857 -855
3383 74 0.0847227  -978 980 -836
3384 5 0.0582256  -834 833 -978 980 836 -835 -857
3385 74 0.0847227  -978 980 -838
3386 5 0.0582256  -834 833 -978 980 838 -837 835
3387 74 0.0847227  -978 980 -840
3388 5 0.0582256  -834 833 -978 980 840 -839 837
3389 74 0.0847227  -978 980 -842
3390 5 0.0582256  -834 833 -978 980 842 -841 839
3391 74 0.0847227  -978 980 -844
3392 5 0.0582256  -834 833 -978 980 844 -843 841
3393 74 0.0847227  -978 980 -846
3394 5 0.0582256  -834 833 -978 980 846 -845 843
3395 74 0.0847227  -978 980 -848
3396 5 0.0582256  -834 833 -978 980 848 -847 845
3397 74 0.0847227  -978 980 -850
3398 5 0.0582256  -834 833 -978 980 850 -849 847
3399 74 0.0847227  -978 980 -852
3400 5 0.0582256  -834 833 -978 980 852 -851 849
3401 74 0.0847227  -978 980 -854
3402 5 0.0582256  -834 833 -978 980 854 -853 851
3403 74 0.0847227  -978 980 -856
3404 5 0.0582256  -834 833 -978 980 856 -855 853
3405 74 0.0847227  -978 980 -858
3406 5 0.0582256  -834 833 -978 980 858 -857 855
3407 74 0.0847227  -978 980 -859
3408 5 0.0582256  -834 833 -978 980 859 835 857
3409 74 0.0847227  -978 980 -860
3410 5 0.0582256  -834 833 -978 980 860 837 -835
3411 74 0.0847227  -978 980 -861
3412 5 0.0582256  -834 833 -978 980 861 839 -837
3413 74 0.0847227  -978 980 -862
3414 5 0.0582256  -834 833 -978 980 862 841 -839
3415 74 0.0847227  -978 980 -863
3416 5 0.0582256  -834 833 -978 980 863 843 -841
3417 74 0.0847227  -978 980 -864
3418 5 0.0582256  -834 833 -978 980 864 845 -843
3419 74 0.0847227  -978 980 -865
3420 5 0.0582256  -834 833 -978 980 865 847 -845
3421 74 0.0847227  -978 980 -866
3422 5 0.0582256  -834 833 -978 980 866 849 -847
3423 74 0.0847227  -978 980 -867
3424 5 0.0582256  -834 833 -978 980 867 851 -849
3425 74 0.0847227  -978 980 -868
3426 5 0.0582256  -834 833 -978 980 868 853 -851
3427 74 0.0847227  -978 980 -869
3428 5 0.0582256  -834 833 -978 980 869 855 -853
3429 74 0.0847227  -978 980 -870
3430 5 0.0582256  -834 833 -978 980 870 857 -855
3431 48 0.117208  -876 874 -986 985 -875 873
3432 48 0.117208  -876 874 -986 985 -877 875
3433 48 0.117208  -876 874 -986 985 -878 877
3434 48 0.117208  -876 874 -986 985 -879 878
3435 48 0.117208  -876 874 -986 985 -880 879
3436 48 0.117208  -876 874 -986 985 873 880
3437 48 0.117208  -876 874 -986 985 875 -873
3438 48 0.117208  -876 874 -986 985 877 -875
3439 48 0.117208  -876 874 -986 985 878 -877
3440 48 0.117208  -876 874 -986 985 879 -878
3441 48 0.117208  -876 874 -986 985 880 -879
3442 48 0.117208  -876 874 -986 985 -873 -880
3443 5 0.0582256  -886 885 -884 883 -990 989
3444 0  ( -989 : 990 ) -886 885 -884 883 -988 987
3445 48 0.117208  -894 893 -887 891 -990 989
3446 48 0.117208  -894 893 -892 888 -990 989
3447 48 0.117208  893 -889 -888 887 -990 989
3448 48 0.117208  -894 890 -888 887 -990 989
3449 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -989
        987
3450 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -988
        990
3451 5 0.0582256  ( -883 : 884 : -885 : 886 ) -890 889 -888 887 -988
        987
3452 5 0.0582256  977 -979 -833 ( -895 : 896 : -897 : 898 ) -988 987
3453 74 0.0847227  -913 -988 987
3454 5 0.0582256  895 -891 977 -979 -833 913 -988 987 -902 897
3455 74 0.0847227  -914 -988 987
3456 5 0.0582256  895 -891 977 -979 -833 914 -988 987 -904 902
3457 74 0.0847227  -915 -988 987
3458 5 0.0582256  895 -891 977 -979 -833 915 -988 987 -906 904
3459 74 0.0847227  -916 -988 987
3460 5 0.0582256  895 -891 977 -979 -833 916 -988 987 845 906
3461 74 0.0847227  -917 -988 987
3462 5 0.0582256  895 -891 977 -979 -833 917 -988 987 -908 -845
3463 74 0.0847227  -918 -988 987
3464 5 0.0582256  895 -891 977 -979 -833 918 -988 987 -910 908
3465 74 0.0847227  -919 -988 987
3466 5 0.0582256  895 -891 977 -979 -833 919 -988 987 -912 910
3467 74 0.0847227  -920 -988 987
3468 5 0.0582256  895 -891 977 -979 -833 920 -988 987 -898 912
3469 74 0.0847227  -921 -988 987
3470 5 0.0582256  -898 894 977 -979 -833 921 -988 987 -901 891
3471 74 0.0847227  -922 -988 987
3472 5 0.0582256  -898 894 977 -979 -833 922 -988 987 -903 901
3473 74 0.0847227  -923 -988 987
3474 5 0.0582256  -898 894 977 -979 -833 923 -988 987 -905 903
3475 74 0.0847227  -924 -988 987
3476 5 0.0582256  -898 894 977 -979 -833 924 -988 987 857 905
3477 74 0.0847227  -925 -988 987
3478 5 0.0582256  -898 894 977 -979 -833 925 -988 987 -907 -857
3479 74 0.0847227  -926 -988 987
3480 5 0.0582256  -898 894 977 -979 -833 926 -988 987 -909 907
3481 74 0.0847227  -927 -988 987
3482 5 0.0582256  -898 894 977 -979 -833 927 -988 987 -911 909
3483 74 0.0847227  -928 -988 987
3484 5 0.0582256  -898 894 977 -979 -833 928 -988 987 -892 911
3485 74 0.0847227  -929 -988 987
3486 5 0.0582256  -896 892 977 -979 -833 929 -988 987 912 -898
3487 74 0.0847227  -930 -988 987
3488 5 0.0582256  -896 892 977 -979 -833 930 -988 987 910 -912
3489 74 0.0847227  -931 -988 987
3490 5 0.0582256  -896 892 977 -979 -833 931 -988 987 908 -910
3491 74 0.0847227  -932 -988 987
3492 5 0.0582256  -896 892 977 -979 -833 932 -988 987 -845 -908
3493 74 0.0847227  -933 -988 987
3494 5 0.0582256  -896 892 977 -979 -833 933 -988 987 906 845
3495 74 0.0847227  -934 -988 987
3496 5 0.0582256  -896 892 977 -979 -833 934 -988 987 904 -906
3497 74 0.0847227  -935 -988 987
3498 5 0.0582256  -896 892 977 -979 -833 935 -988 987 902 -904
3499 74 0.0847227  -936 -988 987
3500 5 0.0582256  -896 892 977 -979 -833 936 -988 987 897 -902
3501 74 0.0847227  -937 -988 987
3502 5 0.0582256  897 -893 977 -979 -833 937 -988 987 911 -892
3503 74 0.0847227  -938 -988 987
3504 5 0.0582256  897 -893 977 -979 -833 938 -988 987 909 -911
3505 74 0.0847227  -939 -988 987
3506 5 0.0582256  897 -893 977 -979 -833 939 -988 987 907 -909
3507 74 0.0847227  -940 -988 987
3508 5 0.0582256  897 -893 977 -979 -833 940 -988 987 -857 -907
3509 74 0.0847227  -941 -988 987
3510 5 0.0582256  897 -893 977 -979 -833 941 -988 987 905 857
3511 74 0.0847227  -942 -988 987
3512 5 0.0582256  897 -893 977 -979 -833 942 -988 987 903 -905
3513 74 0.0847227  -943 -988 987
3514 5 0.0582256  897 -893 977 -979 -833 943 -988 987 901 -903
3515 74 0.0847227  -944 -988 987
3516 5 0.0582256  897 -893 977 -979 -833 944 -988 987 891 -901
3517 5 0.0582256  -886 885 -884 883 -994 993
3518 0  ( -993 : 994 ) -886 885 -884 883 -992 991
3519 48 0.117208  -894 893 -887 891 -994 993
3520 48 0.117208  -894 893 -892 888 -994 993
3521 48 0.117208  893 -889 -888 887 -994 993
3522 48 0.117208  -894 890 -888 887 -994 993
3523 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -993
        991
3524 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -992
        994
3525 5 0.0582256  ( -883 : 884 : -885 : 886 ) -890 889 -888 887 -992
        991
3526 5 0.0582256  980 -978 -833 ( -895 : 896 : -897 : 898 ) -992 991
3527 74 0.0847227  -913 -992 991
3528 5 0.0582256  895 -891 980 -978 -833 913 -992 991 -902 897
3529 74 0.0847227  -914 -992 991
3530 5 0.0582256  895 -891 980 -978 -833 914 -992 991 -904 902
3531 74 0.0847227  -915 -992 991
3532 5 0.0582256  895 -891 980 -978 -833 915 -992 991 -906 904
3533 74 0.0847227  -916 -992 991
3534 5 0.0582256  895 -891 980 -978 -833 916 -992 991 845 906
3535 74 0.0847227  -917 -992 991
3536 5 0.0582256  895 -891 980 -978 -833 917 -992 991 -908 -845
3537 74 0.0847227  -918 -992 991
3538 5 0.0582256  895 -891 980 -978 -833 918 -992 991 -910 908
3539 74 0.0847227  -919 -992 991
3540 5 0.0582256  895 -891 980 -978 -833 919 -992 991 -912 910
3541 74 0.0847227  -920 -992 991
3542 5 0.0582256  895 -891 980 -978 -833 920 -992 991 -898 912
3543 74 0.0847227  -921 -992 991
3544 5 0.0582256  -898 894 980 -978 -833 921 -992 991 -901 891
3545 74 0.0847227  -922 -992 991
3546 5 0.0582256  -898 894 980 -978 -833 922 -992 991 -903 901
3547 74 0.0847227  -923 -992 991
3548 5 0.0582256  -898 894 980 -978 -833 923 -992 991 -905 903
3549 74 0.0847227  -924 -992 991
3550 5 0.0582256  -898 894 980 -978 -833 924 -992 991 857 905
3551 74 0.0847227  -925 -992 991
3552 5 0.0582256  -898 894 980 -978 -833 925 -992 991 -907 -857
3553 74 0.0847227  -926 -992 991
3554 5 0.0582256  -898 894 980 -978 -833 926 -992 991 -909 907
3555 74 0.0847227  -927 -992 991
3556 5 0.0582256  -898 894 980 -978 -833 927 -992 991 -911 909
3557 74 0.0847227  -928 -992 991
3558 5 0.0582256  -898 894 980 -978 -833 928 -992 991 -892 911
3559 74 0.0847227  -929 -992 991
3560 5 0.0582256  -896 892 980 -978 -833 929 -992 991 912 -898
3561 74 0.0847227  -930 -992 991
3562 5 0.0582256  -896 892 980 -978 -833 930 -992 991 910 -912
3563 74 0.0847227  -931 -992 991
3564 5 0.0582256  -896 892 980 -978 -833 931 -992 991 908 -910
3565 74 0.0847227  -932 -992 991
3566 5 0.0582256  -896 892 980 -978 -833 932 -992 991 -845 -908
3567 74 0.0847227  -933 -992 991
3568 5 0.0582256  -896 892 980 -978 -833 933 -992 991 906 845
3569 74 0.0847227  -934 -992 991
3570 5 0.0582256  -896 892 980 -978 -833 934 -992 991 904 -906
3571 74 0.0847227  -935 -992 991
3572 5 0.0582256  -896 892 980 -978 -833 935 -992 991 902 -904
3573 74 0.0847227  -936 -992 991
3574 5 0.0582256  -896 892 980 -978 -833 936 -992 991 897 -902
3575 74 0.0847227  -937 -992 991
3576 5 0.0582256  897 -893 980 -978 -833 937 -992 991 911 -892
3577 74 0.0847227  -938 -992 991
3578 5 0.0582256  897 -893 980 -978 -833 938 -992 991 909 -911
3579 74 0.0847227  -939 -992 991
3580 5 0.0582256  897 -893 980 -978 -833 939 -992 991 907 -909
3581 74 0.0847227  -940 -992 991
3582 5 0.0582256  897 -893 980 -978 -833 940 -992 991 -857 -907
3583 74 0.0847227  -941 -992 991
3584 5 0.0582256  897 -893 980 -978 -833 941 -992 991 905 857
3585 74 0.0847227  -942 -992 991
3586 5 0.0582256  897 -893 980 -978 -833 942 -992 991 903 -905
3587 74 0.0847227  -943 -992 991
3588 5 0.0582256  897 -893 980 -978 -833 943 -992 991 901 -903
3589 74 0.0847227  -944 -992 991
3590 5 0.0582256  897 -893 980 -978 -833 944 -992 991 891 -901
3591 73 0.0844385  789 985 -984 -951
3592 0  -952 951 -984 985 -950 949
3593 47 0.136566  -952 951 -984 985 ( 950 : -949 )
3594 0  ( -996 : 997 : 680 ) 712 -713 714 -715 973 -1006
3595 47 0.136566  -995 973 -715 714 -712 766
3596 47 0.136566  -995 973 -715 714 -767 713
3597 71 0.0843223  -995 973 -715 714 -766 718
3598 71 0.0843223  -995 973 -715 714 -719 767
3599 71 0.0843223  -995 973 -715 714 -718 722
3600 71 0.0843223  -995 973 -715 714 -723 719
3601 49 0.074981  -995 973 -715 714 -722 726
3602 49 0.074981  -995 973 -715 714 -727 723
3603 49 0.074981  -995 973 -715 714 -726 730
3604 49 0.074981  -995 973 -715 714 -731 727
3605 49 0.074981  -995 973 -715 714 -730 733
3606 49 0.074981  -995 973 -715 714 -734 731
3607 49 0.074981  -995 973 -715 714 -733 736
3608 49 0.074981  -995 973 -715 714 -737 734
3609 47 0.136566  -995 973 715 -717 -713 712
3610 71 0.0843223  -995 973 717 -721 -713 712
3611 71 0.0843223  -995 973 721 -725 -713 712
3612 49 0.074981  -995 973 725 -729 -713 712
3613 49 0.074981  -995 973 729 -732 -713 712
3614 49 0.074981  -995 973 732 -735 -713 712
3615 49 0.074981  -995 973 735 -738 -713 712
3616 47 0.136566  -995 973 716 -714 -737 736
3617 71 0.0843223  -995 973 720 -716 -737 736
3618 71 0.0843223  -995 973 724 -720 -737 736
3619 49 0.074981  -995 973 728 -724 -737 736
3620 47 0.136566  -995 973 -717 715 766 -712
3621 47 0.136566  -995 973 -717 715 -767 713
3622 71 0.0843223  -995 973 -717 715 718 -766
3623 71 0.0843223  -995 973 -717 715 -719 767
3624 71 0.0843223  -995 973 -717 715 722 -718
3625 71 0.0843223  -995 973 -717 715 -723 719
3626 49 0.074981  -995 973 -717 715 726 -722
3627 49 0.074981  -995 973 -717 715 -727 723
3628 49 0.074981  -995 973 -717 715 730 -726
3629 49 0.074981  -995 973 -717 715 -731 727
3630 49 0.074981  -995 973 -717 715 733 -730
3631 49 0.074981  -995 973 -717 715 -734 731
3632 49 0.074981  -995 973 -717 715 736 -733
3633 49 0.074981  -995 973 -717 715 -737 734
3634 71 0.0843223  -995 973 -721 717 766 -712
3635 71 0.0843223  -995 973 -721 717 -767 713
3636 71 0.0843223  -995 973 -721 717 718 -766
3637 71 0.0843223  -995 973 -721 717 -719 767
3638 71 0.0843223  -995 973 -721 717 722 -718
3639 71 0.0843223  -995 973 -721 717 -723 719
3640 49 0.074981  -995 973 -721 717 726 -722
3641 49 0.074981  -995 973 -721 717 -727 723
3642 49 0.074981  -995 973 -721 717 730 -726
3643 49 0.074981  -995 973 -721 717 -731 727
3644 49 0.074981  -995 973 -721 717 733 -730
3645 49 0.074981  -995 973 -721 717 -734 731
3646 49 0.074981  -995 973 -721 717 736 -733
3647 49 0.074981  -995 973 -721 717 -737 734
3648 71 0.0843223  -995 973 -725 721 766 -712
3649 71 0.0843223  -995 973 -725 721 -767 713
3650 71 0.0843223  -995 973 -725 721 718 -766
3651 71 0.0843223  -995 973 -725 721 -719 767
3652 71 0.0843223  -995 973 -725 721 722 -718
3653 71 0.0843223  -995 973 -725 721 -723 719
3654 49 0.074981  -995 973 -725 721 726 -722
3655 49 0.074981  -995 973 -725 721 -727 723
3656 49 0.074981  -995 973 -725 721 730 -726
3657 49 0.074981  -995 973 -725 721 -731 727
3658 49 0.074981  -995 973 -725 721 733 -730
3659 49 0.074981  -995 973 -725 721 -734 731
3660 49 0.074981  -995 973 -725 721 736 -733
3661 49 0.074981  -995 973 -725 721 -737 734
3662 49 0.074981  -995 973 -729 725 766 -712
3663 49 0.074981  -995 973 -729 725 -767 713
3664 49 0.074981  -995 973 -729 725 718 -766
3665 49 0.074981  -995 973 -729 725 -719 767
3666 49 0.074981  -995 973 -729 725 722 -718
3667 49 0.074981  -995 973 -729 725 -723 719
3668 49 0.074981  -995 973 -729 725 726 -722
3669 49 0.074981  -995 973 -729 725 -727 723
3670 49 0.074981  -995 973 -729 725 730 -726
3671 49 0.074981  -995 973 -729 725 -731 727
3672 49 0.074981  -995 973 -729 725 733 -730
3673 49 0.074981  -995 973 -729 725 -734 731
3674 49 0.074981  -995 973 -729 725 736 -733
3675 49 0.074981  -995 973 -729 725 -737 734
3676 49 0.074981  -995 973 -732 729 766 -712
3677 49 0.074981  -995 973 -732 729 -767 713
3678 49 0.074981  -995 973 -732 729 718 -766
3679 49 0.074981  -995 973 -732 729 -719 767
3680 49 0.074981  -995 973 -732 729 722 -718
3681 49 0.074981  -995 973 -732 729 -723 719
3682 49 0.074981  -995 973 -732 729 726 -722
3683 49 0.074981  -995 973 -732 729 -727 723
3684 49 0.074981  -995 973 -732 729 730 -726
3685 49 0.074981  -995 973 -732 729 -731 727
3686 49 0.074981  -995 973 -732 729 733 -730
3687 49 0.074981  -995 973 -732 729 -734 731
3688 49 0.074981  -995 973 -732 729 736 -733
3689 49 0.074981  -995 973 -732 729 -737 734
3690 49 0.074981  -995 973 -735 732 766 -712
3691 49 0.074981  -995 973 -735 732 -767 713
3692 49 0.074981  -995 973 -735 732 718 -766
3693 49 0.074981  -995 973 -735 732 -719 767
3694 49 0.074981  -995 973 -735 732 722 -718
3695 49 0.074981  -995 973 -735 732 -723 719
3696 49 0.074981  -995 973 -735 732 726 -722
3697 49 0.074981  -995 973 -735 732 -727 723
3698 49 0.074981  -995 973 -735 732 730 -726
3699 49 0.074981  -995 973 -735 732 -731 727
3700 49 0.074981  -995 973 -735 732 733 -730
3701 49 0.074981  -995 973 -735 732 -734 731
3702 49 0.074981  -995 973 -735 732 736 -733
3703 49 0.074981  -995 973 -735 732 -737 734
3704 49 0.074981  -995 973 -738 735 766 -712
3705 49 0.074981  -995 973 -738 735 -767 713
3706 49 0.074981  -995 973 -738 735 718 -766
3707 49 0.074981  -995 973 -738 735 -719 767
3708 49 0.074981  -995 973 -738 735 722 -718
3709 49 0.074981  -995 973 -738 735 -723 719
3710 49 0.074981  -995 973 -738 735 726 -722
3711 49 0.074981  -995 973 -738 735 -727 723
3712 49 0.074981  -995 973 -738 735 730 -726
3713 49 0.074981  -995 973 -738 735 -731 727
3714 49 0.074981  -995 973 -738 735 733 -730
3715 49 0.074981  -995 973 -738 735 -734 731
3716 49 0.074981  -995 973 -738 735 736 -733
3717 49 0.074981  -995 973 -738 735 -737 734
3718 47 0.136566  -1006 995 -715 714 -712 766
3719 47 0.136566  -1006 995 -715 714 -767 713
3720 71 0.0843223  -1006 995 -715 714 -766 718
3721 71 0.0843223  -1006 995 -715 714 -719 767
3722 71 0.0843223  -1006 995 -715 714 -718 722
3723 71 0.0843223  -1006 995 -715 714 -723 719
3724 49 0.074981  -1006 995 -715 714 -722 726
3725 49 0.074981  -1006 995 -715 714 -727 723
3726 49 0.074981  -1006 995 -715 714 -726 730
3727 49 0.074981  -1006 995 -715 714 -731 727
3728 49 0.074981  -1006 995 -715 714 -730 733
3729 49 0.074981  -1006 995 -715 714 -734 731
3730 49 0.074981  -1006 995 -715 714 -733 736
3731 49 0.074981  -1006 995 -715 714 -737 734
3732 47 0.136566  -1006 995 715 -717 -713 712
3733 71 0.0843223  -1006 995 717 -721 -713 712
3734 71 0.0843223  -1006 995 721 -725 -713 712
3735 49 0.074981  -1006 995 725 -729 -713 712
3736 49 0.074981  -1006 995 729 -732 -713 712
3737 49 0.074981  -1006 995 732 -735 -713 712
3738 49 0.074981  -1006 995 735 -738 -713 712
3739 47 0.136566  -1006 995 716 -714 -737 736
3740 71 0.0843223  -1006 995 720 -716 -737 736
3741 71 0.0843223  -1006 995 724 -720 -737 736
3742 49 0.074981  -1006 995 728 -724 -737 736
3743 47 0.136566  -1006 995 -717 715 766 -712
3744 47 0.136566  -1006 995 -717 715 -767 713
3745 71 0.0843223  -1006 995 -717 715 718 -766
3746 71 0.0843223  -1006 995 -717 715 -719 767
3747 71 0.0843223  -1006 995 -717 715 722 -718
3748 71 0.0843223  -1006 995 -717 715 -723 719
3749 49 0.074981  -1006 995 -717 715 726 -722
3750 49 0.074981  -1006 995 -717 715 -727 723
3751 49 0.074981  -1006 995 -717 715 730 -726
3752 49 0.074981  -1006 995 -717 715 -731 727
3753 49 0.074981  -1006 995 -717 715 733 -730
3754 49 0.074981  -1006 995 -717 715 -734 731
3755 49 0.074981  -1006 995 -717 715 736 -733
3756 49 0.074981  -1006 995 -717 715 -737 734
3757 71 0.0843223  -1006 995 -721 717 766 -712
3758 71 0.0843223  -1006 995 -721 717 -767 713
3759 71 0.0843223  -1006 995 -721 717 718 -766
3760 71 0.0843223  -1006 995 -721 717 -719 767
3761 71 0.0843223  -1006 995 -721 717 722 -718
3762 71 0.0843223  -1006 995 -721 717 -723 719
3763 49 0.074981  -1006 995 -721 717 726 -722
3764 49 0.074981  -1006 995 -721 717 -727 723
3765 49 0.074981  -1006 995 -721 717 730 -726
3766 49 0.074981  -1006 995 -721 717 -731 727
3767 49 0.074981  -1006 995 -721 717 733 -730
3768 49 0.074981  -1006 995 -721 717 -734 731
3769 49 0.074981  -1006 995 -721 717 736 -733
3770 49 0.074981  -1006 995 -721 717 -737 734
3771 71 0.0843223  -1006 995 -725 721 766 -712
3772 71 0.0843223  -1006 995 -725 721 -767 713
3773 71 0.0843223  -1006 995 -725 721 718 -766
3774 71 0.0843223  -1006 995 -725 721 -719 767
3775 71 0.0843223  -1006 995 -725 721 722 -718
3776 71 0.0843223  -1006 995 -725 721 -723 719
3777 49 0.074981  -1006 995 -725 721 726 -722
3778 49 0.074981  -1006 995 -725 721 -727 723
3779 49 0.074981  -1006 995 -725 721 730 -726
3780 49 0.074981  -1006 995 -725 721 -731 727
3781 49 0.074981  -1006 995 -725 721 733 -730
3782 49 0.074981  -1006 995 -725 721 -734 731
3783 49 0.074981  -1006 995 -725 721 736 -733
3784 49 0.074981  -1006 995 -725 721 -737 734
3785 49 0.074981  -1006 995 -729 725 766 -712
3786 49 0.074981  -1006 995 -729 725 -767 713
3787 49 0.074981  -1006 995 -729 725 718 -766
3788 49 0.074981  -1006 995 -729 725 -719 767
3789 49 0.074981  -1006 995 -729 725 722 -718
3790 49 0.074981  -1006 995 -729 725 -723 719
3791 49 0.074981  -1006 995 -729 725 726 -722
3792 49 0.074981  -1006 995 -729 725 -727 723
3793 49 0.074981  -1006 995 -729 725 730 -726
3794 49 0.074981  -1006 995 -729 725 -731 727
3795 49 0.074981  -1006 995 -729 725 733 -730
3796 49 0.074981  -1006 995 -729 725 -734 731
3797 49 0.074981  -1006 995 -729 725 736 -733
3798 49 0.074981  -1006 995 -729 725 -737 734
3799 49 0.074981  -1006 995 -732 729 766 -712
3800 49 0.074981  -1006 995 -732 729 -767 713
3801 49 0.074981  -1006 995 -732 729 718 -766
3802 49 0.074981  -1006 995 -732 729 -719 767
3803 49 0.074981  -1006 995 -732 729 722 -718
3804 49 0.074981  -1006 995 -732 729 -723 719
3805 49 0.074981  -1006 995 -732 729 726 -722
3806 49 0.074981  -1006 995 -732 729 -727 723
3807 49 0.074981  -1006 995 -732 729 730 -726
3808 49 0.074981  -1006 995 -732 729 -731 727
3809 49 0.074981  -1006 995 -732 729 733 -730
3810 49 0.074981  -1006 995 -732 729 -734 731
3811 49 0.074981  -1006 995 -732 729 736 -733
3812 49 0.074981  -1006 995 -732 729 -737 734
3813 49 0.074981  -1006 995 -735 732 766 -712
3814 49 0.074981  -1006 995 -735 732 -767 713
3815 49 0.074981  -1006 995 -735 732 718 -766
3816 49 0.074981  -1006 995 -735 732 -719 767
3817 49 0.074981  -1006 995 -735 732 722 -718
3818 49 0.074981  -1006 995 -735 732 -723 719
3819 49 0.074981  -1006 995 -735 732 726 -722
3820 49 0.074981  -1006 995 -735 732 -727 723
3821 49 0.074981  -1006 995 -735 732 730 -726
3822 49 0.074981  -1006 995 -735 732 -731 727
3823 49 0.074981  -1006 995 -735 732 733 -730
3824 49 0.074981  -1006 995 -735 732 -734 731
3825 49 0.074981  -1006 995 -735 732 736 -733
3826 49 0.074981  -1006 995 -735 732 -737 734
3827 49 0.074981  -1006 995 -738 735 766 -712
3828 49 0.074981  -1006 995 -738 735 -767 713
3829 49 0.074981  -1006 995 -738 735 718 -766
3830 49 0.074981  -1006 995 -738 735 -719 767
3831 49 0.074981  -1006 995 -738 735 722 -718
3832 49 0.074981  -1006 995 -738 735 -723 719
3833 49 0.074981  -1006 995 -738 735 726 -722
3834 49 0.074981  -1006 995 -738 735 -727 723
3835 49 0.074981  -1006 995 -738 735 730 -726
3836 49 0.074981  -1006 995 -738 735 -731 727
3837 49 0.074981  -1006 995 -738 735 733 -730
3838 49 0.074981  -1006 995 -738 735 -734 731
3839 49 0.074981  -1006 995 -738 735 736 -733
3840 49 0.074981  -1006 995 -738 735 -737 734
3841 83 0.0499651  -1001 1000 -683
3842 83 0.0499651  -1002 1003 -683
3843 0  ( -998 : -689 : -688 : -598 : 599 : 999 ) -676 996 -997 ( -1000
        : 1001 : 683 ) ( -1003 : 1002 : 683 )
3844 5 0.0582256  676 -677 -999 998
3845 5 0.0582256  ( -1000 : 1001 : 683 ) 996 676 -680 -998
3846 5 0.0582256  ( -1003 : 1002 : 683 ) -997 676 -680 999
3847 0  677 -680 -999 998
3848 0  -999 998 687 -595 686 594
3849 73 0.0844385  ( -594 : -686 : -687 : 595 ) -999 998 689 -599 688
        598
3850 0  ( -1035 : 1033 : 793 ) ( -1036 : 791 : 1037 ) ( -1034 : 793 :
        1036 ) ( -1015 : 1013 : 793 ) ( -1016 : 791 : 1017 ) ( -1014 :
        793 : 1016 ) -752 751 -750 749 -1005 1004 ( -1011 : -776 : -778
        : -780 : -781 : -782 : -783 : 779 : 777 : 1012 ) ( -1031 : -776
        : -778 : -780 : -781 : -782 : -783 : 779 : 777 : 1032 )
3851 38 0.06359  ( -770 : -772 : 773 : 771 ) 1005 -1010 766 -767 768
        -769
3852 54 0.0833854  ( -736 : -728 : -1007 : 1051 : 738 : 737 ) -752 751
        -750 749 -1004 1006 ( -770 : -772 : 773 : 771 )
3853 54 0.0833854  ( -749 : 750 : -751 : 752 ) -758 757 -756 755 -1007
        1006
3854 54 0.0833854  ( -736 : -728 : -1007 : 1051 : 738 : 737 ) ( -1005 :
        -766 : -768 : 769 : 767 : 1010 ) -752 751 -750 749 -1007 1005 (
        -770 : -772 : 773 : 771 )
3855 49 0.074981  ( -736 : -728 : -1007 : 1051 : 738 : 737 ) ( -1006 :
        1007 : -755 : 756 : -757 : 758 ) -764 763 -762 761 -1009 1008 (
        -736 : -728 : -973 : 1006 : 738 : 737 )
3856 0  -1004 1006 -773 772 -771 770
3857 0  1005 -1007 -773 772 -771 770
3858 0  ( -1029 : 1030 : 952 ) -786 -1014 1013 789
3859 5 0.0582256  ( -1015 : 1013 : 793 ) ( -1014 : 793 : 1016 ) ( -1014
        : 1012 : 834 ) ( -1013 : 1014 : 786 ) 783 782 781 780 -779 778
        -777 776 -1012 1011 ( -1011 : 1013 : 834 ) ( -1019 : -874 : 876
        : 1020 )
3860 0  ( -1021 : 1022 ) 1011 -1013 -833
3861 0  ( -1025 : 1026 ) 1014 -1012 -833
3862 5 0.0582256  789 -1013 1015 -792
3863 5 0.0582256  789 -1016 1014 -792
3864 33 0.0913215  -789 -1016 1015
3865 73 0.0844385  -791 1016 -1017
3866 74 0.0847227  -1013 1015 -795
3867 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 795
        -794 -816
3868 48 0.117208  -832 831 -1018 1018 -794 -816
3869 74 0.0847227  -1013 1015 -797
3870 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 797
        -796 794
3871 48 0.117208  -832 831 -1018 1018 -796 794
3872 74 0.0847227  -1013 1015 -799
3873 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 799
        -798 796
3874 48 0.117208  -832 831 -1018 1018 -798 796
3875 74 0.0847227  -1013 1015 -801
3876 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 801
        -800 798
3877 48 0.117208  -832 831 -1018 1018 -800 798
3878 74 0.0847227  -1013 1015 -803
3879 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 803
        -802 800
3880 48 0.117208  -832 831 -1018 1018 -802 800
3881 74 0.0847227  -1013 1015 -805
3882 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 805
        -804 802
3883 48 0.117208  -832 831 -1018 1018 -804 802
3884 74 0.0847227  -1013 1015 -807
3885 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 807
        -806 804
3886 48 0.117208  -832 831 -1018 1018 -806 804
3887 74 0.0847227  -1013 1015 -809
3888 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 809
        -808 806
3889 48 0.117208  -832 831 -1018 1018 -808 806
3890 74 0.0847227  -1013 1015 -811
3891 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 811
        -810 808
3892 48 0.117208  -832 831 -1018 1018 -810 808
3893 74 0.0847227  -1013 1015 -813
3894 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 813
        -812 810
3895 48 0.117208  -832 831 -1018 1018 -812 810
3896 74 0.0847227  -1013 1015 -815
3897 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 815
        -814 812
3898 48 0.117208  -832 831 -1018 1018 -814 812
3899 74 0.0847227  -1013 1015 -817
3900 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 817
        -816 814
3901 48 0.117208  -832 831 -1018 1018 -816 814
3902 74 0.0847227  -1013 1015 -818
3903 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 818
        794 816
3904 48 0.117208  -832 831 -1018 1018 794 816
3905 74 0.0847227  -1013 1015 -819
3906 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 819
        796 -794
3907 48 0.117208  -832 831 -1018 1018 796 -794
3908 74 0.0847227  -1013 1015 -820
3909 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 820
        798 -796
3910 48 0.117208  -832 831 -1018 1018 798 -796
3911 74 0.0847227  -1013 1015 -821
3912 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 821
        800 -798
3913 48 0.117208  -832 831 -1018 1018 800 -798
3914 74 0.0847227  -1013 1015 -822
3915 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 822
        802 -800
3916 48 0.117208  -832 831 -1018 1018 802 -800
3917 74 0.0847227  -1013 1015 -823
3918 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 823
        804 -802
3919 48 0.117208  -832 831 -1018 1018 804 -802
3920 74 0.0847227  -1013 1015 -824
3921 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 824
        806 -804
3922 48 0.117208  -832 831 -1018 1018 806 -804
3923 74 0.0847227  -1013 1015 -825
3924 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 825
        808 -806
3925 48 0.117208  -832 831 -1018 1018 808 -806
3926 74 0.0847227  -1013 1015 -826
3927 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 826
        810 -808
3928 48 0.117208  -832 831 -1018 1018 810 -808
3929 74 0.0847227  -1013 1015 -827
3930 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 827
        812 -810
3931 48 0.117208  -832 831 -1018 1018 812 -810
3932 74 0.0847227  -1013 1015 -828
3933 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 828
        814 -812
3934 48 0.117208  -832 831 -1018 1018 814 -812
3935 74 0.0847227  -1013 1015 -829
3936 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1013 1015 829
        816 -814
3937 48 0.117208  -832 831 -1018 1018 816 -814
3938 74 0.0847227  -1016 1014 -795
3939 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 795
        -794 -816
3940 48 0.117208  -832 831 -1018 1018 -794 -816
3941 74 0.0847227  -1016 1014 -797
3942 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 797
        -796 794
3943 48 0.117208  -832 831 -1018 1018 -796 794
3944 74 0.0847227  -1016 1014 -799
3945 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 799
        -798 796
3946 48 0.117208  -832 831 -1018 1018 -798 796
3947 74 0.0847227  -1016 1014 -801
3948 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 801
        -800 798
3949 48 0.117208  -832 831 -1018 1018 -800 798
3950 74 0.0847227  -1016 1014 -803
3951 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 803
        -802 800
3952 48 0.117208  -832 831 -1018 1018 -802 800
3953 74 0.0847227  -1016 1014 -805
3954 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 805
        -804 802
3955 48 0.117208  -832 831 -1018 1018 -804 802
3956 74 0.0847227  -1016 1014 -807
3957 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 807
        -806 804
3958 48 0.117208  -832 831 -1018 1018 -806 804
3959 74 0.0847227  -1016 1014 -809
3960 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 809
        -808 806
3961 48 0.117208  -832 831 -1018 1018 -808 806
3962 74 0.0847227  -1016 1014 -811
3963 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 811
        -810 808
3964 48 0.117208  -832 831 -1018 1018 -810 808
3965 74 0.0847227  -1016 1014 -813
3966 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 813
        -812 810
3967 48 0.117208  -832 831 -1018 1018 -812 810
3968 74 0.0847227  -1016 1014 -815
3969 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 815
        -814 812
3970 48 0.117208  -832 831 -1018 1018 -814 812
3971 74 0.0847227  -1016 1014 -817
3972 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 817
        -816 814
3973 48 0.117208  -832 831 -1018 1018 -816 814
3974 74 0.0847227  -1016 1014 -818
3975 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 818
        794 816
3976 48 0.117208  -832 831 -1018 1018 794 816
3977 74 0.0847227  -1016 1014 -819
3978 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 819
        796 -794
3979 48 0.117208  -832 831 -1018 1018 796 -794
3980 74 0.0847227  -1016 1014 -820
3981 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 820
        798 -796
3982 48 0.117208  -832 831 -1018 1018 798 -796
3983 74 0.0847227  -1016 1014 -821
3984 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 821
        800 -798
3985 48 0.117208  -832 831 -1018 1018 800 -798
3986 74 0.0847227  -1016 1014 -822
3987 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 822
        802 -800
3988 48 0.117208  -832 831 -1018 1018 802 -800
3989 74 0.0847227  -1016 1014 -823
3990 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 823
        804 -802
3991 48 0.117208  -832 831 -1018 1018 804 -802
3992 74 0.0847227  -1016 1014 -824
3993 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 824
        806 -804
3994 48 0.117208  -832 831 -1018 1018 806 -804
3995 74 0.0847227  -1016 1014 -825
3996 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 825
        808 -806
3997 48 0.117208  -832 831 -1018 1018 808 -806
3998 74 0.0847227  -1016 1014 -826
3999 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 826
        810 -808
4000 48 0.117208  -832 831 -1018 1018 810 -808
4001 74 0.0847227  -1016 1014 -827
4002 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 827
        812 -810
4003 48 0.117208  -832 831 -1018 1018 812 -810
4004 74 0.0847227  -1016 1014 -828
4005 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 828
        814 -812
4006 48 0.117208  -832 831 -1018 1018 814 -812
4007 74 0.0847227  -1016 1014 -829
4008 5 0.0582256  ( -1018 : -831 : 832 : 1018 ) -793 792 -1016 1014 829
        816 -814
4009 48 0.117208  -832 831 -1018 1018 816 -814
4010 74 0.0847227  -1013 1011 -836
4011 5 0.0582256  -834 833 -1013 1011 836 -835 -857
4012 74 0.0847227  -1013 1011 -838
4013 5 0.0582256  -834 833 -1013 1011 838 -837 835
4014 74 0.0847227  -1013 1011 -840
4015 5 0.0582256  -834 833 -1013 1011 840 -839 837
4016 74 0.0847227  -1013 1011 -842
4017 5 0.0582256  -834 833 -1013 1011 842 -841 839
4018 74 0.0847227  -1013 1011 -844
4019 5 0.0582256  -834 833 -1013 1011 844 -843 841
4020 74 0.0847227  -1013 1011 -846
4021 5 0.0582256  -834 833 -1013 1011 846 -845 843
4022 74 0.0847227  -1013 1011 -848
4023 5 0.0582256  -834 833 -1013 1011 848 -847 845
4024 74 0.0847227  -1013 1011 -850
4025 5 0.0582256  -834 833 -1013 1011 850 -849 847
4026 74 0.0847227  -1013 1011 -852
4027 5 0.0582256  -834 833 -1013 1011 852 -851 849
4028 74 0.0847227  -1013 1011 -854
4029 5 0.0582256  -834 833 -1013 1011 854 -853 851
4030 74 0.0847227  -1013 1011 -856
4031 5 0.0582256  -834 833 -1013 1011 856 -855 853
4032 74 0.0847227  -1013 1011 -858
4033 5 0.0582256  -834 833 -1013 1011 858 -857 855
4034 74 0.0847227  -1013 1011 -859
4035 5 0.0582256  -834 833 -1013 1011 859 835 857
4036 74 0.0847227  -1013 1011 -860
4037 5 0.0582256  -834 833 -1013 1011 860 837 -835
4038 74 0.0847227  -1013 1011 -861
4039 5 0.0582256  -834 833 -1013 1011 861 839 -837
4040 74 0.0847227  -1013 1011 -862
4041 5 0.0582256  -834 833 -1013 1011 862 841 -839
4042 74 0.0847227  -1013 1011 -863
4043 5 0.0582256  -834 833 -1013 1011 863 843 -841
4044 74 0.0847227  -1013 1011 -864
4045 5 0.0582256  -834 833 -1013 1011 864 845 -843
4046 74 0.0847227  -1013 1011 -865
4047 5 0.0582256  -834 833 -1013 1011 865 847 -845
4048 74 0.0847227  -1013 1011 -866
4049 5 0.0582256  -834 833 -1013 1011 866 849 -847
4050 74 0.0847227  -1013 1011 -867
4051 5 0.0582256  -834 833 -1013 1011 867 851 -849
4052 74 0.0847227  -1013 1011 -868
4053 5 0.0582256  -834 833 -1013 1011 868 853 -851
4054 74 0.0847227  -1013 1011 -869
4055 5 0.0582256  -834 833 -1013 1011 869 855 -853
4056 74 0.0847227  -1013 1011 -870
4057 5 0.0582256  -834 833 -1013 1011 870 857 -855
4058 74 0.0847227  -1012 1014 -836
4059 5 0.0582256  -834 833 -1012 1014 836 -835 -857
4060 74 0.0847227  -1012 1014 -838
4061 5 0.0582256  -834 833 -1012 1014 838 -837 835
4062 74 0.0847227  -1012 1014 -840
4063 5 0.0582256  -834 833 -1012 1014 840 -839 837
4064 74 0.0847227  -1012 1014 -842
4065 5 0.0582256  -834 833 -1012 1014 842 -841 839
4066 74 0.0847227  -1012 1014 -844
4067 5 0.0582256  -834 833 -1012 1014 844 -843 841
4068 74 0.0847227  -1012 1014 -846
4069 5 0.0582256  -834 833 -1012 1014 846 -845 843
4070 74 0.0847227  -1012 1014 -848
4071 5 0.0582256  -834 833 -1012 1014 848 -847 845
4072 74 0.0847227  -1012 1014 -850
4073 5 0.0582256  -834 833 -1012 1014 850 -849 847
4074 74 0.0847227  -1012 1014 -852
4075 5 0.0582256  -834 833 -1012 1014 852 -851 849
4076 74 0.0847227  -1012 1014 -854
4077 5 0.0582256  -834 833 -1012 1014 854 -853 851
4078 74 0.0847227  -1012 1014 -856
4079 5 0.0582256  -834 833 -1012 1014 856 -855 853
4080 74 0.0847227  -1012 1014 -858
4081 5 0.0582256  -834 833 -1012 1014 858 -857 855
4082 74 0.0847227  -1012 1014 -859
4083 5 0.0582256  -834 833 -1012 1014 859 835 857
4084 74 0.0847227  -1012 1014 -860
4085 5 0.0582256  -834 833 -1012 1014 860 837 -835
4086 74 0.0847227  -1012 1014 -861
4087 5 0.0582256  -834 833 -1012 1014 861 839 -837
4088 74 0.0847227  -1012 1014 -862
4089 5 0.0582256  -834 833 -1012 1014 862 841 -839
4090 74 0.0847227  -1012 1014 -863
4091 5 0.0582256  -834 833 -1012 1014 863 843 -841
4092 74 0.0847227  -1012 1014 -864
4093 5 0.0582256  -834 833 -1012 1014 864 845 -843
4094 74 0.0847227  -1012 1014 -865
4095 5 0.0582256  -834 833 -1012 1014 865 847 -845
4096 74 0.0847227  -1012 1014 -866
4097 5 0.0582256  -834 833 -1012 1014 866 849 -847
4098 74 0.0847227  -1012 1014 -867
4099 5 0.0582256  -834 833 -1012 1014 867 851 -849
4100 74 0.0847227  -1012 1014 -868
4101 5 0.0582256  -834 833 -1012 1014 868 853 -851
4102 74 0.0847227  -1012 1014 -869
4103 5 0.0582256  -834 833 -1012 1014 869 855 -853
4104 74 0.0847227  -1012 1014 -870
4105 5 0.0582256  -834 833 -1012 1014 870 857 -855
4106 48 0.117208  -876 874 -1020 1019 -875 873
4107 48 0.117208  -876 874 -1020 1019 -877 875
4108 48 0.117208  -876 874 -1020 1019 -878 877
4109 48 0.117208  -876 874 -1020 1019 -879 878
4110 48 0.117208  -876 874 -1020 1019 -880 879
4111 48 0.117208  -876 874 -1020 1019 873 880
4112 48 0.117208  -876 874 -1020 1019 875 -873
4113 48 0.117208  -876 874 -1020 1019 877 -875
4114 48 0.117208  -876 874 -1020 1019 878 -877
4115 48 0.117208  -876 874 -1020 1019 879 -878
4116 48 0.117208  -876 874 -1020 1019 880 -879
4117 48 0.117208  -876 874 -1020 1019 -873 -880
4118 5 0.0582256  -886 885 -884 883 -1024 1023
4119 0  ( -1023 : 1024 ) -886 885 -884 883 -1022 1021
4120 48 0.117208  -894 893 -887 891 -1024 1023
4121 48 0.117208  -894 893 -892 888 -1024 1023
4122 48 0.117208  893 -889 -888 887 -1024 1023
4123 48 0.117208  -894 890 -888 887 -1024 1023
4124 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -1023
        1021
4125 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -1022
        1024
4126 5 0.0582256  ( -883 : 884 : -885 : 886 ) -890 889 -888 887 -1022
        1021
4127 5 0.0582256  1011 -1013 -833 ( -895 : 896 : -897 : 898 ) -1022
        1021
4128 74 0.0847227  -913 -1022 1021
4129 5 0.0582256  895 -891 1011 -1013 -833 913 -1022 1021 -902 897
4130 74 0.0847227  -914 -1022 1021
4131 5 0.0582256  895 -891 1011 -1013 -833 914 -1022 1021 -904 902
4132 74 0.0847227  -915 -1022 1021
4133 5 0.0582256  895 -891 1011 -1013 -833 915 -1022 1021 -906 904
4134 74 0.0847227  -916 -1022 1021
4135 5 0.0582256  895 -891 1011 -1013 -833 916 -1022 1021 845 906
4136 74 0.0847227  -917 -1022 1021
4137 5 0.0582256  895 -891 1011 -1013 -833 917 -1022 1021 -908 -845
4138 74 0.0847227  -918 -1022 1021
4139 5 0.0582256  895 -891 1011 -1013 -833 918 -1022 1021 -910 908
4140 74 0.0847227  -919 -1022 1021
4141 5 0.0582256  895 -891 1011 -1013 -833 919 -1022 1021 -912 910
4142 74 0.0847227  -920 -1022 1021
4143 5 0.0582256  895 -891 1011 -1013 -833 920 -1022 1021 -898 912
4144 74 0.0847227  -921 -1022 1021
4145 5 0.0582256  -898 894 1011 -1013 -833 921 -1022 1021 -901 891
4146 74 0.0847227  -922 -1022 1021
4147 5 0.0582256  -898 894 1011 -1013 -833 922 -1022 1021 -903 901
4148 74 0.0847227  -923 -1022 1021
4149 5 0.0582256  -898 894 1011 -1013 -833 923 -1022 1021 -905 903
4150 74 0.0847227  -924 -1022 1021
4151 5 0.0582256  -898 894 1011 -1013 -833 924 -1022 1021 857 905
4152 74 0.0847227  -925 -1022 1021
4153 5 0.0582256  -898 894 1011 -1013 -833 925 -1022 1021 -907 -857
4154 74 0.0847227  -926 -1022 1021
4155 5 0.0582256  -898 894 1011 -1013 -833 926 -1022 1021 -909 907
4156 74 0.0847227  -927 -1022 1021
4157 5 0.0582256  -898 894 1011 -1013 -833 927 -1022 1021 -911 909
4158 74 0.0847227  -928 -1022 1021
4159 5 0.0582256  -898 894 1011 -1013 -833 928 -1022 1021 -892 911
4160 74 0.0847227  -929 -1022 1021
4161 5 0.0582256  -896 892 1011 -1013 -833 929 -1022 1021 912 -898
4162 74 0.0847227  -930 -1022 1021
4163 5 0.0582256  -896 892 1011 -1013 -833 930 -1022 1021 910 -912
4164 74 0.0847227  -931 -1022 1021
4165 5 0.0582256  -896 892 1011 -1013 -833 931 -1022 1021 908 -910
4166 74 0.0847227  -932 -1022 1021
4167 5 0.0582256  -896 892 1011 -1013 -833 932 -1022 1021 -845 -908
4168 74 0.0847227  -933 -1022 1021
4169 5 0.0582256  -896 892 1011 -1013 -833 933 -1022 1021 906 845
4170 74 0.0847227  -934 -1022 1021
4171 5 0.0582256  -896 892 1011 -1013 -833 934 -1022 1021 904 -906
4172 74 0.0847227  -935 -1022 1021
4173 5 0.0582256  -896 892 1011 -1013 -833 935 -1022 1021 902 -904
4174 74 0.0847227  -936 -1022 1021
4175 5 0.0582256  -896 892 1011 -1013 -833 936 -1022 1021 897 -902
4176 74 0.0847227  -937 -1022 1021
4177 5 0.0582256  897 -893 1011 -1013 -833 937 -1022 1021 911 -892
4178 74 0.0847227  -938 -1022 1021
4179 5 0.0582256  897 -893 1011 -1013 -833 938 -1022 1021 909 -911
4180 74 0.0847227  -939 -1022 1021
4181 5 0.0582256  897 -893 1011 -1013 -833 939 -1022 1021 907 -909
4182 74 0.0847227  -940 -1022 1021
4183 5 0.0582256  897 -893 1011 -1013 -833 940 -1022 1021 -857 -907
4184 74 0.0847227  -941 -1022 1021
4185 5 0.0582256  897 -893 1011 -1013 -833 941 -1022 1021 905 857
4186 74 0.0847227  -942 -1022 1021
4187 5 0.0582256  897 -893 1011 -1013 -833 942 -1022 1021 903 -905
4188 74 0.0847227  -943 -1022 1021
4189 5 0.0582256  897 -893 1011 -1013 -833 943 -1022 1021 901 -903
4190 74 0.0847227  -944 -1022 1021
4191 5 0.0582256  897 -893 1011 -1013 -833 944 -1022 1021 891 -901
4192 5 0.0582256  -886 885 -884 883 -1028 1027
4193 0  ( -1027 : 1028 ) -886 885 -884 883 -1026 1025
4194 48 0.117208  -894 893 -887 891 -1028 1027
4195 48 0.117208  -894 893 -892 888 -1028 1027
4196 48 0.117208  893 -889 -888 887 -1028 1027
4197 48 0.117208  -894 890 -888 887 -1028 1027
4198 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -1027
        1025
4199 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -1026
        1028
4200 5 0.0582256  ( -883 : 884 : -885 : 886 ) -890 889 -888 887 -1026
        1025
4201 5 0.0582256  1014 -1012 -833 ( -895 : 896 : -897 : 898 ) -1026
        1025
4202 74 0.0847227  -913 -1026 1025
4203 5 0.0582256  895 -891 1014 -1012 -833 913 -1026 1025 -902 897
4204 74 0.0847227  -914 -1026 1025
4205 5 0.0582256  895 -891 1014 -1012 -833 914 -1026 1025 -904 902
4206 74 0.0847227  -915 -1026 1025
4207 5 0.0582256  895 -891 1014 -1012 -833 915 -1026 1025 -906 904
4208 74 0.0847227  -916 -1026 1025
4209 5 0.0582256  895 -891 1014 -1012 -833 916 -1026 1025 845 906
4210 74 0.0847227  -917 -1026 1025
4211 5 0.0582256  895 -891 1014 -1012 -833 917 -1026 1025 -908 -845
4212 74 0.0847227  -918 -1026 1025
4213 5 0.0582256  895 -891 1014 -1012 -833 918 -1026 1025 -910 908
4214 74 0.0847227  -919 -1026 1025
4215 5 0.0582256  895 -891 1014 -1012 -833 919 -1026 1025 -912 910
4216 74 0.0847227  -920 -1026 1025
4217 5 0.0582256  895 -891 1014 -1012 -833 920 -1026 1025 -898 912
4218 74 0.0847227  -921 -1026 1025
4219 5 0.0582256  -898 894 1014 -1012 -833 921 -1026 1025 -901 891
4220 74 0.0847227  -922 -1026 1025
4221 5 0.0582256  -898 894 1014 -1012 -833 922 -1026 1025 -903 901
4222 74 0.0847227  -923 -1026 1025
4223 5 0.0582256  -898 894 1014 -1012 -833 923 -1026 1025 -905 903
4224 74 0.0847227  -924 -1026 1025
4225 5 0.0582256  -898 894 1014 -1012 -833 924 -1026 1025 857 905
4226 74 0.0847227  -925 -1026 1025
4227 5 0.0582256  -898 894 1014 -1012 -833 925 -1026 1025 -907 -857
4228 74 0.0847227  -926 -1026 1025
4229 5 0.0582256  -898 894 1014 -1012 -833 926 -1026 1025 -909 907
4230 74 0.0847227  -927 -1026 1025
4231 5 0.0582256  -898 894 1014 -1012 -833 927 -1026 1025 -911 909
4232 74 0.0847227  -928 -1026 1025
4233 5 0.0582256  -898 894 1014 -1012 -833 928 -1026 1025 -892 911
4234 74 0.0847227  -929 -1026 1025
4235 5 0.0582256  -896 892 1014 -1012 -833 929 -1026 1025 912 -898
4236 74 0.0847227  -930 -1026 1025
4237 5 0.0582256  -896 892 1014 -1012 -833 930 -1026 1025 910 -912
4238 74 0.0847227  -931 -1026 1025
4239 5 0.0582256  -896 892 1014 -1012 -833 931 -1026 1025 908 -910
4240 74 0.0847227  -932 -1026 1025
4241 5 0.0582256  -896 892 1014 -1012 -833 932 -1026 1025 -845 -908
4242 74 0.0847227  -933 -1026 1025
4243 5 0.0582256  -896 892 1014 -1012 -833 933 -1026 1025 906 845
4244 74 0.0847227  -934 -1026 1025
4245 5 0.0582256  -896 892 1014 -1012 -833 934 -1026 1025 904 -906
4246 74 0.0847227  -935 -1026 1025
4247 5 0.0582256  -896 892 1014 -1012 -833 935 -1026 1025 902 -904
4248 74 0.0847227  -936 -1026 1025
4249 5 0.0582256  -896 892 1014 -1012 -833 936 -1026 1025 897 -902
4250 74 0.0847227  -937 -1026 1025
4251 5 0.0582256  897 -893 1014 -1012 -833 937 -1026 1025 911 -892
4252 74 0.0847227  -938 -1026 1025
4253 5 0.0582256  897 -893 1014 -1012 -833 938 -1026 1025 909 -911
4254 74 0.0847227  -939 -1026 1025
4255 5 0.0582256  897 -893 1014 -1012 -833 939 -1026 1025 907 -909
4256 74 0.0847227  -940 -1026 1025
4257 5 0.0582256  897 -893 1014 -1012 -833 940 -1026 1025 -857 -907
4258 74 0.0847227  -941 -1026 1025
4259 5 0.0582256  897 -893 1014 -1012 -833 941 -1026 1025 905 857
4260 74 0.0847227  -942 -1026 1025
4261 5 0.0582256  897 -893 1014 -1012 -833 942 -1026 1025 903 -905
4262 74 0.0847227  -943 -1026 1025
4263 5 0.0582256  897 -893 1014 -1012 -833 943 -1026 1025 901 -903
4264 74 0.0847227  -944 -1026 1025
4265 5 0.0582256  897 -893 1014 -1012 -833 944 -1026 1025 891 -901
4266 73 0.0844385  789 1029 -1030 -951
4267 0  -952 951 -1030 1029 -950 949
4268 47 0.136566  -952 951 -1030 1029 ( 950 : -949 )
4269 0  ( -1049 : 1050 : 952 ) -786 -1034 1033 789
4270 5 0.0582256  ( -1035 : 1033 : 793 ) ( -1034 : 793 : 1036 ) ( -1034
        : 1032 : 834 ) ( -1033 : 1034 : 786 ) 783 782 781 780 -779 778
        -777 776 -1032 1031 ( -1031 : 1033 : 834 ) ( -1039 : -874 : 876
        : 1040 )
4271 0  ( -1041 : 1042 ) 1031 -1033 -833
4272 0  ( -1045 : 1046 ) 1034 -1032 -833
4273 5 0.0582256  789 -1033 1035 -792
4274 5 0.0582256  789 -1036 1034 -792
4275 33 0.0913215  -789 -1036 1035
4276 73 0.0844385  -791 1036 -1037
4277 74 0.0847227  -1033 1035 -795
4278 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 795
        -794 -816
4279 48 0.117208  -832 831 -1038 1038 -794 -816
4280 74 0.0847227  -1033 1035 -797
4281 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 797
        -796 794
4282 48 0.117208  -832 831 -1038 1038 -796 794
4283 74 0.0847227  -1033 1035 -799
4284 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 799
        -798 796
4285 48 0.117208  -832 831 -1038 1038 -798 796
4286 74 0.0847227  -1033 1035 -801
4287 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 801
        -800 798
4288 48 0.117208  -832 831 -1038 1038 -800 798
4289 74 0.0847227  -1033 1035 -803
4290 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 803
        -802 800
4291 48 0.117208  -832 831 -1038 1038 -802 800
4292 74 0.0847227  -1033 1035 -805
4293 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 805
        -804 802
4294 48 0.117208  -832 831 -1038 1038 -804 802
4295 74 0.0847227  -1033 1035 -807
4296 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 807
        -806 804
4297 48 0.117208  -832 831 -1038 1038 -806 804
4298 74 0.0847227  -1033 1035 -809
4299 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 809
        -808 806
4300 48 0.117208  -832 831 -1038 1038 -808 806
4301 74 0.0847227  -1033 1035 -811
4302 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 811
        -810 808
4303 48 0.117208  -832 831 -1038 1038 -810 808
4304 74 0.0847227  -1033 1035 -813
4305 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 813
        -812 810
4306 48 0.117208  -832 831 -1038 1038 -812 810
4307 74 0.0847227  -1033 1035 -815
4308 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 815
        -814 812
4309 48 0.117208  -832 831 -1038 1038 -814 812
4310 74 0.0847227  -1033 1035 -817
4311 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 817
        -816 814
4312 48 0.117208  -832 831 -1038 1038 -816 814
4313 74 0.0847227  -1033 1035 -818
4314 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 818
        794 816
4315 48 0.117208  -832 831 -1038 1038 794 816
4316 74 0.0847227  -1033 1035 -819
4317 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 819
        796 -794
4318 48 0.117208  -832 831 -1038 1038 796 -794
4319 74 0.0847227  -1033 1035 -820
4320 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 820
        798 -796
4321 48 0.117208  -832 831 -1038 1038 798 -796
4322 74 0.0847227  -1033 1035 -821
4323 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 821
        800 -798
4324 48 0.117208  -832 831 -1038 1038 800 -798
4325 74 0.0847227  -1033 1035 -822
4326 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 822
        802 -800
4327 48 0.117208  -832 831 -1038 1038 802 -800
4328 74 0.0847227  -1033 1035 -823
4329 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 823
        804 -802
4330 48 0.117208  -832 831 -1038 1038 804 -802
4331 74 0.0847227  -1033 1035 -824
4332 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 824
        806 -804
4333 48 0.117208  -832 831 -1038 1038 806 -804
4334 74 0.0847227  -1033 1035 -825
4335 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 825
        808 -806
4336 48 0.117208  -832 831 -1038 1038 808 -806
4337 74 0.0847227  -1033 1035 -826
4338 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 826
        810 -808
4339 48 0.117208  -832 831 -1038 1038 810 -808
4340 74 0.0847227  -1033 1035 -827
4341 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 827
        812 -810
4342 48 0.117208  -832 831 -1038 1038 812 -810
4343 74 0.0847227  -1033 1035 -828
4344 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 828
        814 -812
4345 48 0.117208  -832 831 -1038 1038 814 -812
4346 74 0.0847227  -1033 1035 -829
4347 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1033 1035 829
        816 -814
4348 48 0.117208  -832 831 -1038 1038 816 -814
4349 74 0.0847227  -1036 1034 -795
4350 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 795
        -794 -816
4351 48 0.117208  -832 831 -1038 1038 -794 -816
4352 74 0.0847227  -1036 1034 -797
4353 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 797
        -796 794
4354 48 0.117208  -832 831 -1038 1038 -796 794
4355 74 0.0847227  -1036 1034 -799
4356 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 799
        -798 796
4357 48 0.117208  -832 831 -1038 1038 -798 796
4358 74 0.0847227  -1036 1034 -801
4359 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 801
        -800 798
4360 48 0.117208  -832 831 -1038 1038 -800 798
4361 74 0.0847227  -1036 1034 -803
4362 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 803
        -802 800
4363 48 0.117208  -832 831 -1038 1038 -802 800
4364 74 0.0847227  -1036 1034 -805
4365 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 805
        -804 802
4366 48 0.117208  -832 831 -1038 1038 -804 802
4367 74 0.0847227  -1036 1034 -807
4368 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 807
        -806 804
4369 48 0.117208  -832 831 -1038 1038 -806 804
4370 74 0.0847227  -1036 1034 -809
4371 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 809
        -808 806
4372 48 0.117208  -832 831 -1038 1038 -808 806
4373 74 0.0847227  -1036 1034 -811
4374 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 811
        -810 808
4375 48 0.117208  -832 831 -1038 1038 -810 808
4376 74 0.0847227  -1036 1034 -813
4377 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 813
        -812 810
4378 48 0.117208  -832 831 -1038 1038 -812 810
4379 74 0.0847227  -1036 1034 -815
4380 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 815
        -814 812
4381 48 0.117208  -832 831 -1038 1038 -814 812
4382 74 0.0847227  -1036 1034 -817
4383 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 817
        -816 814
4384 48 0.117208  -832 831 -1038 1038 -816 814
4385 74 0.0847227  -1036 1034 -818
4386 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 818
        794 816
4387 48 0.117208  -832 831 -1038 1038 794 816
4388 74 0.0847227  -1036 1034 -819
4389 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 819
        796 -794
4390 48 0.117208  -832 831 -1038 1038 796 -794
4391 74 0.0847227  -1036 1034 -820
4392 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 820
        798 -796
4393 48 0.117208  -832 831 -1038 1038 798 -796
4394 74 0.0847227  -1036 1034 -821
4395 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 821
        800 -798
4396 48 0.117208  -832 831 -1038 1038 800 -798
4397 74 0.0847227  -1036 1034 -822
4398 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 822
        802 -800
4399 48 0.117208  -832 831 -1038 1038 802 -800
4400 74 0.0847227  -1036 1034 -823
4401 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 823
        804 -802
4402 48 0.117208  -832 831 -1038 1038 804 -802
4403 74 0.0847227  -1036 1034 -824
4404 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 824
        806 -804
4405 48 0.117208  -832 831 -1038 1038 806 -804
4406 74 0.0847227  -1036 1034 -825
4407 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 825
        808 -806
4408 48 0.117208  -832 831 -1038 1038 808 -806
4409 74 0.0847227  -1036 1034 -826
4410 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 826
        810 -808
4411 48 0.117208  -832 831 -1038 1038 810 -808
4412 74 0.0847227  -1036 1034 -827
4413 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 827
        812 -810
4414 48 0.117208  -832 831 -1038 1038 812 -810
4415 74 0.0847227  -1036 1034 -828
4416 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 828
        814 -812
4417 48 0.117208  -832 831 -1038 1038 814 -812
4418 74 0.0847227  -1036 1034 -829
4419 5 0.0582256  ( -1038 : -831 : 832 : 1038 ) -793 792 -1036 1034 829
        816 -814
4420 48 0.117208  -832 831 -1038 1038 816 -814
4421 74 0.0847227  -1033 1031 -836
4422 5 0.0582256  -834 833 -1033 1031 836 -835 -857
4423 74 0.0847227  -1033 1031 -838
4424 5 0.0582256  -834 833 -1033 1031 838 -837 835
4425 74 0.0847227  -1033 1031 -840
4426 5 0.0582256  -834 833 -1033 1031 840 -839 837
4427 74 0.0847227  -1033 1031 -842
4428 5 0.0582256  -834 833 -1033 1031 842 -841 839
4429 74 0.0847227  -1033 1031 -844
4430 5 0.0582256  -834 833 -1033 1031 844 -843 841
4431 74 0.0847227  -1033 1031 -846
4432 5 0.0582256  -834 833 -1033 1031 846 -845 843
4433 74 0.0847227  -1033 1031 -848
4434 5 0.0582256  -834 833 -1033 1031 848 -847 845
4435 74 0.0847227  -1033 1031 -850
4436 5 0.0582256  -834 833 -1033 1031 850 -849 847
4437 74 0.0847227  -1033 1031 -852
4438 5 0.0582256  -834 833 -1033 1031 852 -851 849
4439 74 0.0847227  -1033 1031 -854
4440 5 0.0582256  -834 833 -1033 1031 854 -853 851
4441 74 0.0847227  -1033 1031 -856
4442 5 0.0582256  -834 833 -1033 1031 856 -855 853
4443 74 0.0847227  -1033 1031 -858
4444 5 0.0582256  -834 833 -1033 1031 858 -857 855
4445 74 0.0847227  -1033 1031 -859
4446 5 0.0582256  -834 833 -1033 1031 859 835 857
4447 74 0.0847227  -1033 1031 -860
4448 5 0.0582256  -834 833 -1033 1031 860 837 -835
4449 74 0.0847227  -1033 1031 -861
4450 5 0.0582256  -834 833 -1033 1031 861 839 -837
4451 74 0.0847227  -1033 1031 -862
4452 5 0.0582256  -834 833 -1033 1031 862 841 -839
4453 74 0.0847227  -1033 1031 -863
4454 5 0.0582256  -834 833 -1033 1031 863 843 -841
4455 74 0.0847227  -1033 1031 -864
4456 5 0.0582256  -834 833 -1033 1031 864 845 -843
4457 74 0.0847227  -1033 1031 -865
4458 5 0.0582256  -834 833 -1033 1031 865 847 -845
4459 74 0.0847227  -1033 1031 -866
4460 5 0.0582256  -834 833 -1033 1031 866 849 -847
4461 74 0.0847227  -1033 1031 -867
4462 5 0.0582256  -834 833 -1033 1031 867 851 -849
4463 74 0.0847227  -1033 1031 -868
4464 5 0.0582256  -834 833 -1033 1031 868 853 -851
4465 74 0.0847227  -1033 1031 -869
4466 5 0.0582256  -834 833 -1033 1031 869 855 -853
4467 74 0.0847227  -1033 1031 -870
4468 5 0.0582256  -834 833 -1033 1031 870 857 -855
4469 74 0.0847227  -1032 1034 -836
4470 5 0.0582256  -834 833 -1032 1034 836 -835 -857
4471 74 0.0847227  -1032 1034 -838
4472 5 0.0582256  -834 833 -1032 1034 838 -837 835
4473 74 0.0847227  -1032 1034 -840
4474 5 0.0582256  -834 833 -1032 1034 840 -839 837
4475 74 0.0847227  -1032 1034 -842
4476 5 0.0582256  -834 833 -1032 1034 842 -841 839
4477 74 0.0847227  -1032 1034 -844
4478 5 0.0582256  -834 833 -1032 1034 844 -843 841
4479 74 0.0847227  -1032 1034 -846
4480 5 0.0582256  -834 833 -1032 1034 846 -845 843
4481 74 0.0847227  -1032 1034 -848
4482 5 0.0582256  -834 833 -1032 1034 848 -847 845
4483 74 0.0847227  -1032 1034 -850
4484 5 0.0582256  -834 833 -1032 1034 850 -849 847
4485 74 0.0847227  -1032 1034 -852
4486 5 0.0582256  -834 833 -1032 1034 852 -851 849
4487 74 0.0847227  -1032 1034 -854
4488 5 0.0582256  -834 833 -1032 1034 854 -853 851
4489 74 0.0847227  -1032 1034 -856
4490 5 0.0582256  -834 833 -1032 1034 856 -855 853
4491 74 0.0847227  -1032 1034 -858
4492 5 0.0582256  -834 833 -1032 1034 858 -857 855
4493 74 0.0847227  -1032 1034 -859
4494 5 0.0582256  -834 833 -1032 1034 859 835 857
4495 74 0.0847227  -1032 1034 -860
4496 5 0.0582256  -834 833 -1032 1034 860 837 -835
4497 74 0.0847227  -1032 1034 -861
4498 5 0.0582256  -834 833 -1032 1034 861 839 -837
4499 74 0.0847227  -1032 1034 -862
4500 5 0.0582256  -834 833 -1032 1034 862 841 -839
4501 74 0.0847227  -1032 1034 -863
4502 5 0.0582256  -834 833 -1032 1034 863 843 -841
4503 74 0.0847227  -1032 1034 -864
4504 5 0.0582256  -834 833 -1032 1034 864 845 -843
4505 74 0.0847227  -1032 1034 -865
4506 5 0.0582256  -834 833 -1032 1034 865 847 -845
4507 74 0.0847227  -1032 1034 -866
4508 5 0.0582256  -834 833 -1032 1034 866 849 -847
4509 74 0.0847227  -1032 1034 -867
4510 5 0.0582256  -834 833 -1032 1034 867 851 -849
4511 74 0.0847227  -1032 1034 -868
4512 5 0.0582256  -834 833 -1032 1034 868 853 -851
4513 74 0.0847227  -1032 1034 -869
4514 5 0.0582256  -834 833 -1032 1034 869 855 -853
4515 74 0.0847227  -1032 1034 -870
4516 5 0.0582256  -834 833 -1032 1034 870 857 -855
4517 48 0.117208  -876 874 -1040 1039 -875 873
4518 48 0.117208  -876 874 -1040 1039 -877 875
4519 48 0.117208  -876 874 -1040 1039 -878 877
4520 48 0.117208  -876 874 -1040 1039 -879 878
4521 48 0.117208  -876 874 -1040 1039 -880 879
4522 48 0.117208  -876 874 -1040 1039 873 880
4523 48 0.117208  -876 874 -1040 1039 875 -873
4524 48 0.117208  -876 874 -1040 1039 877 -875
4525 48 0.117208  -876 874 -1040 1039 878 -877
4526 48 0.117208  -876 874 -1040 1039 879 -878
4527 48 0.117208  -876 874 -1040 1039 880 -879
4528 48 0.117208  -876 874 -1040 1039 -873 -880
4529 5 0.0582256  -886 885 -884 883 -1044 1043
4530 0  ( -1043 : 1044 ) -886 885 -884 883 -1042 1041
4531 48 0.117208  -894 893 -887 891 -1044 1043
4532 48 0.117208  -894 893 -892 888 -1044 1043
4533 48 0.117208  893 -889 -888 887 -1044 1043
4534 48 0.117208  -894 890 -888 887 -1044 1043
4535 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -1043
        1041
4536 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -1042
        1044
4537 5 0.0582256  ( -883 : 884 : -885 : 886 ) -890 889 -888 887 -1042
        1041
4538 5 0.0582256  1031 -1033 -833 ( -895 : 896 : -897 : 898 ) -1042
        1041
4539 74 0.0847227  -913 -1042 1041
4540 5 0.0582256  895 -891 1031 -1033 -833 913 -1042 1041 -902 897
4541 74 0.0847227  -914 -1042 1041
4542 5 0.0582256  895 -891 1031 -1033 -833 914 -1042 1041 -904 902
4543 74 0.0847227  -915 -1042 1041
4544 5 0.0582256  895 -891 1031 -1033 -833 915 -1042 1041 -906 904
4545 74 0.0847227  -916 -1042 1041
4546 5 0.0582256  895 -891 1031 -1033 -833 916 -1042 1041 845 906
4547 74 0.0847227  -917 -1042 1041
4548 5 0.0582256  895 -891 1031 -1033 -833 917 -1042 1041 -908 -845
4549 74 0.0847227  -918 -1042 1041
4550 5 0.0582256  895 -891 1031 -1033 -833 918 -1042 1041 -910 908
4551 74 0.0847227  -919 -1042 1041
4552 5 0.0582256  895 -891 1031 -1033 -833 919 -1042 1041 -912 910
4553 74 0.0847227  -920 -1042 1041
4554 5 0.0582256  895 -891 1031 -1033 -833 920 -1042 1041 -898 912
4555 74 0.0847227  -921 -1042 1041
4556 5 0.0582256  -898 894 1031 -1033 -833 921 -1042 1041 -901 891
4557 74 0.0847227  -922 -1042 1041
4558 5 0.0582256  -898 894 1031 -1033 -833 922 -1042 1041 -903 901
4559 74 0.0847227  -923 -1042 1041
4560 5 0.0582256  -898 894 1031 -1033 -833 923 -1042 1041 -905 903
4561 74 0.0847227  -924 -1042 1041
4562 5 0.0582256  -898 894 1031 -1033 -833 924 -1042 1041 857 905
4563 74 0.0847227  -925 -1042 1041
4564 5 0.0582256  -898 894 1031 -1033 -833 925 -1042 1041 -907 -857
4565 74 0.0847227  -926 -1042 1041
4566 5 0.0582256  -898 894 1031 -1033 -833 926 -1042 1041 -909 907
4567 74 0.0847227  -927 -1042 1041
4568 5 0.0582256  -898 894 1031 -1033 -833 927 -1042 1041 -911 909
4569 74 0.0847227  -928 -1042 1041
4570 5 0.0582256  -898 894 1031 -1033 -833 928 -1042 1041 -892 911
4571 74 0.0847227  -929 -1042 1041
4572 5 0.0582256  -896 892 1031 -1033 -833 929 -1042 1041 912 -898
4573 74 0.0847227  -930 -1042 1041
4574 5 0.0582256  -896 892 1031 -1033 -833 930 -1042 1041 910 -912
4575 74 0.0847227  -931 -1042 1041
4576 5 0.0582256  -896 892 1031 -1033 -833 931 -1042 1041 908 -910
4577 74 0.0847227  -932 -1042 1041
4578 5 0.0582256  -896 892 1031 -1033 -833 932 -1042 1041 -845 -908
4579 74 0.0847227  -933 -1042 1041
4580 5 0.0582256  -896 892 1031 -1033 -833 933 -1042 1041 906 845
4581 74 0.0847227  -934 -1042 1041
4582 5 0.0582256  -896 892 1031 -1033 -833 934 -1042 1041 904 -906
4583 74 0.0847227  -935 -1042 1041
4584 5 0.0582256  -896 892 1031 -1033 -833 935 -1042 1041 902 -904
4585 74 0.0847227  -936 -1042 1041
4586 5 0.0582256  -896 892 1031 -1033 -833 936 -1042 1041 897 -902
4587 74 0.0847227  -937 -1042 1041
4588 5 0.0582256  897 -893 1031 -1033 -833 937 -1042 1041 911 -892
4589 74 0.0847227  -938 -1042 1041
4590 5 0.0582256  897 -893 1031 -1033 -833 938 -1042 1041 909 -911
4591 74 0.0847227  -939 -1042 1041
4592 5 0.0582256  897 -893 1031 -1033 -833 939 -1042 1041 907 -909
4593 74 0.0847227  -940 -1042 1041
4594 5 0.0582256  897 -893 1031 -1033 -833 940 -1042 1041 -857 -907
4595 74 0.0847227  -941 -1042 1041
4596 5 0.0582256  897 -893 1031 -1033 -833 941 -1042 1041 905 857
4597 74 0.0847227  -942 -1042 1041
4598 5 0.0582256  897 -893 1031 -1033 -833 942 -1042 1041 903 -905
4599 74 0.0847227  -943 -1042 1041
4600 5 0.0582256  897 -893 1031 -1033 -833 943 -1042 1041 901 -903
4601 74 0.0847227  -944 -1042 1041
4602 5 0.0582256  897 -893 1031 -1033 -833 944 -1042 1041 891 -901
4603 5 0.0582256  -886 885 -884 883 -1048 1047
4604 0  ( -1047 : 1048 ) -886 885 -884 883 -1046 1045
4605 48 0.117208  -894 893 -887 891 -1048 1047
4606 48 0.117208  -894 893 -892 888 -1048 1047
4607 48 0.117208  893 -889 -888 887 -1048 1047
4608 48 0.117208  -894 890 -888 887 -1048 1047
4609 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -1047
        1045
4610 5 0.0582256  ( -887 : 888 : -889 : 890 ) -894 893 -892 891 -1046
        1048
4611 5 0.0582256  ( -883 : 884 : -885 : 886 ) -890 889 -888 887 -1046
        1045
4612 5 0.0582256  1034 -1032 -833 ( -895 : 896 : -897 : 898 ) -1046
        1045
4613 74 0.0847227  -913 -1046 1045
4614 5 0.0582256  895 -891 1034 -1032 -833 913 -1046 1045 -902 897
4615 74 0.0847227  -914 -1046 1045
4616 5 0.0582256  895 -891 1034 -1032 -833 914 -1046 1045 -904 902
4617 74 0.0847227  -915 -1046 1045
4618 5 0.0582256  895 -891 1034 -1032 -833 915 -1046 1045 -906 904
4619 74 0.0847227  -916 -1046 1045
4620 5 0.0582256  895 -891 1034 -1032 -833 916 -1046 1045 845 906
4621 74 0.0847227  -917 -1046 1045
4622 5 0.0582256  895 -891 1034 -1032 -833 917 -1046 1045 -908 -845
4623 74 0.0847227  -918 -1046 1045
4624 5 0.0582256  895 -891 1034 -1032 -833 918 -1046 1045 -910 908
4625 74 0.0847227  -919 -1046 1045
4626 5 0.0582256  895 -891 1034 -1032 -833 919 -1046 1045 -912 910
4627 74 0.0847227  -920 -1046 1045
4628 5 0.0582256  895 -891 1034 -1032 -833 920 -1046 1045 -898 912
4629 74 0.0847227  -921 -1046 1045
4630 5 0.0582256  -898 894 1034 -1032 -833 921 -1046 1045 -901 891
4631 74 0.0847227  -922 -1046 1045
4632 5 0.0582256  -898 894 1034 -1032 -833 922 -1046 1045 -903 901
4633 74 0.0847227  -923 -1046 1045
4634 5 0.0582256  -898 894 1034 -1032 -833 923 -1046 1045 -905 903
4635 74 0.0847227  -924 -1046 1045
4636 5 0.0582256  -898 894 1034 -1032 -833 924 -1046 1045 857 905
4637 74 0.0847227  -925 -1046 1045
4638 5 0.0582256  -898 894 1034 -1032 -833 925 -1046 1045 -907 -857
4639 74 0.0847227  -926 -1046 1045
4640 5 0.0582256  -898 894 1034 -1032 -833 926 -1046 1045 -909 907
4641 74 0.0847227  -927 -1046 1045
4642 5 0.0582256  -898 894 1034 -1032 -833 927 -1046 1045 -911 909
4643 74 0.0847227  -928 -1046 1045
4644 5 0.0582256  -898 894 1034 -1032 -833 928 -1046 1045 -892 911
4645 74 0.0847227  -929 -1046 1045
4646 5 0.0582256  -896 892 1034 -1032 -833 929 -1046 1045 912 -898
4647 74 0.0847227  -930 -1046 1045
4648 5 0.0582256  -896 892 1034 -1032 -833 930 -1046 1045 910 -912
4649 74 0.0847227  -931 -1046 1045
4650 5 0.0582256  -896 892 1034 -1032 -833 931 -1046 1045 908 -910
4651 74 0.0847227  -932 -1046 1045
4652 5 0.0582256  -896 892 1034 -1032 -833 932 -1046 1045 -845 -908
4653 74 0.0847227  -933 -1046 1045
4654 5 0.0582256  -896 892 1034 -1032 -833 933 -1046 1045 906 845
4655 74 0.0847227  -934 -1046 1045
4656 5 0.0582256  -896 892 1034 -1032 -833 934 -1046 1045 904 -906
4657 74 0.0847227  -935 -1046 1045
4658 5 0.0582256  -896 892 1034 -1032 -833 935 -1046 1045 902 -904
4659 74 0.0847227  -936 -1046 1045
4660 5 0.0582256  -896 892 1034 -1032 -833 936 -1046 1045 897 -902
4661 74 0.0847227  -937 -1046 1045
4662 5 0.0582256  897 -893 1034 -1032 -833 937 -1046 1045 911 -892
4663 74 0.0847227  -938 -1046 1045
4664 5 0.0582256  897 -893 1034 -1032 -833 938 -1046 1045 909 -911
4665 74 0.0847227  -939 -1046 1045
4666 5 0.0582256  897 -893 1034 -1032 -833 939 -1046 1045 907 -909
4667 74 0.0847227  -940 -1046 1045
4668 5 0.0582256  897 -893 1034 -1032 -833 940 -1046 1045 -857 -907
4669 74 0.0847227  -941 -1046 1045
4670 5 0.0582256  897 -893 1034 -1032 -833 941 -1046 1045 905 857
4671 74 0.0847227  -942 -1046 1045
4672 5 0.0582256  897 -893 1034 -1032 -833 942 -1046 1045 903 -905
4673 74 0.0847227  -943 -1046 1045
4674 5 0.0582256  897 -893 1034 -1032 -833 943 -1046 1045 901 -903
4675 74 0.0847227  -944 -1046 1045
4676 5 0.0582256  897 -893 1034 -1032 -833 944 -1046 1045 891 -901
4677 73 0.0844385  789 1049 -1050 -951
4678 0  -952 951 -1050 1049 -950 949
4679 47 0.136566  -952 951 -1050 1049 ( 950 : -949 )
4680 0  ( -1056 : 1051 : 680 ) 712 -713 714 -715 1007 -1051
4681 47 0.136566  -1052 1007 -715 714 -712 766
4682 47 0.136566  -1052 1007 -715 714 -767 713
4683 71 0.0843223  -1052 1007 -715 714 -766 718
4684 71 0.0843223  -1052 1007 -715 714 -719 767
4685 71 0.0843223  -1052 1007 -715 714 -718 722
4686 71 0.0843223  -1052 1007 -715 714 -723 719
4687 49 0.074981  -1052 1007 -715 714 -722 726
4688 49 0.074981  -1052 1007 -715 714 -727 723
4689 49 0.074981  -1052 1007 -715 714 -726 730
4690 49 0.074981  -1052 1007 -715 714 -731 727
4691 49 0.074981  -1052 1007 -715 714 -730 733
4692 49 0.074981  -1052 1007 -715 714 -734 731
4693 49 0.074981  -1052 1007 -715 714 -733 736
4694 49 0.074981  -1052 1007 -715 714 -737 734
4695 47 0.136566  -1052 1007 715 -717 -713 712
4696 71 0.0843223  -1052 1007 717 -721 -713 712
4697 71 0.0843223  -1052 1007 721 -725 -713 712
4698 49 0.074981  -1052 1007 725 -729 -713 712
4699 49 0.074981  -1052 1007 729 -732 -713 712
4700 49 0.074981  -1052 1007 732 -735 -713 712
4701 49 0.074981  -1052 1007 735 -738 -713 712
4702 47 0.136566  -1052 1007 716 -714 -737 736
4703 71 0.0843223  -1052 1007 720 -716 -737 736
4704 71 0.0843223  -1052 1007 724 -720 -737 736
4705 49 0.074981  -1052 1007 728 -724 -737 736
4706 47 0.136566  -1052 1007 -717 715 766 -712
4707 47 0.136566  -1052 1007 -717 715 -767 713
4708 71 0.0843223  -1052 1007 -717 715 718 -766
4709 71 0.0843223  -1052 1007 -717 715 -719 767
4710 71 0.0843223  -1052 1007 -717 715 722 -718
4711 71 0.0843223  -1052 1007 -717 715 -723 719
4712 49 0.074981  -1052 1007 -717 715 726 -722
4713 49 0.074981  -1052 1007 -717 715 -727 723
4714 49 0.074981  -1052 1007 -717 715 730 -726
4715 49 0.074981  -1052 1007 -717 715 -731 727
4716 49 0.074981  -1052 1007 -717 715 733 -730
4717 49 0.074981  -1052 1007 -717 715 -734 731
4718 49 0.074981  -1052 1007 -717 715 736 -733
4719 49 0.074981  -1052 1007 -717 715 -737 734
4720 71 0.0843223  -1052 1007 -721 717 766 -712
4721 71 0.0843223  -1052 1007 -721 717 -767 713
4722 71 0.0843223  -1052 1007 -721 717 718 -766
4723 71 0.0843223  -1052 1007 -721 717 -719 767
4724 71 0.0843223  -1052 1007 -721 717 722 -718
4725 71 0.0843223  -1052 1007 -721 717 -723 719
4726 49 0.074981  -1052 1007 -721 717 726 -722
4727 49 0.074981  -1052 1007 -721 717 -727 723
4728 49 0.074981  -1052 1007 -721 717 730 -726
4729 49 0.074981  -1052 1007 -721 717 -731 727
4730 49 0.074981  -1052 1007 -721 717 733 -730
4731 49 0.074981  -1052 1007 -721 717 -734 731
4732 49 0.074981  -1052 1007 -721 717 736 -733
4733 49 0.074981  -1052 1007 -721 717 -737 734
4734 71 0.0843223  -1052 1007 -725 721 766 -712
4735 71 0.0843223  -1052 1007 -725 721 -767 713
4736 71 0.0843223  -1052 1007 -725 721 718 -766
4737 71 0.0843223  -1052 1007 -725 721 -719 767
4738 71 0.0843223  -1052 1007 -725 721 722 -718
4739 71 0.0843223  -1052 1007 -725 721 -723 719
4740 49 0.074981  -1052 1007 -725 721 726 -722
4741 49 0.074981  -1052 1007 -725 721 -727 723
4742 49 0.074981  -1052 1007 -725 721 730 -726
4743 49 0.074981  -1052 1007 -725 721 -731 727
4744 49 0.074981  -1052 1007 -725 721 733 -730
4745 49 0.074981  -1052 1007 -725 721 -734 731
4746 49 0.074981  -1052 1007 -725 721 736 -733
4747 49 0.074981  -1052 1007 -725 721 -737 734
4748 49 0.074981  -1052 1007 -729 725 766 -712
4749 49 0.074981  -1052 1007 -729 725 -767 713
4750 49 0.074981  -1052 1007 -729 725 718 -766
4751 49 0.074981  -1052 1007 -729 725 -719 767
4752 49 0.074981  -1052 1007 -729 725 722 -718
4753 49 0.074981  -1052 1007 -729 725 -723 719
4754 49 0.074981  -1052 1007 -729 725 726 -722
4755 49 0.074981  -1052 1007 -729 725 -727 723
4756 49 0.074981  -1052 1007 -729 725 730 -726
4757 49 0.074981  -1052 1007 -729 725 -731 727
4758 49 0.074981  -1052 1007 -729 725 733 -730
4759 49 0.074981  -1052 1007 -729 725 -734 731
4760 49 0.074981  -1052 1007 -729 725 736 -733
4761 49 0.074981  -1052 1007 -729 725 -737 734
4762 49 0.074981  -1052 1007 -732 729 766 -712
4763 49 0.074981  -1052 1007 -732 729 -767 713
4764 49 0.074981  -1052 1007 -732 729 718 -766
4765 49 0.074981  -1052 1007 -732 729 -719 767
4766 49 0.074981  -1052 1007 -732 729 722 -718
4767 49 0.074981  -1052 1007 -732 729 -723 719
4768 49 0.074981  -1052 1007 -732 729 726 -722
4769 49 0.074981  -1052 1007 -732 729 -727 723
4770 49 0.074981  -1052 1007 -732 729 730 -726
4771 49 0.074981  -1052 1007 -732 729 -731 727
4772 49 0.074981  -1052 1007 -732 729 733 -730
4773 49 0.074981  -1052 1007 -732 729 -734 731
4774 49 0.074981  -1052 1007 -732 729 736 -733
4775 49 0.074981  -1052 1007 -732 729 -737 734
4776 49 0.074981  -1052 1007 -735 732 766 -712
4777 49 0.074981  -1052 1007 -735 732 -767 713
4778 49 0.074981  -1052 1007 -735 732 718 -766
4779 49 0.074981  -1052 1007 -735 732 -719 767
4780 49 0.074981  -1052 1007 -735 732 722 -718
4781 49 0.074981  -1052 1007 -735 732 -723 719
4782 49 0.074981  -1052 1007 -735 732 726 -722
4783 49 0.074981  -1052 1007 -735 732 -727 723
4784 49 0.074981  -1052 1007 -735 732 730 -726
4785 49 0.074981  -1052 1007 -735 732 -731 727
4786 49 0.074981  -1052 1007 -735 732 733 -730
4787 49 0.074981  -1052 1007 -735 732 -734 731
4788 49 0.074981  -1052 1007 -735 732 736 -733
4789 49 0.074981  -1052 1007 -735 732 -737 734
4790 49 0.074981  -1052 1007 -738 735 766 -712
4791 49 0.074981  -1052 1007 -738 735 -767 713
4792 49 0.074981  -1052 1007 -738 735 718 -766
4793 49 0.074981  -1052 1007 -738 735 -719 767
4794 49 0.074981  -1052 1007 -738 735 722 -718
4795 49 0.074981  -1052 1007 -738 735 -723 719
4796 49 0.074981  -1052 1007 -738 735 726 -722
4797 49 0.074981  -1052 1007 -738 735 -727 723
4798 49 0.074981  -1052 1007 -738 735 730 -726
4799 49 0.074981  -1052 1007 -738 735 -731 727
4800 49 0.074981  -1052 1007 -738 735 733 -730
4801 49 0.074981  -1052 1007 -738 735 -734 731
4802 49 0.074981  -1052 1007 -738 735 736 -733
4803 49 0.074981  -1052 1007 -738 735 -737 734
4804 47 0.136566  -1053 1052 -715 714 -712 766
4805 47 0.136566  -1053 1052 -715 714 -767 713
4806 71 0.0843223  -1053 1052 -715 714 -766 718
4807 71 0.0843223  -1053 1052 -715 714 -719 767
4808 71 0.0843223  -1053 1052 -715 714 -718 722
4809 71 0.0843223  -1053 1052 -715 714 -723 719
4810 49 0.074981  -1053 1052 -715 714 -722 726
4811 49 0.074981  -1053 1052 -715 714 -727 723
4812 49 0.074981  -1053 1052 -715 714 -726 730
4813 49 0.074981  -1053 1052 -715 714 -731 727
4814 49 0.074981  -1053 1052 -715 714 -730 733
4815 49 0.074981  -1053 1052 -715 714 -734 731
4816 49 0.074981  -1053 1052 -715 714 -733 736
4817 49 0.074981  -1053 1052 -715 714 -737 734
4818 47 0.136566  -1053 1052 715 -717 -713 712
4819 71 0.0843223  -1053 1052 717 -721 -713 712
4820 71 0.0843223  -1053 1052 721 -725 -713 712
4821 49 0.074981  -1053 1052 725 -729 -713 712
4822 49 0.074981  -1053 1052 729 -732 -713 712
4823 49 0.074981  -1053 1052 732 -735 -713 712
4824 49 0.074981  -1053 1052 735 -738 -713 712
4825 47 0.136566  -1053 1052 716 -714 -737 736
4826 71 0.0843223  -1053 1052 720 -716 -737 736
4827 71 0.0843223  -1053 1052 724 -720 -737 736
4828 49 0.074981  -1053 1052 728 -724 -737 736
4829 47 0.136566  -1053 1052 -717 715 766 -712
4830 47 0.136566  -1053 1052 -717 715 -767 713
4831 71 0.0843223  -1053 1052 -717 715 718 -766
4832 71 0.0843223  -1053 1052 -717 715 -719 767
4833 71 0.0843223  -1053 1052 -717 715 722 -718
4834 71 0.0843223  -1053 1052 -717 715 -723 719
4835 49 0.074981  -1053 1052 -717 715 726 -722
4836 49 0.074981  -1053 1052 -717 715 -727 723
4837 49 0.074981  -1053 1052 -717 715 730 -726
4838 49 0.074981  -1053 1052 -717 715 -731 727
4839 49 0.074981  -1053 1052 -717 715 733 -730
4840 49 0.074981  -1053 1052 -717 715 -734 731
4841 49 0.074981  -1053 1052 -717 715 736 -733
4842 49 0.074981  -1053 1052 -717 715 -737 734
4843 71 0.0843223  -1053 1052 -721 717 766 -712
4844 71 0.0843223  -1053 1052 -721 717 -767 713
4845 71 0.0843223  -1053 1052 -721 717 718 -766
4846 71 0.0843223  -1053 1052 -721 717 -719 767
4847 71 0.0843223  -1053 1052 -721 717 722 -718
4848 71 0.0843223  -1053 1052 -721 717 -723 719
4849 49 0.074981  -1053 1052 -721 717 726 -722
4850 49 0.074981  -1053 1052 -721 717 -727 723
4851 49 0.074981  -1053 1052 -721 717 730 -726
4852 49 0.074981  -1053 1052 -721 717 -731 727
4853 49 0.074981  -1053 1052 -721 717 733 -730
4854 49 0.074981  -1053 1052 -721 717 -734 731
4855 49 0.074981  -1053 1052 -721 717 736 -733
4856 49 0.074981  -1053 1052 -721 717 -737 734
4857 71 0.0843223  -1053 1052 -725 721 766 -712
4858 71 0.0843223  -1053 1052 -725 721 -767 713
4859 71 0.0843223  -1053 1052 -725 721 718 -766
4860 71 0.0843223  -1053 1052 -725 721 -719 767
4861 71 0.0843223  -1053 1052 -725 721 722 -718
4862 71 0.0843223  -1053 1052 -725 721 -723 719
4863 49 0.074981  -1053 1052 -725 721 726 -722
4864 49 0.074981  -1053 1052 -725 721 -727 723
4865 49 0.074981  -1053 1052 -725 721 730 -726
4866 49 0.074981  -1053 1052 -725 721 -731 727
4867 49 0.074981  -1053 1052 -725 721 733 -730
4868 49 0.074981  -1053 1052 -725 721 -734 731
4869 49 0.074981  -1053 1052 -725 721 736 -733
4870 49 0.074981  -1053 1052 -725 721 -737 734
4871 49 0.074981  -1053 1052 -729 725 766 -712
4872 49 0.074981  -1053 1052 -729 725 -767 713
4873 49 0.074981  -1053 1052 -729 725 718 -766
4874 49 0.074981  -1053 1052 -729 725 -719 767
4875 49 0.074981  -1053 1052 -729 725 722 -718
4876 49 0.074981  -1053 1052 -729 725 -723 719
4877 49 0.074981  -1053 1052 -729 725 726 -722
4878 49 0.074981  -1053 1052 -729 725 -727 723
4879 49 0.074981  -1053 1052 -729 725 730 -726
4880 49 0.074981  -1053 1052 -729 725 -731 727
4881 49 0.074981  -1053 1052 -729 725 733 -730
4882 49 0.074981  -1053 1052 -729 725 -734 731
4883 49 0.074981  -1053 1052 -729 725 736 -733
4884 49 0.074981  -1053 1052 -729 725 -737 734
4885 49 0.074981  -1053 1052 -732 729 766 -712
4886 49 0.074981  -1053 1052 -732 729 -767 713
4887 49 0.074981  -1053 1052 -732 729 718 -766
4888 49 0.074981  -1053 1052 -732 729 -719 767
4889 49 0.074981  -1053 1052 -732 729 722 -718
4890 49 0.074981  -1053 1052 -732 729 -723 719
4891 49 0.074981  -1053 1052 -732 729 726 -722
4892 49 0.074981  -1053 1052 -732 729 -727 723
4893 49 0.074981  -1053 1052 -732 729 730 -726
4894 49 0.074981  -1053 1052 -732 729 -731 727
4895 49 0.074981  -1053 1052 -732 729 733 -730
4896 49 0.074981  -1053 1052 -732 729 -734 731
4897 49 0.074981  -1053 1052 -732 729 736 -733
4898 49 0.074981  -1053 1052 -732 729 -737 734
4899 49 0.074981  -1053 1052 -735 732 766 -712
4900 49 0.074981  -1053 1052 -735 732 -767 713
4901 49 0.074981  -1053 1052 -735 732 718 -766
4902 49 0.074981  -1053 1052 -735 732 -719 767
4903 49 0.074981  -1053 1052 -735 732 722 -718
4904 49 0.074981  -1053 1052 -735 732 -723 719
4905 49 0.074981  -1053 1052 -735 732 726 -722
4906 49 0.074981  -1053 1052 -735 732 -727 723
4907 49 0.074981  -1053 1052 -735 732 730 -726
4908 49 0.074981  -1053 1052 -735 732 -731 727
4909 49 0.074981  -1053 1052 -735 732 733 -730
4910 49 0.074981  -1053 1052 -735 732 -734 731
4911 49 0.074981  -1053 1052 -735 732 736 -733
4912 49 0.074981  -1053 1052 -735 732 -737 734
4913 49 0.074981  -1053 1052 -738 735 766 -712
4914 49 0.074981  -1053 1052 -738 735 -767 713
4915 49 0.074981  -1053 1052 -738 735 718 -766
4916 49 0.074981  -1053 1052 -738 735 -719 767
4917 49 0.074981  -1053 1052 -738 735 722 -718
4918 49 0.074981  -1053 1052 -738 735 -723 719
4919 49 0.074981  -1053 1052 -738 735 726 -722
4920 49 0.074981  -1053 1052 -738 735 -727 723
4921 49 0.074981  -1053 1052 -738 735 730 -726
4922 49 0.074981  -1053 1052 -738 735 -731 727
4923 49 0.074981  -1053 1052 -738 735 733 -730
4924 49 0.074981  -1053 1052 -738 735 -734 731
4925 49 0.074981  -1053 1052 -738 735 736 -733
4926 49 0.074981  -1053 1052 -738 735 -737 734
4927 47 0.136566  -1054 1053 -715 714 -712 766
4928 47 0.136566  -1054 1053 -715 714 -767 713
4929 71 0.0843223  -1054 1053 -715 714 -766 718
4930 71 0.0843223  -1054 1053 -715 714 -719 767
4931 71 0.0843223  -1054 1053 -715 714 -718 722
4932 71 0.0843223  -1054 1053 -715 714 -723 719
4933 49 0.074981  -1054 1053 -715 714 -722 726
4934 49 0.074981  -1054 1053 -715 714 -727 723
4935 49 0.074981  -1054 1053 -715 714 -726 730
4936 49 0.074981  -1054 1053 -715 714 -731 727
4937 49 0.074981  -1054 1053 -715 714 -730 733
4938 49 0.074981  -1054 1053 -715 714 -734 731
4939 49 0.074981  -1054 1053 -715 714 -733 736
4940 49 0.074981  -1054 1053 -715 714 -737 734
4941 47 0.136566  -1054 1053 715 -717 -713 712
4942 71 0.0843223  -1054 1053 717 -721 -713 712
4943 71 0.0843223  -1054 1053 721 -725 -713 712
4944 49 0.074981  -1054 1053 725 -729 -713 712
4945 49 0.074981  -1054 1053 729 -732 -713 712
4946 49 0.074981  -1054 1053 732 -735 -713 712
4947 49 0.074981  -1054 1053 735 -738 -713 712
4948 47 0.136566  -1054 1053 716 -714 -737 736
4949 71 0.0843223  -1054 1053 720 -716 -737 736
4950 71 0.0843223  -1054 1053 724 -720 -737 736
4951 49 0.074981  -1054 1053 728 -724 -737 736
4952 47 0.136566  -1054 1053 -717 715 766 -712
4953 47 0.136566  -1054 1053 -717 715 -767 713
4954 71 0.0843223  -1054 1053 -717 715 718 -766
4955 71 0.0843223  -1054 1053 -717 715 -719 767
4956 71 0.0843223  -1054 1053 -717 715 722 -718
4957 71 0.0843223  -1054 1053 -717 715 -723 719
4958 49 0.074981  -1054 1053 -717 715 726 -722
4959 49 0.074981  -1054 1053 -717 715 -727 723
4960 49 0.074981  -1054 1053 -717 715 730 -726
4961 49 0.074981  -1054 1053 -717 715 -731 727
4962 49 0.074981  -1054 1053 -717 715 733 -730
4963 49 0.074981  -1054 1053 -717 715 -734 731
4964 49 0.074981  -1054 1053 -717 715 736 -733
4965 49 0.074981  -1054 1053 -717 715 -737 734
4966 71 0.0843223  -1054 1053 -721 717 766 -712
4967 71 0.0843223  -1054 1053 -721 717 -767 713
4968 71 0.0843223  -1054 1053 -721 717 718 -766
4969 71 0.0843223  -1054 1053 -721 717 -719 767
4970 71 0.0843223  -1054 1053 -721 717 722 -718
4971 71 0.0843223  -1054 1053 -721 717 -723 719
4972 49 0.074981  -1054 1053 -721 717 726 -722
4973 49 0.074981  -1054 1053 -721 717 -727 723
4974 49 0.074981  -1054 1053 -721 717 730 -726
4975 49 0.074981  -1054 1053 -721 717 -731 727
4976 49 0.074981  -1054 1053 -721 717 733 -730
4977 49 0.074981  -1054 1053 -721 717 -734 731
4978 49 0.074981  -1054 1053 -721 717 736 -733
4979 49 0.074981  -1054 1053 -721 717 -737 734
4980 71 0.0843223  -1054 1053 -725 721 766 -712
4981 71 0.0843223  -1054 1053 -725 721 -767 713
4982 71 0.0843223  -1054 1053 -725 721 718 -766
4983 71 0.0843223  -1054 1053 -725 721 -719 767
4984 71 0.0843223  -1054 1053 -725 721 722 -718
4985 71 0.0843223  -1054 1053 -725 721 -723 719
4986 49 0.074981  -1054 1053 -725 721 726 -722
4987 49 0.074981  -1054 1053 -725 721 -727 723
4988 49 0.074981  -1054 1053 -725 721 730 -726
4989 49 0.074981  -1054 1053 -725 721 -731 727
4990 49 0.074981  -1054 1053 -725 721 733 -730
4991 49 0.074981  -1054 1053 -725 721 -734 731
4992 49 0.074981  -1054 1053 -725 721 736 -733
4993 49 0.074981  -1054 1053 -725 721 -737 734
4994 49 0.074981  -1054 1053 -729 725 766 -712
4995 49 0.074981  -1054 1053 -729 725 -767 713
4996 49 0.074981  -1054 1053 -729 725 718 -766
4997 49 0.074981  -1054 1053 -729 725 -719 767
4998 49 0.074981  -1054 1053 -729 725 722 -718
4999 49 0.074981  -1054 1053 -729 725 -723 719
5000 49 0.074981  -1054 1053 -729 725 726 -722
5001 49 0.074981  -1054 1053 -729 725 -727 723
5002 49 0.074981  -1054 1053 -729 725 730 -726
5003 49 0.074981  -1054 1053 -729 725 -731 727
5004 49 0.074981  -1054 1053 -729 725 733 -730
5005 49 0.074981  -1054 1053 -729 725 -734 731
5006 49 0.074981  -1054 1053 -729 725 736 -733
5007 49 0.074981  -1054 1053 -729 725 -737 734
5008 49 0.074981  -1054 1053 -732 729 766 -712
5009 49 0.074981  -1054 1053 -732 729 -767 713
5010 49 0.074981  -1054 1053 -732 729 718 -766
5011 49 0.074981  -1054 1053 -732 729 -719 767
5012 49 0.074981  -1054 1053 -732 729 722 -718
5013 49 0.074981  -1054 1053 -732 729 -723 719
5014 49 0.074981  -1054 1053 -732 729 726 -722
5015 49 0.074981  -1054 1053 -732 729 -727 723
5016 49 0.074981  -1054 1053 -732 729 730 -726
5017 49 0.074981  -1054 1053 -732 729 -731 727
5018 49 0.074981  -1054 1053 -732 729 733 -730
5019 49 0.074981  -1054 1053 -732 729 -734 731
5020 49 0.074981  -1054 1053 -732 729 736 -733
5021 49 0.074981  -1054 1053 -732 729 -737 734
5022 49 0.074981  -1054 1053 -735 732 766 -712
5023 49 0.074981  -1054 1053 -735 732 -767 713
5024 49 0.074981  -1054 1053 -735 732 718 -766
5025 49 0.074981  -1054 1053 -735 732 -719 767
5026 49 0.074981  -1054 1053 -735 732 722 -718
5027 49 0.074981  -1054 1053 -735 732 -723 719
5028 49 0.074981  -1054 1053 -735 732 726 -722
5029 49 0.074981  -1054 1053 -735 732 -727 723
5030 49 0.074981  -1054 1053 -735 732 730 -726
5031 49 0.074981  -1054 1053 -735 732 -731 727
5032 49 0.074981  -1054 1053 -735 732 733 -730
5033 49 0.074981  -1054 1053 -735 732 -734 731
5034 49 0.074981  -1054 1053 -735 732 736 -733
5035 49 0.074981  -1054 1053 -735 732 -737 734
5036 49 0.074981  -1054 1053 -738 735 766 -712
5037 49 0.074981  -1054 1053 -738 735 -767 713
5038 49 0.074981  -1054 1053 -738 735 718 -766
5039 49 0.074981  -1054 1053 -738 735 -719 767
5040 49 0.074981  -1054 1053 -738 735 722 -718
5041 49 0.074981  -1054 1053 -738 735 -723 719
5042 49 0.074981  -1054 1053 -738 735 726 -722
5043 49 0.074981  -1054 1053 -738 735 -727 723
5044 49 0.074981  -1054 1053 -738 735 730 -726
5045 49 0.074981  -1054 1053 -738 735 -731 727
5046 49 0.074981  -1054 1053 -738 735 733 -730
5047 49 0.074981  -1054 1053 -738 735 -734 731
5048 49 0.074981  -1054 1053 -738 735 736 -733
5049 49 0.074981  -1054 1053 -738 735 -737 734
5050 47 0.136566  -1055 1054 -715 714 -712 766
5051 47 0.136566  -1055 1054 -715 714 -767 713
5052 71 0.0843223  -1055 1054 -715 714 -766 718
5053 71 0.0843223  -1055 1054 -715 714 -719 767
5054 71 0.0843223  -1055 1054 -715 714 -718 722
5055 71 0.0843223  -1055 1054 -715 714 -723 719
5056 49 0.074981  -1055 1054 -715 714 -722 726
5057 49 0.074981  -1055 1054 -715 714 -727 723
5058 49 0.074981  -1055 1054 -715 714 -726 730
5059 49 0.074981  -1055 1054 -715 714 -731 727
5060 49 0.074981  -1055 1054 -715 714 -730 733
5061 49 0.074981  -1055 1054 -715 714 -734 731
5062 49 0.074981  -1055 1054 -715 714 -733 736
5063 49 0.074981  -1055 1054 -715 714 -737 734
5064 47 0.136566  -1055 1054 715 -717 -713 712
5065 71 0.0843223  -1055 1054 717 -721 -713 712
5066 71 0.0843223  -1055 1054 721 -725 -713 712
5067 49 0.074981  -1055 1054 725 -729 -713 712
5068 49 0.074981  -1055 1054 729 -732 -713 712
5069 49 0.074981  -1055 1054 732 -735 -713 712
5070 49 0.074981  -1055 1054 735 -738 -713 712
5071 47 0.136566  -1055 1054 716 -714 -737 736
5072 71 0.0843223  -1055 1054 720 -716 -737 736
5073 71 0.0843223  -1055 1054 724 -720 -737 736
5074 49 0.074981  -1055 1054 728 -724 -737 736
5075 47 0.136566  -1055 1054 -717 715 766 -712
5076 47 0.136566  -1055 1054 -717 715 -767 713
5077 71 0.0843223  -1055 1054 -717 715 718 -766
5078 71 0.0843223  -1055 1054 -717 715 -719 767
5079 71 0.0843223  -1055 1054 -717 715 722 -718
5080 71 0.0843223  -1055 1054 -717 715 -723 719
5081 49 0.074981  -1055 1054 -717 715 726 -722
5082 49 0.074981  -1055 1054 -717 715 -727 723
5083 49 0.074981  -1055 1054 -717 715 730 -726
5084 49 0.074981  -1055 1054 -717 715 -731 727
5085 49 0.074981  -1055 1054 -717 715 733 -730
5086 49 0.074981  -1055 1054 -717 715 -734 731
5087 49 0.074981  -1055 1054 -717 715 736 -733
5088 49 0.074981  -1055 1054 -717 715 -737 734
5089 71 0.0843223  -1055 1054 -721 717 766 -712
5090 71 0.0843223  -1055 1054 -721 717 -767 713
5091 71 0.0843223  -1055 1054 -721 717 718 -766
5092 71 0.0843223  -1055 1054 -721 717 -719 767
5093 71 0.0843223  -1055 1054 -721 717 722 -718
5094 71 0.0843223  -1055 1054 -721 717 -723 719
5095 49 0.074981  -1055 1054 -721 717 726 -722
5096 49 0.074981  -1055 1054 -721 717 -727 723
5097 49 0.074981  -1055 1054 -721 717 730 -726
5098 49 0.074981  -1055 1054 -721 717 -731 727
5099 49 0.074981  -1055 1054 -721 717 733 -730
5100 49 0.074981  -1055 1054 -721 717 -734 731
5101 49 0.074981  -1055 1054 -721 717 736 -733
5102 49 0.074981  -1055 1054 -721 717 -737 734
5103 71 0.0843223  -1055 1054 -725 721 766 -712
5104 71 0.0843223  -1055 1054 -725 721 -767 713
5105 71 0.0843223  -1055 1054 -725 721 718 -766
5106 71 0.0843223  -1055 1054 -725 721 -719 767
5107 71 0.0843223  -1055 1054 -725 721 722 -718
5108 71 0.0843223  -1055 1054 -725 721 -723 719
5109 49 0.074981  -1055 1054 -725 721 726 -722
5110 49 0.074981  -1055 1054 -725 721 -727 723
5111 49 0.074981  -1055 1054 -725 721 730 -726
5112 49 0.074981  -1055 1054 -725 721 -731 727
5113 49 0.074981  -1055 1054 -725 721 733 -730
5114 49 0.074981  -1055 1054 -725 721 -734 731
5115 49 0.074981  -1055 1054 -725 721 736 -733
5116 49 0.074981  -1055 1054 -725 721 -737 734
5117 49 0.074981  -1055 1054 -729 725 766 -712
5118 49 0.074981  -1055 1054 -729 725 -767 713
5119 49 0.074981  -1055 1054 -729 725 718 -766
5120 49 0.074981  -1055 1054 -729 725 -719 767
5121 49 0.074981  -1055 1054 -729 725 722 -718
5122 49 0.074981  -1055 1054 -729 725 -723 719
5123 49 0.074981  -1055 1054 -729 725 726 -722
5124 49 0.074981  -1055 1054 -729 725 -727 723
5125 49 0.074981  -1055 1054 -729 725 730 -726
5126 49 0.074981  -1055 1054 -729 725 -731 727
5127 49 0.074981  -1055 1054 -729 725 733 -730
5128 49 0.074981  -1055 1054 -729 725 -734 731
5129 49 0.074981  -1055 1054 -729 725 736 -733
5130 49 0.074981  -1055 1054 -729 725 -737 734
5131 49 0.074981  -1055 1054 -732 729 766 -712
5132 49 0.074981  -1055 1054 -732 729 -767 713
5133 49 0.074981  -1055 1054 -732 729 718 -766
5134 49 0.074981  -1055 1054 -732 729 -719 767
5135 49 0.074981  -1055 1054 -732 729 722 -718
5136 49 0.074981  -1055 1054 -732 729 -723 719
5137 49 0.074981  -1055 1054 -732 729 726 -722
5138 49 0.074981  -1055 1054 -732 729 -727 723
5139 49 0.074981  -1055 1054 -732 729 730 -726
5140 49 0.074981  -1055 1054 -732 729 -731 727
5141 49 0.074981  -1055 1054 -732 729 733 -730
5142 49 0.074981  -1055 1054 -732 729 -734 731
5143 49 0.074981  -1055 1054 -732 729 736 -733
5144 49 0.074981  -1055 1054 -732 729 -737 734
5145 49 0.074981  -1055 1054 -735 732 766 -712
5146 49 0.074981  -1055 1054 -735 732 -767 713
5147 49 0.074981  -1055 1054 -735 732 718 -766
5148 49 0.074981  -1055 1054 -735 732 -719 767
5149 49 0.074981  -1055 1054 -735 732 722 -718
5150 49 0.074981  -1055 1054 -735 732 -723 719
5151 49 0.074981  -1055 1054 -735 732 726 -722
5152 49 0.074981  -1055 1054 -735 732 -727 723
5153 49 0.074981  -1055 1054 -735 732 730 -726
5154 49 0.074981  -1055 1054 -735 732 -731 727
5155 49 0.074981  -1055 1054 -735 732 733 -730
5156 49 0.074981  -1055 1054 -735 732 -734 731
5157 49 0.074981  -1055 1054 -735 732 736 -733
5158 49 0.074981  -1055 1054 -735 732 -737 734
5159 49 0.074981  -1055 1054 -738 735 766 -712
5160 49 0.074981  -1055 1054 -738 735 -767 713
5161 49 0.074981  -1055 1054 -738 735 718 -766
5162 49 0.074981  -1055 1054 -738 735 -719 767
5163 49 0.074981  -1055 1054 -738 735 722 -718
5164 49 0.074981  -1055 1054 -738 735 -723 719
5165 49 0.074981  -1055 1054 -738 735 726 -722
5166 49 0.074981  -1055 1054 -738 735 -727 723
5167 49 0.074981  -1055 1054 -738 735 730 -726
5168 49 0.074981  -1055 1054 -738 735 -731 727
5169 49 0.074981  -1055 1054 -738 735 733 -730
5170 49 0.074981  -1055 1054 -738 735 -734 731
5171 49 0.074981  -1055 1054 -738 735 736 -733
5172 49 0.074981  -1055 1054 -738 735 -737 734
5173 47 0.136566  -1051 1055 -715 714 -712 766
5174 47 0.136566  -1051 1055 -715 714 -767 713
5175 71 0.0843223  -1051 1055 -715 714 -766 718
5176 71 0.0843223  -1051 1055 -715 714 -719 767
5177 71 0.0843223  -1051 1055 -715 714 -718 722
5178 71 0.0843223  -1051 1055 -715 714 -723 719
5179 49 0.074981  -1051 1055 -715 714 -722 726
5180 49 0.074981  -1051 1055 -715 714 -727 723
5181 49 0.074981  -1051 1055 -715 714 -726 730
5182 49 0.074981  -1051 1055 -715 714 -731 727
5183 49 0.074981  -1051 1055 -715 714 -730 733
5184 49 0.074981  -1051 1055 -715 714 -734 731
5185 49 0.074981  -1051 1055 -715 714 -733 736
5186 49 0.074981  -1051 1055 -715 714 -737 734
5187 47 0.136566  -1051 1055 715 -717 -713 712
5188 71 0.0843223  -1051 1055 717 -721 -713 712
5189 71 0.0843223  -1051 1055 721 -725 -713 712
5190 49 0.074981  -1051 1055 725 -729 -713 712
5191 49 0.074981  -1051 1055 729 -732 -713 712
5192 49 0.074981  -1051 1055 732 -735 -713 712
5193 49 0.074981  -1051 1055 735 -738 -713 712
5194 47 0.136566  -1051 1055 716 -714 -737 736
5195 71 0.0843223  -1051 1055 720 -716 -737 736
5196 71 0.0843223  -1051 1055 724 -720 -737 736
5197 49 0.074981  -1051 1055 728 -724 -737 736
5198 47 0.136566  -1051 1055 -717 715 766 -712
5199 47 0.136566  -1051 1055 -717 715 -767 713
5200 71 0.0843223  -1051 1055 -717 715 718 -766
5201 71 0.0843223  -1051 1055 -717 715 -719 767
5202 71 0.0843223  -1051 1055 -717 715 722 -718
5203 71 0.0843223  -1051 1055 -717 715 -723 719
5204 49 0.074981  -1051 1055 -717 715 726 -722
5205 49 0.074981  -1051 1055 -717 715 -727 723
5206 49 0.074981  -1051 1055 -717 715 730 -726
5207 49 0.074981  -1051 1055 -717 715 -731 727
5208 49 0.074981  -1051 1055 -717 715 733 -730
5209 49 0.074981  -1051 1055 -717 715 -734 731
5210 49 0.074981  -1051 1055 -717 715 736 -733
5211 49 0.074981  -1051 1055 -717 715 -737 734
5212 71 0.0843223  -1051 1055 -721 717 766 -712
5213 71 0.0843223  -1051 1055 -721 717 -767 713
5214 71 0.0843223  -1051 1055 -721 717 718 -766
5215 71 0.0843223  -1051 1055 -721 717 -719 767
5216 71 0.0843223  -1051 1055 -721 717 722 -718
5217 71 0.0843223  -1051 1055 -721 717 -723 719
5218 49 0.074981  -1051 1055 -721 717 726 -722
5219 49 0.074981  -1051 1055 -721 717 -727 723
5220 49 0.074981  -1051 1055 -721 717 730 -726
5221 49 0.074981  -1051 1055 -721 717 -731 727
5222 49 0.074981  -1051 1055 -721 717 733 -730
5223 49 0.074981  -1051 1055 -721 717 -734 731
5224 49 0.074981  -1051 1055 -721 717 736 -733
5225 49 0.074981  -1051 1055 -721 717 -737 734
5226 71 0.0843223  -1051 1055 -725 721 766 -712
5227 71 0.0843223  -1051 1055 -725 721 -767 713
5228 71 0.0843223  -1051 1055 -725 721 718 -766
5229 71 0.0843223  -1051 1055 -725 721 -719 767
5230 71 0.0843223  -1051 1055 -725 721 722 -718
5231 71 0.0843223  -1051 1055 -725 721 -723 719
5232 49 0.074981  -1051 1055 -725 721 726 -722
5233 49 0.074981  -1051 1055 -725 721 -727 723
5234 49 0.074981  -1051 1055 -725 721 730 -726
5235 49 0.074981  -1051 1055 -725 721 -731 727
5236 49 0.074981  -1051 1055 -725 721 733 -730
5237 49 0.074981  -1051 1055 -725 721 -734 731
5238 49 0.074981  -1051 1055 -725 721 736 -733
5239 49 0.074981  -1051 1055 -725 721 -737 734
5240 49 0.074981  -1051 1055 -729 725 766 -712
5241 49 0.074981  -1051 1055 -729 725 -767 713
5242 49 0.074981  -1051 1055 -729 725 718 -766
5243 49 0.074981  -1051 1055 -729 725 -719 767
5244 49 0.074981  -1051 1055 -729 725 722 -718
5245 49 0.074981  -1051 1055 -729 725 -723 719
5246 49 0.074981  -1051 1055 -729 725 726 -722
5247 49 0.074981  -1051 1055 -729 725 -727 723
5248 49 0.074981  -1051 1055 -729 725 730 -726
5249 49 0.074981  -1051 1055 -729 725 -731 727
5250 49 0.074981  -1051 1055 -729 725 733 -730
5251 49 0.074981  -1051 1055 -729 725 -734 731
5252 49 0.074981  -1051 1055 -729 725 736 -733
5253 49 0.074981  -1051 1055 -729 725 -737 734
5254 49 0.074981  -1051 1055 -732 729 766 -712
5255 49 0.074981  -1051 1055 -732 729 -767 713
5256 49 0.074981  -1051 1055 -732 729 718 -766
5257 49 0.074981  -1051 1055 -732 729 -719 767
5258 49 0.074981  -1051 1055 -732 729 722 -718
5259 49 0.074981  -1051 1055 -732 729 -723 719
5260 49 0.074981  -1051 1055 -732 729 726 -722
5261 49 0.074981  -1051 1055 -732 729 -727 723
5262 49 0.074981  -1051 1055 -732 729 730 -726
5263 49 0.074981  -1051 1055 -732 729 -731 727
5264 49 0.074981  -1051 1055 -732 729 733 -730
5265 49 0.074981  -1051 1055 -732 729 -734 731
5266 49 0.074981  -1051 1055 -732 729 736 -733
5267 49 0.074981  -1051 1055 -732 729 -737 734
5268 49 0.074981  -1051 1055 -735 732 766 -712
5269 49 0.074981  -1051 1055 -735 732 -767 713
5270 49 0.074981  -1051 1055 -735 732 718 -766
5271 49 0.074981  -1051 1055 -735 732 -719 767
5272 49 0.074981  -1051 1055 -735 732 722 -718
5273 49 0.074981  -1051 1055 -735 732 -723 719
5274 49 0.074981  -1051 1055 -735 732 726 -722
5275 49 0.074981  -1051 1055 -735 732 -727 723
5276 49 0.074981  -1051 1055 -735 732 730 -726
5277 49 0.074981  -1051 1055 -735 732 -731 727
5278 49 0.074981  -1051 1055 -735 732 733 -730
5279 49 0.074981  -1051 1055 -735 732 -734 731
5280 49 0.074981  -1051 1055 -735 732 736 -733
5281 49 0.074981  -1051 1055 -735 732 -737 734
5282 49 0.074981  -1051 1055 -738 735 766 -712
5283 49 0.074981  -1051 1055 -738 735 -767 713
5284 49 0.074981  -1051 1055 -738 735 718 -766
5285 49 0.074981  -1051 1055 -738 735 -719 767
5286 49 0.074981  -1051 1055 -738 735 722 -718
5287 49 0.074981  -1051 1055 -738 735 -723 719
5288 49 0.074981  -1051 1055 -738 735 726 -722
5289 49 0.074981  -1051 1055 -738 735 -727 723
5290 49 0.074981  -1051 1055 -738 735 730 -726
5291 49 0.074981  -1051 1055 -738 735 -731 727
5292 49 0.074981  -1051 1055 -738 735 733 -730
5293 49 0.074981  -1051 1055 -738 735 -734 731
5294 49 0.074981  -1051 1055 -738 735 736 -733
5295 49 0.074981  -1051 1055 -738 735 -737 734
5296 83 0.0499651  -1060 1059 -683
5297 83 0.0499651  -1061 1062 -683
5298 0  ( -1057 : -689 : -688 : -598 : 599 : 1063 ) -676 1056 -1051 (
        -1059 : 1060 : 683 ) ( -1062 : 1061 : 683 )
5299 5 0.0582256  676 -677 -1058 1057
5300 5 0.0582256  ( -1059 : 1060 : 683 ) 1056 676 -680 -1057
5301 5 0.0582256  ( -1062 : 1061 : 683 ) -1051 676 -680 1058
5302 0  677 -680 -1058 1057
5303 0  -1063 1057 687 -595 686 594
5304 73 0.0844385  ( -594 : -686 : -687 : 595 ) -1063 1057 689 -599 688
        598
5305 0  -1064 1051 687 -595 686 594
5306 73 0.0844385  ( -594 : -686 : -687 : 595 ) -1064 1051 689 -599 688
        598
5307 0  ( -1051 : -689 : -688 : -598 : 599 : 1064 ) ( ( 1065 -1066 1067
        -1068 1069 -1070 ) : ( 1066 -1085 -1088 ) )
5308 47 0.136566  -1088 1085 -1086 1066
5309 5 0.0582256  -1088 1086 -1087 1066
5310 47 0.136566  ( 1066 : -1067 : 1068 : -1069 : 1070 ) 1087 -1075
        1069 -1074 1073 -1072 1065
5311 47 0.136566  ( -1089 : -1091 : 1092 : 1090 ) -1065 1071 1073 -1074
        1069 -1075
5312 49 0.074981  ( 1072 : -1073 : 1074 : -1069 : 1075 ) 1087 -1080
        1069 -1079 1078 -1077 1071
5313 49 0.074981  ( -1089 : -1091 : 1092 : 1090 ) -1071 1076 1078 -1079
        1069 -1080
5314 3 0.0878729  ( 1077 : -1078 : 1079 : -1069 : 1080 ) 1087 -1084
        1069 -1083 1082 -1093 1076
5315 3 0.0878729  ( -1089 : -1091 : 1092 : 1090 ) -1076 1081 1082 -1083
        1069 -1084
5316 0  ( -1051 : -689 : -688 : -598 : 599 : 1064 ) 1089 -1090 1091
        -1092 1081 -1065
5317 0  ( -1093 : 1088 : 1087 ) ( -1081 : -1082 : -1069 : 1084 : 1083 :
        1093 ) 1095 -1096 1097 -1098 1093 -1094
5318 49 0.074981  -1099 1093 -1098 1097 -1095 1100
5319 49 0.074981  -1099 1093 -1098 1097 -1101 1096
5320 49 0.074981  -1099 1093 -1098 1097 -1100 1105
5321 49 0.074981  -1099 1093 -1098 1097 -1106 1101
5322 49 0.074981  -1099 1093 -1098 1097 -1105 1110
5323 49 0.074981  -1099 1093 -1098 1097 -1111 1106
5324 49 0.074981  -1099 1093 -1098 1097 -1110 1115
5325 49 0.074981  -1099 1093 -1098 1097 -1116 1111
5326 49 0.074981  -1099 1093 -1098 1097 -1115 1119
5327 49 0.074981  -1099 1093 -1098 1097 -1120 1116
5328 49 0.074981  -1099 1093 -1098 1097 -1119 1123
5329 49 0.074981  -1099 1093 -1098 1097 -1124 1120
5330 49 0.074981  -1099 1093 -1098 1097 -1123 1127
5331 49 0.074981  -1099 1093 -1098 1097 -1128 1124
5332 49 0.074981  -1099 1093 1098 -1103 -1096 1095
5333 49 0.074981  -1099 1093 1103 -1108 -1096 1095
5334 49 0.074981  -1099 1093 1108 -1113 -1096 1095
5335 49 0.074981  -1099 1093 1113 -1118 -1096 1095
5336 49 0.074981  -1099 1093 1118 -1122 -1096 1095
5337 49 0.074981  -1099 1093 1122 -1126 -1096 1095
5338 49 0.074981  -1099 1093 1126 -1130 -1096 1095
5339 49 0.074981  -1099 1093 1102 -1097 -1128 1127
5340 49 0.074981  -1099 1093 1107 -1102 -1128 1127
5341 49 0.074981  -1099 1093 1112 -1107 -1128 1127
5342 49 0.074981  -1099 1093 1117 -1112 -1128 1127
5343 49 0.074981  -1099 1093 1121 -1117 -1128 1127
5344 49 0.074981  -1099 1093 1125 -1121 -1128 1127
5345 49 0.074981  -1099 1093 1129 -1125 -1128 1127
5346 49 0.074981  -1099 1093 -1103 1098 1100 -1095
5347 49 0.074981  -1099 1093 -1103 1098 -1101 1096
5348 49 0.074981  -1099 1093 -1103 1098 1105 -1100
5349 49 0.074981  -1099 1093 -1103 1098 -1106 1101
5350 49 0.074981  -1099 1093 -1103 1098 1110 -1105
5351 49 0.074981  -1099 1093 -1103 1098 -1111 1106
5352 49 0.074981  -1099 1093 -1103 1098 1115 -1110
5353 49 0.074981  -1099 1093 -1103 1098 -1116 1111
5354 49 0.074981  -1099 1093 -1103 1098 1119 -1115
5355 49 0.074981  -1099 1093 -1103 1098 -1120 1116
5356 49 0.074981  -1099 1093 -1103 1098 1123 -1119
5357 49 0.074981  -1099 1093 -1103 1098 -1124 1120
5358 49 0.074981  -1099 1093 -1103 1098 1127 -1123
5359 49 0.074981  -1099 1093 -1103 1098 -1128 1124
5360 49 0.074981  -1099 1093 -1108 1103 1100 -1095
5361 49 0.074981  -1099 1093 -1108 1103 -1101 1096
5362 49 0.074981  -1099 1093 -1108 1103 1105 -1100
5363 49 0.074981  -1099 1093 -1108 1103 -1106 1101
5364 49 0.074981  -1099 1093 -1108 1103 1110 -1105
5365 49 0.074981  -1099 1093 -1108 1103 -1111 1106
5366 49 0.074981  -1099 1093 -1108 1103 1115 -1110
5367 49 0.074981  -1099 1093 -1108 1103 -1116 1111
5368 49 0.074981  -1099 1093 -1108 1103 1119 -1115
5369 49 0.074981  -1099 1093 -1108 1103 -1120 1116
5370 49 0.074981  -1099 1093 -1108 1103 1123 -1119
5371 49 0.074981  -1099 1093 -1108 1103 -1124 1120
5372 49 0.074981  -1099 1093 -1108 1103 1127 -1123
5373 49 0.074981  -1099 1093 -1108 1103 -1128 1124
5374 49 0.074981  -1099 1093 -1113 1108 1100 -1095
5375 49 0.074981  -1099 1093 -1113 1108 -1101 1096
5376 49 0.074981  -1099 1093 -1113 1108 1105 -1100
5377 49 0.074981  -1099 1093 -1113 1108 -1106 1101
5378 49 0.074981  -1099 1093 -1113 1108 1110 -1105
5379 49 0.074981  -1099 1093 -1113 1108 -1111 1106
5380 49 0.074981  -1099 1093 -1113 1108 1115 -1110
5381 49 0.074981  -1099 1093 -1113 1108 -1116 1111
5382 49 0.074981  -1099 1093 -1113 1108 1119 -1115
5383 49 0.074981  -1099 1093 -1113 1108 -1120 1116
5384 49 0.074981  -1099 1093 -1113 1108 1123 -1119
5385 49 0.074981  -1099 1093 -1113 1108 -1124 1120
5386 49 0.074981  -1099 1093 -1113 1108 1127 -1123
5387 49 0.074981  -1099 1093 -1113 1108 -1128 1124
5388 49 0.074981  -1099 1093 -1118 1113 1100 -1095
5389 49 0.074981  -1099 1093 -1118 1113 -1101 1096
5390 49 0.074981  -1099 1093 -1118 1113 1105 -1100
5391 49 0.074981  -1099 1093 -1118 1113 -1106 1101
5392 49 0.074981  -1099 1093 -1118 1113 1110 -1105
5393 49 0.074981  -1099 1093 -1118 1113 -1111 1106
5394 49 0.074981  -1099 1093 -1118 1113 1115 -1110
5395 49 0.074981  -1099 1093 -1118 1113 -1116 1111
5396 49 0.074981  -1099 1093 -1118 1113 1119 -1115
5397 49 0.074981  -1099 1093 -1118 1113 -1120 1116
5398 49 0.074981  -1099 1093 -1118 1113 1123 -1119
5399 49 0.074981  -1099 1093 -1118 1113 -1124 1120
5400 49 0.074981  -1099 1093 -1118 1113 1127 -1123
5401 49 0.074981  -1099 1093 -1118 1113 -1128 1124
5402 49 0.074981  -1099 1093 -1122 1118 1100 -1095
5403 49 0.074981  -1099 1093 -1122 1118 -1101 1096
5404 49 0.074981  -1099 1093 -1122 1118 1105 -1100
5405 49 0.074981  -1099 1093 -1122 1118 -1106 1101
5406 49 0.074981  -1099 1093 -1122 1118 1110 -1105
5407 49 0.074981  -1099 1093 -1122 1118 -1111 1106
5408 49 0.074981  -1099 1093 -1122 1118 1115 -1110
5409 49 0.074981  -1099 1093 -1122 1118 -1116 1111
5410 49 0.074981  -1099 1093 -1122 1118 1119 -1115
5411 49 0.074981  -1099 1093 -1122 1118 -1120 1116
5412 49 0.074981  -1099 1093 -1122 1118 1123 -1119
5413 49 0.074981  -1099 1093 -1122 1118 -1124 1120
5414 49 0.074981  -1099 1093 -1122 1118 1127 -1123
5415 49 0.074981  -1099 1093 -1122 1118 -1128 1124
5416 49 0.074981  -1099 1093 -1126 1122 1100 -1095
5417 49 0.074981  -1099 1093 -1126 1122 -1101 1096
5418 49 0.074981  -1099 1093 -1126 1122 1105 -1100
5419 49 0.074981  -1099 1093 -1126 1122 -1106 1101
5420 49 0.074981  -1099 1093 -1126 1122 1110 -1105
5421 49 0.074981  -1099 1093 -1126 1122 -1111 1106
5422 49 0.074981  -1099 1093 -1126 1122 1115 -1110
5423 49 0.074981  -1099 1093 -1126 1122 -1116 1111
5424 49 0.074981  -1099 1093 -1126 1122 1119 -1115
5425 49 0.074981  -1099 1093 -1126 1122 -1120 1116
5426 49 0.074981  -1099 1093 -1126 1122 1123 -1119
5427 49 0.074981  -1099 1093 -1126 1122 -1124 1120
5428 49 0.074981  -1099 1093 -1126 1122 1127 -1123
5429 49 0.074981  -1099 1093 -1126 1122 -1128 1124
5430 49 0.074981  -1099 1093 -1130 1126 1100 -1095
5431 49 0.074981  -1099 1093 -1130 1126 -1101 1096
5432 49 0.074981  -1099 1093 -1130 1126 1105 -1100
5433 49 0.074981  -1099 1093 -1130 1126 -1106 1101
5434 49 0.074981  -1099 1093 -1130 1126 1110 -1105
5435 49 0.074981  -1099 1093 -1130 1126 -1111 1106
5436 49 0.074981  -1099 1093 -1130 1126 1115 -1110
5437 49 0.074981  -1099 1093 -1130 1126 -1116 1111
5438 49 0.074981  -1099 1093 -1130 1126 1119 -1115
5439 49 0.074981  -1099 1093 -1130 1126 -1120 1116
5440 49 0.074981  -1099 1093 -1130 1126 1123 -1119
5441 49 0.074981  -1099 1093 -1130 1126 -1124 1120
5442 49 0.074981  -1099 1093 -1130 1126 1127 -1123
5443 49 0.074981  -1099 1093 -1130 1126 -1128 1124
5444 49 0.074981  -1104 1099 -1098 1097 -1095 1100
5445 49 0.074981  -1104 1099 -1098 1097 -1101 1096
5446 49 0.074981  -1104 1099 -1098 1097 -1100 1105
5447 49 0.074981  -1104 1099 -1098 1097 -1106 1101
5448 49 0.074981  -1104 1099 -1098 1097 -1105 1110
5449 49 0.074981  -1104 1099 -1098 1097 -1111 1106
5450 49 0.074981  -1104 1099 -1098 1097 -1110 1115
5451 49 0.074981  -1104 1099 -1098 1097 -1116 1111
5452 49 0.074981  -1104 1099 -1098 1097 -1115 1119
5453 49 0.074981  -1104 1099 -1098 1097 -1120 1116
5454 49 0.074981  -1104 1099 -1098 1097 -1119 1123
5455 49 0.074981  -1104 1099 -1098 1097 -1124 1120
5456 49 0.074981  -1104 1099 -1098 1097 -1123 1127
5457 49 0.074981  -1104 1099 -1098 1097 -1128 1124
5458 49 0.074981  -1104 1099 1098 -1103 -1096 1095
5459 49 0.074981  -1104 1099 1103 -1108 -1096 1095
5460 49 0.074981  -1104 1099 1108 -1113 -1096 1095
5461 49 0.074981  -1104 1099 1113 -1118 -1096 1095
5462 49 0.074981  -1104 1099 1118 -1122 -1096 1095
5463 49 0.074981  -1104 1099 1122 -1126 -1096 1095
5464 49 0.074981  -1104 1099 1126 -1130 -1096 1095
5465 49 0.074981  -1104 1099 1102 -1097 -1128 1127
5466 49 0.074981  -1104 1099 1107 -1102 -1128 1127
5467 49 0.074981  -1104 1099 1112 -1107 -1128 1127
5468 49 0.074981  -1104 1099 1117 -1112 -1128 1127
5469 49 0.074981  -1104 1099 1121 -1117 -1128 1127
5470 49 0.074981  -1104 1099 1125 -1121 -1128 1127
5471 49 0.074981  -1104 1099 1129 -1125 -1128 1127
5472 49 0.074981  -1104 1099 -1103 1098 1100 -1095
5473 49 0.074981  -1104 1099 -1103 1098 -1101 1096
5474 49 0.074981  -1104 1099 -1103 1098 1105 -1100
5475 49 0.074981  -1104 1099 -1103 1098 -1106 1101
5476 49 0.074981  -1104 1099 -1103 1098 1110 -1105
5477 49 0.074981  -1104 1099 -1103 1098 -1111 1106
5478 49 0.074981  -1104 1099 -1103 1098 1115 -1110
5479 49 0.074981  -1104 1099 -1103 1098 -1116 1111
5480 49 0.074981  -1104 1099 -1103 1098 1119 -1115
5481 49 0.074981  -1104 1099 -1103 1098 -1120 1116
5482 49 0.074981  -1104 1099 -1103 1098 1123 -1119
5483 49 0.074981  -1104 1099 -1103 1098 -1124 1120
5484 49 0.074981  -1104 1099 -1103 1098 1127 -1123
5485 49 0.074981  -1104 1099 -1103 1098 -1128 1124
5486 49 0.074981  -1104 1099 -1108 1103 1100 -1095
5487 49 0.074981  -1104 1099 -1108 1103 -1101 1096
5488 49 0.074981  -1104 1099 -1108 1103 1105 -1100
5489 49 0.074981  -1104 1099 -1108 1103 -1106 1101
5490 49 0.074981  -1104 1099 -1108 1103 1110 -1105
5491 49 0.074981  -1104 1099 -1108 1103 -1111 1106
5492 49 0.074981  -1104 1099 -1108 1103 1115 -1110
5493 49 0.074981  -1104 1099 -1108 1103 -1116 1111
5494 49 0.074981  -1104 1099 -1108 1103 1119 -1115
5495 49 0.074981  -1104 1099 -1108 1103 -1120 1116
5496 49 0.074981  -1104 1099 -1108 1103 1123 -1119
5497 49 0.074981  -1104 1099 -1108 1103 -1124 1120
5498 49 0.074981  -1104 1099 -1108 1103 1127 -1123
5499 49 0.074981  -1104 1099 -1108 1103 -1128 1124
5500 49 0.074981  -1104 1099 -1113 1108 1100 -1095
5501 49 0.074981  -1104 1099 -1113 1108 -1101 1096
5502 49 0.074981  -1104 1099 -1113 1108 1105 -1100
5503 49 0.074981  -1104 1099 -1113 1108 -1106 1101
5504 49 0.074981  -1104 1099 -1113 1108 1110 -1105
5505 49 0.074981  -1104 1099 -1113 1108 -1111 1106
5506 49 0.074981  -1104 1099 -1113 1108 1115 -1110
5507 49 0.074981  -1104 1099 -1113 1108 -1116 1111
5508 49 0.074981  -1104 1099 -1113 1108 1119 -1115
5509 49 0.074981  -1104 1099 -1113 1108 -1120 1116
5510 49 0.074981  -1104 1099 -1113 1108 1123 -1119
5511 49 0.074981  -1104 1099 -1113 1108 -1124 1120
5512 49 0.074981  -1104 1099 -1113 1108 1127 -1123
5513 49 0.074981  -1104 1099 -1113 1108 -1128 1124
5514 49 0.074981  -1104 1099 -1118 1113 1100 -1095
5515 49 0.074981  -1104 1099 -1118 1113 -1101 1096
5516 49 0.074981  -1104 1099 -1118 1113 1105 -1100
5517 49 0.074981  -1104 1099 -1118 1113 -1106 1101
5518 49 0.074981  -1104 1099 -1118 1113 1110 -1105
5519 49 0.074981  -1104 1099 -1118 1113 -1111 1106
5520 49 0.074981  -1104 1099 -1118 1113 1115 -1110
5521 49 0.074981  -1104 1099 -1118 1113 -1116 1111
5522 49 0.074981  -1104 1099 -1118 1113 1119 -1115
5523 49 0.074981  -1104 1099 -1118 1113 -1120 1116
5524 49 0.074981  -1104 1099 -1118 1113 1123 -1119
5525 49 0.074981  -1104 1099 -1118 1113 -1124 1120
5526 49 0.074981  -1104 1099 -1118 1113 1127 -1123
5527 49 0.074981  -1104 1099 -1118 1113 -1128 1124
5528 49 0.074981  -1104 1099 -1122 1118 1100 -1095
5529 49 0.074981  -1104 1099 -1122 1118 -1101 1096
5530 49 0.074981  -1104 1099 -1122 1118 1105 -1100
5531 49 0.074981  -1104 1099 -1122 1118 -1106 1101
5532 49 0.074981  -1104 1099 -1122 1118 1110 -1105
5533 49 0.074981  -1104 1099 -1122 1118 -1111 1106
5534 49 0.074981  -1104 1099 -1122 1118 1115 -1110
5535 49 0.074981  -1104 1099 -1122 1118 -1116 1111
5536 49 0.074981  -1104 1099 -1122 1118 1119 -1115
5537 49 0.074981  -1104 1099 -1122 1118 -1120 1116
5538 49 0.074981  -1104 1099 -1122 1118 1123 -1119
5539 49 0.074981  -1104 1099 -1122 1118 -1124 1120
5540 49 0.074981  -1104 1099 -1122 1118 1127 -1123
5541 49 0.074981  -1104 1099 -1122 1118 -1128 1124
5542 49 0.074981  -1104 1099 -1126 1122 1100 -1095
5543 49 0.074981  -1104 1099 -1126 1122 -1101 1096
5544 49 0.074981  -1104 1099 -1126 1122 1105 -1100
5545 49 0.074981  -1104 1099 -1126 1122 -1106 1101
5546 49 0.074981  -1104 1099 -1126 1122 1110 -1105
5547 49 0.074981  -1104 1099 -1126 1122 -1111 1106
5548 49 0.074981  -1104 1099 -1126 1122 1115 -1110
5549 49 0.074981  -1104 1099 -1126 1122 -1116 1111
5550 49 0.074981  -1104 1099 -1126 1122 1119 -1115
5551 49 0.074981  -1104 1099 -1126 1122 -1120 1116
5552 49 0.074981  -1104 1099 -1126 1122 1123 -1119
5553 49 0.074981  -1104 1099 -1126 1122 -1124 1120
5554 49 0.074981  -1104 1099 -1126 1122 1127 -1123
5555 49 0.074981  -1104 1099 -1126 1122 -1128 1124
5556 49 0.074981  -1104 1099 -1130 1126 1100 -1095
5557 49 0.074981  -1104 1099 -1130 1126 -1101 1096
5558 49 0.074981  -1104 1099 -1130 1126 1105 -1100
5559 49 0.074981  -1104 1099 -1130 1126 -1106 1101
5560 49 0.074981  -1104 1099 -1130 1126 1110 -1105
5561 49 0.074981  -1104 1099 -1130 1126 -1111 1106
5562 49 0.074981  -1104 1099 -1130 1126 1115 -1110
5563 49 0.074981  -1104 1099 -1130 1126 -1116 1111
5564 49 0.074981  -1104 1099 -1130 1126 1119 -1115
5565 49 0.074981  -1104 1099 -1130 1126 -1120 1116
5566 49 0.074981  -1104 1099 -1130 1126 1123 -1119
5567 49 0.074981  -1104 1099 -1130 1126 -1124 1120
5568 49 0.074981  -1104 1099 -1130 1126 1127 -1123
5569 49 0.074981  -1104 1099 -1130 1126 -1128 1124
5570 49 0.074981  -1109 1104 -1098 1097 -1095 1100
5571 49 0.074981  -1109 1104 -1098 1097 -1101 1096
5572 49 0.074981  -1109 1104 -1098 1097 -1100 1105
5573 49 0.074981  -1109 1104 -1098 1097 -1106 1101
5574 49 0.074981  -1109 1104 -1098 1097 -1105 1110
5575 49 0.074981  -1109 1104 -1098 1097 -1111 1106
5576 49 0.074981  -1109 1104 -1098 1097 -1110 1115
5577 49 0.074981  -1109 1104 -1098 1097 -1116 1111
5578 49 0.074981  -1109 1104 -1098 1097 -1115 1119
5579 49 0.074981  -1109 1104 -1098 1097 -1120 1116
5580 49 0.074981  -1109 1104 -1098 1097 -1119 1123
5581 49 0.074981  -1109 1104 -1098 1097 -1124 1120
5582 49 0.074981  -1109 1104 -1098 1097 -1123 1127
5583 49 0.074981  -1109 1104 -1098 1097 -1128 1124
5584 49 0.074981  -1109 1104 1098 -1103 -1096 1095
5585 49 0.074981  -1109 1104 1103 -1108 -1096 1095
5586 49 0.074981  -1109 1104 1108 -1113 -1096 1095
5587 49 0.074981  -1109 1104 1113 -1118 -1096 1095
5588 49 0.074981  -1109 1104 1118 -1122 -1096 1095
5589 49 0.074981  -1109 1104 1122 -1126 -1096 1095
5590 49 0.074981  -1109 1104 1126 -1130 -1096 1095
5591 49 0.074981  -1109 1104 1102 -1097 -1128 1127
5592 49 0.074981  -1109 1104 1107 -1102 -1128 1127
5593 49 0.074981  -1109 1104 1112 -1107 -1128 1127
5594 49 0.074981  -1109 1104 1117 -1112 -1128 1127
5595 49 0.074981  -1109 1104 1121 -1117 -1128 1127
5596 49 0.074981  -1109 1104 1125 -1121 -1128 1127
5597 49 0.074981  -1109 1104 1129 -1125 -1128 1127
5598 49 0.074981  -1109 1104 -1103 1098 1100 -1095
5599 49 0.074981  -1109 1104 -1103 1098 -1101 1096
5600 49 0.074981  -1109 1104 -1103 1098 1105 -1100
5601 49 0.074981  -1109 1104 -1103 1098 -1106 1101
5602 49 0.074981  -1109 1104 -1103 1098 1110 -1105
5603 49 0.074981  -1109 1104 -1103 1098 -1111 1106
5604 49 0.074981  -1109 1104 -1103 1098 1115 -1110
5605 49 0.074981  -1109 1104 -1103 1098 -1116 1111
5606 49 0.074981  -1109 1104 -1103 1098 1119 -1115
5607 49 0.074981  -1109 1104 -1103 1098 -1120 1116
5608 49 0.074981  -1109 1104 -1103 1098 1123 -1119
5609 49 0.074981  -1109 1104 -1103 1098 -1124 1120
5610 49 0.074981  -1109 1104 -1103 1098 1127 -1123
5611 49 0.074981  -1109 1104 -1103 1098 -1128 1124
5612 49 0.074981  -1109 1104 -1108 1103 1100 -1095
5613 49 0.074981  -1109 1104 -1108 1103 -1101 1096
5614 49 0.074981  -1109 1104 -1108 1103 1105 -1100
5615 49 0.074981  -1109 1104 -1108 1103 -1106 1101
5616 49 0.074981  -1109 1104 -1108 1103 1110 -1105
5617 49 0.074981  -1109 1104 -1108 1103 -1111 1106
5618 49 0.074981  -1109 1104 -1108 1103 1115 -1110
5619 49 0.074981  -1109 1104 -1108 1103 -1116 1111
5620 49 0.074981  -1109 1104 -1108 1103 1119 -1115
5621 49 0.074981  -1109 1104 -1108 1103 -1120 1116
5622 49 0.074981  -1109 1104 -1108 1103 1123 -1119
5623 49 0.074981  -1109 1104 -1108 1103 -1124 1120
5624 49 0.074981  -1109 1104 -1108 1103 1127 -1123
5625 49 0.074981  -1109 1104 -1108 1103 -1128 1124
5626 49 0.074981  -1109 1104 -1113 1108 1100 -1095
5627 49 0.074981  -1109 1104 -1113 1108 -1101 1096
5628 49 0.074981  -1109 1104 -1113 1108 1105 -1100
5629 49 0.074981  -1109 1104 -1113 1108 -1106 1101
5630 49 0.074981  -1109 1104 -1113 1108 1110 -1105
5631 49 0.074981  -1109 1104 -1113 1108 -1111 1106
5632 49 0.074981  -1109 1104 -1113 1108 1115 -1110
5633 49 0.074981  -1109 1104 -1113 1108 -1116 1111
5634 49 0.074981  -1109 1104 -1113 1108 1119 -1115
5635 49 0.074981  -1109 1104 -1113 1108 -1120 1116
5636 49 0.074981  -1109 1104 -1113 1108 1123 -1119
5637 49 0.074981  -1109 1104 -1113 1108 -1124 1120
5638 49 0.074981  -1109 1104 -1113 1108 1127 -1123
5639 49 0.074981  -1109 1104 -1113 1108 -1128 1124
5640 49 0.074981  -1109 1104 -1118 1113 1100 -1095
5641 49 0.074981  -1109 1104 -1118 1113 -1101 1096
5642 49 0.074981  -1109 1104 -1118 1113 1105 -1100
5643 49 0.074981  -1109 1104 -1118 1113 -1106 1101
5644 49 0.074981  -1109 1104 -1118 1113 1110 -1105
5645 49 0.074981  -1109 1104 -1118 1113 -1111 1106
5646 49 0.074981  -1109 1104 -1118 1113 1115 -1110
5647 49 0.074981  -1109 1104 -1118 1113 -1116 1111
5648 49 0.074981  -1109 1104 -1118 1113 1119 -1115
5649 49 0.074981  -1109 1104 -1118 1113 -1120 1116
5650 49 0.074981  -1109 1104 -1118 1113 1123 -1119
5651 49 0.074981  -1109 1104 -1118 1113 -1124 1120
5652 49 0.074981  -1109 1104 -1118 1113 1127 -1123
5653 49 0.074981  -1109 1104 -1118 1113 -1128 1124
5654 49 0.074981  -1109 1104 -1122 1118 1100 -1095
5655 49 0.074981  -1109 1104 -1122 1118 -1101 1096
5656 49 0.074981  -1109 1104 -1122 1118 1105 -1100
5657 49 0.074981  -1109 1104 -1122 1118 -1106 1101
5658 49 0.074981  -1109 1104 -1122 1118 1110 -1105
5659 49 0.074981  -1109 1104 -1122 1118 -1111 1106
5660 49 0.074981  -1109 1104 -1122 1118 1115 -1110
5661 49 0.074981  -1109 1104 -1122 1118 -1116 1111
5662 49 0.074981  -1109 1104 -1122 1118 1119 -1115
5663 49 0.074981  -1109 1104 -1122 1118 -1120 1116
5664 49 0.074981  -1109 1104 -1122 1118 1123 -1119
5665 49 0.074981  -1109 1104 -1122 1118 -1124 1120
5666 49 0.074981  -1109 1104 -1122 1118 1127 -1123
5667 49 0.074981  -1109 1104 -1122 1118 -1128 1124
5668 49 0.074981  -1109 1104 -1126 1122 1100 -1095
5669 49 0.074981  -1109 1104 -1126 1122 -1101 1096
5670 49 0.074981  -1109 1104 -1126 1122 1105 -1100
5671 49 0.074981  -1109 1104 -1126 1122 -1106 1101
5672 49 0.074981  -1109 1104 -1126 1122 1110 -1105
5673 49 0.074981  -1109 1104 -1126 1122 -1111 1106
5674 49 0.074981  -1109 1104 -1126 1122 1115 -1110
5675 49 0.074981  -1109 1104 -1126 1122 -1116 1111
5676 49 0.074981  -1109 1104 -1126 1122 1119 -1115
5677 49 0.074981  -1109 1104 -1126 1122 -1120 1116
5678 49 0.074981  -1109 1104 -1126 1122 1123 -1119
5679 49 0.074981  -1109 1104 -1126 1122 -1124 1120
5680 49 0.074981  -1109 1104 -1126 1122 1127 -1123
5681 49 0.074981  -1109 1104 -1126 1122 -1128 1124
5682 49 0.074981  -1109 1104 -1130 1126 1100 -1095
5683 49 0.074981  -1109 1104 -1130 1126 -1101 1096
5684 49 0.074981  -1109 1104 -1130 1126 1105 -1100
5685 49 0.074981  -1109 1104 -1130 1126 -1106 1101
5686 49 0.074981  -1109 1104 -1130 1126 1110 -1105
5687 49 0.074981  -1109 1104 -1130 1126 -1111 1106
5688 49 0.074981  -1109 1104 -1130 1126 1115 -1110
5689 49 0.074981  -1109 1104 -1130 1126 -1116 1111
5690 49 0.074981  -1109 1104 -1130 1126 1119 -1115
5691 49 0.074981  -1109 1104 -1130 1126 -1120 1116
5692 49 0.074981  -1109 1104 -1130 1126 1123 -1119
5693 49 0.074981  -1109 1104 -1130 1126 -1124 1120
5694 49 0.074981  -1109 1104 -1130 1126 1127 -1123
5695 49 0.074981  -1109 1104 -1130 1126 -1128 1124
5696 49 0.074981  -1114 1109 -1098 1097 -1095 1100
5697 49 0.074981  -1114 1109 -1098 1097 -1101 1096
5698 49 0.074981  -1114 1109 -1098 1097 -1100 1105
5699 49 0.074981  -1114 1109 -1098 1097 -1106 1101
5700 49 0.074981  -1114 1109 -1098 1097 -1105 1110
5701 49 0.074981  -1114 1109 -1098 1097 -1111 1106
5702 49 0.074981  -1114 1109 -1098 1097 -1110 1115
5703 49 0.074981  -1114 1109 -1098 1097 -1116 1111
5704 49 0.074981  -1114 1109 -1098 1097 -1115 1119
5705 49 0.074981  -1114 1109 -1098 1097 -1120 1116
5706 49 0.074981  -1114 1109 -1098 1097 -1119 1123
5707 49 0.074981  -1114 1109 -1098 1097 -1124 1120
5708 49 0.074981  -1114 1109 -1098 1097 -1123 1127
5709 49 0.074981  -1114 1109 -1098 1097 -1128 1124
5710 49 0.074981  -1114 1109 1098 -1103 -1096 1095
5711 49 0.074981  -1114 1109 1103 -1108 -1096 1095
5712 49 0.074981  -1114 1109 1108 -1113 -1096 1095
5713 49 0.074981  -1114 1109 1113 -1118 -1096 1095
5714 49 0.074981  -1114 1109 1118 -1122 -1096 1095
5715 49 0.074981  -1114 1109 1122 -1126 -1096 1095
5716 49 0.074981  -1114 1109 1126 -1130 -1096 1095
5717 49 0.074981  -1114 1109 1102 -1097 -1128 1127
5718 49 0.074981  -1114 1109 1107 -1102 -1128 1127
5719 49 0.074981  -1114 1109 1112 -1107 -1128 1127
5720 49 0.074981  -1114 1109 1117 -1112 -1128 1127
5721 49 0.074981  -1114 1109 1121 -1117 -1128 1127
5722 49 0.074981  -1114 1109 1125 -1121 -1128 1127
5723 49 0.074981  -1114 1109 1129 -1125 -1128 1127
5724 49 0.074981  -1114 1109 -1103 1098 1100 -1095
5725 49 0.074981  -1114 1109 -1103 1098 -1101 1096
5726 49 0.074981  -1114 1109 -1103 1098 1105 -1100
5727 49 0.074981  -1114 1109 -1103 1098 -1106 1101
5728 49 0.074981  -1114 1109 -1103 1098 1110 -1105
5729 49 0.074981  -1114 1109 -1103 1098 -1111 1106
5730 49 0.074981  -1114 1109 -1103 1098 1115 -1110
5731 49 0.074981  -1114 1109 -1103 1098 -1116 1111
5732 49 0.074981  -1114 1109 -1103 1098 1119 -1115
5733 49 0.074981  -1114 1109 -1103 1098 -1120 1116
5734 49 0.074981  -1114 1109 -1103 1098 1123 -1119
5735 49 0.074981  -1114 1109 -1103 1098 -1124 1120
5736 49 0.074981  -1114 1109 -1103 1098 1127 -1123
5737 49 0.074981  -1114 1109 -1103 1098 -1128 1124
5738 49 0.074981  -1114 1109 -1108 1103 1100 -1095
5739 49 0.074981  -1114 1109 -1108 1103 -1101 1096
5740 49 0.074981  -1114 1109 -1108 1103 1105 -1100
5741 49 0.074981  -1114 1109 -1108 1103 -1106 1101
5742 49 0.074981  -1114 1109 -1108 1103 1110 -1105
5743 49 0.074981  -1114 1109 -1108 1103 -1111 1106
5744 49 0.074981  -1114 1109 -1108 1103 1115 -1110
5745 49 0.074981  -1114 1109 -1108 1103 -1116 1111
5746 49 0.074981  -1114 1109 -1108 1103 1119 -1115
5747 49 0.074981  -1114 1109 -1108 1103 -1120 1116
5748 49 0.074981  -1114 1109 -1108 1103 1123 -1119
5749 49 0.074981  -1114 1109 -1108 1103 -1124 1120
5750 49 0.074981  -1114 1109 -1108 1103 1127 -1123
5751 49 0.074981  -1114 1109 -1108 1103 -1128 1124
5752 49 0.074981  -1114 1109 -1113 1108 1100 -1095
5753 49 0.074981  -1114 1109 -1113 1108 -1101 1096
5754 49 0.074981  -1114 1109 -1113 1108 1105 -1100
5755 49 0.074981  -1114 1109 -1113 1108 -1106 1101
5756 49 0.074981  -1114 1109 -1113 1108 1110 -1105
5757 49 0.074981  -1114 1109 -1113 1108 -1111 1106
5758 49 0.074981  -1114 1109 -1113 1108 1115 -1110
5759 49 0.074981  -1114 1109 -1113 1108 -1116 1111
5760 49 0.074981  -1114 1109 -1113 1108 1119 -1115
5761 49 0.074981  -1114 1109 -1113 1108 -1120 1116
5762 49 0.074981  -1114 1109 -1113 1108 1123 -1119
5763 49 0.074981  -1114 1109 -1113 1108 -1124 1120
5764 49 0.074981  -1114 1109 -1113 1108 1127 -1123
5765 49 0.074981  -1114 1109 -1113 1108 -1128 1124
5766 49 0.074981  -1114 1109 -1118 1113 1100 -1095
5767 49 0.074981  -1114 1109 -1118 1113 -1101 1096
5768 49 0.074981  -1114 1109 -1118 1113 1105 -1100
5769 49 0.074981  -1114 1109 -1118 1113 -1106 1101
5770 49 0.074981  -1114 1109 -1118 1113 1110 -1105
5771 49 0.074981  -1114 1109 -1118 1113 -1111 1106
5772 49 0.074981  -1114 1109 -1118 1113 1115 -1110
5773 49 0.074981  -1114 1109 -1118 1113 -1116 1111
5774 49 0.074981  -1114 1109 -1118 1113 1119 -1115
5775 49 0.074981  -1114 1109 -1118 1113 -1120 1116
5776 49 0.074981  -1114 1109 -1118 1113 1123 -1119
5777 49 0.074981  -1114 1109 -1118 1113 -1124 1120
5778 49 0.074981  -1114 1109 -1118 1113 1127 -1123
5779 49 0.074981  -1114 1109 -1118 1113 -1128 1124
5780 49 0.074981  -1114 1109 -1122 1118 1100 -1095
5781 49 0.074981  -1114 1109 -1122 1118 -1101 1096
5782 49 0.074981  -1114 1109 -1122 1118 1105 -1100
5783 49 0.074981  -1114 1109 -1122 1118 -1106 1101
5784 49 0.074981  -1114 1109 -1122 1118 1110 -1105
5785 49 0.074981  -1114 1109 -1122 1118 -1111 1106
5786 49 0.074981  -1114 1109 -1122 1118 1115 -1110
5787 49 0.074981  -1114 1109 -1122 1118 -1116 1111
5788 49 0.074981  -1114 1109 -1122 1118 1119 -1115
5789 49 0.074981  -1114 1109 -1122 1118 -1120 1116
5790 49 0.074981  -1114 1109 -1122 1118 1123 -1119
5791 49 0.074981  -1114 1109 -1122 1118 -1124 1120
5792 49 0.074981  -1114 1109 -1122 1118 1127 -1123
5793 49 0.074981  -1114 1109 -1122 1118 -1128 1124
5794 49 0.074981  -1114 1109 -1126 1122 1100 -1095
5795 49 0.074981  -1114 1109 -1126 1122 -1101 1096
5796 49 0.074981  -1114 1109 -1126 1122 1105 -1100
5797 49 0.074981  -1114 1109 -1126 1122 -1106 1101
5798 49 0.074981  -1114 1109 -1126 1122 1110 -1105
5799 49 0.074981  -1114 1109 -1126 1122 -1111 1106
5800 49 0.074981  -1114 1109 -1126 1122 1115 -1110
5801 49 0.074981  -1114 1109 -1126 1122 -1116 1111
5802 49 0.074981  -1114 1109 -1126 1122 1119 -1115
5803 49 0.074981  -1114 1109 -1126 1122 -1120 1116
5804 49 0.074981  -1114 1109 -1126 1122 1123 -1119
5805 49 0.074981  -1114 1109 -1126 1122 -1124 1120
5806 49 0.074981  -1114 1109 -1126 1122 1127 -1123
5807 49 0.074981  -1114 1109 -1126 1122 -1128 1124
5808 49 0.074981  -1114 1109 -1130 1126 1100 -1095
5809 49 0.074981  -1114 1109 -1130 1126 -1101 1096
5810 49 0.074981  -1114 1109 -1130 1126 1105 -1100
5811 49 0.074981  -1114 1109 -1130 1126 -1106 1101
5812 49 0.074981  -1114 1109 -1130 1126 1110 -1105
5813 49 0.074981  -1114 1109 -1130 1126 -1111 1106
5814 49 0.074981  -1114 1109 -1130 1126 1115 -1110
5815 49 0.074981  -1114 1109 -1130 1126 -1116 1111
5816 49 0.074981  -1114 1109 -1130 1126 1119 -1115
5817 49 0.074981  -1114 1109 -1130 1126 -1120 1116
5818 49 0.074981  -1114 1109 -1130 1126 1123 -1119
5819 49 0.074981  -1114 1109 -1130 1126 -1124 1120
5820 49 0.074981  -1114 1109 -1130 1126 1127 -1123
5821 49 0.074981  -1114 1109 -1130 1126 -1128 1124
5822 49 0.074981  -1094 1114 -1098 1097 -1095 1100
5823 49 0.074981  -1094 1114 -1098 1097 -1101 1096
5824 49 0.074981  -1094 1114 -1098 1097 -1100 1105
5825 49 0.074981  -1094 1114 -1098 1097 -1106 1101
5826 49 0.074981  -1094 1114 -1098 1097 -1105 1110
5827 49 0.074981  -1094 1114 -1098 1097 -1111 1106
5828 49 0.074981  -1094 1114 -1098 1097 -1110 1115
5829 49 0.074981  -1094 1114 -1098 1097 -1116 1111
5830 49 0.074981  -1094 1114 -1098 1097 -1115 1119
5831 49 0.074981  -1094 1114 -1098 1097 -1120 1116
5832 49 0.074981  -1094 1114 -1098 1097 -1119 1123
5833 49 0.074981  -1094 1114 -1098 1097 -1124 1120
5834 49 0.074981  -1094 1114 -1098 1097 -1123 1127
5835 49 0.074981  -1094 1114 -1098 1097 -1128 1124
5836 49 0.074981  -1094 1114 1098 -1103 -1096 1095
5837 49 0.074981  -1094 1114 1103 -1108 -1096 1095
5838 49 0.074981  -1094 1114 1108 -1113 -1096 1095
5839 49 0.074981  -1094 1114 1113 -1118 -1096 1095
5840 49 0.074981  -1094 1114 1118 -1122 -1096 1095
5841 49 0.074981  -1094 1114 1122 -1126 -1096 1095
5842 49 0.074981  -1094 1114 1126 -1130 -1096 1095
5843 49 0.074981  -1094 1114 1102 -1097 -1128 1127
5844 49 0.074981  -1094 1114 1107 -1102 -1128 1127
5845 49 0.074981  -1094 1114 1112 -1107 -1128 1127
5846 49 0.074981  -1094 1114 1117 -1112 -1128 1127
5847 49 0.074981  -1094 1114 1121 -1117 -1128 1127
5848 49 0.074981  -1094 1114 1125 -1121 -1128 1127
5849 49 0.074981  -1094 1114 1129 -1125 -1128 1127
5850 49 0.074981  -1094 1114 -1103 1098 1100 -1095
5851 49 0.074981  -1094 1114 -1103 1098 -1101 1096
5852 49 0.074981  -1094 1114 -1103 1098 1105 -1100
5853 49 0.074981  -1094 1114 -1103 1098 -1106 1101
5854 49 0.074981  -1094 1114 -1103 1098 1110 -1105
5855 49 0.074981  -1094 1114 -1103 1098 -1111 1106
5856 49 0.074981  -1094 1114 -1103 1098 1115 -1110
5857 49 0.074981  -1094 1114 -1103 1098 -1116 1111
5858 49 0.074981  -1094 1114 -1103 1098 1119 -1115
5859 49 0.074981  -1094 1114 -1103 1098 -1120 1116
5860 49 0.074981  -1094 1114 -1103 1098 1123 -1119
5861 49 0.074981  -1094 1114 -1103 1098 -1124 1120
5862 49 0.074981  -1094 1114 -1103 1098 1127 -1123
5863 49 0.074981  -1094 1114 -1103 1098 -1128 1124
5864 49 0.074981  -1094 1114 -1108 1103 1100 -1095
5865 49 0.074981  -1094 1114 -1108 1103 -1101 1096
5866 49 0.074981  -1094 1114 -1108 1103 1105 -1100
5867 49 0.074981  -1094 1114 -1108 1103 -1106 1101
5868 49 0.074981  -1094 1114 -1108 1103 1110 -1105
5869 49 0.074981  -1094 1114 -1108 1103 -1111 1106
5870 49 0.074981  -1094 1114 -1108 1103 1115 -1110
5871 49 0.074981  -1094 1114 -1108 1103 -1116 1111
5872 49 0.074981  -1094 1114 -1108 1103 1119 -1115
5873 49 0.074981  -1094 1114 -1108 1103 -1120 1116
5874 49 0.074981  -1094 1114 -1108 1103 1123 -1119
5875 49 0.074981  -1094 1114 -1108 1103 -1124 1120
5876 49 0.074981  -1094 1114 -1108 1103 1127 -1123
5877 49 0.074981  -1094 1114 -1108 1103 -1128 1124
5878 49 0.074981  -1094 1114 -1113 1108 1100 -1095
5879 49 0.074981  -1094 1114 -1113 1108 -1101 1096
5880 49 0.074981  -1094 1114 -1113 1108 1105 -1100
5881 49 0.074981  -1094 1114 -1113 1108 -1106 1101
5882 49 0.074981  -1094 1114 -1113 1108 1110 -1105
5883 49 0.074981  -1094 1114 -1113 1108 -1111 1106
5884 49 0.074981  -1094 1114 -1113 1108 1115 -1110
5885 49 0.074981  -1094 1114 -1113 1108 -1116 1111
5886 49 0.074981  -1094 1114 -1113 1108 1119 -1115
5887 49 0.074981  -1094 1114 -1113 1108 -1120 1116
5888 49 0.074981  -1094 1114 -1113 1108 1123 -1119
5889 49 0.074981  -1094 1114 -1113 1108 -1124 1120
5890 49 0.074981  -1094 1114 -1113 1108 1127 -1123
5891 49 0.074981  -1094 1114 -1113 1108 -1128 1124
5892 49 0.074981  -1094 1114 -1118 1113 1100 -1095
5893 49 0.074981  -1094 1114 -1118 1113 -1101 1096
5894 49 0.074981  -1094 1114 -1118 1113 1105 -1100
5895 49 0.074981  -1094 1114 -1118 1113 -1106 1101
5896 49 0.074981  -1094 1114 -1118 1113 1110 -1105
5897 49 0.074981  -1094 1114 -1118 1113 -1111 1106
5898 49 0.074981  -1094 1114 -1118 1113 1115 -1110
5899 49 0.074981  -1094 1114 -1118 1113 -1116 1111
5900 49 0.074981  -1094 1114 -1118 1113 1119 -1115
5901 49 0.074981  -1094 1114 -1118 1113 -1120 1116
5902 49 0.074981  -1094 1114 -1118 1113 1123 -1119
5903 49 0.074981  -1094 1114 -1118 1113 -1124 1120
5904 49 0.074981  -1094 1114 -1118 1113 1127 -1123
5905 49 0.074981  -1094 1114 -1118 1113 -1128 1124
5906 49 0.074981  -1094 1114 -1122 1118 1100 -1095
5907 49 0.074981  -1094 1114 -1122 1118 -1101 1096
5908 49 0.074981  -1094 1114 -1122 1118 1105 -1100
5909 49 0.074981  -1094 1114 -1122 1118 -1106 1101
5910 49 0.074981  -1094 1114 -1122 1118 1110 -1105
5911 49 0.074981  -1094 1114 -1122 1118 -1111 1106
5912 49 0.074981  -1094 1114 -1122 1118 1115 -1110
5913 49 0.074981  -1094 1114 -1122 1118 -1116 1111
5914 49 0.074981  -1094 1114 -1122 1118 1119 -1115
5915 49 0.074981  -1094 1114 -1122 1118 -1120 1116
5916 49 0.074981  -1094 1114 -1122 1118 1123 -1119
5917 49 0.074981  -1094 1114 -1122 1118 -1124 1120
5918 49 0.074981  -1094 1114 -1122 1118 1127 -1123
5919 49 0.074981  -1094 1114 -1122 1118 -1128 1124
5920 49 0.074981  -1094 1114 -1126 1122 1100 -1095
5921 49 0.074981  -1094 1114 -1126 1122 -1101 1096
5922 49 0.074981  -1094 1114 -1126 1122 1105 -1100
5923 49 0.074981  -1094 1114 -1126 1122 -1106 1101
5924 49 0.074981  -1094 1114 -1126 1122 1110 -1105
5925 49 0.074981  -1094 1114 -1126 1122 -1111 1106
5926 49 0.074981  -1094 1114 -1126 1122 1115 -1110
5927 49 0.074981  -1094 1114 -1126 1122 -1116 1111
5928 49 0.074981  -1094 1114 -1126 1122 1119 -1115
5929 49 0.074981  -1094 1114 -1126 1122 -1120 1116
5930 49 0.074981  -1094 1114 -1126 1122 1123 -1119
5931 49 0.074981  -1094 1114 -1126 1122 -1124 1120
5932 49 0.074981  -1094 1114 -1126 1122 1127 -1123
5933 49 0.074981  -1094 1114 -1126 1122 -1128 1124
5934 49 0.074981  -1094 1114 -1130 1126 1100 -1095
5935 49 0.074981  -1094 1114 -1130 1126 -1101 1096
5936 49 0.074981  -1094 1114 -1130 1126 1105 -1100
5937 49 0.074981  -1094 1114 -1130 1126 -1106 1101
5938 49 0.074981  -1094 1114 -1130 1126 1110 -1105
5939 49 0.074981  -1094 1114 -1130 1126 -1111 1106
5940 49 0.074981  -1094 1114 -1130 1126 1115 -1110
5941 49 0.074981  -1094 1114 -1130 1126 -1116 1111
5942 49 0.074981  -1094 1114 -1130 1126 1119 -1115
5943 49 0.074981  -1094 1114 -1130 1126 -1120 1116
5944 49 0.074981  -1094 1114 -1130 1126 1123 -1119
5945 49 0.074981  -1094 1114 -1130 1126 -1124 1120
5946 49 0.074981  -1094 1114 -1130 1126 1127 -1123
5947 49 0.074981  -1094 1114 -1130 1126 -1128 1124
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++

c -------------------------------------------------------
c --------------- SURFACE CARDS -------------------------
c -------------------------------------------------------
1 so 20000
2 p -0.743144825 -0.669130606 0.0 0.0
3 p -0.669130606 0.743144825 0.0 0.0
4 pz -37.1
5 pz 37.1
6 cz 34.3
7 pz -40.1
8 pz 40.1
9 pz -10.1
10 p 0.743144825 0.669130606 0.0 113.95
11 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  0  0  0
        -132.25
12 p 0.743144825 0.669130606 0.0 313.95
13 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  0  0  0
        -225
14 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  0  0  0
        -256
15 p 0.743144825 0.669130606 0.0 441.45
16 cz 37.3
17 pz 8.1
18 cz 30.3
19 cz 30.6
20 pz 10.95
21 cz 30
22 pz 16.65
23 pz 16.95
24 cz 32.3
25 cz 32.6
26 pz 17.65
27 cz 32
28 pz 17.95
29 p -0.207911691 0.978147601 0.0 -2.675
30 p 0.951056516 -0.309016994 0.0 2.675
31 kz 12.25 2712.396457
32 kz 15.15 1855.179573
33 p -0.207911691 0.978147601 0.0 -2.975
34 p 0.951056516 -0.309016994 0.0 2.975
35 kz 11.95 2712.396457
36 kz 15.45 1855.179573
37 pz 12.25
38 pz 15.15
39 pz 11.95
40 pz 15.45
41 p 0.207911691 -0.978147601 0.0 -2.675
42 p -0.951056516 0.309016994 0.0 2.675
43 p 0.0128455963 -0.01426648 0.999815712 12.2477425
44 p 0.015531053 -0.0172489818 -0.999730593 -15.1459185
45 p 0.207911691 -0.978147601 0.0 -2.975
46 p -0.951056516 0.309016994 0.0 2.975
47 p 0.0128455963 -0.01426648 0.999815712 11.9477425
48 p 0.015531053 -0.0172489818 -0.999730593 -15.4459185
49 pz -41.3
50 pz 41.3
51 cz 37.5
52 pz -75
53 pz 75
54 cz 65
55 pz -200
56 pz 200
57 cz 200
58 pz -400
59 pz 400
60 cz 550
61 pz 406
62 cz 556
63 pz 214
64 cz 500
65 cz 506
66 pz 220
67 pz 290
68 pz 296
69 p -0.986285602 0.165047606 0.0 0.0
70 p -0.292371705 0.956304756 0.0 0.0
71 p 0.587785252 0.809016994 0.0 1.42108547e-14
72 pz -120
73 pz 150
74 cz 2450
75 p -0.292371705 0.956304756 0.0 -80
76 p 0.587785252 0.809016994 0.0 80
77 pz -220
78 pz 325
79 cz 2800
80 pz 170
81 p -0.202787295 0.979222811 0.0 -1.13686838e-13
82 p -0.111468932 0.993767919 0.0 0.0
83 p -0.0191974424 0.999815712 0.0 0.0
84 p 0.0732381971 0.997314477 0.0 5.68434189e-14
85 p 0.165047606 0.986285602 0.0 0.0
86 p 0.255445758 0.966823389 0.0 0.0
87 p 0.343659695 0.939094252 0.0 2.84217094e-14
88 p 0.428935133 0.903335293 0.0 0.0
89 p 0.510542918 0.859852272 0.0 0.0
90 p -0.267238376 0.963630453 0.0 0.0
91 p 0.920504853 -0.390731128 0.0 0.0
92 cz 1150
93 p 0.587785252 0.809016994 0.0 -80
94 p 0.920504853 -0.390731128 0.0 80
95 cz 1500
96 p 0.701600522 0.712570493 0.0 0.0
97 p 0.79980108 0.600265135 0.0 0.0
98 p 0.880201391 0.47460037 0.0 -1.42108547e-14
99 p 0.941012078 0.338372973 0.0 -2.84217094e-14
100 p 0.980879748 0.194614801 0.0 2.84217094e-14
101 p 0.998917111 0.0465253122 0.0 5.68434189e-14
102 p 0.994722733 -0.102599637 0.0 0.0
103 p 0.968389961 -0.249441144 0.0 0.0
104 p 0.0610485395 -0.998134798 0.0 0.0
105 p 0.866025404 0.5 0.0 1.42108547e-14
106 p 0.920504853 -0.390731128 0.0 -80
107 p 0.866025404 0.5 0.0 80
108 p 0.952661481 -0.304033061 0.0 -1.13686838e-13
109 p 0.976672278 -0.214735327 0.0 0.0
110 p 0.992331938 -0.123601477 0.0 0.0
111 p 0.99950656 -0.0314107591 0.0 5.68434189e-14
112 p 0.998134798 0.0610485395 0.0 0.0
113 p 0.988228381 0.152985836 0.0 0.0
114 p 0.969872015 0.243615012 0.0 2.84217094e-14
115 p 0.943222658 0.332161132 0.0 0.0
116 p 0.908508178 0.417867074 0.0 0.0
117 p 0.930417568 -0.366501227 0.0 0.0
118 p 0.866025404 0.5 0.0 -80
119 p -0.292371705 0.956304756 0.0 80
120 p 0.782004182 0.623273182 0.0 0.0
121 p 0.680578797 0.732674894 0.0 0.0
122 p 0.564006558 0.825770309 0.0 -1.42108547e-14
123 p 0.434881877 0.900487508 0.0 -2.84217094e-14
124 p 0.296078533 0.9551636 0.0 2.84217094e-14
125 p 0.150685712 0.988581719 0.0 5.68434189e-14
126 p 0.00193925351 0.99999812 0.0 0.0
127 p -0.146850365 0.989158718 0.0 0.0
128 pz 856
129 cz 526
130 pz 466
131 cz 586
132 cz 591
133 p -0.669130606 0.743144825 0.0 1006
134 pz 655
135 pz 855
136 p 0.669130606 -0.743144825 0.0 1006
137 p -0.743144825 -0.669130606 0.0 112.2
138 pz -4
139 pz 4
140 c/z -83.38084942 -75.07645403 45
141 pz -4.1
142 pz 4.1
143 c/z -83.38084942 -75.07645403 48
144 pz -4.3
145 pz 4.3
146 c/z -83.38084942 -75.07645403 85
147 pz -4.8
148 pz 4.8
149 c/z -83.38084942 -75.07645403 125
150 pz -7.8
151 pz 7.8
152 pz -3.3
153 pz 3.3
154 c/z -83.38084942 -75.07645403 74.5
155 pz -3.8
156 pz 3.8
157 c/z -83.38084942 -75.07645403 75
158 pz -5.8
159 pz 5.8
160 gq  6.01392e-05  6.01392e-05  0.00138  6.776263578e-21  0  0
        0.01002891516  0.009030075769  0  -0.2429172335
161 gq  1  1  23.01803079  1.110223025e-16  0  0  166.7616988
        150.1529081  0  -4090.892457
162 c/z -83.38084942 -75.07645403 131.15
163 c/z -83.38084942 -75.07645403 64.07
164 pz 435
165 c/z -83.38084942 -75.07645403 5
166 c/z -83.38084942 -75.07645403 13.5
167 c/z -83.38084942 -75.07645403 14
168 c/z -83.38084942 -75.07645403 20
169 c/z -83.38084942 -75.07645403 23
170 c/z -83.38084942 -75.07645403 25
171 pz -8.8
172 pz 8.8
173 c/z -83.38084942 -75.07645403 66.07
174 pz -10.8
175 pz 15
176 c/z -83.38084942 -75.07645403 50
177 pz 17
178 c/z -83.38084942 -75.07645403 27.5
179 pz 12
180 c/z -83.38084942 -75.07645403 29.5
181 pz 20
182 pz 24
183 pz 26
184 pz -35
185 c/z -83.38084942 -75.07645403 42
186 pz -25
187 c/z -83.38084942 -75.07645403 32
188 pz -15
189 c/z -83.38084942 -75.07645403 22
190 pz -20
191 c/z -83.38084942 -75.07645403 34
192 pz -24
193 c/z -83.38084942 -75.07645403 36
194 c/z -61.08650465 -55.00253584 1.5
195 c/z -61.08650465 -55.00253584 1.7
196 c/z -64.91100516 -51.43613143 1.5
197 c/z -64.91100516 -51.43613143 1.7
198 c/z -69.29670253 -48.58802625 1.5
199 c/z -69.29670253 -48.58802625 1.7
200 c/z -74.11033959 -46.54475854 1.5
201 c/z -74.11033959 -46.54475854 1.7
202 c/z -79.20565639 -45.36841197 1.5
203 c/z -79.20565639 -45.36841197 1.7
204 c/z -84.42783432 -45.09472922 1.5
205 c/z -84.42783432 -45.09472922 1.7
206 c/z -89.61820014 -45.73202601 1.5
207 c/z -89.61820014 -45.73202601 1.7
208 c/z -94.61904722 -47.2609384 1.5
209 c/z -94.61904722 -47.2609384 1.7
210 c/z -99.27842735 -49.63501115 1.5
211 c/z -99.27842735 -49.63501115 1.7
212 c/z -103.4547676 -52.78210927 1.5
213 c/z -103.4547676 -52.78210927 1.7
214 c/z -107.021172 -56.60660977 1.5
215 c/z -107.021172 -56.60660977 1.7
216 c/z -109.8692772 -60.99230715 1.5
217 c/z -109.8692772 -60.99230715 1.7
218 c/z -111.9125449 -65.8059442 1.5
219 c/z -111.9125449 -65.8059442 1.7
220 c/z -113.0888915 -70.901261 1.5
221 c/z -113.0888915 -70.901261 1.7
222 c/z -113.3625742 -76.12343893 1.5
223 c/z -113.3625742 -76.12343893 1.7
224 c/z -112.7252774 -81.31380476 1.5
225 c/z -112.7252774 -81.31380476 1.7
226 c/z -111.1963651 -86.31465184 1.5
227 c/z -111.1963651 -86.31465184 1.7
228 c/z -108.8222923 -90.97403196 1.5
229 c/z -108.8222923 -90.97403196 1.7
230 c/z -105.6751942 -95.15037222 1.5
231 c/z -105.6751942 -95.15037222 1.7
232 c/z -101.8506937 -98.71677664 1.5
233 c/z -101.8506937 -98.71677664 1.7
234 c/z -97.4649963 -101.5648818 1.5
235 c/z -97.4649963 -101.5648818 1.7
236 c/z -92.65135925 -103.6081495 1.5
237 c/z -92.65135925 -103.6081495 1.7
238 c/z -87.55604245 -104.7844961 1.5
239 c/z -87.55604245 -104.7844961 1.7
240 c/z -82.33386452 -105.0581788 1.5
241 c/z -82.33386452 -105.0581788 1.7
242 c/z -77.14349869 -104.4208821 1.5
243 c/z -77.14349869 -104.4208821 1.7
244 c/z -72.14265162 -102.8919697 1.5
245 c/z -72.14265162 -102.8919697 1.7
246 c/z -67.48327149 -100.5178969 1.5
247 c/z -67.48327149 -100.5178969 1.7
248 c/z -63.30693123 -97.3707988 1.5
249 c/z -63.30693123 -97.3707988 1.7
250 c/z -59.74052681 -93.54629829 1.5
251 c/z -59.74052681 -93.54629829 1.7
252 c/z -56.89242163 -89.16060092 1.5
253 c/z -56.89242163 -89.16060092 1.7
254 c/z -54.84915393 -84.34696386 1.5
255 c/z -54.84915393 -84.34696386 1.7
256 c/z -53.67280736 -79.25164706 1.5
257 c/z -53.67280736 -79.25164706 1.7
258 c/z -53.39912461 -74.02946913 1.5
259 c/z -53.39912461 -74.02946913 1.7
260 c/z -54.0364214 -68.83910331 1.5
261 c/z -54.0364214 -68.83910331 1.7
262 c/z -55.56533378 -63.83825623 1.5
263 c/z -55.56533378 -63.83825623 1.7
264 c/z -57.93940653 -59.17887611 1.5
265 c/z -57.93940653 -59.17887611 1.7
266 p -0.788010754 0.615661475 0.0 19.4833255
267 p -0.882947593 0.469471563 0.0 38.3746601
268 p -0.951056516 0.309016994 0.0 56.1
269 p -0.990268069 0.139173101 0.0 72.1207698
270 p -0.999390827 -0.0348994967 0.0 85.9501865
271 p -0.978147601 -0.207911691 0.0 97.1680503
272 p -0.927183855 -0.374606593 0.0 105.433512
273 p -0.848048096 -0.529919264 0.0 110.49543
274 p -0.615661475 -0.788010754 0.0 110.49543
275 p -0.469471563 -0.882947593 0.0 105.433512
276 p -0.309016994 -0.951056516 0.0 97.1680503
277 p -0.139173101 -0.990268069 0.0 85.9501865
278 p 0.0348994967 -0.999390827 0.0 72.1207698
279 p 0.207911691 -0.978147601 0.0 56.1
280 p 0.374606593 -0.927183855 0.0 38.3746601
281 p 0.529919264 -0.848048096 0.0 19.4833255
282 pz -2.25
283 pz 2.25
284 p -0.656059029 0.75470958 0.0 -1.95816
285 p -0.688354576 0.725374371 0.0 2.9370536
286 p -0.777145961 0.629320391 0.0 17.551947
287 p -0.803856861 0.594822787 0.0 22.3690822
288 p -0.874619707 0.48480962 0.0 36.5287469
289 p -0.894934362 0.446197813 0.0 41.1214376
290 p -0.945518576 0.325568154 0.0 54.3956394
291 p -0.958819735 0.284015345 0.0 58.624339
292 p -0.987688341 0.156434465 0.0 70.6097479
293 p -0.993571856 0.113203214 0.0 74.3459694
294 p -0.999847695 -0.0174524064 0.0 84.6784149
295 p -0.998134798 -0.0610485395 0.0 87.8086352
296 p -0.981627183 -0.190808995 0.0 96.1741711
297 p -0.97236992 -0.233445364 0.0 98.60328
298 p -0.933580426 -0.35836795 0.0 104.747724
299 p -0.917060074 -0.398749069 0.0 106.401914
300 p -0.857167301 -0.515038075 0.0 110.13857
301 p -0.833885822 -0.551936985 0.0 110.96758
302 p -0.75470958 -0.656059029 0.0 112.182911
303 p -0.725374371 -0.688354576 0.0 112.161552
304 p -0.629320391 -0.777145961 0.0 110.818632
305 p -0.594822787 -0.803856861 0.0 109.947552
306 p -0.48480962 -0.874619707 0.0 106.087184
307 p -0.446197813 -0.894934362 0.0 104.392851
308 p -0.325568154 -0.945518576 0.0 98.1323311
309 p -0.284015345 -0.958819735 0.0 95.6662264
310 p -0.156434465 -0.987688341 0.0 87.1957769
311 p -0.113203214 -0.993571856 0.0 84.0328319
312 p 0.0174524064 -0.999847695 0.0 73.6098231
313 p 0.0610485395 -0.998134798 0.0 69.8461422
314 p 0.190808995 -0.981627183 0.0 57.787272
315 p 0.233445364 -0.97236992 0.0 53.5372129
316 p 0.35836795 -0.933580426 0.0 40.2088839
317 p 0.398749069 -0.917060074 0.0 35.6015824
318 p 0.515038075 -0.857167301 0.0 21.4087693
319 p 0.551936985 -0.833885822 0.0 16.5842159
320 pz 16.45
321 p 0.0790090432 0.996873899 0.0 -0.399164699
322 p -0.743144825 -0.669130606 0.0 14.2
323 p 0.999671614 -0.0256254734 0.0 -0.399164699
324 pz 12.05
325 pz 15.05
326 c/z -5.528541789 -4.977921386 5
327 c/z -14.11940969 -1.795215674 2.506
328 c/z -3.261261491 -13.85441095 2.506
329 p -0.743144825 -0.669130606 0.0 3.81061679
330 p 0.896750711 -0.442536058 0.0 -12.7951179
331 p -0.346375825 0.938095831 0.0 -12.7951179
332 p 0.0790090432 0.996873899 0.0 -0.0991646991
333 p -0.743144825 -0.669130606 0.0 14.5
334 p 0.999671614 -0.0256254734 0.0 -0.0991646991
335 pz 11.75
336 pz 15.35
337 c/z -5.528541789 -4.977921386 5.3
338 c/z -14.11940969 -1.795215674 2.806
339 c/z -3.261261491 -13.85441095 2.806
340 p -0.743144825 -0.669130606 0.0 3.59289059
341 p 0.896750711 -0.442536058 0.0 -12.9062083
342 p -0.346375825 0.938095831 0.0 -12.9062083
343 p 0.0790090432 0.996873899 0.0 0.400835301
344 p -0.743144825 -0.669130606 0.0 15
345 p 0.999671614 -0.0256254734 0.0 0.400835301
346 pz 11.25
347 pz 15.85
348 c/z -5.528541789 -4.977921386 5.8
349 c/z -14.11940969 -1.795215674 3.306
350 c/z -3.261261491 -13.85441095 3.306
351 p -0.743144825 -0.669130606 0.0 3.23001359
352 p 0.896750711 -0.442536058 0.0 -13.0913589
353 p -0.346375825 0.938095831 0.0 -13.0913589
354 p 0.0790090432 0.996873899 0.0 0.700835301
355 p -0.743144825 -0.669130606 0.0 15.3
356 p 0.999671614 -0.0256254734 0.0 0.700835301
357 c/z -5.528541789 -4.977921386 6.1
358 c/z -14.11940969 -1.795215674 3.606
359 c/z -3.261261491 -13.85441095 3.606
360 p -0.743144825 -0.669130606 0.0 3.01228739
361 p 0.896750711 -0.442536058 0.0 -13.2024492
362 p -0.346375825 0.938095831 0.0 -13.2024492
363 p -0.910215067 -0.414135886 0.0 9.20158972
364 p 0.507010588 0.861939826 0.0 -9.20158972
365 p -0.0790090432 -0.996873899 0.0 -0.399164699
366 p 0.743144825 0.669130606 0.0 14.2
367 p -0.999671614 0.0256254734 0.0 -0.399164699
368 c/z 5.528541789 4.977921386 5
369 c/z 14.11940969 1.795215674 2.506
370 c/z 3.261261491 13.85441095 2.506
371 p 0.743144825 0.669130606 0.0 3.81061679
372 p -0.896750711 0.442536058 0.0 -12.7951179
373 p 0.346375825 -0.938095831 0.0 -12.7951179
374 p -0.0790090432 -0.996873899 0.0 -0.0991646991
375 p 0.743144825 0.669130606 0.0 14.5
376 p -0.999671614 0.0256254734 0.0 -0.0991646991
377 pz 15.65
378 c/z 5.528541789 4.977921386 5.3
379 c/z 14.11940969 1.795215674 2.806
380 c/z 3.261261491 13.85441095 2.806
381 p 0.743144825 0.669130606 0.0 3.59289059
382 p -0.896750711 0.442536058 0.0 -12.9062083
383 p 0.346375825 -0.938095831 0.0 -12.9062083
384 p -0.0790090432 -0.996873899 0.0 0.400835301
385 p 0.743144825 0.669130606 0.0 15
386 p -0.999671614 0.0256254734 0.0 0.400835301
387 pz 16.15
388 c/z 5.528541789 4.977921386 5.8
389 c/z 14.11940969 1.795215674 3.306
390 c/z 3.261261491 13.85441095 3.306
391 p 0.743144825 0.669130606 0.0 3.23001359
392 p -0.896750711 0.442536058 0.0 -13.0913589
393 p 0.346375825 -0.938095831 0.0 -13.0913589
394 p -0.0790090432 -0.996873899 0.0 0.700835301
395 p 0.743144825 0.669130606 0.0 15.3
396 p -0.999671614 0.0256254734 0.0 0.700835301
397 c/z 5.528541789 4.977921386 6.1
398 c/z 14.11940969 1.795215674 3.606
399 c/z 3.261261491 13.85441095 3.606
400 p 0.743144825 0.669130606 0.0 3.01228739
401 p -0.896750711 0.442536058 0.0 -13.2024492
402 p 0.346375825 -0.938095831 0.0 -13.2024492
403 p 0.910215067 0.414135886 0.0 9.20158972
404 p -0.507010588 -0.861939826 0.0 -9.20158972
405 p -0.0523359562 -0.998629535 0.0 3.27743993
406 p -0.998629535 0.0523359562 0.0 -3.27743993
407 c/z 0.8057123567 -9.034549262 2
408 p -0.669130606 0.743144825 0.0 -8.6673189
409 p -0.0523359562 -0.998629535 0.0 10.98
410 p 0.998629535 -0.0523359562 0.0 10.98
411 c/z 8.900837182 -1.745666131 2
412 p 0.0523359562 0.998629535 0.0 3.27743993
413 p 0.998629535 -0.0523359562 0.0 -3.27743993
414 c/z -0.8057123567 9.034549262 2
415 p 0.669130606 -0.743144825 0.0 -8.6673189
416 p -0.998629535 0.0523359562 0.0 10.98
417 p 0.0523359562 0.998629535 0.0 10.98
418 c/z -8.900837182 1.745666131 2
419 p -0.0523359562 -0.998629535 0.0 3.47743993
420 p -0.998629535 0.0523359562 0.0 -3.47743993
421 c/z 0.8057123567 -9.034549262 2.2
422 p -0.669130606 0.743144825 0.0 -8.8673189
423 p -0.0523359562 -0.998629535 0.0 11.18
424 p 0.998629535 -0.0523359562 0.0 11.18
425 c/z 8.900837182 -1.745666131 2.2
426 p 0.0523359562 0.998629535 0.0 3.47743993
427 p 0.998629535 -0.0523359562 0.0 -3.47743993
428 c/z -0.8057123567 9.034549262 2.2
429 p 0.669130606 -0.743144825 0.0 -8.8673189
430 p -0.998629535 0.0523359562 0.0 11.18
431 p 0.0523359562 0.998629535 0.0 11.18
432 c/z -8.900837182 1.745666131 2.2
433 p -0.669130606 0.743144825 0.0 -7.88
434 p -0.669130606 0.743144825 0.0 7.88
435 p -0.669130606 0.743144825 0.0 -8.227
436 p -0.669130606 0.743144825 0.0 8.227
437 p -0.951056516 0.309016994 0.0 3.07651984
438 p -0.207911691 0.978147601 0.0 -3.07651984
439 p -0.951056516 0.309016994 0.0 2.77600903
440 p -0.207911691 0.978147601 0.0 -2.77600903
441 p 0.951056516 -0.309016994 0.0 3.07738587
442 p 0.207911691 -0.978147601 0.0 -3.07738587
443 p 0.951056516 -0.309016994 0.0 2.77687505
444 p 0.207911691 -0.978147601 0.0 -2.77687505
445 pz 11.7
446 pz 15.7
447 cz 10
448 kz 11.87857143 3136
449 kz 15.52142857 3136
450 pz 15.4
451 kz 12.17857143 3136
452 kz 15.22142857 3136
453 pz 11.8785714
454 pz 15.5214286
455 pz 12.1785714
456 pz 15.2214286
457 p -0.275637356 0.961261696 0.0 -1.42108547e-14
458 p 0.927183855 -0.374606593 0.0 1.42108547e-14
459 pz -40
460 pz 40
461 pz -50
462 pz 50
463 cz 370
464 pz 3.7
465 pz 25.7
466 pz 24.9
467 pz -8.3
468 pz 35.7
469 pz -8.2
470 pz 34.9
471 p 0.116670737 -0.99317065 0.0 14.1431934
472 p 0.0 -1 0.0 -6.99931633
473 p 0.0 -1 0.0 13.0006837
474 p 0.0 -1 0.0 -6.39931633
475 p 0.0 -1 0.0 12.4006837
476 p 0.0 -1 0.0 -10.9993163
477 p 0.0 -1 0.0 17.0006837
478 p 0.0 -1 0.0 -10.3993163
479 p 0.0 -1 0.0 16.4006837
480 cz 2495
481 cz 2510
482 cz 2525
483 cz 2555
484 cz 2570
485 cz 2590
486 cz 2605
487 cz 2625
488 cz 2655
489 cz 2670
490 cz 2701
491 cz 2716.5
492 cz 2742
493 cz 2757.5
494 pz 180
495 pz 185
496 pz 205
497 pz 250
498 pz 315
499 cz 1195
500 cz 1210
501 cz 1225
502 cz 1255
503 cz 1270
504 cz 1290
505 cz 1305
506 cz 1325
507 cz 1355
508 cz 1370
509 cz 1401
510 cz 1416.5
511 cz 1442
512 cz 1457.5
513 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -2.542696304  2.823950337  -27.1  186.0025
514 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -2.542696304  2.823950337  -27.1  185.5225
515 p 0.313244941 0.949672368 0.0 -15.0081779
516 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -2.542696304  2.823950337  -27.1  184.9625
517 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -2.542696304  2.823950337  -27.1  183.9725
518 p 0.313244941 0.949672368 0.0 -16.7183727
519 gq  0.9603191625  0.03968083752  1  0.3904165399  0  0  25.46400231
        5.176178954  -27.1  351.1945606
520 gq  0.9603191625  0.03968083752  1  0.3904165399  0  0  25.46400231
        5.176178954  -27.1  350.7145606
521 gq  0.9603191625  0.03968083752  1  0.3904165399  0  0  25.46400231
        5.176178954  -27.1  350.1545606
522 gq  0.9603191625  0.03968083752  1  0.3904165399  0  0  25.46400231
        5.176178954  -27.1  349.1645606
523 p 0.743144825 0.669130606 0.0 -25.2
524 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -4.813725582  5.346183874  -27.1  195.330909
525 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -4.813725582  5.346183874  -27.1  194.850909
526 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -4.813725582  5.346183874  -27.1  194.290909
527 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -4.813725582  5.346183874  -27.1  193.300909
528 p 0.743144825 0.669130606 0.0 -27.2
529 p 0.525482745 0.473146789 -0.707106781 -37.6534361
530 p 0.0 0.0 -1 -43.55
531 c/z -27.09598678 -29.23757701 1.1
532 c/z -27.09598678 -29.23757701 1.3
533 c/z -27.09598678 -29.23757701 1.5
534 c/z -27.09598678 -29.23757701 1.8
535 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  2.542696304
        -2.823950337  -27.1  186.0025
536 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  2.542696304
        -2.823950337  -27.1  185.5225
537 p 0.977212976 0.212261159 0.0 -15.0081779
538 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  2.542696304
        -2.823950337  -27.1  184.9625
539 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  2.542696304
        -2.823950337  -27.1  183.9725
540 p 0.977212976 0.212261159 0.0 -16.7183727
541 gq  0.09032599712  0.9096740029  1  0.5732964726  0  0  7.809536335
        24.78344981  -27.1  351.1945606
542 gq  0.09032599712  0.9096740029  1  0.5732964726  0  0  7.809536335
        24.78344981  -27.1  350.7145606
543 gq  0.09032599712  0.9096740029  1  0.5732964726  0  0  7.809536335
        24.78344981  -27.1  350.1545606
544 gq  0.09032599712  0.9096740029  1  0.5732964726  0  0  7.809536335
        24.78344981  -27.1  349.1645606
545 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  4.813725582
        -5.346183874  -27.1  195.330909
546 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  4.813725582
        -5.346183874  -27.1  194.850909
547 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  4.813725582
        -5.346183874  -27.1  194.290909
548 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  4.813725582
        -5.346183874  -27.1  193.300909
549 c/z -31.90971236 -23.89139314 1.1
550 c/z -31.90971236 -23.89139314 1.3
551 c/z -31.90971236 -23.89139314 1.5
552 c/z -31.90971236 -23.89139314 1.8
553 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  2.542696304
        -2.823950337  -27.4  190.09
554 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  2.542696304
        -2.823950337  -27.4  189.61
555 p -0.313244941 -0.949672368 0.0 -15.0081779
556 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  2.542696304
        -2.823950337  -27.4  189.05
557 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  2.542696304
        -2.823950337  -27.4  188.06
558 p -0.313244941 -0.949672368 0.0 -16.7183727
559 gq  0.9603191625  0.03968083752  1  0.3904165399  0  0
        -25.46400231  -5.176178954  -27.4  355.2820606
560 gq  0.9603191625  0.03968083752  1  0.3904165399  0  0
        -25.46400231  -5.176178954  -27.4  354.8020606
561 gq  0.9603191625  0.03968083752  1  0.3904165399  0  0
        -25.46400231  -5.176178954  -27.4  354.2420606
562 gq  0.9603191625  0.03968083752  1  0.3904165399  0  0
        -25.46400231  -5.176178954  -27.4  353.2520606
563 p -0.743144825 -0.669130606 0.0 -25.2
564 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  4.813725582
        -5.346183874  -27.4  199.418409
565 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  4.813725582
        -5.346183874  -27.4  198.938409
566 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  4.813725582
        -5.346183874  -27.4  198.378409
567 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0  4.813725582
        -5.346183874  -27.4  197.388409
568 p -0.743144825 -0.669130606 0.0 -27.2
569 p -0.525482745 -0.473146789 -0.707106781 -37.7595021
570 p 0.0 0.0 -1 -43.7
571 c/z 27.09598678 29.23757701 1.1
572 c/z 27.09598678 29.23757701 1.3
573 c/z 27.09598678 29.23757701 1.5
574 c/z 27.09598678 29.23757701 1.8
575 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -2.542696304  2.823950337  -27.4  190.09
576 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -2.542696304  2.823950337  -27.4  189.61
577 p -0.977212976 -0.212261159 0.0 -15.0081779
578 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -2.542696304  2.823950337  -27.4  189.05
579 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -2.542696304  2.823950337  -27.4  188.06
580 p -0.977212976 -0.212261159 0.0 -16.7183727
581 gq  0.09032599712  0.9096740029  1  0.5732964726  0  0
        -7.809536335  -24.78344981  -27.4  355.2820606
582 gq  0.09032599712  0.9096740029  1  0.5732964726  0  0
        -7.809536335  -24.78344981  -27.4  354.8020606
583 gq  0.09032599712  0.9096740029  1  0.5732964726  0  0
        -7.809536335  -24.78344981  -27.4  354.2420606
584 gq  0.09032599712  0.9096740029  1  0.5732964726  0  0
        -7.809536335  -24.78344981  -27.4  353.2520606
585 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -4.813725582  5.346183874  -27.4  199.418409
586 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -4.813725582  5.346183874  -27.4  198.938409
587 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -4.813725582  5.346183874  -27.4  198.378409
588 gq  0.4477357684  0.5522642316  1  -0.9945218954  0  0
        -4.813725582  5.346183874  -27.4  197.388409
589 c/z 31.90971236 23.89139314 1.1
590 c/z 31.90971236 23.89139314 1.3
591 c/z 31.90971236 23.89139314 1.5
592 c/z 31.90971236 23.89139314 1.8
593 p 0.0208318263 0.0 0.999782994 9.23843441
594 p 0.0 -1 0.0 1.50068367
595 p 0.0 -1 0.0 4.50068367
596 c/y 24.97748848 -8384.48 8398.5
597 c/y 24.97748848 -8384.48 8401.5
598 p 0.0 -1 0.0 0.700683672
599 p 0.0 -1 0.0 5.30068367
600 c/y 24.97748848 -8384.48 8397.7
601 c/y 24.97748848 -8384.48 8402.3
602 p 0.99913207 0.0 -0.0416546114 556.475776
603 p 0.99913207 0.0 -0.0416546114 598.475776
604 gq  0.00173510665  1  0.9982648934  5.412337245e-16
        -1.387778781e-17  0.0832369162  -1.985889355  6.001367345
        -47.63375952  520.9838032
605 gq  0.00173510665  1  0.9982648934  5.412337245e-16
        -1.387778781e-17  0.0832369162  -1.985889355  6.001367345
        -47.63375952  513.2338032
606 p 0.99913207 0.0 -0.0416546114 557.475776
607 p 0.99913207 0.0 -0.0416546114 597.475776
608 gq  0.00173510665  1  0.9982648934  5.412337245e-16
        -1.387778781e-17  0.0832369162  -1.985889355  6.001367345
        -47.63375952  444.9838032
609 p 0.99913207 0.0 -0.0416546114 556.825776
610 p 0.99913207 0.0 -0.0416546114 557.125776
611 gq  0.00173510665  1  0.9982648934  5.412337245e-16
        -1.387778781e-17  0.0832369162  -1.985889355  6.001367345
        -47.63375952  486.9838032
612 p 0.99913207 0.0 -0.0416546114 598.125776
613 p 0.99913207 0.0 -0.0416546114 597.825776
614 p 0.044033377 0.0 0.99903006 18.3648428
615 c/y 208.0861353 -8392.113945 8398.5
616 c/y 208.0861353 -8392.113945 8401.5
617 c/y 208.0861353 -8392.113945 8397.7
618 c/y 208.0861353 -8392.113945 8402.3
619 p 0.998922387 0.0 -0.0464118929 598.855944
620 p 0.998922387 0.0 -0.0464118929 647.355944
621 gq  0.002154063802  1  0.9978459362  5.689893001e-16
        -1.387778781e-17  0.09272375771  -2.467922076  6.001367345
        -53.1170452  659.631885
622 gq  0.002154063802  1  0.9978459362  5.689893001e-16
        -1.387778781e-17  0.09272375771  -2.467922076  6.001367345
        -53.1170452  651.881885
623 p 0.998922387 0.0 -0.0464118929 599.855944
624 p 0.998922387 0.0 -0.0464118929 646.355944
625 gq  0.002154063802  1  0.9978459362  5.689893001e-16
        -1.387778781e-17  0.09272375771  -2.467922076  6.001367345
        -53.1170452  583.631885
626 p 0.998922387 0.0 -0.0464118929 599.205944
627 p 0.998922387 0.0 -0.0464118929 599.505944
628 gq  0.002154063802  1  0.9978459362  5.689893001e-16
        -1.387778781e-17  0.09272375771  -2.467922076  6.001367345
        -53.1170452  625.631885
629 p 0.998922387 0.0 -0.0464118929 647.005944
630 p 0.998922387 0.0 -0.0464118929 646.705944
631 p 0.049176586 0.0 0.9987901 21.4474109
632 c/y 210.5835923 -8392.229982 8398.5
633 c/y 210.5835923 -8392.229982 8401.5
634 c/y 210.5835923 -8392.229982 8397.7
635 c/y 210.5835923 -8392.229982 8402.3
636 p 0.999404555 0.0 -0.0345041431 648.125129
637 p 0.999404555 0.0 -0.0345041431 973.625129
638 gq  0.001190535892  1  0.9988094641  4.163336342e-16
        -1.387778781e-17  0.06896719559  -1.292359183  6.001367345
        -37.43288593  303.4768878
639 gq  0.001190535892  1  0.9988094641  4.163336342e-16
        -1.387778781e-17  0.06896719559  -1.292359183  6.001367345
        -37.43288593  295.7268878
640 p 0.999404555 0.0 -0.0345041431 649.125129
641 p 0.999404555 0.0 -0.0345041431 972.625129
642 gq  0.001190535892  1  0.9988094641  4.163336342e-16
        -1.387778781e-17  0.06896719559  -1.292359183  6.001367345
        -37.43288593  227.4768878
643 p 0.999404555 0.0 -0.0345041431 648.475129
644 p 0.999404555 0.0 -0.0345041431 648.775129
645 gq  0.001190535892  1  0.9988094641  4.163336342e-16
        -1.387778781e-17  0.06896719559  -1.292359183  6.001367345
        -37.43288593  269.4768878
646 p 0.999404555 0.0 -0.0345041431 973.275129
647 p 0.999404555 0.0 -0.0345041431 972.975129
648 p 0.99865016 0.0 -0.0519409025 648.699575
649 p 0.99865016 0.0 -0.0519409025 972.199575
650 p -0.0327025016 0.0 -0.99946513 -24.3386999
651 c/y 1085.687616 8384.962846 8398.5
652 c/y 1085.687616 8384.962846 8401.5
653 c/y 1085.687616 8384.962846 8397.7
654 c/y 1085.687616 8384.962846 8402.3
655 p 0.999958695 0.0 -0.00908893268 974.360617
656 p 0.999958695 0.0 -0.00908893268 1098.36062
657 gq  8.260869727e-05  1  0.9999173913  -5.412337245e-16
        6.938893904e-18  0.01817711452  0.09893642757  6.001367345
        10.88492395  -17.62305806
658 gq  8.260869727e-05  1  0.9999173913  -5.412337245e-16
        6.938893904e-18  0.01817711452  0.09893642757  6.001367345
        10.88492395  -25.37305806
659 p 0.999958695 0.0 -0.00908893268 975.360617
660 p 0.999958695 0.0 -0.00908893268 1097.36062
661 gq  8.260869727e-05  1  0.9999173913  -5.412337245e-16
        6.938893904e-18  0.01817711452  0.09893642757  6.001367345
        10.88492395  -93.62305806
662 p 0.999958695 0.0 -0.00908893268 974.710617
663 p 0.999958695 0.0 -0.00908893268 975.010617
664 gq  8.260869727e-05  1  0.9999173913  -5.412337245e-16
        6.938893904e-18  0.01817711452  0.09893642757  6.001367345
        10.88492395  -51.62305806
665 p 0.999958695 0.0 -0.00908893268 1098.01062
666 p 0.999958695 0.0 -0.00908893268 1097.71062
667 p 0.999909518 0.0 -0.0134519752 975.375089
668 p 0.999909518 0.0 -0.0134519752 1097.37509
669 p -0.00619043665 0.0 -0.999980839 1.47436489
670 c/y 1088.267404 8384.92814 8398.5
671 c/y 1088.267404 8384.92814 8401.5
672 c/y 1088.267404 8384.92814 8397.7
673 c/y 1088.267404 8384.92814 8402.3
674 p 0.999999426 0.0 0.00107142837 1098.50492
675 p 0.999999426 0.0 0.00107142837 1123.00492
676 gq  1.147958744e-06  1  0.999998852  -3.261280135e-15
        -3.686287386e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  216.5785418
677 gq  1.147958744e-06  1  0.999998852  -3.261280135e-15
        -3.686287386e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  208.8285418
678 p 0.999999426 0.0 0.00107142837 1099.50492
679 p 0.999999426 0.0 0.00107142837 1122.00492
680 gq  1.147958744e-06  1  0.999998852  -3.261280135e-15
        -3.686287386e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  140.5785418
681 p 0.999999426 0.0 0.00107142837 1098.85492
682 p 0.999999426 0.0 0.00107142837 1099.15492
683 gq  1.147958744e-06  1  0.999998852  -3.261280135e-15
        -3.686287386e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  182.5785418
684 p 0.999999426 0.0 0.00107142837 1122.65492
685 p 0.999999426 0.0 0.00107142837 1122.35492
686 p 0.00107142837 0.0 -0.999999426 14.7426734
687 p -0.00107142837 0.0 0.999999426 -17.7426734
688 p 0.00107142837 0.0 -0.999999426 13.9426734
689 p -0.00107142837 0.0 0.999999426 -18.5426734
690 p 0.999404555 0.0 -0.0345041431 851.125129
691 p 0.999404555 0.0 -0.0345041431 901.125129
692 p 0.0 -1 0.0 -4.49931633
693 p 0.0 -1 0.0 10.5006837
694 p -0.00107142837 0.0 0.999999426 -23.7426734
695 p -0.00107142837 0.0 0.999999426 -8.7426734
696 p -0.00107142837 0.0 0.999999426 -26.2426734
697 p -0.00107142837 0.0 0.999999426 -6.2426734
698 p 0.999999426 0.0 0.00107142837 1322.60492
699 p 0.0 -1 0.0 -9.99931633
700 p 0.0 -1 0.0 16.0006837
701 p -0.00107142837 0.0 0.999999426 -29.2426734
702 p -0.00107142837 0.0 0.999999426 -3.2426734
703 p 0.0 -1 0.0 -12.4993163
704 p 0.0 -1 0.0 18.5006837
705 p -0.00107142837 0.0 0.999999426 -31.7426734
706 p -0.00107142837 0.0 0.999999426 -0.742673404
707 p 0.999999426 0.0 0.00107142837 1525.60492
708 p 0.0 -1 0.0 -2.49931633
709 p 0.00107142837 0.0 -0.999999426 10.7426734
710 py -8.50068367
711 p -0.00107142837 0.0 0.999999426 -21.7426734
712 p 0.0 -1 0.0 -16.4993163
713 p 0.0 -1 0.0 22.5006837
714 p -0.00107142837 0.0 0.999999426 -35.7426734
715 p -0.00107142837 0.0 0.999999426 3.2573266
716 p -0.00107142837 0.0 0.999999426 -36.2426734
717 p -0.00107142837 0.0 0.999999426 3.7573266
718 p 0.0 -1 0.0 -21.9993163
719 p 0.0 -1 0.0 28.0006837
720 p -0.00107142837 0.0 0.999999426 -41.2426734
721 p -0.00107142837 0.0 0.999999426 8.7573266
722 p 0.0 -1 0.0 -26.9993163
723 p 0.0 -1 0.0 33.0006837
724 p -0.00107142837 0.0 0.999999426 -46.2426734
725 p -0.00107142837 0.0 0.999999426 13.7573266
726 p 0.0 -1 0.0 -41.9993163
727 p 0.0 -1 0.0 48.0006837
728 p -0.00107142837 0.0 0.999999426 -106.242673
729 p -0.00107142837 0.0 0.999999426 28.7573266
730 p 0.0 -1 0.0 -56.9993163
731 p 0.0 -1 0.0 63.0006837
732 p -0.00107142837 0.0 0.999999426 43.7573266
733 p 0.0 -1 0.0 -71.9993163
734 p 0.0 -1 0.0 78.0006837
735 p -0.00107142837 0.0 0.999999426 58.7573266
736 p 0.0 -1 0.0 -86.9993163
737 p 0.0 -1 0.0 93.0006837
738 p -0.00107142837 0.0 0.999999426 73.7573266
739 p 0.999999426 0.0 0.00107142837 1567.15492
740 p 0.999999426 0.0 0.00107142837 1618.15492
741 p 0.999999426 0.0 0.00107142837 1568.15492
742 p 0.999999426 0.0 0.00107142837 1617.15492
743 p 0.999999426 0.0 0.00107142837 1567.50492
744 p 0.999999426 0.0 0.00107142837 1567.80492
745 p 0.999999426 0.0 0.00107142837 1617.80492
746 p 0.999999426 0.0 0.00107142837 1617.50492
747 p 0.999999426 0.0 0.00107142837 1535.65492
748 p 0.999999426 0.0 0.00107142837 1560.65492
749 p 0.0 -1 0.0 -76.9993163
750 p 0.0 -1 0.0 83.0006837
751 p -0.00107142837 0.0 0.999999426 -82.2426734
752 p -0.00107142837 0.0 0.999999426 83.7573266
753 p 0.999999426 0.0 0.00107142837 1529.65492
754 p 0.999999426 0.0 0.00107142837 1566.65492
755 p 0.0 -1 0.0 -82.9993163
756 p 0.0 -1 0.0 89.0006837
757 p -0.00107142837 0.0 0.999999426 -88.2426734
758 p -0.00107142837 0.0 0.999999426 89.7573266
759 p 0.999999426 0.0 0.00107142837 1519.65492
760 p 0.999999426 0.0 0.00107142837 1576.65492
761 p 0.0 -1 0.0 -92.9993163
762 p 0.0 -1 0.0 99.0006837
763 p -0.00107142837 0.0 0.999999426 -98.2426734
764 p -0.00107142837 0.0 0.999999426 99.7573266
765 p 0.999999426 0.0 0.00107142837 1565.65492
766 p 0.0 -1 0.0 -16.9993163
767 p 0.0 -1 0.0 23.0006837
768 p -0.00107142837 0.0 0.999999426 -18.7426734
769 p -0.00107142837 0.0 0.999999426 -13.7426734
770 p 0.0 -1 0.0 -1.99931633
771 p 0.0 -1 0.0 8.00068367
772 p -0.00107142837 0.0 0.999999426 -21.2426734
773 p -0.00107142837 0.0 0.999999426 -11.2426734
774 p 0.999999426 0.0 0.00107142837 1542.15492
775 p 0.999999426 0.0 0.00107142837 1554.15492
776 p 0.0 -1 0.0 -40.2493163
777 p 0.0 -1 0.0 46.2506837
778 p -0.00107142837 0.0 0.999999426 -31.4926734
779 p -0.00107142837 0.0 0.999999426 55.0073266
780 p -0.000757614263 -0.707106781 0.707106375 -38.0013254
781 p 0.000757614263 -0.707106781 -0.707106375 -54.6286961
782 p 0.000757614264 0.707106781 -0.707106375 -58.8723036
783 p -0.000757614263 0.707106781 0.707106375 -42.2449329
784 p 0.999999426 0.0 0.00107142837 1544.87992
785 p 0.999999426 0.0 0.00107142837 1551.42992
786 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  -1306.048053
787 p 0.999999426 0.0 0.00107142837 1541.60992
788 p 0.999999426 0.0 0.00107142837 1554.69992
789 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  146.9888312
790 p 0.999999426 0.0 0.00107142837 1559.69992
791 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  47.23883119
792 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  3.238831188
793 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  -83.80116881
794 p 0.000139849465 -0.991444861 -0.130526117 1.44037334
795 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.05433711802  6.001367345
        -50.71462408  651.7481146
796 p 0.000410017885 -0.923879533 -0.382683213 -1.72706387
797 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.05334409944  13.04124537
        -49.78780709  661.9756653
798 p 0.000652244263 -0.79335334 -0.60876108 -4.77680453
799 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.05043271625  19.60136734
        -47.07051716  649.7124327
800 p 0.000850021273 -0.608761429 -0.793352885 -7.50101385
801 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.04580137442  25.23467179
        -42.7479331  615.7941357
802 p 0.000989870738 -0.382683432 -0.923879002 -9.71404147
803 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.03976569224  29.55725833
        -37.11463189  562.5322504
804 p 0.00106226215 -0.130526192 -0.991444292 -11.2650732
805 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.03273699147  32.27454982
        -30.55451368  493.556486
806 p 0.00106226215 0.130526192 -0.991444292 -12.0484089
807 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  33.20136734
        -23.5146397  413.5674271
808 p 0.000989870738 0.382683432 -0.923879002 -12.0106653
809 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.01765154144  32.27454982
        -16.47476571  328.0161957
810 p 0.000850021273 0.608761429 -0.793352885 -11.1544148
811 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.01062284067  29.55725833
        -9.914647501  242.7329669
812 p 0.000652244263 0.79335334 -0.60876108 -9.53800935
813 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.004587158491  25.23467179
        -4.281346287  163.5296519
814 p 0.000410017885 0.923879533 -0.382683213 -7.27160433
815 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -4.41833387e-05  19.60136734
        0.04123776707  95.80382559
816 p 0.000139849465 0.991444861 -0.130526117 -4.50965148
817 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.002955566523  13.04124537
        2.7585277  44.17089116
818 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.00394858511  6.001367345
        3.685344692  12.14954777
819 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.002955566523
        -1.038510682  2.7585277  1.921997061
820 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -4.418333853e-05
        -7.598632655  0.04123776707  14.1852297
821 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.004587158491  -13.2319371
        -4.281346287  48.10352664
822 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.01062284067  -17.55452364
        -9.914647501  101.365412
823 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.01765154144  -20.27181513
        -16.47476571  170.3411764
824 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  -21.19863266
        -23.5146397  250.3302353
825 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.03273699147  -20.27181513
        -30.55451368  335.8814667
826 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.03976569224  -17.55452364
        -37.11463189  421.1646954
827 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.04580137442  -13.2319371
        -42.7479331  500.3680105
828 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.05043271625  -7.598632655
        -47.07051716  568.0938368
829 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.05334409944  -1.038510682
        -49.78780709  619.7267712
830 p 0.999999426 0.0 0.00107142837 1548.15492
831 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  -37.72116881
832 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  -43.20116881
833 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  172.8285418
834 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  112.8060418
835 p 0.000277306067 -0.965925826 -0.258818897 7.10235108
836 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01074548441  8.957785599
        10.02911494  45.0462962
837 p 0.000535714183 -0.866025404 -0.499999713 10.720005
838 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01238514984  14.66914709
        11.55946876  87.04133693
839 p 0.000757614263 -0.707106781 -0.707106375 13.6071083
840 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01555274022  19.78981371
        14.51588532  150.4269738
841 p 0.000927884184 -0.5 -0.866024907 15.5669096
842 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.0200323895  23.9708205
        18.69688971  230.8835804
843 p 0.00103492033 -0.258819045 -0.965925272 16.4658518
844 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.02551881697  26.92723876
        23.81755339  322.928172
845 p 0.00107142837 0.0 -0.999999426 16.2426734
846 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03163813168  28.45759345
        29.52891161  420.2880617
847 p 0.00103492033 0.258819045 -0.965925272 14.9125836
848 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03797331244  28.45759345
        35.44174472  516.328334
849 p 0.000927884184 0.5 -0.866024907 12.566226
850 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.04409262715  26.92723876
        41.15310293  604.504003
851 p 0.000757614263 0.707106781 -0.707106375 9.36350074
852 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.04957905463  23.9708205
        46.27376661  678.8060426
853 p 0.000535714183 0.866025404 -0.499999713 5.52266841
854 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05405870391  19.78981371
        50.45477101  734.1708916
855 p 0.000277306067 0.965925826 -0.258818897 1.30547536
856 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05722629429  14.66914709
        53.41118756  766.825527
857 py -3.00068367
858 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05886595972  8.957785599
        54.94154138  774.5445894
859 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05886595972  3.044949091
        54.94154138  756.8020374
860 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05722629429  -2.666412398
        53.41118756  714.8069967
861 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05405870391  -7.787079022
        50.45477101  651.4213598
862 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.04957905463  -11.96808581
        46.27376661  570.9647532
863 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.04409262715  -14.92450407
        41.15310293  478.9201616
864 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03797331244  -16.45485877
        35.44174472  381.5602719
865 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03163813168  -16.45485877
        29.52891161  285.5199996
866 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.02551881697  -14.92450407
        23.81755339  197.3443306
867 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.0200323895  -11.96808581
        18.69688971  123.042291
868 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01555274022  -7.787079022
        14.51588532  67.67744204
869 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01238514984  -2.666412398
        11.55946876  35.02280663
870 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01074548441  3.044949091
        10.02911494  27.30374423
871 p 0.999999426 0.0 0.00107142837 1547.95492
872 p 0.999999426 0.0 0.00107142837 1548.35492
873 p 0.000277306067 -0.965925826 -0.258818897 -0.144582187
874 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  -1452.761169
875 p 0.000757614263 -0.707106781 -0.707106375 -6.19188159
876 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  -1484.921169
877 p 0.00103492033 -0.258819045 -0.965925272 -10.5800713
878 p 0.00103492033 0.258819045 -0.965925272 -12.1333395
879 p 0.000757614263 0.707106781 -0.707106375 -10.4354891
880 p 0.000277306067 0.965925826 -0.258818897 -5.9414579
881 p 0.999999426 0.0 0.00107142837 1543.01742
882 p 0.999999426 0.0 0.00107142837 1544.01742
883 p 0.0 -1 0.0 -2.79931633
884 p 0.0 -1 0.0 8.80068367
885 p -0.00107142837 0.0 0.999999426 -22.0426734
886 p -0.00107142837 0.0 0.999999426 -10.4426734
887 p 0.0 -1 0.0 -3.29931633
888 p 0.0 -1 0.0 9.30068367
889 p -0.00107142837 0.0 0.999999426 -22.5426734
890 p -0.00107142837 0.0 0.999999426 -9.9426734
891 p 0.0 -1 0.0 -3.44931633
892 p 0.0 -1 0.0 9.45068367
893 p -0.00107142837 0.0 0.999999426 -22.6926734
894 p -0.00107142837 0.0 0.999999426 -9.7926734
895 p 0.0 -1 0.0 -4.74931643
896 p 0.0 -1 0.0 10.7506838
897 p -0.00107142837 0.0 0.999999426 -23.9926735
898 p -0.00107142837 0.0 0.999999426 -8.4926733
899 p 0.999999426 0.0 0.00107142837 1543.36742
900 p 0.999999426 0.0 0.00107142837 1543.66742
901 p 0.0 -1 0.0 -1.83681633
902 p -0.00107142837 0.0 0.999999426 -21.0801734
903 p 0.0 -1 0.0 -0.224316328
904 p -0.00107142837 0.0 0.999999426 -19.4676734
905 p 0.0 -1 0.0 1.38818367
906 p -0.00107142837 0.0 0.999999426 -17.8551734
907 p 0.0 -1 0.0 4.61318367
908 p -0.00107142837 0.0 0.999999426 -14.6301734
909 p 0.0 -1 0.0 6.22568367
910 p -0.00107142837 0.0 0.999999426 -13.0176734
911 p 0.0 -1 0.0 7.83818367
912 p -0.00107142837 0.0 0.999999426 -11.4051734
913 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.04689946975  -8.198632755
        43.77282168  495.7299242
914 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.04344411327  -8.198632755
        40.54782353  427.746365
915 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03998875679  -8.198632755
        37.32282539  364.9631182
916 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.0365334003  -8.198632755
        34.09782724  307.380184
917 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03307804382  -8.198632755
        30.87282909  254.9975623
918 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.02962268734  -8.198632755
        27.64783094  207.815253
919 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.02616733086  -8.198632755
        24.42283279  165.8332563
920 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.02271197438  -8.198632755
        21.19783464  129.0515721
921 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01959143915  -5.286132655
        18.28533621  90.48427567
922 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01959143915  -2.061132655
        18.28533621  84.56054301
923 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01959143915  1.163867345
        18.28533621  83.83712286
924 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01959143915  4.388867345
        18.28533621  88.3140152
925 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01959143915  7.613867345
        18.28533621  97.99122004
926 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01959143915  10.83886734
        18.28533621  112.8687374
927 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01959143915  14.06386734
        18.28533621  132.9465672
928 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.01959143915  17.28886734
        18.28533621  158.2247096
929 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.02271197438  20.20136744
        21.19783464  214.270989
930 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.02616733086  20.20136744
        24.42283279  251.0526732
931 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.02962268734  20.20136744
        27.64783094  293.0346699
932 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03307804382  20.20136744
        30.87282909  340.2169792
933 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.0365334003  20.20136744
        34.09782724  392.5996009
934 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.03998875679  20.20136744
        37.32282539  450.1825351
935 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.04344411327  20.20136744
        40.54782353  512.9657818
936 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.04689946975  20.20136744
        43.77282168  580.9493411
937 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05002000497  17.28886734
        46.68532011  619.5166375
938 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05002000497  14.06386734
        46.68532011  594.2384951
939 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05002000497  10.83886734
        46.68532011  574.1606653
940 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05002000497  7.613867345
        46.68532011  559.283148
941 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05002000497  4.388867345
        46.68532011  549.6059431
942 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05002000497  1.163867345
        46.68532011  545.1290508
943 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05002000497  -2.061132655
        46.68532011  545.8524709
944 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  -0.05002000497  -5.286132655
        46.68532011  551.7762036
945 p 0.999999426 0.0 0.00107142837 1552.29242
946 p 0.999999426 0.0 0.00107142837 1553.29242
947 p 0.999999426 0.0 0.00107142837 1552.64242
948 p 0.999999426 0.0 0.00107142837 1552.94242
949 p 0.00106735126 0.0871557427 -0.996194126 -11.9741132
950 p 0.000452805194 0.906307787 -0.422618019 -7.68840391
951 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  -252.7611688
952 gq  1.147958744e-06  1  0.999998852  -2.02615702e-15
        -2.602085214e-18  -0.002142855503  0.02519426646  6.001367345
        -23.5146397  -1077.761169
953 p 0.999999426 0.0 0.00107142837 1738.65492
954 p 0.999999426 0.0 0.00107142837 1910.65492
955 p 0.999999426 0.0 0.00107142837 2082.65492
956 p 0.0 -1 0.0 -36.9993163
957 p 0.0 -1 0.0 43.0006837
958 p 0.0 -1 0.0 -46.9993163
959 p 0.0 -1 0.0 53.0006837
960 p 0.0 -1 0.0 -66.9993163
961 p 0.0 -1 0.0 73.0006837
962 p 0.999999426 0.0 0.00107142837 1618.65492
963 p 0.999999426 0.0 0.00107142837 2262.65492
964 p 0.999999426 0.0 0.00107142837 1619.65492
965 p 0.999999426 0.0 0.00107142837 2261.65492
966 p 0.999999426 0.0 0.00107142837 1619.00492
967 p 0.999999426 0.0 0.00107142837 1619.30492
968 p 0.999999426 0.0 0.00107142837 2262.30492
969 p 0.999999426 0.0 0.00107142837 2262.00492
970 p 0.999999426 0.0 0.00107142837 2270.65492
971 p 0.999999426 0.0 0.00107142837 2295.65492
972 p 0.999999426 0.0 0.00107142837 2264.65492
973 p 0.999999426 0.0 0.00107142837 2301.65492
974 p 0.999999426 0.0 0.00107142837 2254.65492
975 p 0.999999426 0.0 0.00107142837 2311.65492
976 p 0.999999426 0.0 0.00107142837 2300.65492
977 p 0.999999426 0.0 0.00107142837 2277.15492
978 p 0.999999426 0.0 0.00107142837 2289.15492
979 p 0.999999426 0.0 0.00107142837 2279.87992
980 p 0.999999426 0.0 0.00107142837 2286.42992
981 p 0.999999426 0.0 0.00107142837 2276.60992
982 p 0.999999426 0.0 0.00107142837 2289.69992
983 p 0.999999426 0.0 0.00107142837 2294.69992
984 p 0.999999426 0.0 0.00107142837 2283.15492
985 p 0.999999426 0.0 0.00107142837 2282.95492
986 p 0.999999426 0.0 0.00107142837 2283.35492
987 p 0.999999426 0.0 0.00107142837 2278.01742
988 p 0.999999426 0.0 0.00107142837 2279.01742
989 p 0.999999426 0.0 0.00107142837 2278.36742
990 p 0.999999426 0.0 0.00107142837 2278.66742
991 p 0.999999426 0.0 0.00107142837 2287.29242
992 p 0.999999426 0.0 0.00107142837 2288.29242
993 p 0.999999426 0.0 0.00107142837 2287.64242
994 p 0.999999426 0.0 0.00107142837 2287.94242
995 p 0.999999426 0.0 0.00107142837 2411.15492
996 p 0.999999426 0.0 0.00107142837 2302.15492
997 p 0.999999426 0.0 0.00107142837 2609.15492
998 p 0.999999426 0.0 0.00107142837 2303.15492
999 p 0.999999426 0.0 0.00107142837 2608.15492
1000 p 0.999999426 0.0 0.00107142837 2302.50492
1001 p 0.999999426 0.0 0.00107142837 2302.80492
1002 p 0.999999426 0.0 0.00107142837 2608.80492
1003 p 0.999999426 0.0 0.00107142837 2608.50492
1004 p 0.999999426 0.0 0.00107142837 2615.65492
1005 p 0.999999426 0.0 0.00107142837 2670.65492
1006 p 0.999999426 0.0 0.00107142837 2609.65492
1007 p 0.999999426 0.0 0.00107142837 2676.65492
1008 p 0.999999426 0.0 0.00107142837 2599.65492
1009 p 0.999999426 0.0 0.00107142837 2686.65492
1010 p 0.999999426 0.0 0.00107142837 2675.65492
1011 p 0.999999426 0.0 0.00107142837 2622.15492
1012 p 0.999999426 0.0 0.00107142837 2634.15492
1013 p 0.999999426 0.0 0.00107142837 2624.87992
1014 p 0.999999426 0.0 0.00107142837 2631.42992
1015 p 0.999999426 0.0 0.00107142837 2621.60992
1016 p 0.999999426 0.0 0.00107142837 2634.69992
1017 p 0.999999426 0.0 0.00107142837 2639.69992
1018 p 0.999999426 0.0 0.00107142837 2628.15492
1019 p 0.999999426 0.0 0.00107142837 2627.95492
1020 p 0.999999426 0.0 0.00107142837 2628.35492
1021 p 0.999999426 0.0 0.00107142837 2623.01742
1022 p 0.999999426 0.0 0.00107142837 2624.01742
1023 p 0.999999426 0.0 0.00107142837 2623.36742
1024 p 0.999999426 0.0 0.00107142837 2623.66742
1025 p 0.999999426 0.0 0.00107142837 2632.29242
1026 p 0.999999426 0.0 0.00107142837 2633.29242
1027 p 0.999999426 0.0 0.00107142837 2632.64242
1028 p 0.999999426 0.0 0.00107142837 2632.94242
1029 p 0.999999426 0.0 0.00107142837 2628.05492
1030 p 0.999999426 0.0 0.00107142837 2628.25492
1031 p 0.999999426 0.0 0.00107142837 2652.15492
1032 p 0.999999426 0.0 0.00107142837 2664.15492
1033 p 0.999999426 0.0 0.00107142837 2654.87992
1034 p 0.999999426 0.0 0.00107142837 2661.42992
1035 p 0.999999426 0.0 0.00107142837 2651.60992
1036 p 0.999999426 0.0 0.00107142837 2664.69992
1037 p 0.999999426 0.0 0.00107142837 2669.69992
1038 p 0.999999426 0.0 0.00107142837 2658.15492
1039 p 0.999999426 0.0 0.00107142837 2657.95492
1040 p 0.999999426 0.0 0.00107142837 2658.35492
1041 p 0.999999426 0.0 0.00107142837 2653.01742
1042 p 0.999999426 0.0 0.00107142837 2654.01742
1043 p 0.999999426 0.0 0.00107142837 2653.36742
1044 p 0.999999426 0.0 0.00107142837 2653.66742
1045 p 0.999999426 0.0 0.00107142837 2662.29242
1046 p 0.999999426 0.0 0.00107142837 2663.29242
1047 p 0.999999426 0.0 0.00107142837 2662.64242
1048 p 0.999999426 0.0 0.00107142837 2662.94242
1049 p 0.999999426 0.0 0.00107142837 2658.05492
1050 p 0.999999426 0.0 0.00107142837 2658.25492
1051 p 0.999999426 0.0 0.00107142837 3370.15492
1052 p 0.999999426 0.0 0.00107142837 2761.35492
1053 p 0.999999426 0.0 0.00107142837 2913.55492
1054 p 0.999999426 0.0 0.00107142837 3065.75492
1055 p 0.999999426 0.0 0.00107142837 3217.95492
1056 p 0.999999426 0.0 0.00107142837 2677.15492
1057 p 0.999999426 0.0 0.00107142837 2678.15492
1058 p 0.999999426 0.0 0.00107142837 3369.15492
1059 p 0.999999426 0.0 0.00107142837 2677.50492
1060 p 0.999999426 0.0 0.00107142837 2677.80492
1061 p 0.999999426 0.0 0.00107142837 3369.80492
1062 p 0.999999426 0.0 0.00107142837 3369.50492
1063 p 0.999999426 0.0 0.00107142837 3368.15492
1064 p 0.999999426 0.0 0.00107142837 3850.15492
1065 p 0.999999426 0.0 0.00107142837 3727.65492
1066 p 0.999999426 0.0 0.00107142837 3977.65492
1067 p 0.0 -1 0.0 -121.999316
1068 p 0.0 -1 0.0 128.000684
1069 p -0.00107142837 0.0 0.999999426 -205.942673
1070 p -0.00107142837 0.0 0.999999426 224.057327
1071 p 0.999999426 0.0 0.00107142837 3727.15492
1072 p 0.999999426 0.0 0.00107142837 3978.15492
1073 p 0.0 -1 0.0 -122.499316
1074 p 0.0 -1 0.0 128.500684
1075 p -0.00107142837 0.0 0.999999426 224.557327
1076 p 0.999999426 0.0 0.00107142837 3667.15492
1077 p 0.999999426 0.0 0.00107142837 4038.15492
1078 p 0.0 -1 0.0 -182.499316
1079 p 0.0 -1 0.0 188.500684
1080 p -0.00107142837 0.0 0.999999426 284.557327
1081 p 0.999999426 0.0 0.00107142837 3657.15492
1082 p 0.0 -1 0.0 -192.499316
1083 p 0.0 -1 0.0 198.500684
1084 p -0.00107142837 0.0 0.999999426 294.557327
1085 gq  1.147958744e-06  1  0.999998852  -7.494005416e-16
        -1.084202172e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  -9727.171458
1086 gq  1.147958744e-06  1  0.999998852  -7.494005416e-16
        -1.084202172e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  -9928.171458
1087 gq  1.147958744e-06  1  0.999998852  -7.494005416e-16
        -1.084202172e-18  -0.002142855503  -0.03480572206  6.001367345
        32.48532816  -10131.17146
1088 p 0.999999426 0.0 0.00107142837 6047.65492
1089 p 0.0 -1 0.0 -0.0993163276
1090 p 0.0 -1 0.0 6.10068367
1091 p -0.00107142837 0.0 0.999999426 -19.3426734
1092 p -0.00107142837 0.0 0.999999426 -13.1426734
1093 p 0.999999426 0.0 0.00107142837 4048.15492
1094 p 0.999999426 0.0 0.00107142837 6249.55492
1095 p 0.0 -1 0.0 -106.999316
1096 p 0.0 -1 0.0 113.000684
1097 p -0.00107142837 0.0 0.999999426 -136.242673
1098 p -0.00107142837 0.0 0.999999426 93.7573266
1099 p 0.999999426 0.0 0.00107142837 4488.43492
1100 p 0.0 -1 0.0 -115.570745
1101 p 0.0 -1 0.0 121.572112
1102 p -0.00107142837 0.0 0.999999426 -146.242673
1103 p -0.00107142837 0.0 0.999999426 102.328755
1104 p 0.999999426 0.0 0.00107142837 4928.71492
1105 p 0.0 -1 0.0 -124.142173
1106 p 0.0 -1 0.0 130.143541
1107 p -0.00107142837 0.0 0.999999426 -156.242673
1108 p -0.00107142837 0.0 0.999999426 110.900184
1109 p 0.999999426 0.0 0.00107142837 5368.99492
1110 p 0.0 -1 0.0 -132.713602
1111 p 0.0 -1 0.0 138.714969
1112 p -0.00107142837 0.0 0.999999426 -166.242673
1113 p -0.00107142837 0.0 0.999999426 119.471612
1114 p 0.999999426 0.0 0.00107142837 5809.27492
1115 p 0.0 -1 0.0 -141.285031
1116 p 0.0 -1 0.0 147.286398
1117 p -0.00107142837 0.0 0.999999426 -176.242673
1118 p -0.00107142837 0.0 0.999999426 128.043041
1119 p 0.0 -1 0.0 -149.856459
1120 p 0.0 -1 0.0 155.857827
1121 p -0.00107142837 0.0 0.999999426 -186.242673
1122 p -0.00107142837 0.0 0.999999426 136.614469
1123 p 0.0 -1 0.0 -158.427888
1124 p 0.0 -1 0.0 164.429255
1125 p -0.00107142837 0.0 0.999999426 -196.242673
1126 p -0.00107142837 0.0 0.999999426 145.185898
1127 p 0.0 -1 0.0 -166.999316
1128 p 0.0 -1 0.0 173.000684
1129 p -0.00107142837 0.0 0.999999426 -206.242673
1130 p -0.00107142837 0.0 0.999999426 153.757327
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++

c -------------------------------------------------------
c --------------- MATERIAL CARDS ------------------------
c -------------------------------------------------------
c c
c Material : Stainless304 rho=0.0878729
c           (rho=7.96703
c         g/cc)
m3      6000.70c 0.00031864 14028.70c 0.00170336 15031.70c 6.95038e-05
        16032.70c 4.4752e-05 24000.50c 0.0174813 25055.70c 0.00174159
        26054.70c 0.00338 26056.70c 0.053455 26057.70c 0.00128218
        27059.70c 0.0002453 28000.50c 0.00815128 hlib=.70h  pnlib=70u
mx3:h 6012.70h j j j j j j j j j j
c c
c Material : Aluminium rho=0.0582256
c           (rho=2.64651 g/cc)
m5      13027.70c 0.054381 12024.70c 0.00207 12025.70c 0.0002621
        12026.70c 0.0002885 14028.70c 0.000217 22048.70c 8.7e-05
        24052.70c 7.8e-05 24053.70c 1e-05 25055.70c 0.000408 26056.70c
        0.000214 26057.70c 5e-06 29063.70c 4e-05 29065.70c 2e-05
        30000.70c 0.000145 hlib=.70h  pnlib=70u
mt5     al27.12t
c c
c Material : helium rho=2.45e-05
c           (rho=0.000162839 g/cc)
m9      2004.70c 2.45e-05 hlib=.70h  pnlib=70u
c c
c Material : H2O rho=0.100283
c           (rho=0.99975 g/cc)
m11     1001.70c 0.0668456 1002.70c 1.003e-05 8016.70c 0.0334278
        hlib=.70h  pnlib=70u
mt11    lwtr.01t
c c
c Material : ParaH2 rho=0.041957
c           (rho=0.0702164 g/cc)
m25     1001.70c 0.041957 hlib=.70h  pnlib=70u
mt25    hpara.10t
c c
c Material : Nickel rho=0.0913215
c           (rho=8.90043 g/cc)
m33     28058.70c 0.062169 28060.70c 0.0239472 28061.70c 0.00104097
        28062.70c 0.00331906 28064.70c 0.00084527 hlib=.70h  pnlib=70u
c c
c Material : Tungsten rho=0.06359
c           (rho=19.413 g/cc)
m38     74182.70c 0.016871 74183.70c 0.00911077 74184.70c 0.0195076
        74186.70c 0.0181006 hlib=.70h  pnlib=70u
c c
c Material : B4C rho=0.136566
c           (rho=2.50608 g/cc)
m47     5010.70c 0.021741 5011.70c 0.087512 6000.70c 0.027313 hlib=.70h
        pnlib=70u
mx47:h j j 6012.70h
c c
c Material : Poly rho=0.117208
c           (rho=0.91 g/cc)
m48     6000.70c 0.0390694 1001.70c 0.078139 hlib=.70h  pnlib=70u
mx48:h 6012.70h j
mt48    poly.01t
c c
c Material : Concrete rho=0.074981
c           (rho=2.33578 g/cc)
m49     1001.70c 0.00776555 1002.70c 1.16501e-06 8016.70c 0.0438499
        11023.70c 0.00104778 12000.60c 0.000148662 13027.70c 0.00238815
        14028.70c 0.0158026 16032.70c 5.63433e-05 19000.60c 0.000693104
        20000.60c 0.00291501 26054.70c 1.84504e-05 26056.70c
        0.000286826 26057.70c 6.5671e-06 26058.70c 8.75613e-07
        hlib=.70h  pnlib=70u
mt49    lwtr.01t
c c
c Material : CastIron rho=0.0833854
c           (rho=7.03049 g/cc)
m54     6000.70c 0.00636 14028.70c 0.0051404 25055.70c 0.000386
        26054.70c 0.00419 26056.70c 0.065789 26057.70c 0.00152
        hlib=.70h  pnlib=70u
mx54:h 6012.70h j j j j j
c c
c Material : Steel71 rho=0.0843223
c           (rho=7.79995 g/cc)
m71     6000.70c 0.000101187 14028.70c 0.000295128 24000.50c
        0.000168645 25055.70c 0.000505934 26054.70c 0.0048414 26056.70c
        0.0759996 26057.70c 0.00175516 26058.70c 0.00023358 28058.70c
        0.000172218 28060.70c 6.6335e-05 28061.70c 2.88126e-06
        28062.70c 9.19272e-06 28064.70c 2.33992e-06 29063.70c
        0.000116651 29065.70c 5.19931e-05 hlib=.70h  pnlib=70u
mx71:h 6012.70h j j j j j j j j j j j j j j
c c
c Material : Copper rho=0.0844385
c           (rho=8.91001 g/cc)
m73     29063.70c 0.0583892 29065.70c 0.0260493 hlib=.70h  pnlib=70u
c c
c Material : ChipIRSteel rho=0.0847227
c           (rho=7.79997 g/cc)
m74     5010.70c 3.45922e-06 5011.70c 1.39238e-05 6000.70c 0.000469403
        7014.70c 3.01962e-05 13027.70c 0.000217659 14028.70c
        0.000167282 25055.70c 0.000342073 15031.70c 7.58418e-05
        16032.70c 5.56352e-05 16033.70c 4.4541e-07 16034.70c
        2.51422e-06 16036.70c 1.17213e-08 26054.70c 0.00484959
        26056.70c 0.0761283 26057.70c 0.00175813 26058.70c 0.000233975
        27059.70c 1.59442e-05 29063.70c 7.67107e-05 29065.70c
        3.4191e-05 28058.70c 5.45039e-05 28060.70c 2.09938e-05
        28061.70c 9.11867e-07 28062.70c 2.90933e-06 28064.70c
        7.40542e-07 24050.70c 3.92601e-06 24052.70c 7.57092e-05
        24053.70c 8.58481e-06 24054.70c 2.13694e-06 42092.70c
        7.26785e-06 42094.70c 4.53016e-06 42095.70c 7.79678e-06
        42096.70c 8.16899e-06 42097.70c 4.67709e-06 42098.70c
        1.18176e-05 42100.70c 4.71627e-06 41093.70c 5.05693e-06
        23051.42c 9.22231e-06 50112.70c 4.05881e-08 50114.70c
        2.76167e-08 50115.70c 1.42268e-08 50116.70c 6.08404e-07
        50117.70c 3.21358e-07 50118.70c 1.01345e-06 50119.70c
        3.59435e-07 50120.70c 1.36326e-06 50122.70c 1.93735e-07
        22046.70c 8.09753e-07 22047.70c 7.3025e-07 22048.70c
        7.23575e-06 22049.70c 5.31002e-07 22050.70c 5.08427e-07
        hlib=.70h  pnlib=70u
mx74:h j j 6012.70h j j j j j j j j j j j j j j j j j j j j j j j j j j
        j j j j j j j j j j j j j j j j j j j j j j
c c
c Material : Silicon300K rho=0.0499651
c           (rho=2.33021 g/cc)
m83     14028.70c 0.0460848 14029.70c 0.00234 14030.70c 0.0015403
        hlib=.70h  pnlib=70u
c c
c Material : Be5H2O rho=0.122325
c           (rho=1.80555 g/cc)
m118     1001.70c 0.00334228 4009.70c 0.117311 8016.70c 0.00167114
        hlib=.70h  pnlib=70u
mx118:h j model j
mt118    be.60t lwtr.01t
c c
c Material : Tungsten_15.1g rho=0.0494621
c           (rho=15.1 g/cc)
m120     74182.70c 0.0131232 74183.70c 0.00708653 74184.70c 0.0151734
        74186.70c 0.014079 hlib=.70h  pnlib=70u
c c
c Material : Iron_10H2O rho=0.0862739
c           (rho=7.17 g/cc)
m121     1001.70c 0.00668924 8016.70c 0.00334462 26054.70c 0.00445623
        26056.70c 0.0699533 26057.70c 0.00161553 26058.70c 0.000214997
        hlib=.70h  pnlib=70u
mt121    lwtr.01t
c c
c Material : Invar36 rho=0.0860477
c           (rho=8.11 g/cc)
m127     6000.70c 8.59617e-05 14028.70c 0.000277468 14029.70c
        1.40956e-05 14030.70c 9.30278e-06 29063.70c 0.000297213
        29065.70c 0.000132596 25055.70c 0.00051577 15031.70c
        2.14904e-05 16032.70c 2.04138e-05 16033.70c 1.61178e-07
        16034.70c 9.13343e-07 16036.70c 2.06308e-09 24050.70c
        1.86752e-05 24052.70c 0.000360132 24053.70c 4.08361e-05
        24054.70c 1.0165e-05 28058.70c 0.0216524 28060.70c 0.00834048
        28061.70c 0.000362555 28062.70c 0.00115598 28064.70c
        0.000294395 26054.70c 0.00306492 26056.70c 0.0481127 26057.70c
        0.00111113 26058.70c 0.000147871 hlib=.70h  pnlib=70u
mx127:h 6012.70h j j j j j j j j j j j j j j j j j j j j j j j j
c c
c Material : ParaOrtho%99.5 rho=0.041975
c           (rho=0.071299
c         g/cc)
m141     1001.70c 0.0417651 1004.70c 0.000209875 hlib=.70h  pnlib=70u
mt141    hpara.10t hortho.10t
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -------------------------------------------------------
c --------------- TRANSFORM CARDS -----------------------
c -------------------------------------------------------
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -------------------------------------------------------
c --------------- WEIGHT CARDS --------------------------
c -------------------------------------------------------
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -----------------------------------------------------------
c ------------------- TALLY CARDS ---------------------------
c -----------------------------------------------------------
c -------------------------------------------------------
c --------------- SOURCE CARDS --------------------------
c -------------------------------------------------------
sdef ara=24 axs=0.0 1 0.0 dir=1 erg=2000 par=9 tr=1 vec=0.0 1 0.0 x=d1
        z=d2
si1 H -4 -2.4 -0.8 0.8 2.4 4
sp1 D 0 1 1 1 1 1
si2 H -1.5 -0.9 -0.3 0.3 0.9 1.5
sp2 D 0 1 1 1 1 1
tr1 22.2943448 20.0739182 0.0
        -0.669130606 0.743144825 0.0 -0.743144825 -0.669130606 0.0 0.0
        0.0 1
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -------------------------------------------------------
c --------------- PHYSICS CARDS --------------------------
c -------------------------------------------------------
nps 10000
RAND  SEED=375642321
mode n p h / d t s a z * k ?
vol 1 5946r
imp:n 0 0 1 5944r
imp:p 0 0 1 5944r
imp:h 0 0 1 5944r
imp:/ 0 0 1 5944r
imp:d 0 0 1 5944r
imp:t 0 0 1 5944r
imp:s 0 0 1 5944r
imp:a 0 0 1 5944r
imp:z 0 0 1 5944r
imp:* 0 0 1 5944r
imp:k 0 0 1 5944r
imp:? 0 0 1 5944r
cut:n 1e+08 0 0.4 -0.1
cut:h,/,d,t,s,a,z,/,*,k,? 1e+08 0 0.5 0.25
cut:p 1e+08 0.001 0.5 0.25
phys:n 2000 0
phys:p 100
phys:h 2000
phys:/,d,t,s,a,z,/,*,k,? 2000
lca 2 1 1 0023 1 1 0 1 1 0
lea 1 4 1 0 1 0 0 1
prdmp 1e7 1e7 0 2 1e7
print 10 20 40 50 110 120
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++

