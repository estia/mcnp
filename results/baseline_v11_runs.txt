# Tallies :
1  - bunker neutron
11 - outside neutron
21 - bunker gamma
31 - outside gamma

# run        source                   comments
 1-g9eebf1f  mcstas_gammas_2.i        Selene 1+2 gammas
 2-ge90c8ac  mcstas_gammas.i          Selene 1 gammas
 3-g00475f7  rssa.i                   Fast neutrons with shutter open   | not run for ressources, use baseline_v10-7-gb9c60c4
 4-ged90fdd  rssa.i                   Fast neutrons with shutter closed | not run for ressources, use baseline_v10-0-g386ffc6
 5-ge10e0c1  mcstas_gammas.i          Selene 1 gammas, shutter closed
 6-g3056b20  mcstas_source_shutter.i  Neutron flux from McStas started before shutter, shutter closed 
 7-gf199ce3  mcstas_source_shutter.i  Neutron flux from McStas started before shutter, polarizer installed
 8-gcf23a0c  mcstas_source_shutter.i  Neutron flux from McStas started before shutter, no polarizer, vanadium 10cm at MF
 9-g552981d  mcstas_source_shutter.i  Neutron flux from McStas started before shutter, no polarizer, Cd at MF
10-gb5106c9  mcstas_source_shutter.i  Neutron flux from McStas started before shutter, no polarizer, B4C at MF
11-g7b8aa27  mcstas_source_sample.i   Open neutron beam without sample
12-g2bc10e5  mcstas_source_sample.i   Water at sample position
13-g18d4e89  mcstas_source_sample.i   Cadmium at sample position
14-gbe9aea3  mcstas_source_sample.i   GGG at sample position

# image creation calls
# H1.1_open_shutter - Fast neutrons+gamma + Selene 1-gamma + Selene 2-gamma + Emptey Cave neutron+gamma
python .\plot_tally.py -x baseline_v11-H1.1_open_shutter .\results\e2-estia_baseline_v11-1-g9eebf1f.mct.gz:31  .\results\e2-estia_baseline_v11-11-g7b8aa27.mct.gz:11 .\results\e2-estia_baseline_v11-11-g7b8aa27.mct.gz:31 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:11 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:31

# H1.1_open_shutter_pol - Fast neutrons+gamma + Selene 1-gamma + polarizer gamma + 1/2 Selene 2-gamma + Emptey Cave neutron+gamma
python .\plot_tally.py -x baseline_v11-H1.1_open_shutter_pol .\results\e2-estia_baseline_v11-1-g9eebf1f.mct.gz:31:0.5  .\results\e2-estia_baseline_v11-2-ge90c8ac.mct.gz:31:0.5  .\results\e2-estia_baseline_v11-7-gf199ce3.mct.gz:31  .\results\e2-estia_baseline_v11-11-g7b8aa27.mct.gz:11:0.5 .\results\e2-estia_baseline_v11-11-g7b8aa27.mct.gz:31:0.5 .\results\e2-estia_baseline_v10-0-g386ffc6.mct.gz:11 .\results\e2-estia_baseline_v10-0-g386ffc6.mct.gz:31


# image creation calls
# H1.2_closed_shutter - Fast neutrons+gamma + Selene 1-gamma + Shutter closed neutrons+gamma
python .\plot_tally.py -x baseline_v11-H1.2_closed_shutter .\results\e2-estia_baseline_v11-5-ge10e0c1.mct.gz:31  .\results\e2-estia_baseline_v11-6-g3056b20.mct.gz:11 .\results\e2-estia_baseline_v11-6-g3056b20.mct.gz:31 .\results\e2-estia_baseline_v10-0-g386ffc6.mct.gz:11 .\results\e2-estia_baseline_v10-0-g386ffc6.mct.gz:31


# H1.3_Water_sample - Fast neutrons+gamma + Selene 1-gamma + Selene 2-gamma + Water-neutron+gamma
python .\plot_tally.py -x baseline_v11-H1.3_Water_sample .\results\e2-estia_baseline_v11-1-g9eebf1f.mct.gz:31  .\results\e2-estia_baseline_v11-12-g2bc10e5.mct.gz:11 .\results\e2-estia_baseline_v11-12-g2bc10e5.mct.gz:31 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:11 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:31


# H1.4_Cd_sample - Fast neutrons+gamma + Selene 1-gamma + Selene 2-gamma + Cd-neutron+gamma (40% beam)
python .\plot_tally.py -x baseline_v11-H1.4_Cd_sample .\results\e2-estia_baseline_v11-1-g9eebf1f.mct.gz:31  .\results\e2-estia_baseline_v11-13-g18d4e89.mct.gz:11:0.4 .\results\e2-estia_baseline_v11-13-g18d4e89.mct.gz:31:0.4 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:11 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:31


# H1.4_GGG_sample - Fast neutrons+gamma + Selene 1-gamma + Selene 2-gamma + Cd-neutron+gamma (40% beam)
python .\plot_tally.py -x baseline_v11-H1.4_GGG_sample .\results\e2-estia_baseline_v11-1-g9eebf1f.mct.gz:31  .\results\e2-estia_baseline_v11-14-gbe9aea3.mct.gz:11:0.4 .\results\e2-estia_baseline_v11-14-gbe9aea3.mct.gz:31:0.4 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:11 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:31


# H1.5_Incoherent_MF - Fast neutrons+gamma + Selene 1-gamma + MF-neutron+gamma
# image creation calls
python .\plot_tally.py -x baseline_v11-H1.5_Incoherent_MF .\results\e2-estia_baseline_v11-2-ge90c8ac.mct.gz:31  .\results\e2-estia_baseline_v11-8-gcf23a0c.mct.gz:11  .\results\e2-estia_baseline_v11-8-gcf23a0c.mct.gz:31  .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:11 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:31


# H1.6_B4C_MF - Fast neutrons+gamma + Selene 1-gamma + MF-neutron+gamma
python .\plot_tally.py -x baseline_v11-H1.6_B4C_MF .\results\e2-estia_baseline_v11-2-ge90c8ac.mct.gz:31  .\results\e2-estia_baseline_v11-10-gb5106c9.mct.gz:11  .\results\e2-estia_baseline_v11-10-gb5106c9.mct.gz:31  .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:11 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:31


# H2.1_Cd_MF - Fast neutrons+gamma + Selene 1-gamma + Selene 2-gamma + MF-neutron+gamma
python .\plot_tally.py -x baseline_v11-H2.1_Cd_MF .\results\e2-estia_baseline_v11-2-ge90c8ac.mct.gz:31  .\results\e2-estia_baseline_v11-9-g552981d.mct.gz:11  .\results\e2-estia_baseline_v11-9-g552981d.mct.gz:31  .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:11 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:31


# H2.2_Cd_sample - Fast neutrons+gamma + Selene 1-gamma + Selene 2-gamma + Cd-neutron+gamma
python .\plot_tally.py -x baseline_v11-H2.2_Cd_sample .\results\e2-estia_baseline_v11-1-g9eebf1f.mct.gz:31  .\results\e2-estia_baseline_v11-13-g18d4e89.mct.gz:11 .\results\e2-estia_baseline_v11-13-g18d4e89.mct.gz:31 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:11 .\results\e2-estia_baseline_v10-7-gb9c60c4.mct.gz:31


