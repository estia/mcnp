#!/usr/bin/env python
"""
Generate a single input file from Estia model.
"""

maintxt=open('e2-estia.i', 'r').readlines()

outp=open('estia_merged.i', 'w')
for line in maintxt:
  if not line.startswith('read'):
    outp.write(line)
  else:
    fname=line.split('=', 1)[1].split()[0]
    outp.write(open(fname, 'r').read().strip()+'\n')

outp.close()
