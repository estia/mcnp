Input File ESTIA
c -------------------------------------------------------
c --------------- CELL CARDS --------------------------
c -------------------------------------------------------
read file=geometry/ess_monolith_cells.i echo
read file=geometry/ess_outside_cells.i echo
c tallies
100701 0 -100701:-101701
100702 0 -100702
100703 0 -100703
100704 0 -100704
100705 0 -100705
100706 0 -100706
100707 0 -100707
100708 0 -100708
100709 0 -100709
100710 0 -100710
read file=geometry/estia_inbunker.i echo
read file=geometry/estia_guides.i echo
read file=geometry/estia_cave.i echo
read file=geometry/estia_shutter.i echo
read file=geometry/estia_mf.i echo
read file=geometry/estia_universes.i echo
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++

c -------------------------------------------------------
c --------------- SURFACE CARDS -------------------------
c -------------------------------------------------------
c modified ESS geometry
read file=geometry/ess_monolith_surfaces.i echo
read file=geometry/ess_outside_surfaces.i echo
c tallies
c 700 s 117.0 -155.00 13.5 3.0
100701 1 s 542.0 -6.3  6.3 3.0
101701 1 s 542.0 -6.3 -6.3 3.0
100702 1 s 1010 -0.8 0 5.0
100703 1 s 1082 -25.0 0 5.0
100704 1 s 1082 -0.25 0 5.0
100705 1 s 1165 0.7 0 5.0
100706 21 sx -22 12.0
100707 s 2177 0.0 -13 6.0
100708 s 1548  191 0 30.0
100709 s 1539 -165 0 30.0
100710 s 2177 -91  -13 12.0
c Estia geometry
read file=geometry/estia_surfaces.i echo

read file=geometry/sm_reflectivity.i echo $ Adds SM reflection, requires patch
read file=geometry/materials.i echo
read file=geometry/materials_monolith.i echo
c
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c The model is base on a coordinate sytem with TCS at center
c but rotated arounc z-axis to align with ISCS XZ-plane
c (-35.6 degrees). This was included in CATIA model as axis system.
c -------------------------------------------------------
c --------------- TRANSFORM CARDS -----------------------
c Estia ISCS with origin at first focal point of feeder
TR1 15.05639956 -2.13420532 13.7      0.99992537  0          -0.012217
                                      0           1           0
                                      0.012217    0           0.99992537
                                      1
C center of feeder vessel with x-axis parallel to cylinder
TR2 775.66787865 -7.64605191 4.3862   0.99992537  0          -0.012217
                                      0           1           0
                                      0.012217    0           0.99992537
                                      1
c c-axis parallel x-axis and origin at middle focus
TR3 2314.8848 -2.1342 -14.3991        0.99992537  0          -0.012217
                                      0           1           0
                                      0.012217    0           0.99992537
                                      1
c c-axis parallel x-axis and origin at virtual source
TR4 1114.9743 -2.1342   0.261         0.99992537  0          -0.012217
                                      0           1           0
                                      0.012217    0           0.99992537
                                      1
c Parallel to c-axis but horizontal and centered at MF
TR13 2314.8848 -2.1342 -14.3991       1 0 0    0 1 0    0 0 1   1
c Parallel to c-axis but horizontal and centered at VS
TR14 1114.9743 -2.1342   0.261        1 0 0    0 1 0    0 0 1   1
c Parallel to c-axis but horizontal and centered at sample
TR15 3514.7952 -2.1342 -29.0595       1 0 0    0 1 0    0 0 1   1
c Flange axis for Selene 1
TR21 1334.9578  0.36581  -2.4264      0.99992537  0          -0.012217
                                      0           1           0
                                      0.012217    0           0.99992537
c Shutter coordinate system centered in Y,Z flange and X to center 
TR22 2149.5000  1.16580 -12.6000      1           0           0
                                      0           1           0
                                      0           0           1
c McStas coordinates with origin at virtual source
TR54 1114.9743 -2.1342   0.261        0           1           0
                                      0.012217    0           0.99992537
                                      0.99992537  0          -0.012217
                                      1
c Coordinate system of the Estia beamport focal point
TR100 9.57163854 -4.09313273 13.7     0.99997563 -0.00698126 0 
                                      0.00698126  0.99997563 0
                                      0 0 1          1
c Same coordinates but with origin at TCS
TR101 0 0 0                           0.99997563 -0.00698126 0 
                                      0.00698126  0.99997563 0
                                      0 0 1          1
c Transformation from SKADI model coordinate system here
TR102 -4.06550381 -2.91061485 1.0     0.99376792 -0.11146893 0   
                                      0.11146893  0.99376792 0   
                                      0 0 1          1
c Coordinate for surface source within bunker wall
c close to plane 206305 with same surface normal and center around beam
TR103 1202.6185  0.05148 -0.86     0.9999754884138854  -0.00698105188428727 0.0
                                   0.00698105188428727  0.9999754884138854  0.0
                                      0 0 1          1
c Coordingates for instrument boundary meshes, y-direction points outside
c SKADI side wall starting at bunker boundary
c TR121 962.0 -1155.0 -150.0    0.675590  -0.737277  0.000000
c                               0.737277   0.675590  0.000000
c                               0          0         1
c c Fence on E01 side and left cave side
c TR122 1089.0 -2723.0 -180.0   0.582123  -0.813101  0.000000
c                              -0.813101  -0.582123  0.000000
c                               0          0         1
c c Downstream cave side
c TR123 2841.0 -3360.0 -180.0  -0.813101  -0.582123  0.000000
c                               0.582123  -0.813101  0.000000
c                               0          0         1
c c Dillitation joint
c TR124 1104.0 -1820.0 -180.0   0.000000  -1.000000  0.000000
c                              -1.000000   0.000000  0.000000
c                               0          0         1
read file=geometry/ess_monolith_transformations.i echo
c -------------------------------------------------------
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -------------------------------------------------------
c --------------- WEIGHT CARDS --------------------------
c -------------------------------------------------------
c weight window generator, can be run with previous weight window files
c WWGE:n   1e-6 1e-3 1e-1 4 10 100 3e3
c mesh based weight windows, can be reused after change of cell structure
c WWG      15 0 0
c MESH     GEOM=CYL ORIGIN=0 0 -500 REF=133.7 -176.7 13.5 AXS=0 0 1 
c MESH     GEOM=CYL ORIGIN=0 0 -500 REF=0 -1 2.5 AXS=0 0 1 
c          IMESH=20 200 550 1150 1500 2112.01  2157.17 4600 
c          IINTS=2  2   5    1   10    1        1       4
c          JMESH=100 300 460 500 540 700 900 1200
c          JINTS=  2   4   4   8   8   4   4    6
c          KMESH=298 303 309 315 360
c          KINTS= 14   1  20   1   2
c generate cell based weight windows, needs to be updated on geometry change
c WWG    34 61003 0
c TF34 2 $ set second tally cell (behind BWC) as reference
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++
c -----------------------------------------------------------
c ------------------- TALLY CARDS ---------------------------
c
c read file=tallies/bunker_mesh.i echo
c read file=tallies/bunker_mesh_p.i echo
c read file=tallies/bunker_mesh_np.i echo
c read file=tallies/bunker_mesh_act.i echo
c read file=tallies/full_mesh.i echo
c read file=tallies/full_mesh_p.i echo
read file=tallies/full_mesh_np.i echo
c read file=tallies/vs_fine_mesh.i echo
c read file=tallies/boundary_meshes.i echo
c read file=tallies/boundary_wall_meshes.i echo
c read file=tallies/selene_source_mesh_p.i echo
c read file=tallies/NBPI_f4.i echo
c read file=tallies/BBG_f4.i echo
c read file=tallies/BBG_f5.i echo
c read file=tallies/inbunker_f4.i echo
c read file=tallies/CPC1_f5.i echo
c read file=tallies/CPC2_f5.i echo
c read file=tallies/chopper_pit_doses.i echo
c read file=tallies/bunker_feedthrough_f1.i echo
c read file=tallies/bunker_wall_f4.i echo
c read file=tallies/bunker_wall_f5.i echo
c read file=tallies/pre_selene1_mesh.i
c read file=tallies/e1_e2_f4.i echo
c read file=tallies/e1_e2_f5.i echo
c read file=tallies/shutter_f4.i echo
c read file=tallies/shutter_f5.i echo
c read file=tallies/shutter_wall_f4.i echo
c read file=tallies/shutter_wall_mesh_np.i echo
c read file=tallies/middle_focus_mesh.i echo
c read file=tallies/detector_f5.i echo
c read file=tallies/selene1_walls_f4.i echo
c read file=tallies/optical_cave_f5.i echo
c read file=tallies/optical_cave_gammas.i echo
c read file=tallies/experimental_cave_f5.i echo
c read file=tallies/cave_mesh.i echo
c read file=tallies/selene1_f5.i echo
c read file=tallies/selene1_f6.i echo
c 
c ===============================
c Global Variance Reduction Cards
c ===============================
c Exponential transform in collimators, only works with weight windows
ext:n 0 276r $ in monolith
      0 0 0 309r $ ess outside
      0 146r 0.75v2 8r 0.75v3 4r $CPC1+2
      0 28r 0.75v3 4r $ bunker wall
      0 37r 0.75v3 7r $ BWC
      0 276r
c exponential transform reference vectors: origin, VS, shutter
vect v1 0 0 0 v2 1114.8 -1.75 0.5 v3 2137 -1.25 -12.5
c forced collisions in neutron guides
fcl:n 0 276r $ in monolith
      0 309r $ ess outside
      0 -0.15 -0.3 4r $ NBOA
      0 23r -0.3 $ BBG
      0 13r -1.0 6r $ NFGA
      0 206r -1 $ Glass in Selene 1
      0 54r -1 7r $ S1 back plate and mineral cast in universe
      0 195r
c 
c -----------------------------------------------------------
c -------------------------------------------------------
c --------------- PHYSICS CARDS --------------------------
c -------------------------------------------------------
c dbcn 375642321 j j j j j j j j j j j 15291711
c stop calculation either at XeY histories, 6.5 days or tally 15 error <0.001
c nps 39242646 $ works with rssa file from McStas, original tracks 39242646
stop nps 1e8 CTME 4e5 $f31 0.01
c nps 28e11 $ works with rssa file, value is scaled to 20x source particles
c write particle tracks
c ssw 206505 pty=n $ record all neutron tracks crossing outward
c random number generator options
rand gen=2 seed=29976355590901 $ default 19073486328125
c histp
mode  n p h / z $ neutron, photon, proton, pi+, pi0
imp:n,p,h,/,z 1 276r 0 0 1 827r
c Reduce model size for WWG runs until earlier stages of the model:
c void 201255 201257 203123 $ void cells downstream of BBG
c imp:n,p,h,/,z 1 276r 0 0 1 323r 0 1 0 1 34r 0 1 414r $ zero for above
c void  100268 201255 204001 204002 204003 204004 204005 
c      204013 204055 204101 204102 204103 204201  $ void cells downstream of BBG
c imp:n,p,h,/,z 1 276r 0 0 1 224r 0 1 97r  0 1 64r 0 4r 
c      1 6r 0 1 11r 0 1 8r 0 2r 1 4r 0 1 343r $ zero for above
c
cut:h,/,z 1e+08 1e-3
phys:n 3000 0
phys:p 3000
phys:h,/,z 3000
c Selection of used physics models, default from MCNP6.2 is CEM03+LAQGSM03
LCA              2J
C                00  $ No ISABEL (default with INCL4)
                 01  $ BERTINI for nucleons/pions and ISABEL for other particles
C                02  $ ISABEL for all particles
                 5J
C                00  $ BERTINI or ISABEL
                 01  $ CEM03
C                02  $ INCL4
C                00  $ ISABEL <940MeV/nucleon then LAQGSM03
                 01  $ LAQGSM03 for all heavy ion interactions
C ------------------------------------------------------------------------------
LEA              6J
                 00  $ DRESNER
C                02  $ ABLA
                 1J
c * For testing for geometry errors
c read file=sources/geometry_test.i echo
c * Source for simulation from proton beam (needs different WW and ESplit)
c read file=sources/proton.i echo
c * Source Term from Valentina for 2m adapted to size
c read file=sources/e2_2m.i echo
c read file=sources/e2_2m_mod.i echo
c read file=sources/rssa.i echo
c * Estimated source within bunker wall to optimize collimator efficiency
c read file=sources/bunker_wall.i echo $ approximate, not for official results
c * McStas source terms for gamma doeses
c read file=sources/mcstas_rssa.i echo
c read file=sources/mcstas_source_vs.i echo
read file=sources/mcstas_source_sample.i echo
c read file=sources/mcstas_source_mf.i echo
c read file=sources/mcstas_gammas.i echo
c read file=sources/mcstas_gammas_2.i echo
c read file=sources/mcstas_source_shutter.i echo
prdmp -120 -30 1 2 5e5
print 30 32 40 
c      120 $O importance function quality
c      128 $O universe map
c      150 $O DXTRAN diagnostics
c      160 $D TFC bin tally analysis
c      161 $D f(x) tally density plot
c      162 $D cumulative f(x) and tally density plot
c ++++++++++++++++++++++ END ++++++++++++++++++++++++++++

