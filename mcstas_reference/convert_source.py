#!/usr/bin/env python
from numpy import *
import sys

def convert(fname):
  data=loadtxt(fname)[::-1].T
  E=0.01239842/data[0] # convert wavelength into MeV energy
  I=hstack([0., 0., data[1]])
  Ebins=hstack([0, E[0]-(E[1]-E[0])/2., E[1:]-(E[1:]-E[:-1])/2., E[-1]+(E[-1]-E[-2])/2.])

  out='si11  '
  for i,Ei in enumerate(Ebins):
    out+='%.5e'%Ei
    if i%5==4:
      out+='\n      '
    else:
      out+=' '

  out=out.strip()+'\nsp11  '
  for i,Ii in enumerate(I):
    out+='%.5e'%Ii
    if i%5==4:
      out+='\n      '
    else:
      out+=' '
  
  return out
