#!/usr/bin/env python
from numpy import *
from pylab import *

L1='wwn%i:n   % .4E % .4E % .4E % .4E % .4E\n'
Li='         % .4E % .4E % .4E % .4E % .4E\n'

def read_wwn(fname, max_E=6):
  txt=open(fname, 'r').read()
  data=[]
  for i in range(1,max_E+1):
    ti=txt.split('wwn%i:n'%i,1)[1].split('ww',1)[0]
    data.append(array(ti.strip().split(), dtype=float))
  return vstack(data)

def combine_wwn(items, max_WW=1e15):
  output=items[0][:]
  output[output>max_WW]=0.
  for itemi in items:
    output=where((itemi>max_WW)|(itemi==0), output, where(output==0, itemi, minimum(output, itemi)))
  return output
  
def write_wwn(fin, fout, data, max_E=6):
  txt=open(fin, 'r').read()
  L=L1+Li*(data.shape[1]//5-1)
  L+=Li.rsplit(' %', 5-data.shape[1]%5)[0]+'\n'
  with open(fout, 'w') as fh:
    fh.write(txt.split('wwn1:n',1)[0])
    for i in range(1,max_E+1):
      fh.write(L%tuple([i]+data[i-1].tolist()))
    fh.write('ww'+txt.split('wwn%i:n'%i,1)[1].split('ww',1)[1])

if __name__=='__main__':
  import sys
  
