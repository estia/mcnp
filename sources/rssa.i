c source definition based on surface source generated from 2m source term
cut:n j j -0.2 -0.1 $ put weight cut-off for 0-WW to 10% of source weight
c cut all photons that can't produce neutrons
cut:p 1e+08 1.667
c make sure to supply the correct WWINP file
WWP:n  j j 4 j -1 j 1.0 1j 6.0E+14 1j
ssr 
