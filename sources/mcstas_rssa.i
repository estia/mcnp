c source definition based on surface source generated from McStas
cut:n j j -0.2 -0.1 $ put weight cut-off for 0-WW to 10% of source weight
c cut:n j 0   0 0 $ use analog capture for source from McStas
c cut all photons that can't produce neutrons
cut:p 1e+08 0.05
c adjust weight windows to be essentially ignored
WWP:n  5e20 j 2 j -1 j 1.0 1j 1.0E-10 1j
c read McStas tracks, scaling is total weight/average weight in McStas
c in McStas the n/s is w*n while in MCNP n/s = n/particle
c Therefore the weight factor here is just number of McStas events
ssr OLD 204600 NEW 204600 TR 54 WGT 39242646
