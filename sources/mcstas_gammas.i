c source definition for Selene 1 gammas based on McStas
cut:n j 1e-11
cut:p 1e+08 0.01
c make sure to supply the correct WWINP file
WWP:n   j j 5 j -1 j 1.0
c the source definition with gammas/s
sdef  pos=d1 $ dummy for X-position and horizontal/vertical face selection 
      x=fpos d11 y=fpos d12 z=fpos d13  
      erg=fpos d14 
      tr=1 par=p wgt=9.59833e+09
si1 l  1 0 0    2 0 0    3 0 0    4 0 0    5 0 0    
       6 0 0    7 0 0    8 0 0    9 0 0   10 0 0   
      11 0 0   12 0 0   13 0 0   14 0 0   15 0 0
       1 1 0    2 1 0    3 1 0    4 1 0    5 1 0    
       6 1 0    7 1 0    8 1 0    9 1 0   10 1 0   
      11 1 0   12 1 0   13 1 0   14 1 0   15 1 0
sp1   7.29220E+09 4.79340E+08 3.21180E+08 3.03858E+08 2.58578E+08
      2.25073E+08 1.96544E+08 1.27443E+08 1.04253E+08 6.00576E+07
      5.56826E+07 5.57082E+07 4.47933E+07 4.57106E+07 2.79109E+07
      7.29220E+09 4.79340E+08 3.21180E+08 3.03858E+08 2.58578E+08
      2.25073E+08 1.96544E+08 1.27443E+08 1.04253E+08 6.00576E+07
      5.56826E+07 5.57082E+07 4.47933E+07 4.57106E+07 2.79109E+07
c setting distributions, x and E depend on x-position
c                        y and z have two sets but symmetric around center
ds11 s 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115
       101 102 103 104 105 106 107 108 109 110 111 112 113 114 115
ds12 s 201 202 203 204 205 206 207 208 207 206 205 204 203 202 201
       211 212 213 214 215 216 217 218 217 216 215 214 213 212 211
ds13 s 301 302 303 304 305 306 307 308 307 306 305 304 303 302 301
       311 312 313 314 315 316 317 318 317 316 315 314 313 312 311
ds14 s 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415
       401 402 403 404 405 406 407 408 409 410 411 412 413 414 415
c x-ranges for each segment
si101  1340 1388
si102  1388 1436
si103  1436 1484
si104  1484 1532
si105  1532 1580
si106  1578 1628
si107  1628 1676
si108  1676 1724
si109  1724 1772
si110  1772 1820
si111  1820 1868
si112  1868 1916 
si113  1916 1964
si114  1964 2012
si115  2012 2060
sp101  0 1
sp102  0 1
sp103  0 1
sp104  0 1
sp105  0 1
sp106  0 1
sp107  0 1
sp108  0 1
sp109  0 1
sp110  0 1
sp111  0 1
sp112  0 1
sp113  0 1
sp114  0 1
sp115  0 1
c y-ranges, first the ellipse distance for each segment, 
c           then beam coverage on horizontal mirror
si201   8.400  8.969
si202   8.969  9.429
si203   9.429  9.796
si204   9.796 10.080
si205  10.080 10.288
si206  10.288 10.424
si207  10.424 10.492
si208  10.492 10.493
si211   2.304  8.684
si212   2.723  9.199
si213   3.142  9.612
si214   3.561  9.938
si215   3.979 10.184
si216   4.398 10.356
si217   4.817 10.458
si218   5.236 10.492
sp201  0 1
sp202  0 1
sp203  0 1
sp204  0 1
sp205  0 1
sp206  0 1
sp207  0 1
sp208  0 1
sp211  0 1
sp212  0 1
sp213  0 1
sp214  0 1
sp215  0 1
sp216  0 1
sp217  0 1
sp218  0 1
c z-ranges, first beam coverage on horizontal mirror,
c           then the ellipse distance for each segment, 
si301   -8.684  -2.304
si302   -9.199  -2.723
si303   -9.612  -3.142
si304   -9.938  -3.561
si305  -10.184  -3.979
si306  -10.356  -4.398
si307  -10.458  -4.817
si308  -10.492  -5.236
si311   -8.969  -8.400
si312   -9.429  -8.969
si313   -9.796  -9.429
si314  -10.080  -9.796
si315  -10.288 -10.080
si316  -10.424 -10.288
si317  -10.492 -10.424
si318  -10.493 -10.492
sp301  0 1
sp302  0 1
sp303  0 1
sp304  0 1
sp305  0 1
sp306  0 1
sp307  0 1
sp308  0 1
sp311  0 1
sp312  0 1
sp313  0 1
sp314  0 1
sp315  0 1
sp316  0 1
sp317  0 1
sp318  0 1
c Energy distribution for all positions along the guide
si401 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp401 0.00000E+00 1.56557E+05 2.76591E+05 8.82267E+05 3.79148E+06 
      7.22100E+09 7.15782E+04 2.36360E+05 1.64987E+06 1.20477E+07 
      5.83592E+06 5.13734E+06 1.05131E+07 9.13771E+06 2.23406E+06 
      5.55688E+06 3.73177E+06 9.84371E+06 3.67345E+04 5.62062E+04
C ------------------------------------------------------------------------
si402 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp402 0.00000E+00 1.75066E+05 3.14271E+05 1.00187E+06 4.19715E+06 
      4.29576E+08 6.49313E+04 8.35318E+04 1.59407E+06 1.13391E+07 
      3.24825E+06 2.40414E+06 3.23346E+06 2.35587E+06 1.81032E+06 
      4.22018E+06 2.81568E+06 1.08595E+07 3.50283E+04 1.16665E+04
C ------------------------------------------------------------------------
si403 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp403 0.00000E+00 1.14282E+05 2.09752E+05 6.68141E+05 2.71818E+06 
      2.88507E+08 4.31570E+04 5.55428E+04 1.05765E+06 7.30837E+06 
      2.10495E+06 1.58164E+06 2.11537E+06 1.54343E+06 1.20473E+06 
      2.79310E+06 1.87709E+06 7.24741E+06 2.27791E+04 7.70741E+03
C ------------------------------------------------------------------------
si404 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp404 0.00000E+00 1.19942E+05 2.24594E+05 7.14916E+05 2.83168E+06 
      2.69546E+08 4.59551E+04 5.82997E+04 1.12511E+06 7.56863E+06 
      2.17669E+06 1.65382E+06 2.17880E+06 1.58300E+06 1.28282E+06 
      2.95750E+06 2.00035E+06 7.75812E+06 2.37907E+04 7.90258E+03
C ------------------------------------------------------------------------
si405 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp405 0.00000E+00 1.02808E+05 1.94757E+05 6.19690E+05 2.41655E+06 
      2.29099E+08 3.97544E+04 5.03268E+04 9.72514E+05 6.44006E+06 
      1.85566E+06 1.42096E+06 1.86323E+06 1.35351E+06 1.11024E+06 
      2.55211E+06 1.73259E+06 6.72701E+06 2.03449E+04 6.77894E+03
C ------------------------------------------------------------------------
si406 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp406 0.00000E+00 8.86042E+04 1.69642E+05 5.39580E+05 2.07421E+06 
      1.99597E+08 3.45601E+04 4.37451E+04 8.44765E+05 5.51349E+06 
      1.59281E+06 1.22897E+06 1.60647E+06 1.16764E+06 9.65712E+05 
      2.21413E+06 1.50825E+06 5.85933E+06 1.74992E+04 5.87135E+03
C ------------------------------------------------------------------------
si407 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp407 0.00000E+00 7.87877E+04 1.51840E+05 4.82849E+05 1.83971E+06 
      1.73871E+08 3.08863E+04 3.89901E+04 7.54666E+05 4.88100E+06 
      1.41072E+06 1.09310E+06 1.42351E+06 1.03396E+06 8.63179E+05 
      1.97565E+06 1.34859E+06 5.24414E+06 1.55375E+04 5.20473E+03
C ------------------------------------------------------------------------
si408 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp408 0.00000E+00 5.01840E+04 9.68964E+04 3.08110E+05 1.17096E+06 
      1.12987E+08 1.97075E+04 2.49206E+04 4.81428E+05 3.10577E+06 
      8.98765E+05 6.97579E+05 9.09029E+05 6.60776E+05 5.50891E+05 
      1.26041E+06 8.60883E+05 3.34661E+06 9.89468E+03 3.33164E+03
C ------------------------------------------------------------------------
si409 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp409 0.00000E+00 4.11446E+04 7.95509E+04 2.52944E+05 9.59525E+05 
      9.23981E+07 1.61749E+04 2.04454E+04 3.95097E+05 2.54402E+06 
      7.36328E+05 5.72028E+05 7.44921E+05 5.41445E+05 4.52162E+05 
      1.03416E+06 7.06655E+05 2.74751E+06 8.11001E+03 2.73082E+03
C ------------------------------------------------------------------------
si410 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp410 0.00000E+00 2.51204E+04 4.88245E+04 1.55217E+05 5.84610E+05 
      5.28251E+07 9.90982E+03 1.24431E+04 2.42028E+05 1.54694E+06 
      4.46975E+05 3.48156E+05 4.50560E+05 3.26731E+05 2.76963E+05 
      6.32447E+05 4.32872E+05 1.68610E+06 4.94348E+03 1.64531E+03
C ------------------------------------------------------------------------
si411 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp411 0.00000E+00 2.36608E+04 4.56443E+04 1.45144E+05 5.52264E+05 
      4.88854E+07 9.27597E+03 1.16381E+04 2.26683E+05 1.46399E+06 
      4.22049E+05 3.26897E+05 4.23743E+05 3.07060E+05 2.59131E+05 
      5.92778E+05 4.04758E+05 1.57629E+06 4.66257E+03 1.54109E+03
C ------------------------------------------------------------------------
si412 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp412 0.00000E+00 2.34986E+04 4.48276E+04 1.42601E+05 5.50860E+05 
      4.89730E+07 9.13069E+03 1.14783E+04 2.23305E+05 1.46457E+06 
      4.21412E+05 3.23890E+05 4.21794E+05 3.05694E+05 2.54959E+05 
      5.84877E+05 3.97945E+05 1.54818E+06 4.64119E+03 1.52926E+03
C ------------------------------------------------------------------------
si413 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp413 0.00000E+00 1.87764E+04 3.52661E+04 1.12245E+05 4.42775E+05 
      3.94292E+07 7.20582E+03 9.07992E+03 1.76424E+05 1.18187E+06 
      3.39132E+05 2.57888E+05 3.37896E+05 2.44903E+05 2.01079E+05 
      4.63087E+05 3.13513E+05 1.21806E+06 3.72001E+03 1.21944E+03
C ------------------------------------------------------------------------
si414 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp414 0.00000E+00 1.82897E+04 3.37346E+04 1.07439E+05 4.34219E+05 
      4.04983E+07 6.92221E+03 8.78390E+03 1.69672E+05 1.16466E+06 
      3.33775E+05 2.50952E+05 3.32061E+05 2.41077E+05 1.93078E+05 
      4.46799E+05 3.00726E+05 1.16534E+06 3.63777E+03 1.19682E+03
C ------------------------------------------------------------------------
si415 1e-12       0.15        0.2         0.3         0.4
      0.5         0.6         0.8         1.0         1.5
      2.0         3.0         4.0         5.0         6.0
      7.0         8.0         9.0        10.0        11.0
sp415 0.00000E+00 1.08301E+04 1.94946E+04 6.21412E+04 2.59394E+05 
      2.48374E+07 4.02189E+03 5.13569E+03 9.87454E+04 6.99903E+05 
      1.99988E+05 1.48066E+05 1.98045E+05 1.43923E+05 1.12085E+05 
      2.61025E+05 1.74300E+05 6.73544E+05 2.16453E+03 7.10515E+02
C ------------------------------------------------------------------------
