#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""
  Script to generate mayavi model. Run in IPython console (not qtconsole).
"""

from mayavi.tools import pipeline
from plot_tally import *
import numpy as np

# build geometry from CAD files (removed from repository)
#mod=pipeline.open('cad_models/target_hall.stl')
#pipeline.surface(mod, color=(0.8,0.8,0.8))
#mod=pipeline.open('cad_models/shielding_wall.stl')
#pipeline.surface(mod, color=(0.4,0.0,0.4))
#mod=pipeline.open('cad_models/shielding_outside.stl')
#pipeline.surface(mod, color=(0.0,0.6,0.0))
  
def load_data(fname, tally=None):
  x,y,z,I,dI=read_tally(fname, use_tally=tally)
  Y,X,Z=meshgrid((y[1:]+y[:-1])/0.2-21., (x[1:]+x[:-1])/0.2+151., (z[1:]+z[:-1])/0.2) 
  Xr=X*cos(36.*pi/180.)+Y*sin(36.*pi/180.)
  Yr=-X*sin(36.*pi/180.)+Y*cos(36.*pi/180.)
  scal=pipeline.scalar_scatter(Xr, Yr, Z, log10(I+1e-9))
  scal=pipeline.delaunay3d(scal)
  pipeline.iso_surface(scal, colormap='Vega20c', vmin=-8, vmax=2, 
                       contours=[-6.01])
