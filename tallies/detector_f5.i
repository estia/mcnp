c f5 detector tally to estimate background at the detector
fc255     neutron flux at detector position
f255:n    2280 -3180 -30 0.0 ND
e255      1e-10 1.58e-10 2.51e-10 3.98e-10 6.31e-10
         1e-9 1.58e-9 2.51e-9 3.98e-9 6.31e-9 
         1e-8 1.58e-8 2.51e-8 3.98e-8 6.31e-8 
         1e-7 1.58e-7 2.51e-7 3.98e-7 6.31e-7 
         1e-6 1.58e-6 2.51e-6 3.98e-6 6.31e-6 
         1e-5 1.58e-5 2.51e-5 3.98e-5 6.31e-5 
         1e-4 1.58e-4 2.51e-4 3.98e-4 6.31e-4 
         1e-3 1.58e-3 2.51e-3 3.98e-3 6.31e-3 
         1e-2 1.58e-2 2.51e-2 3.98e-2 6.31e-2 
         1e-1 1.58e-1 2.51e-1 3.98e-1 6.31e-1 
         1e-0   1.58    2.51    3.98    6.31 
         1e+1 1.58e+1 2.51e+1 3.98e+1 6.31e+1 
         1e+2 1.58e+2 2.51e+2 3.98e+2 6.31e+2
         1e+3 1.58e+3 2.51e+3
fq255 e f
c     reduce probability for contribution inside of bunker
pd255   0.001 276r $ in monolith
        0.005 297r $ ess outside
        0.001 0.001 78r $ NBOA to NFGA
        0.05 75r $ up to after CPC1
        0.1 34r $ up to after CPC2
        0.25 91r $ up to end
