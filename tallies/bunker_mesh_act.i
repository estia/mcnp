c time dependent mesh tally within the bunker
tmesh
c prompt radiation and decay within a single pulse (71ms)
  rmesh1:p dose 1 trans 1
  cora1   500 10i 1000 10i 1500 16i 2300
  corb1   -315 5i -65 6i 55 9i 505
  corc1   -200 1i -100 10i 100 5i 600
  ergsh1  0 -71e5
c delayed radiation emitted in the first second
  rmesh11:p dose 1 trans 1
  cora11   500 10i 1000 10i 1500 16i 2300
  corb11   -315 5i -65 6i 55 9i 505
  corc11   -200 1i -100 10i 100 5i 600
  ergsh11  -71e5 -1e8
c delayed radiation emitted between 1 and 10 seconds
  rmesh21:p dose 1 trans 1
  cora21   500 10i 1000 10i 1500 16i 2300
  corb21   -315 5i -65 6i 55 9i 505
  corc21   -200 1i -100 10i 100 5i 600
  ergsh21  -1e8 -1e9
c delayed radiation emitted between 10 and 100 seconds
  rmesh31:p dose 1 trans 1
  cora31   500 10i 1000 10i 1500 16i 2300
  corb31   -315 5i -65 6i 55 9i 505
  corc31   -200 1i -100 10i 100 5i 600
  ergsh31  -1e9 -1e10
c delayed radiation emitted between 100 and 1 000 seconds
  rmesh41:p dose 1 trans 1
  cora41   500 10i 1000 10i 1500 16i 2300
  corb41   -315 5i -65 6i 55 9i 505
  corc41   -200 1i -100 10i 100 5i 600
  ergsh41  -1e10 -1e11
c delayed radiation emitted between 1 000 and 10 000 seconds
  rmesh51:p dose 1 trans 1
  cora51   500 10i 1000 10i 1500 16i 2300
  corb51   -315 5i -65 6i 55 9i 505
  corc51   -200 1i -100 10i 100 5i 600
  ergsh51  -1e11 -1e12
c delayed radiation emitted between 10 000 and 100 000 seconds
  rmesh61:p dose 1 trans 1
  cora61   500 10i 1000 10i 1500 16i 2300
  corb61   -315 5i -65 6i 55 9i 505
  corc61   -200 1i -100 10i 100 5i 600
  ergsh61  -1e12 -1e13
c delayed radiation after 100 000 seconds (to default ACT integration time)
  rmesh71:p dose 1 trans 1
  cora71   500 10i 1000 10i 1500 16i 2300
  corb71   -315 5i -65 6i 55 9i 505
  corc71   -200 1i -100 10i 100 5i 600
  ergsh71  -1e13 -1e18
c Official ESS dose conversion factors from gamma/cm²/s to Sv/h
c Is described in  ESS-0019931
  mshmf1  0.000000 1.00000e-04
          1.00E-02 2.47E-10   1.50E-02 5.62E-10
          2.00E-02 8.1E-10    3.00E-02 1.13E-09
          4.00E-02 1.26E-09   5.00E-02 1.33E-09
          6.00E-02 1.4E-09    7.00E-02 1.49E-09
          8.00E-02 1.6E-09    1.00E-01 1.87E-09
          1.50E-01 2.69E-09   2.00E-01 3.6E-09
          3.00E-01 5.44E-09   4.00E-01 7.2E-09
          5.00E-01 8.89E-09   5.11E-01 9.07E-09
          6.00E-01 1.05E-08   6.62E-01 1.14E-08
          8.00E-01 1.34E-08   1.00E+00 1.62E-08
          1.12E+00 1.76E-08   1.33E+00 2.01E-08
          1.50E+00 2.2E-08    2.00E+00 2.69E-08
          3.00E+00 3.51E-08   4.00E+00 4.21E-08
          5.00E+00 4.82E-08   6.00E+00 5.4E-08
          6.13E+00 5.47E-08   8.00E+00 6.7E-08
          1.00E+01 7.92E-08   1.50E+01 1.09E-07
          2.00E+01 1.38E-07   3.00E+01 1.85E-07
          4.00E+01 2.23E-07   5.00E+01 2.6E-07
          6.00E+01 2.95E-07   8.00E+01 3.52E-07
          1.00E+02 3.96E-07   1.50E+02 4.68E-07
          2.00E+02 5.15E-07   3.00E+02 5.8E-07
          4.00E+02 6.19E-07   5.00E+02 6.48E-07
          6.00E+02 6.7E-07    8.00E+02 7.02E-07
          1.00E+03 7.24E-07   1.50E+03 7.63E-07
          2.00E+03 7.92E-07   3.00E+03 8.35E-07
          4.00E+03 8.75E-07   5.00E+03 9.04E-07
          6.00E+03 9.29E-07   8.00E+03 9.65E-07
          1.00E+04 9.94E-07
endmd
fc74     neutron flux before CPC1 and behind CPC1 in beam and next to beam
f74:p    702
T74     -1 1 71e5
         1e7  1.58e7  2.51e7  3.98e7  6.31e7
         1e8  1.58e8  2.51e8  3.98e8  6.31e8
         1e9  1.58e9  2.51e9  3.98e9  6.31e9
         1e10 1.58e10 2.51e10 3.98e10 6.31e10
         1e11 1.58e11 2.51e11 3.98e11 6.31e11
         1e12 1.58e12 2.51e12 3.98e12 6.31e12
         1e13 1.58e13 2.51e13 3.98e13 6.31e13
         1e14 1.58e14 2.51e14 3.98e14 6.31e14
         1e15 1.58e15 2.51e15 3.98e15 6.31e15
         1e16 1.58e16 2.51e16 3.98e16 6.31e16
         1e17 1.58e17 2.51e17 3.98e17 6.31e17
         1e18
fq74 e f
