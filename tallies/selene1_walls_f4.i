c f4 tally for all selene guide 1 side walls for WWG runs
fc904     neutron flux on Selene 1 walls (not properly normalized)
f904:n   (108011 108013 108004 108401) 108011 108013 (108004 108401)
e904     1e-3 1.58e-3 2.51e-3 3.98e-3 6.31e-3 
         1e-2 1.58e-2 2.51e-2 3.98e-2 6.31e-2 
         1e-1 1.58e-1 2.51e-1 3.98e-1 6.31e-1 
         1e-0   1.58    2.51    3.98    6.31 
         1e+1 1.58e+1 2.51e+1 3.98e+1 6.31e+1 
         1e+2 1.58e+2 2.51e+2 3.98e+2 6.31e+2
         1e+3 1.58e+3 2.51e+3
fq904 e f
