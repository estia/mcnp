c f4 tally spheres at BBG top+bottom
fc14     neutron flux at BBG
f14:n    100701
e14      1e-10 1.58e-10 2.51e-10 3.98e-10 6.31e-10
         1e-9 1.58e-9 2.51e-9 3.98e-9 6.31e-9 
         1e-8 1.58e-8 2.51e-8 3.98e-8 6.31e-8 
         1e-7 1.58e-7 2.51e-7 3.98e-7 6.31e-7 
         1e-6 1.58e-6 2.51e-6 3.98e-6 6.31e-6 
         1e-5 1.58e-5 2.51e-5 3.98e-5 6.31e-5 
         1e-4 1.58e-4 2.51e-4 3.98e-4 6.31e-4 
         1e-3 1.58e-3 2.51e-3 3.98e-3 6.31e-3 
         1e-2 1.58e-2 2.51e-2 3.98e-2 6.31e-2 
         1e-1 1.58e-1 2.51e-1 3.98e-1 6.31e-1 
         1e-0   1.58    2.51    3.98    6.31 
         1e+1 1.58e+1 2.51e+1 3.98e+1 6.31e+1 
         1e+2 1.58e+2 2.51e+2 3.98e+2 6.31e+2
         1e+3 1.58e+3 2.51e+3
fq14 e f
c  DXTRAN spheres to improve statistics in tallies
c dxt:n   331.2 -448.0  7.0 9.5 12.0 $ around both tally spheres
c        331.2 -448.0 13.3 3.2 6.2 $ around upper tally sphere
c        331.1 -447.9 0.7  3.2 6.2 $ around lower tally sphere
c dxc:n   0.1 276r 0 0 0.1 578r
