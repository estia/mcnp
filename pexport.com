label 0 0 &
shade  4=LightYellow 9=LightGoldenrod 300=LightGoldenrod &
shade  5=green 10=DarkSeaGreen 48=DarkSlateGray &
shade 49=SlateGray 16=DarkOliveGreen 200=DarkSlateGray &
shade 71=RosyBrown 3=red 54=RosyBrown 121=brown &
shade 14=orange &
shade 13=purple 120=DarkOliveGreen &
shade 18=magenta &
shade 12=cyan &
shade 90=DarkSlateGray &
shade 2=PaleGreen 19=DarkOliveGreen 17=DarkOliveGreen &
shade 101=blue 115=blue 400=DodgerBlue &
shade 650=LightCyan 815=SlateGray 826=DarkSlateGray &
shade 870=LightYellow 1360=green 1361=green &
shade 1400=DarkOrchid 2629=VioletRosyBrown 2634=RosyBrown &
shade 2636=RosyBrown 2642=RosyBrown 2644=RosyBrown 2645=RosyBrown 2650=RosyBrown &
shade 2900=orange 6101=RosyBrown 6102=RosyBrown 6104=RosyBrown 6105=RosyBrown &
shade 6108=RosyBrown 6110=magenta 6113=magenta 6114=magenta 6115=magenta 6116=magenta &
shade 6117=magenta 6119=magenta 6120=coral 6121=coral 6122=coral 6123=coral &
shade 6125=coral 6129=coral 6132=purple 6133=purple 6136=purple 6138=purple &
shade 6140=purple 6141=purple 7410=DarkOliveGreen &
shade 31=pink 32=LightPink  &
shade 601=SteelBlue 602=green 603=green 604=red 605=orange 606=purple &
shade 608=turquoise  &
origin 275 -8.3 12.5 basis 1 0 0  0 1 0 extent 300 &
file all term 0 scales 1
origin 300 -8.3 10 basis 1 0 0  0 0 1 extent 300
origin 375 -5.5 10 basis 1 0 0  0 0 1 extent 200
origin 375 -5.5 10 basis 1 0 0  0 0 1 extent 20
origin 575 -8.3 10 basis 1 0 0  0 0 1 extent 50
origin 775 -8.3 10 basis 1 0 0  0 0 1 extent 200
origin 900 -10.2 2 basis 1 0 0  0 1 0 extent 400
origin 1100 -2.2 0.5 basis 1 0 0  0 1 0 extent 100
origin 1100 -2.2 0.5 basis 1 0.022 0  0 0 1 extent 100
origin 1230 -0.3 0  basis 1 0.022 0  0 0 1 extent 100
origin 1720  2.2 0 basis 1 0 0  0 1 0 extent 500
origin 1720 17.7 -6 basis 1 0 0  0 0 1 extent 400
origin 1716  -2 -6 basis 0 1 0  0 0 1 extent 100
origin 2149.5 1.1658 -12.6 basis 1 0 0  0 1 0 extent 50
origin 2149.5 1.1658 -12.6 basis 0 1 0  0 0 1 extent 50
origin 2149.5 1.1658 -12.6 basis 0 1 0  0 0 1 extent 250
origin 2149.5 1.1658 -12.6 basis 1 0 0  0 0 1 extent 50
origin 2316  -2 -14 basis 1 0 0  0 0 1 extent 250
origin 2260  -2 -14 basis 1 0 0  0 1 0 extent 100
origin 2260  -2 -14 basis 1 0 0  0 1 0 extent 120 10
origin 2316  -2 -14 basis 0 1 0  0 0 1 extent 100
origin 2916  -2 -22 basis 0 1 0  0 0 1 extent 100
origin 2135 0 -16 basis 1 0 -0.013  0 1 0 extent 2400
color off &
viewport square &
origin 3050   0 -170 basis 1 0 0  0 1 0 extent 1600
origin 3050   0  -10 basis 1 0 0  0 1 0 extent 1600
origin 3050   0  -30 basis 1 0 0  0 1 0 extent 1600
origin 3050   0  160 basis 1 0 0  0 1 0 extent 1600
origin 3050   0  240 basis 1 0 0  0 1 0 extent 1600
origin 1500   0    0 basis 0 1 0  0 0 1 extent 600
origin 1700   0    0 basis 0 1 0  0 0 1 extent 600
origin 2000   0    0 basis 0 1 0  0 0 1 extent 600
origin 2180   0    0 basis 0 1 0  0 0 1 extent 600
origin 2300   0    0 basis 0 1 0  0 0 1 extent 600
origin 2900   0    0 basis 0 1 0  0 0 1 extent 600
origin 3500   0    0 basis 0 1 0  0 0 1 extent 800
origin 3050   0    0 basis 1 0 0  0 0 1 extent 1600
origin 3050 300    0 basis 1 0 0  0 0 1 extent 1600
origin 3050 330    0 basis 1 0 0  0 0 1 extent 1600
end
